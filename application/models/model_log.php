<?php
class model_log extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function getData()
	{
		return $this->db->query("SELECT * FROM ms_log");
	}

	public function addData($log_type, $log_description)
	{
		$date = date("Y-m-d H:i:s");
		
		$data = array(
			"log_id" => "",
			"log_type" => $log_type,
			"log_description" => $log_description,
			"admin_name" => $this->session->userdata("admin_name"),
			"ip_address" => $this->input->ip_address(),
			"created_date" => $date,
		);
		
		$this->db->insert("ms_log", $data);
	}

	public function deleteData($id)
	{
		$this->db->where("log_id", $id);
		$this->db->delete("ms_log");
	}
}
?>