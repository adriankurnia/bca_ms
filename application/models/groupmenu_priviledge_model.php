<?php
	class Groupmenu_priviledge_model extends CI_Model
	{
		public function groupmenu_priv_list()
		{
			return $this->db->query("SELECT a.id, a.menu_priv_id, c.menu_name, a.group_priv_id, b.group_name, a.create, a.read, a.update, a.delete, c.created_date, c.created_by, c.edited_date, c.edited_by FROM bca_group_menu_priviledge a, bca_group_priviledge b, bca_menu_priviledge c WHERE a.menu_priv_id = c.id AND a.group_priv_id = b.id ORDER BY c.order ASC");
		}

		public function groupmenu_priv_data($id)
		{
			return $this->db->query("SELECT a.id, a.menu_priv_id, c.menu_name, a.group_priv_id, b.group_name, a.create, a.read, a.update, a.delete, c.created_date, c.created_by, c.edited_date, c.edited_by FROM bca_group_menu_priviledge a, bca_group_priviledge b, bca_menu_priviledge c WHERE a.group_priv_id = ".$id." AND a.menu_priv_id = c.id AND a.group_priv_id = b.id ORDER BY c.order ASC");
		}

		public function data_exist($menu_id, $group_id)
		{
			return $this->db->query("SELECT COUNT(*) AS total FROM bca_group_menu_priviledge WHERE menu_priv_id = ".$menu_id." AND group_priv_id = ".$group_id);
		}

		public function insertData($menu_priv_id, $group_priv_id, $crud_field)
		{
			$fulldate = date('Y-m-d H:i:s');
			$data = array(
				'menu_priv_id'	=> $menu_priv_id,
				'group_priv_id' => $group_priv_id,
				$crud_field     => 1,
				'created_date'  => $fulldate,
				'created_by'    => $this->session->userdata('username')
			);
			$this->db->insert('bca_group_menu_priviledge', $data);
			return $this->db->insert_id();
		}

		// public function updateData($menu_priv_id, $group_priv_id, $crud_field)
		// {
		// 	$fulldate = date('Y-m-d H:i:s');

		// 	$data = array(
		// 		'menu_priv_id'	=> $menu_priv_id,
		// 		'group_priv_id' => $group_priv_id,
		// 		$crud_field     => 1,
		// 		'edited_date'   => $fulldate,
		// 		'edited_by'     => $this->session->userdata('username')
		// 	);

		// 	$this->db->where('menu_priv_id', $menu_priv_id);
		// 	$this->db->where('group_priv_id', $group_priv_id);
		// 	$this->db->update('bca_group_menu_priviledge', $data);
		// }

		public function updateData($menu_priv_id, $crud_field)
		{
			$fulldate = date('Y-m-d H:i:s');
			$data = array(
				$crud_field     => 1,
				'edited_date'   => $fulldate,
				'edited_by'     => $this->session->userdata('username')
			);

			$this->db->where('id', $menu_priv_id);
			$this->db->update('bca_group_menu_priviledge', $data);
		}

		public function empty_priviledge($id)
		{
			$data = array(
						'create' => 0,
						'read'   => 0,
						'update' => 0,
						'delete' => 0
					);				
			$this->db->where('group_priv_id', $id);
			$this->db->update('bca_group_menu_priviledge', $data);
		}

		public function deleteData($id)
		{
			$this->db->where('id', $id);
			$this->db->delete('bca_menu_priviledge');
		}

		public function updateValue($id, $field, $new_val)
		{
			$fulldate = date('Y-m-d H:i:s');
			$data = array(
						$field        => $new_val,
						'edited_date' => $fulldate,
						'edited_by'   => $this->session->userdata('username')
					);				
			$this->db->where('id', $id);
			$this->db->update('bca_group_menu_priviledge', $data);
		}

		public function same_group_name($name)
		{
			return $this->db->query("SELECT COUNT(*) AS row FROM bca_group_priviledge WHERE group_name = '".$name."'");
		}

		public function check_trx($id)
        {
            return $this->db->query("SELECT COUNT(*) AS total FROM bca_admin_user a, bca_cms_log b WHERE a.username = b.activity_by AND group_priv_id = ".$id);
        }
	}
?>