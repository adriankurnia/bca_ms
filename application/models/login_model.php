<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function post_login($username, $password)
	{
		$password = md5(md5($password));
		$query = $this->db->where('username', $username)->where('userpass', $password)->get('bca_admin_user');
		$validate = $query->num_rows();
		
		if($validate > 0)
			return $query->row();
		else 
			return 0;
	}

	public function group_priviledge($group_id, $menu_id)
	{
		return $this->db->query("SELECT a.menu_priv_id, b.menu_name, b.controller_name, a.`create`, a.`read`, a.`update`, a.`delete` FROM bca_group_menu_priviledge a, bca_menu_priviledge b WHERE group_priv_id = ".$group_id." AND menu_priv_id = ".$menu_id." AND a.menu_priv_id = b.id");
	}
}