<?php
	class Stall_device_model extends CI_Model
	{
		public function stall_device_list()
		{
			return $this->db->query("SELECT a.device_name, a.s_id, b.l_id, a.loc_id, b.l_room, b.l_floor, a.mac_addr, a.created_date, a.created_by, a.edited_date, a.edited_by FROM bca_stall_device a, bca_canteen_location b WHERE a.loc_id = b.l_id ORDER BY a.device_name ASC");
		}

		public function stall_device_data($id)
		{
			return $this->db->query("SELECT a.device_name, a.s_id, a.loc_id, b.l_id, b.l_room, b.l_floor, a.mac_addr, a.created_date, a.created_by, a.edited_date, a.edited_by FROM bca_stall_device a, bca_canteen_location b WHERE a.loc_id = b.l_id AND a.s_id = ".$id);
		}

		public function insertData($device_name, $loc_id, $mac_addr)
		{
			$fulldate = date('Y-m-d H:i:s');
			$data = array(
				'device_name'  => $device_name,
				'loc_id'	   => $loc_id,
				'mac_addr'     => $mac_addr,
				'created_date' => $fulldate,
				'created_by'   => $this->session->userdata('username')
			);
			$this->db->insert('bca_stall_device', $data);
		}

		public function updateData($id, $device_name, $loc_id, $mac_addr)
		{
			$fulldate = date('Y-m-d H:i:s');

			$data = array(
				'device_name' => $device_name,
				'loc_id'	  => $loc_id,
				'mac_addr'    => $mac_addr,
				'edited_date' => $fulldate,
				'edited_by'   => $this->session->userdata('username')
			);

			$this->db->where('s_id', $id);
			$this->db->update('bca_stall_device', $data);
		}

		public function deleteData($id)
		{
			$this->db->where('s_id', $id);
			$this->db->delete('bca_stall_device');
		}

		public function check_trx($id)
        {
            return $this->db->query("SELECT COUNT(*) AS total FROM bca_stall_device a, bca_food_serving b WHERE a.s_id = b.s_id AND a.s_id = ".$id);
        }
	}
?>