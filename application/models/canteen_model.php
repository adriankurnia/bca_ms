<?php
    class Canteen_model extends CI_Model
    {
        public function canteen_items_list()
        {
            // return $this->db->query("SELECT a.c_id, b.l_id, a.c_name, b.l_room, c.f_type, b.l_floor, a.c_calory, a.c_price, a.buffet, a.active FROM bca_canteen a, bca_canteen_location b, bca_food_type c WHERE a.deleted = 0 AND a.loc_id = b.l_id AND a.c_type = c.f_id");
            return $this->db->query("SELECT a.c_id, a.c_name, c.f_type, a.c_calory, a.c_fat, a.c_protein, a.c_price, a.buffet, a.active, a.image FROM bca_canteen a, bca_food_type c WHERE a.deleted = 0 AND a.c_type = c.f_id");
        }

        public function canteen_item_data($id)
        {
            // return $this->db->query("SELECT a.c_id, a.loc_id, b.l_id, a.c_name, a.c_type, b.l_room, c.f_type, b.l_floor, a.c_calory, a.c_price, a.buffet, a.active FROM bca_canteen a, bca_canteen_location b, bca_food_type c WHERE a.deleted = 0 AND a.c_id = ".$id." AND a.loc_id = b.l_id AND a.c_type = c.f_id");
            return $this->db->query("SELECT a.c_id, a.loc_id, a.c_name, a.c_type, c.f_type, a.c_calory, a.c_fat, a.c_protein, a.c_price, a.buffet, a.active, a.image FROM bca_canteen a, bca_food_type c WHERE a.deleted = 0 AND a.c_id = ".$id." AND a.c_type = c.f_id");
        }

        public function canteen_item_name($id)
        {
            return $this->db->query("SELECT c_id, c_name FROM bca_canteen WHERE c_id = ".$id);
        }

        public function food_location()
        {
            return $this->db->query("SELECT * FROM bca_canteen_location WHERE deleted = 0");
        }

        public function food_type()
        {
        	return $this->db->query("SELECT * FROM bca_food_type");
        }

        public function food_type_data($id)
        {
            return $this->db->query("SELECT * FROM bca_food_type WHERE f_id = ".$id);
        }

        public function insert_canteen_item($name, $calory, $type, $place, $price, $img, $fat, $protein)
        {
            $data = array('c_name' => ucwords($name),
                        'c_type'   => $type,
                        'c_calory' => $calory,
                        'c_fat'    => $fat,
                        'c_protein'=> $protein,
                        // 'loc_id'   => $location,
                        'c_price'  => $price,
                        'buffet'   => $place,
                        'image'    => $img
                        );
            $this->db->insert('bca_canteen', $data);
            return $this->db->insert_id();

            // $id = $this->db->insert_id();
            // $data_stock = array('s_id'        => $id,
            //                 's_stock'         => $stock,
            //                 's_serving_date'  => $serving_date,
            //                 's_creation_user' => $this->session->userdata('username')
            //                 );
            // $this->db->insert('bca_canteen_stock', $data_stock);
        }

        public function update_canteen_item($id, $name, $calory, $type, $place, $price, $fat, $protein, $img = null)
        {
            if($img == null)
            {
                $data = array('c_name' => ucwords($name),
                            'c_type'   => $type,
                            'c_calory' => $calory,
                            'c_fat'    => $fat,
                            'c_protein'=> $protein,
                            // 'loc_id'   => $location,
                            'c_price'  => $price,
                            'buffet'   => $place,
                            );
            }
            else
            {
                $data = array('c_name' => $name,
                            'c_type'   => $type,
                            'c_calory' => $calory,
                            'c_fat'    => $fat,
                            'c_protein'=> $protein,
                            // 'loc_id'   => $location,
                            'c_price'  => $price,
                            'buffet'   => $place,
                            'active'   => $active,
                            'image'    => $img
                            );
            }           
            $this->db->where('c_id', $id);
            $this->db->update('bca_canteen', $data);

            // $data_stock = array('s_stock'     => $stock,
            //                 's_serving_date'  => $serving_date,
            //                 's_modified_date' => $fulldate,
            //                 's_modified_user' => $this->session->userdata('username')
            //                 );              
            // $this->db->where('s_id', $id);
            // $this->db->update('bca_canteen_stock', $data_stock);
        }

        public function delete_canteen_item($id)
        {
            // $this->db->where('c_id', $id);
            // $this->db->delete('bca_canteen');

            // $this->db->where('s_id', $id);
            // $this->db->delete('bca_canteen_stock');

            $data = array('deleted' => 1);              
            $this->db->where('c_id', $id);
            $this->db->update('bca_canteen', $data);
        }

        public function check_trx($id)
        {
            return $this->db->query("SELECT COUNT(*) AS total FROM bca_food_serving WHERE c_id = ".$id);
        }
    }
 ?>