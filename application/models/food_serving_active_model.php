<?php
	class Food_serving_active_model extends CI_Model
	{
		public function active_list()
		{
			return $this->db->query("SELECT * FROM bca_food_serving_active");
		}

		public function active_data($id)
		{
			return $this->db->query("SELECT * FROM bca_food_serving_active WHERE id = ".$id);
		}

		public function data_exist($start, $end)
		{
			return $this->db->query("SELECT * FROM bca_food_serving_holiday WHERE ('".$start."' BETWEEN `start` AND `end`) OR ('".$end."' BETWEEN `start` AND `end`)");
		}

		public function insertData($start, $end)
		{
			$fulldate = date('Y-m-d H:i:s');
			$data = array(
				'start_active' => $start,
				'end_active'   => $end,
				'created_date' => $fulldate,
				'created_by'   => $this->session->userdata('username')
			);
			$this->db->insert('bca_food_serving_active', $data);
			return $this->db->insert_id();
		}

		public function updateData($id, $start, $end)
		{
			$fulldate = date('Y-m-d H:i:s');

			$data = array(
				'start_active' => $start,
				'end_active'   => $end,
				'edited_date'  => $fulldate,
				'edited_by'    => $this->session->userdata('username')
			);

			$this->db->where('id', $id);
			$this->db->update('bca_food_serving_active', $data);
		}

		public function deleteData($id)
		{
			$this->db->where('id', $id);
			$this->db->delete('bca_food_serving_active');
		}
	}
?>