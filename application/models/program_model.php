<?php
	class Program_model extends CI_Model
	{
		public function program_list()
		{
			return $this->db->query("SELECT * FROM bca_program WHERE type = 'MAN'");
		}

		public function program_data($id)
		{
			return $this->db->query("SELECT * FROM bca_program WHERE p_id = ".$id);
		}

		public function program_data_text($program_name)
		{
			return $this->db->query("SELECT * FROM bca_program WHERE program_name = ".$program_name);
		}

		public function insertData($program_name, $subprogram_name, $training_name, $program_begin, $program_end)
		{
			$fulldate = date('Y-m-d H:i:s');
			$data = array(
				'program_name'    => $program_name,
				'subprogram_name' => $subprogram_name,
				'training_name'   => $training_name,
				'program_begin'   => $program_begin,
				'program_end'     => $program_end,
				'created_date'    => $fulldate,
				'created_by'      => $this->session->userdata('username'),
				'type'            => 'MAN' 
			);
			$this->db->insert('bca_program', $data);
			return $this->db->insert_id();
		}

		public function updateData($id, $program_name, $subprogram_name, $training_name, $program_begin, $program_end)
		{
			$fulldate = date('Y-m-d H:i:s');

			$data = array(
				'program_name'    => $program_name,
				'subprogram_name' => $subprogram_name,
				'training_name'   => $training_name,
				'program_begin'   => $program_begin,
				'program_end'     => $program_end,
				'edited_date'     => $fulldate,
				'edited_by'       => $this->session->userdata('username')
			);

			$this->db->where('p_id', $id);
			$this->db->update('bca_program', $data);
		}

		public function deleteData($id)
		{
			$this->db->where('p_id', $id);
			$this->db->delete('bca_program');
		}
	}
?>