<?php
    class Temp_user_model extends CI_Model
    {
        public function temp_user_list()
        {
            return $this->db->query("SELECT *, b.program_name FROM bca_employee a JOIN bca_program b WHERE a.`e_program` = b.p_id AND a.deleted = 0 AND a.e_category = 'EXT'");
        }

    	public function temp_user_data($nip)
    	{
            return $this->db->query("SELECT * FROM bca_employee WHERE e_nip = '".$nip."'");
    	}

        public function data_exist($field, $data, $type, $nip="")
        {
            if($type == "add")
                return $this->db->query("SELECT * FROM bca_employee WHERE ".$field." = '".$data."' ")->num_rows();
            else if($type == "edit")
                return $this->db->query("SELECT * FROM bca_employee WHERE ".$field." = '".$data."' AND e_nip != '".$nip."'")->num_rows();
        }

        public function temp_user_deleted()
        {
            return $this->db->query("SELECT *, b.program_name FROM bca_employee a JOIN bca_program b WHERE a.`e_program` = b.p_id AND a.deleted = 1 AND a.e_category = 'EXT'");
        }

        public function insert_temp_user($nip, $name, $rfid, $expired, $program)
        {
            $fulldate = date('Y-m-d H:i:s');
            $data = array('e_nip'      => $nip,
                        'e_name'       => $name,
                        'e_rfid'       => $rfid,
                        'card_expired' => $expired,
                        'e_program'    => $program,
                        'e_category'   => 'EXT',
                        'created_date' => $fulldate,
                        'created_by'   => $this->session->userdata('username')
                        );
            $this->db->insert('bca_employee', $data);
        }

        public function update_temp_user($nip, $name, $rfid, $expired, $program)
        {
            $fulldate = date('Y-m-d H:i:s', time());
            $data = array('e_name'    => $name,
                        'e_rfid'      => $rfid,
                        'card_expired'=> $expired,
                        'e_program'   => $program,
                        'e_category'  => 'EXT',
                        'edited_date' => $fulldate,
                        'edited_by'   => $this->session->userdata('username')
                        );              
                $this->db->where('e_nip', $nip);
                $this->db->update('bca_employee', $data);
        }

        public function delete_temp_user($nip)
        {
            // $this->db->where('t_nip', $nip);
            // $this->db->delete('bca_temp_user');

            $data = array('deleted' => 1);              
            $this->db->where('e_nip', $nip);
            $this->db->update('bca_employee', $data);
        }

        public function update_reappear_temp_user($nip)
        {
            // $this->db->where('t_nip', $nip);
            // $this->db->delete('bca_temp_user');

            $data = array('deleted' => 0);              
            $this->db->where('e_nip', $nip);
            $this->db->update('bca_employee', $data);
        }

        public function check_trx($nip, $rfid)
        {
            return $this->db->query("SELECT COUNT(*) AS total FROM bca_canteen_trx WHERE e_id = '".$nip."' AND e_rfid = '".$rfid."'");
        }
    }
 ?>