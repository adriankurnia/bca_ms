<?php
	class Menu_priviledge_model extends CI_Model
	{
		public function menu_priv_list()
		{
			return $this->db->query("SELECT * FROM bca_menu_priviledge ORDER BY `order` ASC");
		}

		public function menu_priv_data($id)
		{
			return $this->db->query("SELECT * FROM bca_menu_priviledge WHERE id = ".$id." ORDER BY `order` ASC");
		}

		public function insertData($menu_name, $controller_name)
		{
			$fulldate = date('Y-m-d H:i:s');
			$data = array(
				'menu_name'	      => $menu_name,
				'controller_name' => $controller_name,
				'created_date'    => $fulldate,
				'created_by'      => $this->session->userdata('username')
			);
			$this->db->insert('bca_menu_priviledge', $data);
			return $this->db->insert_id();
		}

		public function updateData($id, $menu_name, $controller_name)
		{
			$fulldate = date('Y-m-d H:i:s');

			$data = array(
				'menu_name'       => $menu_name,
				'controller_name' => $controller_name,
				'edited_date'     => $fulldate,
				'edited_by'       => $this->session->userdata('username')
			);

			$this->db->where('id', $id);
			$this->db->update('bca_menu_priviledge', $data);
		}

		public function deleteData($id)
		{
			$this->db->where('id', $id);
			$this->db->delete('bca_menu_priviledge');
		}
	}
?>