<?php
	class Dashboard_model extends CI_Model
	{
        public function getTotalMakan(){
            $dateNow = date('Y-m-d');
            $this->db->select('*');
            // $this->db->from('bca_canteen_trx');
            $this->db->from('bca_canteen_trx_daily');
            $where="trx_time >= '".$dateNow." 00:00:00' AND trx_time <= '".$dateNow." 23:59:59'";
            $this->db->where($where);
            $this->db->where('trx_status',1);
            $this->db->group_by('e_id');
            $data = $this->db->get()->result();

            if($data){
                return count($data);
            }

        }
        public function getTotalTrx($status){
            $dateNow = date('Y-m-d');
            $this->db->select('*');
            // $this->db->from('bca_canteen_trx');
            $this->db->from('bca_canteen_trx_daily');
            $where="trx_time >= '".$dateNow." 00:00:00' AND trx_time <= '".$dateNow." 23:59:59'";
            $this->db->where($where);
            $this->db->where('trx_status',$status);
            $data = $this->db->get()->result();

            if($data){
                return count($data);
            }
        }
        function getMakananLarisHarga($date){
            $where ='';

            $dateNow = date('Y-m-d', strtotime($date));
            $where.=" AND a.trx_time >= '".$dateNow." 00:00:00' AND a.trx_time <= '".$dateNow." 23:59:59'";

            $today = date('Y-m-d', time());
            if($today == $dateNow) $table = "bca_canteen_trx_daily";
            else $table = "bca_canteen_trx";

            $query = 'select sum(b.c_price) as price from '.$table.' a
            LEFT JOIN bca_canteen b on b.c_id = a.trx_item_id
            where a.trx_status = 1' .$where. ' ';

            $data =   $this->db->query($query)->row();
            if($data){
                return $data->price;
            }else{
                return '0';
            }
        }


        function getMakananLaris(){
            $where ='';


            $dateNow = date('Y-m-d');
            $where.=" AND a.trx_time >= '".$dateNow." 00:00:00' AND a.trx_time <= '".$dateNow." 23:59:59'";

             $query = 'select count(a.e_id) as total,b.c_name
            from bca_canteen_trx_daily a
            LEFT JOIN bca_canteen b on b.c_id = a.trx_item_id
            where  a.trx_status = 1' .$where. ' group by b.c_id order by total desc limit 5';

            return $this->db->query($query)->result();
        }
        public function getLantai(){
            $this->db->select('*');
            $this->db->from('bca_canteen_location');
            $this->db->where('deleted', 0);
            $data = $this->db->get()->result();
            return $data;
        }
         public function getFoodType(){
             $this->db->select('*');
            $this->db->from('bca_food_type');
            $data = $this->db->get()->result();
            return $data;
        }
		public function getStockByLantai(){
            $dataArray = array();
                $getDevice = $this->general_model->select_data('bca_stall_device','loc_id','asc');
                //stall id
                if($getDevice)foreach($getDevice as $list2){
                    $stall_id = $list2->s_id;
                    $getBcaFoodServing =$this->getBcaFoodServing($stall_id);
                        if($getBcaFoodServing)foreach($getBcaFoodServing as $list3){
                            ${"totalMakanan_".$stall_id.'_'.make_alias($list3->makanan_name)}= 0;
                        }
                }




                $getDevice = $this->general_model->select_data('bca_stall_device','loc_id','asc');
                //stall id
                if($getDevice)foreach($getDevice as $list2){
                    $stall_id = $list2->s_id;
                    $getBcaFoodServing =$this->getBcaFoodServing($stall_id);
                        if($getBcaFoodServing)foreach($getBcaFoodServing as $list3){
                             ${"totalMakanan_".$stall_id.'_'.make_alias($list3->makanan_name)}+= $list3->starting_stock;
                        }
                         if($getBcaFoodServing)foreach($getBcaFoodServing as $list3){
                             $dataArray['parameter'][]="totalMakanan_".$stall_id.'_'.make_alias($list3->makanan_name);
                             $dataArray["totalMakanan_".$stall_id.'_'.make_alias($list3->makanan_name)]=${"totalMakanan_".$stall_id.'_'.make_alias($list3->makanan_name)};
                         }
                }
          //  pre($dataArray);
            return $dataArray;

        }

        public function getBcaFoodServing($stall_id = 0 ){
            $dateNow = date('Y-m-d');
            $this->db->select('bca_daily_stock.stock as starting_stock,bca_canteen.c_type,bca_canteen.c_name as makanan_name');
            $this->db->from('bca_daily_stock');
            $this->db->join('bca_canteen','bca_daily_stock.c_id = bca_canteen.c_id ');
            $this->db->where('bca_daily_stock.trx_date',$dateNow);
            if($stall_id > 0){
                $this->db->where('bca_daily_stock.s_id',$stall_id);
            }
            $data = $this->db->get()->result();
           // pre($data);
            return $data;

        }
	}
?>
