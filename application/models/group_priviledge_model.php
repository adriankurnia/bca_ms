<?php
	class Group_priviledge_model extends CI_Model
	{
		public function group_priv_list()
		{
			return $this->db->query("SELECT * FROM bca_group_priviledge");
		}

		public function group_priv_data($id)
		{
			return $this->db->query("SELECT * FROM bca_group_priviledge WHERE id = ".$id);
		}

		public function insertData($group_name)
		{
			$fulldate = date('Y-m-d H:i:s');
			$data = array(
				'group_name'	=> $group_name,
				'created_date'  => $fulldate,
				'created_by'	=> $this->session->userdata('username')
			);
			$this->db->insert('bca_group_priviledge', $data);
			return $this->db->insert_id();
		}

		public function updateData($id, $group_name)
		{
			$fulldate = date('Y-m-d H:i:s');
			$data = array(
				'group_name'  => $group_name,
				'edited_date' => $fulldate,
				'edited_by'   => $this->session->userdata('username')
			);

			$this->db->where('id', $id);
			$this->db->update('bca_group_priviledge', $data);
		}

		public function deleteData($id)
		{
			$this->db->where('id', $id);
			$this->db->delete('bca_group_priviledge');
		}
	}
?>