<?php
class model_admin_module extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function getData()
	{
		return $this->db->query("SELECT * FROM ms_admin_module");
	}

	public function getDataById($id)
	{
		return $this->db->query("SELECT * FROM ms_admin_module WHERE admin_module_id = {$id}");
	}

	public function getDataByControllerName($id)
	{
		return $this->db->query("SELECT * FROM ms_admin_module WHERE admin_module_controller_name = '{$id}'");
	}

	public function addData($post)
	{
		$date = date('Y-m-d H:i:s');
		
		$data = array(
			'admin_module_id' => '',
			'admin_module_name' => $post['admin_module_name'],
			'admin_module_controller_name' => $post['admin_module_controller_name'],
			'created_date' => $date,
			'edited_date' => $date,
			'created_by' => $this->session->userdata('admin_id'),
			'edited_by' => $this->session->userdata('admin_id')
		);
		
		$this->db->insert('ms_admin_module', $data);
	}

	public function updateData($post)
	{
		$date = date('Y-m-d H:i:s');

		$data = array(
			'admin_module_name' => $post['admin_module_name'],
			'admin_module_controller_name' => $post['admin_module_controller_name'],
			'created_date' => $date,
			'edited_date' => $date,
			'created_by' => $this->session->userdata('admin_id'),
			'edited_by' => $this->session->userdata('admin_id')
		);

		$this->db->where('admin_module_id', $post['admin_module_id']);
		$this->db->update('ms_admin_module', $data);
	}

	public function deleteData($id)
	{
		$this->db->where('admin_module_id', $id);
		$this->db->delete('ms_admin_module');
	}
}
?>