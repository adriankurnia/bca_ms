<?php
    class User_list_model extends CI_Model
    {
        public function user_list()
        {
            return $this->db->query("SELECT * FROM bca_admin_user");
        }

    	public function user_data($username)
    	{
    		return $this->db->query("SELECT * FROM bca_admin_user WHERE username = '".$username."' ");
    	}

        public function group_priviledge()
        {
            return $this->db->query("SELECT * FROM bca_group_priviledge");
        }

        public function data_exist($username, $oldUsername = null)
        {
            if($oldUsername == null)
            {
                return $this->db->query("SELECT COUNT(*) AS total FROM bca_admin_user WHERE username = '".$username."' ");
            }
            else
            {
                return $this->db->query("SELECT * FROM bca_admin_user WHERE username = '".$username."' AND username != '".$oldUsername."'");
            }
        }

        public function insert_user($username, $userpass, $group_priviledge)
        {
            $fulldate = date('Y-m-d H:i:s', time());
            $data = array('username'    => $username,
                        'userpass'      => $userpass,
                        'group_priv_id' => $group_priviledge,
                        'userstatus'    => 1,
                        'created_date'  => $fulldate,
                        'created_by'    => $this->session->userdata('username')
                        );
            $this->db->insert('bca_admin_user', $data);
        }

        public function update_user($old, $username, $group_priviledge, $userpass = null)
        {
            $fulldate = date('Y-m-d H:i:s', time());
            if($userpass != null || $userpass != "" || !empty($userpass))
            {
                $data = array('username' => $username,
                        'userpass'       => $userpass,
                        'group_priv_id'  => $group_priviledge,
                        'edited_date'    => $fulldate,
                        'edited_by'      => $this->session->userdata('username')
                        ); 
            }
            else
            {
                $data = array('username' => $username,
                        'group_priv_id'  => $group_priviledge,
                        'edited_date'    => $fulldate,
                        'edited_by'      => $this->session->userdata('username')
                        ); 
            }       
            $this->db->where('username', $old);
            $this->db->update('bca_admin_user', $data);
        }

        public function delete_user($username)
        {
            $this->db->where('username', $username);
            $this->db->delete('bca_admin_user');
        }

        public function check_trx($id)
        {
            return $this->db->query("SELECT COUNT(*) AS total FROM bca_cms_log WHERE activity_by = '".$id."'");
        }
    }
 ?>