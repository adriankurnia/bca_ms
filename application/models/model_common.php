<?php
class model_common extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function getMetaPage($page)
	{
		return $this->db->query("SELECT * FROM ms_meta_page WHERE meta_page = '{$page}'");
	}

	public function getSpecificData($select = '*', $from, $where)
	{
		return $this->db->query("SELECT {$select} FROM {$from} {$where}");
	}
	
}
?>