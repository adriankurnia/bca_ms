<?php
    class Employee_model extends CI_Model
    {
        public function employee_list()
        {
            return $this->db->query("SELECT * FROM bca_employee WHERE deleted = 0 AND e_category != 'EXT'");
        }

    	public function employee_data($id)
    	{
    		return $this->db->query("SELECT * FROM bca_employee WHERE e_nip = '".urldecode($id)."'");
    	}

        public function data_exist($field, $data, $type, $nip="")
        {
            if($type == "add")
                return $this->db->query("SELECT * FROM bca_employee WHERE ".$field." = '".$data."' ")->num_rows();
            else if($type == "edit")
                return $this->db->query("SELECT * FROM bca_employee WHERE ".$field." = '".$data."' AND e_nip != '".$nip."'")->num_rows();
        }

    	public function insert_attendance($nip)
    	{
			$date     = date('Y-m-d', time());
			$check_in = date('H:i:s', time());
			$success  = 1;

    		$data = array(	'e_nip'			=> $nip,
							'ta_date'		=> $date,
							'ta_check_in' 	=> $check_in,
							'ta_success'	=> $success
							);

			$this->db->insert('bca_training_attendance', $data);
    	}

        public function insert_employee($nip, $name, $rfid, $birthdate, $work_unit, $expired)
        {
            $data = array('e_nip'         => $nip,
                        'e_name'          => $name,
                        'e_rfid'          => $rfid,
                        'e_birthdate'    => $birthdate,
                        'e_work_unit'     => $work_unit,
                        'card_expired'    => $expired
                        // 't_creation_user' => $this->session->userdata('username')
                        );
            $this->db->insert('bca_employee', $data);
        }

        public function update_employee($nip, $name, $rfid, $work_unit, $expired)
        {
            $fulldate = date('Y-m-d H:i:s', time());
            $data = array('e_nip'       => $nip,
                        'e_name'        => $name,
                        'e_rfid'        => $rfid,
                        'e_work_unit'   => $work_unit,
                        'card_expired'    => $expired
                        // 't_update_date' => $fulldate,
                        // 't_update_user' => $this->session->userdata('username')
                        );              
                $this->db->where('e_nip', $nip);
                $this->db->update('bca_employee', $data);
        }

        public function delete_employee($nip)
        {
            // $this->db->where('t_nip', $nip);
            // $this->db->delete('bca_temp_user');

            $data = array('deleted' => 1);              
            $this->db->where('e_nip', $nip);
            $this->db->update('bca_employee', $data);
        }
    }
 ?>