<?php
    class Food_serving_model extends CI_Model
    {
        public function food_serving_list()
        {
            $mon = date("m");
            $yer = date("Y");
            return $this->db->query("SELECT a.id, a.c_id, b.c_name, a.s_id, c.device_name, a.start_date, a.end_date, a.starting_stock FROM bca_food_serving a, bca_canteen b, bca_stall_device c WHERE MONTH(a.start_date) = '".$mon."' AND YEAR(a.start_date) = '".$yer."' AND a.s_id = c.s_id AND a.c_id = b.c_id");
            
        }

        public function food_serving_data_all($id)
        {
            return $this->db->query("SELECT a.id, a.c_id, b.c_name, a.s_id, c.device_name, a.start_date, a.end_date, a.starting_stock FROM bca_food_serving a, bca_canteen b, bca_stall_device c WHERE a.s_id = c.s_id AND a.c_id = b.c_id AND a.s_id = ".$id." ORDER BY start_date ASC");
        }

        public function food_serving_data($id, $start = null, $end = null)
        {
            $mon = date("m");
            $yer = date("Y");
            if($start != null && $end != null)
            {
                return $this->db->query("SELECT a.id, a.c_id, b.c_name, a.s_id, c.device_name, a.start_date, a.end_date, a.starting_stock FROM bca_food_serving a, bca_canteen b, bca_stall_device c WHERE DATE(a.start_date) BETWEEN '".$start."' AND '".$end."' AND a.s_id = c.s_id AND a.c_id = b.c_id AND a.s_id = ".$id." ORDER BY start_date ASC");
            }
            else
            {
                return $this->db->query("SELECT a.id, a.c_id, b.c_name, a.s_id, c.device_name, a.start_date, a.end_date, a.starting_stock FROM bca_food_serving a, bca_canteen b, bca_stall_device c WHERE MONTH(a.start_date) = '".$mon."' AND YEAR(a.start_date) = '".$yer."' AND a.s_id = c.s_id AND a.c_id = b.c_id AND a.s_id = ".$id." ORDER BY start_date ASC");
            }
        }

        public function get_food_serving_date($id)
        {
            return $this->db->query("SELECT * FROM bca_food_serving WHERE id = ".$id);
        }

        public function check_exist($s_id, $c_id, $start, $end)
        {
            if($start == $end)
            {
                $where = "'".$start."' <= end_date AND '".$end."' >= start_date";
            }
            else
            {
                $where = "start_date BETWEEN '".$start."' AND '".$end."' OR end_date BETWEEN '".$start."' AND '".$end."'";
            }
            return $this->db->query("SELECT a.*, b.device_name FROM bca_food_serving a, bca_stall_device b WHERE (".$where.") AND a.s_id != ".$s_id." AND a.c_id = ".$c_id." AND a.s_id = b.s_id");
        }

        public function food_name($id = null)
        {
            if($id == null)
            {
                return $this->db->query("SELECT c_id, c_name FROM bca_canteen");
            }
            else
            {
                return $this->db->query("SELECT c_id, c_name FROM bca_canteen WHERE c_id = ".$id);
            }
        }

        public function insert_food_serving($c_id, $s_id, $stock, $start_date, $end_date)
        {
            $fulldate = date('Y-m-d H:i:s');
            $data = array('c_id'         => $c_id,
                        's_id'           => $s_id,
                        'starting_stock' => $stock,
                        'ending_stock'   => $stock,
                        'start_date'     => $start_date,
                        'end_date'       => $end_date,
                        'created_date'   => $fulldate,
                        'created_by'     => $this->session->userdata('username')
                        );
            $this->db->insert('bca_food_serving', $data);
        }

        public function update_stock_item($id, $c_id, $stock, $start_date, $end_date)
        {
            $fulldate = date('Y-m-d H:i:s', time());
            $data = array('c_id'         => $c_id,
                        'starting_stock' => $stock,
                        'ending_stock'   => $stock,
                        'start_date'     => $start_date,
                        'end_date'       => $end_date,
                        'edited_date'    => $fulldate,
                        'edited_by'      => $this->session->userdata('username')
                        );              
            $this->db->where('id', $id);
            $this->db->update('bca_food_serving', $data);

            // $data_stock = array('s_stock'     => $stock,
            //                 's_serving_date'  => $serving_date,
            //                 's_modified_date' => $fulldate,
            //                 's_modified_user' => $this->session->userdata('username')
            //                 );              
            // $this->db->where('s_id', $id);
            // $this->db->update('bca_canteen_stock', $data_stock);
        }

        public function delete_food_serving($id)
        {
            $this->db->where('id', $id);
            $this->db->delete('bca_food_serving');

            // $data = array('deleted' => 1);              
            // $this->db->where('s_id', $id);
            // $this->db->update('bca_canteen_stock', $data);
        }

        public function delete_related_data($s_id, $mon, $yer)
        {
            $this->db->query("DELETE FROM bca_food_serving WHERE MONTH(start_date) = '".$mon."' AND YEAR(start_date) = '".$yer."' AND s_id = ".$s_id);
        }

        public function checkChangesData($id, $c_id, $start_date, $end_date, $starting_stock)
        {
            return $this->db->query("SELECT * FROM bca_food_serving WHERE id = ".$id." AND start_date = '".$start_date."' AND end_date = '".$end_date."' AND c_id = ".$c_id." AND starting_stock = ".$starting_stock);
        }
    }
 ?>