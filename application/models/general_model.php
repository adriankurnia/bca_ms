<?php if(!defined("BASEPATH")) exit("Hack Attempt");
class General_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function insert_data($table, $data)
	{
		$this->db->insert($table, $data);
	}

	function insert_data_batch($table,$data){
		$this->db->insert_batch($table, $data);
	}

	function update_data($table, $data, $where)
	{
		$this->db->update($table, $data, $where);
	}

	function delete_data($table,$where)
	{
		$this->db->delete($table, $where);
	}

	function truncate_data($table)
	{
		$this->db->truncate($table);
	}

	function select_data($table,$orderby='id',$sort='desc'){
		return $this->db->order_by($orderby,$sort)->get($table)->result();
	}
	function select_data2($table,$data1,$data2,$orderby='id',$sort='desc'){
		return $this->db->order_by($orderby,$sort)->where($data1,$data2)->get($table)->result();
	}
    function select_data2_asc($table,$data1,$data2){
		return $this->db->order_by('id','asc')->where($data1,$data2)->get($table)->result();
	}
	function select_data3($table,$data1,$data2,$data3,$data4){
		return $this->db->order_by('id','desc')->where($data1,$data2)->where($data3,$data4)->get($table)->result();
	}
	function select_data_where($table,$where){
		return $this->db->order_by('id','desc')->where($where)->get($table)->result();
	}
    function select_data3_limit($table,$data1,$data2,$data3,$data4,$limit){
		return $this->db->limit($limit)->order_by('id','desc')->where($data1,$data2)->where($data3,$data4)->get($table)->result();
	}
    function select_data4($table,$data1,$data2,$limit){
		return $this->db->limit($limit)->order_by($data1,$data2)->get($table)->result();
	}
	function select_data_detail($table,$data1){
		return $this->db->order_by('id','desc')->where('id',$data1)->get($table)->row();
	}
	function select_data_detail2($table,$data1,$data2){
		return $this->db->where($data1,$data2)->get($table)->row();
	}
	function select_data_detail_with_like($table,$data1,$data2,$data3,$data4){
		 $this->db->order_by('id','desc');
		 $this->db->where($data1,$data2);
		 $no=0;
		 if($data4)foreach($data4 as $list){
		 	if($no==0){
				$this->db->like($data3,'-'.$list.'-');
			}
			$no++;
		 }
		  return $this->db->get($table)->row();
	//	  echo $this->db->last_query();
	}


	function select_data_detail3($table,$where){
		return $this->db->order_by('id','desc')->where($where)->get($table)->row();
	}
	function select_data_detail4($table,$data1,$data2,$data3,$data4,$data5,$data6){
		return $this->db->order_by('id','desc')->where($data1,$data2)->where($data3,$data4)->where($data5,$data6)->get($table)->row();
	}


    function getTrxMakanByType($rfid,$type_id){
        $this->db->select('bca_canteen_trx.trx_id');
        $this->db->from('bca_canteen_trx');
        $this->db->join('bca_canteen', 'bca_canteen.c_id = bca_canteen_trx.trx_item_id','LEFT');
        $this->db->where('bca_canteen_trx.e_rfid',$rfid);
        $this->db->where('bca_canteen.c_type',$type_id);
        $this->db->where('date(bca_canteen_trx.trx_time)',date('Y-m-d'));
        $data =  $this->db->get()->row();
        //echo $this->db->last_query();
        if($data){
            return 1;
        }else{
            return 0;
        }
    }
    function getMakananByTrx($rfid){
        $this->db->select('f_id,f_type');
        $this->db->from('bca_food_type');
       $data =   $this->db->get()->result();
        $newDataz = array();
        if($data)foreach($data as $list){
            $list->status = $this->getTrxMakanByType($rfid,$list->f_id);
            $newDataz[] = $list;
        }
        return $newDataz ; //pre($newDataz);
    }

}
