<?php
class model_admin_privilege_has_admin_module extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function getData()
	{
		return $this->db->query("SELECT * FROM admin_privilege_has_admin_module");
	}

	public function getDataByAdminPrivilegeId($id)
	{
		return $this->db->query("SELECT * FROM admin_privilege_has_admin_module WHERE admin_privilege_id = {$id}");
	}

	public function getDataByAdminPrivilegeIdAndAdminModuleId($privilege_id, $module_id)
	{
		return $this->db->query("SELECT * FROM admin_privilege_has_admin_module WHERE admin_privilege_id = {$privilege_id} AND admin_module_id = {$module_id}");
	}

	public function addData($post)
	{
		$date = date('Y-m-d H:i:s');
		
		$data = array(
			'admin_privilege_has_admin_module_id' => '',
			'admin_privilege_id' => $post['admin_privilege_id'],
			'admin_module_id' => $post['admin_module_id'],
			'admin_create' => $post['admin_create'],
			'admin_read' => $post['admin_read'],
			'admin_update' => $post['admin_update'],
			'admin_delete' => $post['admin_delete'],
			'created_date' => $date,
			'edited_date' => $date,
			'created_by' => $this->session->userdata('admin_id'),
			'edited_by' => $this->session->userdata('admin_id')
		);
		
		$this->db->insert('admin_privilege_has_admin_module', $data);
	}

	public function updateData($post)
	{
		$date = date('Y-m-d H:i:s');

		$data = array(
			'admin_privilege_id' => $post['admin_privilege_id'],
			'admin_module_id' => $post['admin_module_id'],
			'admin_create' => $post['admin_create'],
			'admin_read' => $post['admin_read'],
			'admin_update' => $post['admin_update'],
			'admin_delete' => $post['admin_delete'],
			'edited_date' => $date,
			'edited_by' => $this->session->userdata('admin_id')
		);

		$this->db->where('admin_privilege_has_admin_module_id', $post['admin_privilege_has_admin_module_id']);
		$this->db->update('admin_privilege_has_admin_module', $data);
	}

	public function deleteData($id)
	{
		$this->db->where('admin_privilege_has_admin_module_id', $id);
		$this->db->delete('admin_privilege_has_admin_module');
	}
}
?>