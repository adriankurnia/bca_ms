<?php
	class Food_serving_holiday_model extends CI_Model
	{
		public function holiday_list()
		{
			return $this->db->query("SELECT * FROM bca_food_serving_holiday");
		}

		public function holiday_data($id)
		{
			return $this->db->query("SELECT * FROM bca_food_serving_holiday WHERE id = ".$id);
		}

		public function insertData($holiday_name, $start, $end, $status)
		{
			$fulldate = date('Y-m-d H:i:s');
			$data = array(
				'holiday_name' => $holiday_name,
				'start'        => $start,
				'end'          => $end,
				'created_date' => $fulldate,
				'created_by'   => $this->session->userdata('username'),
				'status'       => $status 
			);
			$this->db->insert('bca_food_serving_holiday', $data);
			return $this->db->insert_id();
		}

		public function updateData($id, $holiday_name, $start, $end, $status)
		{
			$fulldate = date('Y-m-d H:i:s');

			$data = array(
				'holiday_name' => $holiday_name,
				'start'        => $start,
				'end'          => $end,
				'edited_date'  => $fulldate,
				'edited_by'    => $this->session->userdata('username'),
				'status'       => $status
			);

			$this->db->where('id', $id);
			$this->db->update('bca_food_serving_holiday', $data);
		}

		public function deleteData($id)
		{
			$this->db->where('id', $id);
			$this->db->delete('bca_food_serving_holiday');
		}
	}
?>