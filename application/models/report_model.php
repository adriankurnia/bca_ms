<?php
    class Report_model extends CI_Model
    {
        public function main_dessert_drink($date)
        {
            return $this->db->query("SELECT f_id, COUNT(p.f_id) AS span FROM (SELECT a.f_id FROM bca_food_type a, bca_canteen_trx b, bca_canteen c WHERE DATE(b.trx_time) = '".$date."' AND b.trx_item_id = c.c_id AND a.f_id = c.c_type GROUP BY c.c_name) p GROUP BY f_id");
        }

        public function buffet_stall($date)
        {
            return $this->db->query("SELECT buffet, COUNT(buffet) AS total FROM (SELECT buffet FROM bca_canteen a, bca_canteen_trx b WHERE DATE(b.trx_time) = '".$date."' AND buffet != 0 AND a.c_id = b.trx_item_id GROUP BY trx_item_id) p GROUP BY buffet");
        }

        public function main_course($date)
        {
            return $this->db->query("SELECT DISTINCT(b.c_name) AS name, b.c_id, b.buffet FROM bca_canteen_trx a, bca_canteen b WHERE DATE(a.trx_time) = '".$date."' AND b.c_type = 1 AND a.trx_item_id = b.c_id ORDER BY buffet DESC");
        }

        public function dessertOrDrink($date, $type)
        {
            return $this->db->query("SELECT DISTINCT(b.c_name) AS name, b.c_id FROM bca_canteen_trx a, bca_canteen b WHERE DATE(a.trx_time) = '".$date."' AND b.c_type = ".$type." AND a.trx_item_id = b.c_id ORDER BY b.c_id ASC");
        }
        public function getMakananByFilter($type_id = 0,$buffet_id =  0,$date = NULL){
            $where ='';
            if($type_id > 0 ){
                $where.= " AND a.c_type =  '".$type_id."'" ;
            }
            if($buffet_id > 0 ){
                $where.= " AND a.buffet =  '".$buffet_id."'" ;
            }

            $dateNow = date('Y-m-d',strtotime($date));
            $where.="AND start_date <= '".$dateNow."' AND end_date >= '".$dateNow."'";

            $query = 'select a.c_id,a.c_name,b.starting_stock,a.c_price from bca_canteen a JOIN bca_food_serving b on b.c_id = a.c_id where  a.c_id > 0' .$where;

            return $this->db->query($query)->result();
        }
        public function getMakananByFilterStartDateEndDate($type_id = 0,$buffet_id =  0,$start_date = NULL,$end_date = NULL){
            $where ='';
            if($type_id > 0 ){
                $where.= " AND a.c_type =  '".$type_id."'" ;
            }
            if($buffet_id > 0 ){
                $where.= " AND a.buffet =  '".$buffet_id."'" ;
            }

            $start_date = date('Y-m-d',strtotime($start_date));
            $end_date = date('Y-m-d',strtotime($end_date));
            if($start_date == $end_date)
            {
                $where.="AND '".$start_date."' between start_date and end_date";
            }
            else
            {
                $where.="AND ('".$start_date."' BETWEEN start_date AND end_date OR '".$end_date."' BETWEEN start_date AND end_date OR ('".$start_date."' <= start_date AND '".$end_date."' >= end_date))";
            }
            

            $query = 'select a.c_id,a.c_name,b.starting_stock,a.c_price from bca_canteen a JOIN bca_food_serving b on b.c_id = a.c_id where  a.c_id > 0' .$where .' group by b.c_id';
            // if($buffet_id == 1 )die($query);
            return $this->db->query($query)->result();
        }
        // function getTrxByDate($date){
        //     $where ='';


        //     $dateNow = date('Y-m-d',strtotime($date));
        //     $where.=" AND a.trx_time >= '".$dateNow." 00:00:00' AND a.trx_time <= '".$dateNow." 23:59:59'";

        //     $query = 'select a.e_id,b.e_name,c.program_name,c.subprogram_name,c.training_name
        //     from bca_canteen_trx a
        //     LEFT JOIN bca_employee b on b.e_nip = a.e_id
        //     LEFT JOIN bca_program c on b.e_program = c.p_id
        //     where  a.trx_status = 1' .$where. ' GROUP BY a.e_id';

        //     return $this->db->query($query)->result();
        // }
        function getProgram($id)
        {
            return $this->db->query("SELECT program_name, subprogram_name, training_name FROM bca_program WHERE p_id = ".$id);
        }
        function getParticipant($nip, $date)
        {
            $dateNow = date('Y-m-d',strtotime($date));
            return $this->db->query("SELECT co_id FROM bca_course_participant WHERE e_nip = '".$nip."' AND '".$dateNow."' BETWEEN co_start AND co_end");
        }
        function getTrxByDate($date){
            $where ='';

            $dateNow = date('Y-m-d',strtotime($date));
            $where.=" AND a.trx_time >= '".$dateNow." 00:00:00' AND a.trx_time <= '".$dateNow." 23:59:59'";

            $query = 'select a.e_id,b.e_name,c.program_name
            from bca_canteen_trx a
            LEFT JOIN bca_employee b on b.e_nip = a.e_id
            LEFT JOIN bca_program c ON b.e_program = c.p_id 
            where  a.trx_status = 1' .$where. ' GROUP by a.e_id';
            //die($query);
            return $this->db->query($query)->result();
        }
        function getTrxByDateGroupByProgram2($date){
            $where ='';


            $dateNow = date('Y-m-d',strtotime($date));
            $where.=" AND a.trx_time >= '".$dateNow." 00:00:00' AND a.trx_time <= '".$dateNow." 23:59:59'";
            $where.=" AND e_nip like 'EX%' ";
            $query = 'select b.e_program as e_id,b.e_name,c.program_name,c.subprogram_name,c.training_name
            from bca_canteen_trx a
            LEFT JOIN bca_employee b on b.e_nip = a.e_id
            LEFT JOIN bca_program c on b.e_program = c.p_id
            where  a.trx_status = 1' .$where. ' GROUP BY b.e_program';
            // die($query);
            return $this->db->query($query)->result();
        }

        // function getTrxByDateGroupByProgram($date){
        //     $where ='';


        //     $dateNow = date('Y-m-d',strtotime($date));
        //     $where.=" AND a.trx_time >= '".$dateNow." 00:00:00' AND a.trx_time <= '".$dateNow." 23:59:59'";

        //     $query = 'select co_id
        //     from bca_canteen_trx a
        //     LEFT JOIN bca_employee b on b.e_nip = a.e_id
        //     LEFT JOIN bca_course_participant c ON b.e_nip = c.e_nip 
        //     where  a.trx_status = 1' .$where. ' AND "'.$dateNow.'" BETWEEN c.co_start AND c.co_end GROUP BY c.co_id';
            
        //     return $this->db->query($query)->result();
        // }

        function getCourseToday($co_id, $date)
        {
            $dateNow = date('Y-m-d',strtotime($date));
            return $this->db->query("SELECT * FROM bca_course_participant WHERE co_id = '".$co_id."' AND '".$dateNow."' BETWEEN co_start AND co_end");
        }

        function getTrxByDateGroupByProgram($date){
            $where ='';

            $dataSemua = array();
            $dateNow = date('Y-m-d',strtotime($date));
            $where.=" AND a.trx_time >= '".$dateNow." 00:00:00' AND a.trx_time <= '".$dateNow." 23:59:59'";

            $query = 'select c.id as c_id, co_id
            from bca_canteen_trx a
            LEFT JOIN bca_employee b on b.e_nip = a.e_id
            LEFT JOIN bca_course_participant c ON b.e_nip = c.e_nip 
            where  a.trx_status = 1' .$where. ' GROUP BY c.co_id ORDER BY co_id DESC';
            //die($query);
            $result1 = $this->db->query($query)->result();
            if($result1) foreach ($result1 as $list) {
                $dataSemua[] = $list;
            }

            $where.=" AND e_nip like 'EX%' ";
            $query2 = 'select e_nip as c_id, e_program as co_id, program_name
            from bca_canteen_trx a
            LEFT JOIN bca_employee b on b.e_nip = a.e_id
            LEFT JOIN bca_program c ON b.e_program = c.p_id 
            where  a.trx_status = 1' .$where. ' GROUP BY b.e_program ORDER BY e_nip DESC';
            $result2 = $this->db->query($query2)->result();
            if($result2) foreach ($result2 as $list) {
                $dataSemua[] = $list;
            }
            return $dataSemua;
        }

        function getTrxByStartDateEndDate($start_date,$end_date,$nip =''){
            $where ='';


            $start_date = date('Y-m-d',strtotime($start_date));
            $end_date = date('Y-m-d',strtotime($end_date));


            $where.=" AND a.trx_time >= '".$start_date." 00:00:00' AND a.trx_time <= '".$end_date." 23:59:59'";
            if($nip != ''){
                $where.= ' AND a.e_id like "%'.$nip.'%"';
            }

            $query = 'select  b.e_gender,a.trx_time,
            DATE_FORMAT(NOW(), "%Y") - DATE_FORMAT(e_birthdate, "%Y") - (DATE_FORMAT(NOW(), "00-%m-%d") < DATE_FORMAT(e_birthdate, "00-%m-%d")) AS age,
            b.e_eselon,b.e_work_unit as unitkerja,b.e_birthdate as tanggal_lahir,d.c_name as makanan_name,d.c_calory as calory,d.c_fat as fat,d.c_protein as protein, a.e_id,b.e_name,c.program_name,c.subprogram_name,c.training_name,b.e_divisi, b.e_wilayah
            from bca_canteen_trx a
            LEFT JOIN bca_employee b on b.e_nip = a.e_id
            LEFT JOIN bca_program c on b.e_program = c.p_id
            LEFT JOIN bca_canteen d on d.c_id = a.trx_item_id
            where  a.trx_status = 1' .$where. ' order by a.e_id,a.trx_time';
            //die($query);
            return $this->db->query($query)->result();
        }
         function getTrxByStartDateEndDateGroupByEmployee($start_date,$end_date){
            $where ='';


            $start_date = date('Y-m-d',strtotime($start_date));
            $end_date = date('Y-m-d',strtotime($end_date));


            $where.=" AND a.trx_time >= '".$start_date." 00:00:00' AND a.trx_time <= '".$end_date." 23:59:59'";
            $query = 'select  e_id
            from bca_canteen_trx a
            where  a.trx_status = 1' .$where. ' group by a.e_id order by a.e_id';
            //die($query);
            return $this->db->query($query)->result();
        }


        function getTrxByDateCustomerItem($date,$customer,$item){
            $where ='';


            $dateNow = date('Y-m-d',strtotime($date));
            $where.=" AND a.trx_time >= '".$dateNow." 00:00:00' AND a.trx_time <= '".$dateNow." 23:59:59'";
            if($customer != ''){
                $where.=" AND e_id = '".$customer."'";
            }
            $where.=" AND trx_item_id = '".$item."'";

            $query = 'select count(trx_id) as total
            from bca_canteen_trx a
            where  a.trx_status = 1' .$where;

            //die($query);

            $data =  $this->db->query($query)->row();
            if($data){

                return $data->total;
            }
        }
        function getTrxByDateProgramItem($date,$program,$item){
            $where ='';


            $dateNow = date('Y-m-d',strtotime($date));
            $where.=" AND a.trx_time >= '".$dateNow." 00:00:00' AND a.trx_time <= '".$dateNow." 23:59:59'";
            if($program > 0){
                $where.=' AND b.co_id = '.$program;
                $where.= ' AND "'.$dateNow.'" BETWEEN b.co_start AND b.co_end';
            }
            $where.=' AND trx_item_id = '.$item;

               $query = 'select *
            from bca_canteen_trx a
            LEFT JOIN bca_course_participant b on b.e_nip = a.e_id
            where  a.trx_status = 1' .$where .' group by trx_id';
            // if($program == 33333 && $item == 4)die($query);
            $data =  $this->db->query($query)->result();
            if($data){

                return count($data);
            }
        }
        function getTrxByDateProgramItem2($date,$program,$item){
            $where ='';

            $dateNow = date('Y-m-d',strtotime($date));
            $where.=" AND a.trx_time >= '".$dateNow." 00:00:00' AND a.trx_time <= '".$dateNow." 23:59:59'";
            //if($program > 0){
              //  $where.=' AND b.co_id = '.$program;
                $where.= ' AND "'.$dateNow.'" BETWEEN b.co_start AND b.co_end';
            //}
            $where.=' AND trx_item_id = '.$item;

            $query = 'select trx_id
            from bca_canteen_trx a
            LEFT JOIN bca_course_participant b on b.e_nip = a.e_id
            where a.trx_status = 1' .$where .' group by trx_id';

            $union = ' UNION
                        SELECT 
                          trx_id 
                        FROM
                          bca_canteen_trx a 
                          LEFT JOIN bca_employee b 
                            ON b.e_nip = a.e_id 
                        WHERE a.trx_status = 1 
                          AND a.trx_time >= "'.$dateNow.' 00:00:00"
                          AND a.trx_time <= "'.$dateNow.' 23:59:59"
                          AND trx_item_id = '.$item.'
                          AND e_program != "" 
                        GROUP BY trx_id';

            $query .= $union;
            //if($item == 5)
            //die($query); 
            $data =  $this->db->query($query)->result();
            $newData = array();
            if($data)foreach($data as $list){
                $newData[] = $list->trx_id;
            }

            $where ='';

            $dateNow = date('Y-m-d',strtotime($date));
            $where.=" AND trx_time >= '".$dateNow." 00:00:00' AND trx_time <= '".$dateNow." 23:59:59'";
            $program =1;

            if($newData){
                $newData = implode(',',$newData);
                $where.=' AND trx_id NOT in ( '.$newData.')';

            }
            $where.=' AND trx_item_id = '.$item;
            //$where.= ' AND ( "'.$dateNow.'" NOT BETWEEN b.co_start AND b.co_end OR co_id IS NULL)';
            $query = 'select trx_id
            from bca_canteen_trx
            where  trx_status = 1' .$where .' group by trx_id';
            //if($item == 5)die($query);
            //die($query);
            $data =  $this->db->query($query)->result();
            if($data){

                return count($data);
            }
        }

        // function getTrxByDateProgramItem4($date,$program,$item){
        //     $where ='';


        //     $dateNow = date('Y-m-d',strtotime($date));
        //     $where.=" AND a.trx_time >= '".$dateNow." 00:00:00' AND a.trx_time <= '".$dateNow." 23:59:59'";

        //     $where.=' AND trx_item_id = '.$item.' AND e_program != ""';

        //     $query = 'select trx_id
        //     from bca_canteen_trx a
        //     LEFT JOIN bca_employee b on b.e_nip = a.e_id
        //     where a.trx_status = 1' .$where .' group by trx_id';
        //     //die($query);
        //     $data =  $this->db->query($query)->result();
        //     $newData = array();
        //     if($data)foreach($data as $list){
        //         $newData[] = $list->trx_id;
        //     }

        //     $where ='';

        //     $dateNow = date('Y-m-d',strtotime($date));
        //     $where.=" AND trx_time >= '".$dateNow." 00:00:00' AND trx_time <= '".$dateNow." 23:59:59'";
        //     $program =1;

        //     if($newData){
        //         $newData = implode(',',$newData);
        //         $where.=' AND trx_id NOT in ( '.$newData.')';

        //     }
        //     $where.=' AND trx_item_id = '.$item;
        //     $query = 'select trx_id
        //     from bca_canteen_trx
        //     where trx_status = 1' .$where .' group by trx_id';
        //     //die($query);
        //     $data =  $this->db->query($query)->result();
        //     if($data){

        //         return count($data);
        //     }
        // }

        function getTrxByDateProgramItem3($date,$program,$item){
            $where ='';


            $dateNow = date('Y-m-d',strtotime($date));
            $where.=" AND a.trx_time >= '".$dateNow." 00:00:00' AND a.trx_time <= '".$dateNow." 23:59:59'";
            if($program > 0){
                $where.=' AND b.e_program = '.$program;
            }
            $where.=' AND trx_item_id = '.$item;

               $query = 'select *
            from bca_canteen_trx a
            LEFT JOIN bca_employee b ON a.e_id = b.e_nip 
            where a.trx_status = 1' .$where .' group by trx_id';
            //die($query);
            $data =  $this->db->query($query)->result();
            if($data){

                return count($data);
            }
        }

        function getTrxByDateProgramItemDaily($date,$item){
            $where ='';


            $dateNow = date('Y-m-d',strtotime($date));
            $where.=" AND a.trx_time >= '".$dateNow." 00:00:00' AND a.trx_time <= '".$dateNow." 23:59:59'";

            $where.=' AND trx_item_id = '.$item;

            $query = 'select count(trx_id) as total
            from bca_canteen_trx a
             LEFT JOIN bca_employee b on b.e_nip = a.e_id
            where  a.trx_status = 1' .$where;
            //die($query);
            $data =  $this->db->query($query)->row();
            if($data){

                return $data->total;
            }



        }
        function getTrxByDateProgramItemDailyStartDateEndDate($start_date,$end_date,$item){
            $where ='';


              $start_date = date('Y-m-d',strtotime($start_date));
            $end_date = date('Y-m-d',strtotime($end_date));


            $where.=" AND a.trx_time >= '".$start_date." 00:00:00' AND a.trx_time <= '".$end_date." 23:59:59'";

            $where.=' AND trx_item_id = '.$item;

            $query = 'select count(trx_id) as total
            from bca_canteen_trx a
             LEFT JOIN bca_employee b on b.e_nip = a.e_id
            where  a.trx_status = 1' .$where;

            $data =  $this->db->query($query)->row();
            if($data){

                return $data->total;
            }



        }


        public function transaction_record($date)
        {
            return $this->db->query("SELECT 
                                        DISTINCT(trx.e_id),
                                        emp.e_name,
                                        prog.program_name,
                                        prog.subprogram_name,
                                        prog.training_name,
                                        (SELECT a.trx_item_id FROM bca_canteen_trx a, bca_canteen b WHERE DATE(a.trx_time) = '".$date."' AND a.trx_item_id = b.c_id AND b.c_type = 1 AND a.e_id = trx.e_id) AS utama,
                                        (SELECT a.trx_item_id FROM bca_canteen_trx a, bca_canteen b WHERE DATE(a.trx_time) = '".$date."' AND a.trx_item_id = b.c_id AND b.c_type = 2 AND a.e_id = trx.e_id) AS minuman,
                                        (SELECT a.trx_item_id FROM bca_canteen_trx a, bca_canteen b WHERE DATE(a.trx_time) = '".$date."' AND a.trx_item_id = b.c_id AND b.c_type = 3 AND a.e_id = trx.e_id) AS penutup
                                    FROM 
                                        bca_canteen_trx trx, bca_employee emp, bca_program prog
                                    WHERE
                                        DATE(trx.trx_time) = '".$date."'
                                        AND
                                        trx.e_id = emp.e_nip
                                        AND
                                        emp.e_program = prog.p_id");
        }

        public function getSuccessTrx($start, $end)
        {
            return $this->db->query("SELECT a.s_id, a.e_id, a.e_rfid, a.trx_item_id, DATE(a.trx_time) as tanggal, b.c_name FROM bca_canteen_trx a JOIN bca_canteen b ON a.trx_item_id = b.c_id WHERE DATE(trx_time) BETWEEN '".$start."' AND '".$end."' AND trx_status = 1 GROUP BY trx_item_id");
        }

        public function getStartingStock($date, $c_id)
        {
            return $this->db->query("SELECT starting_stock FROM bca_food_serving WHERE '".$date."' BETWEEN start_date AND end_date AND c_id = ".$c_id);
        }

        public function getSisaStock($date, $c_id)
        {
            return $this->db->query("SELECT stock FROM bca_daily_stock WHERE trx_date = '".$date."' AND c_id = ".$c_id);
        }

        public function checkServing($date, $c_id)
        {
            $dateNow = date('Y-m-d',strtotime($date));
            $query = "SELECT COUNT(*) AS total, starting_stock FROM bca_food_serving WHERE '".$dateNow."' BETWEEN start_date and end_date AND c_id = ".$c_id;
            $data = $this->db->query($query)->row();
            //echo $query;
            return $data;
        }

        public function summaryTrxByDate($date_start, $date_end)
        {
            $dateS = explode("-", $date_start);
            $start_date = $dateS[2]."-".$dateS[1]."-".$dateS[0];

            $dateE = explode("-", $date_end);
            $end_date = $dateE[2]."-".$dateE[1]."-".$dateE[0];

            if($start_date == $end_date)
            {
                $where = "DATE(trx_time) = '".$start_date."'";
            }
            else
            {
                $where = "DATE(trx_time) BETWEEN '".$start_date."' AND '".$end_date."'";
            }

            $query = "SELECT e_id, DATE(trx_time) AS trx_date, trx_item_id FROM bca_canteen_trx a JOIN bca_employee b ON a.e_id = b.e_nip WHERE ".$where." AND trx_status = 1 ORDER BY e_name, trx_date";
            return $this->db->query($query)->result();
        }
    }
?>
