<?php
	class Canteen_location_model extends CI_Model
	{
		public function canteen_location_list()
        {
            return $this->db->query("SELECT * FROM bca_canteen_location WHERE deleted = 0");
        }

        public function canteen_location_data($id)
        {
            return $this->db->query("SELECT * FROM bca_canteen_location WHERE l_id = ".$id." AND deleted = 0");
        }

		public function insert_canteen_location($l_room, $l_floor)
		{
			$date = date("Y-m-d H:i:s");
			
			$data = array(
				"l_room"  => $l_room,
				"l_floor" => $l_floor
			);
			
			$this->db->insert("bca_canteen_location", $data);
		}

		public function update_canteen_location($l_id, $l_room, $l_floor)
		{
			$date = date("Y-m-d H:i:s");

			$data = array(
				"l_room"  => $l_room,
				"l_floor" => $l_floor
			);

			$this->db->where("l_id", $l_id);
			$this->db->update("bca_canteen_location", $data);
		}

		public function delete_canteen_location($id)
		{
			// $this->db->where("l_id", $id);
			// $this->db->delete("bca_canteen_location");

			$data = array('deleted' => 1);              
            $this->db->where('l_id', $id);
            $this->db->update('bca_canteen_location', $data);
		}

		public function check_trx($id)
        {
            return $this->db->query("SELECT COUNT(*) AS total FROM bca_canteen_location a, bca_stall_device b, bca_food_serving c WHERE a.`l_id` = b.`loc_id` AND b.s_id = c.s_id AND a.l_id = ".$id);
        }
	}
?>