<?php
	class Food_serving_buffet_model extends CI_Model
	{
		public function event_list()
		{
			return $this->db->query("SELECT * FROM bca_food_serving_buffet");
		}

		public function event_data($id)
		{
			return $this->db->query("SELECT * FROM bca_food_serving_buffet WHERE id = ".$id);
		}

		public function event_time($date)
		{
			return $this->db->query("SELECT * FROM bca_food_serving_buffet WHERE '".$date."' BETWEEN `start` and `end`");
		}

		public function insertData($event_name, $start, $end)
		{
			$fulldate = date('Y-m-d H:i:s');
			$data = array(
				'event_name'   => $event_name,
				'start'        => $start,
				'end'          => $end,
				'created_date' => $fulldate,
				'created_by'   => $this->session->userdata('username')
			);
			$this->db->insert('bca_food_serving_buffet', $data);
			return $this->db->insert_id();
		}

		public function updateData($id, $event_name, $start, $end)
		{
			$fulldate = date('Y-m-d H:i:s');

			$data = array(
				'event_name'   => $event_name,
				'start'        => $start,
				'end'          => $end,
				'edited_date'  => $fulldate,
				'edited_by'    => $this->session->userdata('username')
			);

			$this->db->where('id', $id);
			$this->db->update('bca_food_serving_buffet', $data);
		}

		public function deleteData($id)
		{
			$this->db->where('id', $id);
			$this->db->delete('bca_food_serving_buffet');
		}

		public function daily_stock_today()
		{
			return $this->db->query("SELECT a.id, a.c_id, b.c_name, a.s_id, c.device_name, a.stock, a.trx_date FROM bca_daily_stock a JOIN bca_canteen b ON a.c_id = b.c_id JOIN bca_stall_device c ON a.s_id = c.s_id WHERE a.trx_date = CURDATE()");
		}

		public function check_trx($c_id, $s_id)
        {
            return $this->db->query("SELECT COUNT(*) AS total FROM bca_canteen_trx WHERE DATE(trx_time) = CURDATE() AND trx_item_id = ".$c_id." AND s_id = ".$s_id);
        }

        public function deleteView($id)
		{
			$this->db->where('id', $id);
			$this->db->delete('bca_daily_stock');
		}
	}
?>