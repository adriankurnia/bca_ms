<?php
	class Buffet_detail_model extends CI_Model
	{
		public function buffet_detail_list()
		{
			return $this->db->query("SELECT a.id, a.c_id, b.c_name, a.item_name, a.created_date, a.created_by, a.edited_date, a.edited_by FROM bca_buffet_detail a, bca_canteen b WHERE a.c_id = b.c_id");
		}

		// public function buffet_detail_data($id)
		// {
		// 	return $this->db->query("SELECT a.id, a.c_id, b.c_name, a.item_name, a.created_date, a.created_by, a.edited_date, a.edited_by FROM bca_buffet_detail a, bca_canteen b WHERE a.c_id = b.c_id AND a.id = ".$id);
		// }

		public function buffet_detail_data($id)
		{
			return $this->db->query("SELECT a.id, a.c_id, b.c_name, a.item_name, a.created_date, a.created_by, a.edited_date, a.edited_by FROM bca_buffet_detail a, bca_canteen b WHERE a.c_id = b.c_id AND a.c_id = ".$id);
		}

		public function canteen_buffet()
		{
			return $this->db->query("SELECT c_id, c_name FROM bca_canteen WHERE buffet = 1");
		}

		public function insertData($c_id, $item_name)
		{
			$fulldate = date('Y-m-d H:i:s');
			$data = array(
				'c_id'	       => $c_id,
				'item_name'    => $item_name,
				'created_date' => $fulldate,
				'created_by'   => $this->session->userdata('username')
			);
			$this->db->insert('bca_buffet_detail', $data);
		}

		public function updateData($id, $c_id, $item_name)
		{
			$fulldate = date('Y-m-d H:i:s');

			$data = array(
				'c_id'	      => $c_id,
				'item_name'   => $item_name,
				'edited_date' => $fulldate,
				'edited_by'   => $this->session->userdata('username')
			);

			$this->db->where('id', $id);
			$this->db->update('bca_buffet_detail', $data);
		}

		public function deleteData($id)
		{
			$this->db->where('id', $id);
			$this->db->delete('bca_buffet_detail');
		}
	}
?>