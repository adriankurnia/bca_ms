<?php
	class Food_serving_time_model extends CI_Model
	{
		public function active_list()
		{
			return $this->db->query("SELECT * FROM bca_food_serving_time");
		}

		public function active_data($id)
		{
			return $this->db->query("SELECT * FROM bca_food_serving_time WHERE id = ".$id);
		}

		public function data_exist($date, $id = null)
		{
			if($id == null) 
			{
				return $this->db->query("SELECT * FROM bca_food_serving_time WHERE `date` = '".$date."'");
			}
			else
			{
				return $this->db->query("SELECT * FROM bca_food_serving_time WHERE `date` = '".$date."' and id != ".$id);
			}
		}

		public function insertData($date, $time, $notes)
		{
			$fulldate = date('Y-m-d H:i:s');
			$data = array(
				'notes'      => $notes,
				'date'       => $date,
				'time_start' => $time
			);
			$this->db->insert('bca_food_serving_time', $data);
			return $this->db->insert_id();
		}

		public function updateData($id, $date, $time, $notes)
		{
			$data = array(
				'notes'      => $notes,
				'date'       => $date,
				'time_start' => $time
			);

			$this->db->where('id', $id);
			$this->db->update('bca_food_serving_time', $data);
		}

		public function deleteData($id)
		{
			$this->db->where('id', $id);
			$this->db->delete('bca_food_serving_time');
		}

		public function get_default()
		{
			return $this->db->query("SELECT * FROM bca_serving_time_default WHERE id = 1");
		}

		public function update_default($d_time)
		{
			return $this->db->query("UPDATE bca_serving_time_default SET default_time = '".$d_time."'");
		}
	}
?>