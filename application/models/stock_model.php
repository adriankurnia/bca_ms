<?php
    class Stock_model extends CI_Model
    {
        public function stock_items_list($id)
        {
            if($id == 0)
            {
                return $this->db->query("SELECT a.c_id, b.s_id, a.c_name, b.s_stock, b.s_serving_date FROM bca_canteen a, bca_canteen_stock b WHERE a.c_id = b.c_id AND b.deleted = 0");
            }
            else
            {
                return $this->db->query("SELECT a.c_id, b.s_id, a.c_name, b.s_stock, b.s_serving_date FROM bca_canteen a, bca_canteen_stock b WHERE a.c_id = b.c_id AND b.deleted = 0 AND a.c_id = ".$id);
            }
        }

        public function stock_item_data($id)
        {
            return $this->db->query("SELECT a.c_id, b.s_id, a.c_name, b.s_stock, b.s_serving_date FROM bca_canteen a, bca_canteen_stock b WHERE b.s_id = ".$id." AND a.c_id = b.c_id");
        }

        public function get_stok_serving_date($id)
        {
            return $this->db->query("SELECT s_serving_date FROM bca_canteen_stock WHERE c_id = ".$id);
        }

        public function food_name($id = null)
        {
            if($id == null)
            {
        	   return $this->db->query("SELECT c_id, c_name FROM bca_canteen");
            }
            else
            {
                return $this->db->query("SELECT c_id, c_name FROM bca_canteen WHERE c_id = ".$id);
            }
        }

        public function insert_stock_item($c_id, $stock, $serving_date)
        {
            $data = array('c_id'          => $c_id,
                        's_stock'         => $stock,
                        's_serving_date'  => $serving_date,
                        's_creation_user' => $this->session->userdata('username')
                        );
            $this->db->insert('bca_canteen_stock', $data);

            // $id = $this->db->insert_id();
            // $data_stock = array('s_id'        => $id,
            //                 's_stock'         => $stock,
            //                 's_serving_date'  => $serving_date,
            //                 's_creation_user' => $this->session->userdata('username')
            //                 );
            // $this->db->insert('bca_canteen_stock', $data_stock);
        }

        public function update_stock_item($s_id, $c_id, $stock, $serving_date)
        {
            $fulldate = date('Y-m-d H:i:s', time());
            $data = array('c_id'          => $c_id,
                        's_stock'         => $stock,
                        's_serving_date'  => $serving_date,
                        's_modified_date' => $fulldate,
                        's_modified_user' => $this->session->userdata('username')
                        );              
            $this->db->where('s_id', $s_id);
            $this->db->update('bca_canteen_stock', $data);

            // $data_stock = array('s_stock'     => $stock,
            //                 's_serving_date'  => $serving_date,
            //                 's_modified_date' => $fulldate,
            //                 's_modified_user' => $this->session->userdata('username')
            //                 );              
            // $this->db->where('s_id', $id);
            // $this->db->update('bca_canteen_stock', $data_stock);
        }

        public function delete_stock_item($id)
        {
            // $this->db->where('c_id', $id);
            // $this->db->delete('bca_stock');

            // $this->db->where('s_id', $id);
            // $this->db->delete('bca_canteen_stock');

            $data = array('deleted' => 1);              
            $this->db->where('s_id', $id);
            $this->db->update('bca_canteen_stock', $data);
        }
    }
 ?>