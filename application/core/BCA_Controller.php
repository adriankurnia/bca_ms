<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BCA_Controller extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->session_check();
    }

	public function session_check()
	{
		if(empty($this->session->userdata('loggedin')))
		{
			redirect(base_url()."login");
		}
	}

	public function String_Serialize($str)
	{
		// $str = preg_replace("/\|/","_",$str);
		// $str = preg_replace("/\n/"," ",$str);
		// $str = preg_replace("/\r/"," ",$str);
		// $str = preg_replace("/\t/"," ",$str);
		$str = addslashes($str);
		return $str;
	}

	public function Resize_Img()
	{
		$this->load->library('image_lib');
		$config['image_library']	= 'gd2';
		$config['source_image']		= $target_file_db;
		$config['create_thumb']		= TRUE;
		$config['maintain_ratio']	= TRUE;
		$config['width']			= 500;
		$config['height']			= 150;
		$this->image_lib->clear();		
		$this->image_lib->initialize($config);
		$this->image_lib->resize();
	}

	public function cms_log_activity($act, $notes, $became = null)
	{
		$fulldate = date('Y-m-d H:i:s');
        $data = array('activity'    => $act,
                    'notes'         => $notes,
                    'became'        => $became,
                    'activity_by'   => $this->session->userdata('username'),
                    'activity_time' => $fulldate
                );
        $this->db->insert('bca_cms_log', $data);
	}

	public function date_formating($date)
	{
		$explode = explode("-", $date);
		$new = $explode[2]."-".$explode[1]."-".$explode[0];
		return $new;
	}
}

class UNI_Controller extends CI_Controller {
	
	//declared data
	protected $data;
	
	public function __construct() {
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		
		//load database connection
		$this->load->database();
		
		//load helper
		$this->load->helper('array');
		$this->load->helper('date');
		$this->load->helper('email');
		$this->load->helper('file');
		$this->load->helper('form');
		$this->load->helper('text');
		$this->load->helper('url');
		
		//load library
		$this->load->library('email');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('session');
		$this->load->library('upload');

		//load model
		$this->load->model('model_common');
		$this->load->model("model_admin_privilege");
		$this->load->model("model_admin_module");
		$this->load->model("model_admin_privilege_has_admin_module");
		$this->load->model("model_log");
		
		//custom library
		//$this->load->library('Custom_resize');
		//$this->load->library('curl');
		$this->load->library('layout');
		$this->load->library('session_entry');

		//load helper
		
		$this->layout->set_data($this->data);
	}

	public function string_to_array_converter($string)
	{
		if($string[0] == "{" && $string[strlen($string)-1] == "}")
		{
			$new_string = substr($string, 1, -1);
			$value = explode(",", $new_string);
		}
		else
		{
			$value = "invalid string format";
		}
		return $value;
	}

	public function string_to_character($str) {
	    return ucwords(str_replace("_", " ", $str));
	}
	
	public function site_url_generator($str) {
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", '_', $clean);

		return $clean;
	}

	public function clean_setting_url($str){
		$clean = substr($str, 0, -1);

		return $clean;
	}

	public function upload_image($file, $upload_path, $resize = null)
	{
		//Upload Thumbnail
		$config['upload_path'] = './' . ASSETS_UPLOADS_URL . $this->router->fetch_class() . '/original/' . $upload_path . '/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		//$config['encrypt_name'] = TRUE;
		$this->upload->initialize($config);

		if($this->upload->do_upload($file))
		{
			$upload = $this->upload->data();$file_name = $upload['file_name'];
			

			if($resize == null || $resize == true)
			{
				$this->autoresize($file_name, $config['upload_path'], $upload_path);
			}
		}
		else
		{
			//IF NOT UPLOADED
			$file_name = DEFAULT_IMAGE_NAME;
		}

		return $file_name;
	}

	public function update_image($file, $upload_path, $resize = null)
	{
		if($_FILES[$file]['tmp_name'] != null)
		{
			//Check if Old Image Exist	
			if($this->data[$file] != NULL)
			{
				$this->delete_image($this->data[$file], $upload_path, $resize);
			}

			$this->data[$file] = $this->upload_image($file, $upload_path, $resize);
		}
		else
		{
			//Use Old Image
			$this->data[$file] = $this->data[$file];
		}

		return $this->data[$file];
	}

    public function delete_image($file_name, $upload_path, $resize = null)
	{
		unlink('./'.ASSETS_UPLOADS_URL . $this->router->fetch_class() . '/original/' . $upload_path . '/' . $file_name);

		if($resize == null || $resize == true)
		{
			unlink('./'.ASSETS_UPLOADS_URL . $this->router->fetch_class() . '/resized/' . $upload_path . '/' . $file_name); 
		}
	}

	public function delete_child_image($file_name, $child_name, $upload_path, $resize = null)
	{
		unlink('./'.ASSETS_UPLOADS_URL . $child_name . '/original/' . $upload_path . '/' . $file_name);

		if($resize == null || $resize == true)
		{
			unlink('./'.ASSETS_UPLOADS_URL . $child_name . '/resized/' . $upload_path . '/' . $file_name); 
		}
	}

	// New Code Added at 27 January 2017 - START
	public function upload_file($file, $upload_path, $extension = null)
	{
		if($extension == null)
		{
			$extension = 'doc|docx|pdf';			
		}

		//Upload Thumbnail
		$config['upload_path'] = './' . ASSETS_UPLOADS_URL . $this->router->fetch_class() . '/original/' . $upload_path . '/';
		$config['allowed_types'] = $extension;
		//$config['encrypt_name'] = TRUE;
		$this->upload->initialize($config);

		if($this->upload->do_upload($file))
		{
			$upload = $this->upload->data();
			$file_name = $upload['file_name'];
		}
		else
		{
			$file_name = $this->upload->display_errors();
		}

		return $file_name;
	}

	public function update_file($file, $upload_path, $extension = null)
	{
		if($_FILES[$file]['tmp_name'] != null)
		{
			//Check if Old File Exist	
			if($this->data[$file] != NULL)
			{
				$this->delete_file($this->data[$file], $upload_path);
			}

			$this->data[$file] = $this->upload_file($file, $upload_path, $extension);
		}
		else
		{
			//Use Old File
			$this->data[$file] = $this->data[$file];
		}

		return $this->data[$file];
	}

	public function delete_file($file_name, $upload_path)
	{
		unlink('./'.ASSETS_UPLOADS_URL . $this->router->fetch_class() . '/original/' . $upload_path . '/' . $file_name);
	}

	// New Code Added at 27 January 2017 - END

	public function autoresize($file_name, $original_path, $upload_path, $width = null, $height = null, $maintain_ratio = null)
	{		
		$config_manip['image_library'] = 'gd2';
		$config_manip['source_image'] = $original_path . $file_name;
		$config_manip['new_image'] = './' . ASSETS_UPLOADS_URL . $this->router->fetch_class() .'/resized/'.$upload_path.'/' . $file_name;
		$config_manip['encrypt_name'] = FALSE;

		if($maintain_ratio == null || $maintain_ratio == true)
		{
			$config_manip['maintain_ratio'] = TRUE;
		}
		else
		{
			$config_manip['maintain_ratio'] = FALSE;
		}

		if($width != null)
		{
			$config_manip['width'] = $width;
		}
		if($height != null)
		{
			$config_manip['height'] = $height;
		}

		$this->image_lib->initialize($config_manip);
		$this->image_lib->resize();
	}

	// New Code Added at 12 Febuary 2017 - START

	public function generate_pagination(
			$database_name, 
			$order_by, 
			$order, 
			$page, 
			$url_page, 
			$per_page, 
			$uri_segment, 
			$num_links, 
			$num_tag_open, 
			$num_tag_close, 
			$next_link, 
			$next_tag_open, 
			$next_tag_close, 
			$prev_link, 
			$prev_tag_open, 
			$prev_tag_close, 
			$cur_tag_open, 
			$cur_tag_close
		)
	{
		$item_name = $this->db->get($database_name);
		$item_count = $item_name->num_rows();

		$config = array();
        $config["base_url"] = base_url() . "news/".$url_page."/"; // need to fix
        $config["total_rows"] = $item_count;
        $config["per_page"] = $per_page;
        $config["uri_segment"] = $uri_segment;
        $config['num_links'] = $num_links;
        $config['display_pages'] = true; 
        $config['use_page_numbers'] = true;

        $data['totalPage'] = ceil($config["total_rows"] / $config["per_page"]);
        $data['page'] = $page;

        $config['num_tag_open'] =  $num_tag_open;
        $config['num_tag_close'] = $num_tag_close;

		$config['next_link'] = $next_link;
		$config['next_tag_open'] = $next_tag_open;
		$config['next_tag_close'] = $next_tag_close;

		$config['prev_link'] = $prev_link;
		$config['prev_tag_open'] = $prev_tag_open;
		$config['prev_tag_close'] = $prev_tag_close;

		$config['cur_tag_open'] = $cur_tag_open;
		$config['cur_tag_close'] = $cur_tag_close;

		$this->pagination->initialize($config);

		$offset = ($page - 1) * $config["per_page"];
		
		$data['items'] = $this->db->order_by($order_by, $order)->limit($config["per_page"], $offset)->get($database_name)->result();
		$data["link_items"] = $this->pagination->create_links();

		$result = array();
		$result[] = $data['items'];
		$result[] = $data['link_items'];
		
		return $result;
	}

	public function get_data_before($database_name, $attribute_name, $attribute_value, $order_by, $data_collected)
	{
		$before = $this->db->query("SELECT * FROM {$database_name} 
			WHERE {$attribute_name} < {$attribute_value} 
			ORDER BY {$order_by} DESC 
			LIMIT $data_collected");

		return $before;
	}

	public function get_data_after($database_name, $attribute_name, $attribute_value, $order_by, $data_collected)
	{
		$after = $this->db->query("SELECT * FROM {$database_name} 
			WHERE {$attribute_name} > {$attribute_value} 
			ORDER BY {$order_by} ASC 
			LIMIT $data_collected");

		return $after;
	}

	public function get_data_before_and_after($database_name, $attribute_name, $attribute_value, $order_by, $data_collected)
	{
		$result = array();

		$result += $this->get_data_before($database_name, $attribute_name, $attribute_value, $order_by, $data_collected)->result();
		$result += $this->get_data_after($database_name, $attribute_name, $attribute_value, $order_by, $data_collected)->result();

		return $result;
	}

	// New Code Added at 12 Febuary 2017 - END
	
	//New Code Added at 31 Maret 2017 - START
	
	public function check_image_upload($file, $upload_path, $resize)
	{
		if(!empty($_FILES[$file]['name']))
		{	
			//If image not null, upload image to Server
			return $this->upload_image($file, $upload_path, $resize);
		}
		else
		{
			//Set Default Image
			return DEFAULT_IMAGE_NAME;
		}
	}
	
	public function check_image($file, $upload_path, $form_status ,$resize = null)
	{
		if($form_status == null) // Add Form
		{
			return $this->check_image_upload($file, $upload_path, $resize);
		}
		else if($form_status != null) // Edit Form
		{
			if($this->data[$file] == DEFAULT_IMAGE_NAME || $this->data[$file] == null)
			{
				//If edit_form has defaulr_image or null, than do normal upload with check image
				return $this->check_image_upload($file, $upload_path, $resize);
			}
			else
			{
				//else, do update image
				return $this->update_image($file, $upload_path, $resize);
			}
		}
		
	}
	
	//New Code Added at 31 Maret 2017 - END

}

class UNIAdmin_Controller extends UNI_Controller {
	
	public function __construct() {
		parent::__construct();

		//DEFINE GLOBAL VARIABLE HERE

		// $company_setting = $this->db->where('company_setting_key', 'company_current_location')->get('ms_company_setting')->row()->company_setting_value;

		// $this->data['current_city'] = $company_setting;
		// $this->data['admin_name'] = $this->session->userdata('admin_name');

		// $this->layout->set_masterpage(ADMIN_URL.'page');
		// $unlocked = array('login');

		// if ( ! $this->session_entry->check_logged_admin() AND ! in_array(strtolower(get_class($this)), $unlocked) ) {
		// 	$this->session->set_flashdata('error','Please login first');
		// 	redirect(ADMIN_URL_LOGIN);
		// }else if ( $this->session_entry->check_logged_admin() AND in_array(strtolower(get_class($this)), $unlocked) ) {
		// 	redirect(ADMIN_URL_DASHBOARD);
		// }

		// //Load Setting
		// $this->load->model("model_setting");
		// $this->setting = array();

		// $settings = $this->model_setting->getData()->result();
		// foreach($settings as $setting)
		// {
		// 	$this->setting[$setting->setting_key] = $setting->setting_value;
		// }


		// //Privilege
		// $this->create_privilege  = 0;
		// $this->read_privilege = 0;
		// $this->update_privilege = 0;
		// $this->delete_privilege = 0;

		// //Privilege
		// if($this->uri->segment(2) == $this->clean_setting_url($this->setting['ADMIN_URL_ACCESS_FORBIDDEN']) || $this->uri->segment(2) == 'logout' || $this->uri->segment(2) == 'login' || $this->uri->segment(1) == 'b4ck0ff1c3')
		// {
		// 	// SKIP PRIVILEGE CHECK
		// }
		// else
		// {
		// 	$admin_privilege_id = $this->session->userdata('admin_privilege_id');
		// 	$admin_module_id = $this->model_admin_module->getDataByControllerName($this->uri->segment(2))->row()->admin_module_id;

		// 	$admin_module_has_admin_privilege = $this->model_admin_privilege_has_admin_module->getDataByAdminPrivilegeIdAndAdminModuleId($admin_privilege_id, $admin_module_id)->row();

		// 	$this->create_privilege = $admin_module_has_admin_privilege->admin_create;
		// 	$this->read_privilege = $admin_module_has_admin_privilege->admin_read;
		// 	$this->update_privilege = $admin_module_has_admin_privilege->admin_update;
		// 	$this->delete_privilege = $admin_module_has_admin_privilege->admin_delete;
		// }
	}
}

class UNIUser_Controller extends UNI_Controller {
	
	public function __construct() {
		parent::__construct();

		// $company_setting = $this->db->where('company_setting_key', 'company_current_location')->get('ms_company_setting')->row()->company_setting_value;

		// $this->data['current_city'] = $company_setting;

		// $this->data['admin_name'] = $this->session->userdata('admin_name');

		// $unlocked = array('login');

		// if ( ! $this->session_entry->check_logged_account() AND ! in_array(strtolower(get_class($this)), $unlocked) ) {
		// 	$this->session->set_flashdata('error','Please login first');
		// 	redirect(DISPLAY_URL_LOGIN);
		// }else if ( $this->session_entry->check_logged_account() AND in_array(strtolower(get_class($this)), $unlocked) ) {
		// 	redirect(DISPLAY_URL_HOME);
		// }
	}
}

class UNIPublic_Controller extends UNI_Controller {

	public function __construct() {
		parent::__construct();

		// $company_setting = $this->db->where('company_setting_key', 'company_current_location')->get('ms_company_setting')->row()->company_setting_value;

		// $this->data['current_city'] = $company_setting;

		// //Load Setting
		// $this->load->model("model_setting");
		// $this->setting = array();

		// $settings = $this->model_setting->getData()->result();
		// foreach($settings as $setting)
		// {
		// 	$this->setting[$setting->setting_key] = $setting->setting_value;
		// }
		
		// $this->layout->set_masterpage('page');
	}
}