<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('make_alias'))
{
	function make_alias($string)
	{
		$string=strtolower($string);

		$string=str_replace('&','-',$string);

		$string=preg_replace('/[^a-z0-9]/', "-", $string);

		$string=ltrim(rtrim($string,'-'),'-');

		$string=str_replace('---','-',$string);
		$string=str_replace('--','-',$string);

		return $string;
	}
}
if ( ! function_exists('numberformat')){
function numberformat($input)
	{
        if($input > 0 ){
		return number_format($input, '0', ',', '.').'';
        }else{
            return '-';
        }

	}
}
if ( ! function_exists('money')){
function money($input)
	{
        if($input > 0 ){
		    return number_format($input, '0', ',', '.').'';
        }else{
            return '-';
        }

	}
}



if ( ! function_exists('pre'))
{
	function pre($var)
	{
		echo "<pre>";
		print_r($var);
		echo "</pre>";
	}
}


function indonesian_date ($timestamp = '', $date_format = 'l, j.m.Y | H:i', $suffix = '') {
    if (trim ($timestamp) == '')
    {
            $timestamp = time ();
    }
    elseif (!ctype_digit ($timestamp))
    {
        $timestamp = strtotime ($timestamp);
    }
    # remove S (st,nd,rd,th) there are no such things in indonesia :p
    $date_format = preg_replace ("/S/", "", $date_format);
    $pattern = array (
        '/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
        '/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
        '/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
        '/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
        '/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
        '/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
        '/April/','/June/','/July/','/August/','/September/','/October/',
        '/November/','/December/',
    );
    $replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
        'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
        'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
        'Januari','Februari','Maret','April','Juni','Juli','Agustus','Sepember',
        'Oktober','November','Desember',
    );
    $date = date ($date_format, $timestamp);
    $date = preg_replace ($pattern, $replace, $date);
    $date = "{$date} {$suffix}";
    return $date;
}
if ( ! function_exists('find_latest_date'))
{
	function find_latest_date($trx_item_id)
	{
		$CI =   &get_instance();
		$CI->db->select('trx_time');
		$CI->db->where(array("trx_item_id"=>$trx_item_id,'trx_status'=>1));
		$CI->db->from('bca_canteen_trx');
        $CI->db->order_by('trx_time','desc');
		$query = $CI->db->get();
        $result =  $query->row();

		if($result){
			return indonesian_date($result->trx_time,'H:i');
		}else{
			return '-';
		}
	//	echo "select `".$coloumn."` from `".$table."` where id='".$id."'";
//	echo	$q="select `".$coloumn."` from `".$table."` where id = '".$id."'";
//		$result=mysql_fetch_assoc(mysql_query($q));
	//	return $result[$coloumn];
	}
}
