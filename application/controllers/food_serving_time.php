<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Food_serving_time extends BCA_Controller {

	public $menu_log = "Jam Tapping";

	public function index()
	{
		$this->load->model('food_serving_time_model');
		$data['times'] = $this->food_serving_time_model->active_list()->result();
		$data['default'] = $this->food_serving_time_model->get_default()->row();

		$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 26)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(26)->row();

        $this->session->unset_userdata('posting');
        $this->session->unset_userdata('editing');
		$this->load->view('food_serving_time/view', $data);
	}

	public function add_form()
    {
    	$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(26)->row();
        $this->load->view('food_serving_time/add_form', $data);
    }
    
    public function edit_form($uid = null)
    {
    	$this->session->unset_userdata('editing');

        if($uid == null) {
            $id = $this->uri->segment(3);
        } else {
            $id = $uid;
        }

        $this->load->model('food_serving_time_model');
        $data['time'] = $this->food_serving_time_model->active_data($id)->row();
        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(26)->row();
        $this->load->view('food_serving_time/edit_form', $data);
    }

    public function display()
    {
        $id = $this->uri->segment(3);
        $this->load->model('food_serving_time_model');
        $data['time'] = $this->food_serving_time_model->active_data($id)->row();
        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(26)->row();
        $this->load->view('food_serving_time/display', $data);
    }

    public function default_form()
    {
    	$this->session->unset_userdata('editing');
    	$this->load->model('food_serving_time_model');
        $data['default'] = $this->food_serving_time_model->get_default()->row();

        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(26)->row();

        $this->session->set_userdata('editing', $data);
        $this->load->view('food_serving_time/default_time', $data);
    }

    public function edit_default()
    {
		$redirect             = "food_serving_time";
		$data['default_time'] = $_POST['default_time'].":00";

    	$this->form_validation->set_rules('default_time','Jam Tapping Default','required');
    	if($this->form_validation->run())
		{
			$this->load->model('food_serving_time_model');
			$this->food_serving_time_model->update_default($data['default_time']);

			$log_data_old = "<b>Jam Tapping Default</b><br />".
								"<b>Jam</b>: ".$this->session->userdata('editing')['default']->default_time;

			$log_data_new = "<b>Jam Tapping Default</b><br />".
							"<b>Jam</b>: ".$data['default_time'];

			$this->cms_log_activity("edit", $log_data_old, $log_data_new);
			$error     = 0;
			$error_msg = "Jam Tapping Default berhasil diubah.";
			$newdata = array('msg_stime_list' => $error_msg, 'err_stime_list' => $error);
			$this->session->unset_userdata('editing');
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal mengubah Jam Tapping Default.";
			$newdata   = array('msg_timedefault_list' => $error_msg, 'msg_timedefault_list' => $error);
			$this->session->unset_userdata('editing');
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
    }

    public function new_data()
    {
		$error              = 0;
		$error_msg          = "";
		$redirect           = "food_serving_time";
		
		$data['time_notes'] = $_POST['time_notes'];
		$data['time_date']  = $_POST['time_date'];
		$data['time_time']  = $_POST['time_time'].":00";
		
		$this->session->set_userdata('posting', $data);

		$this->form_validation->set_rules('time_date','Tanggal','required');
		$this->form_validation->set_rules('time_time','Jam','required');
		if($this->form_validation->run())
		{
			$this->load->model('food_serving_time_model');

			$explode           = explode("-", $data['time_date']);
			$data['time_date'] = $explode[2]."-".$explode[1]."-".$explode[0];

			//die("SELECT * FROM bca_food_serving_time WHERE `date` = '".$data['time_date']."'");
			$exist = $this->food_serving_time_model->data_exist($data['time_date'])->num_rows();
			if($exist > 0)
			{
				$error     = 1;
				$error_msg = "Tanggal sudah ada dalam database.";
				$redirect  = "food_serving_time/add_form";
			}

			if($error == 0)
			{
				$this->food_serving_time_model->insertData($data['time_date'], $data['time_time'], $data['time_notes']);
			
				$this->cms_log_activity("insert", "", $this->menu_log.":<br />".$data['time_date']." Jam: ".$data['time_time']);

				$error     = 0;
				$error_msg = "Jam Tapping telah ditambahkan.";
			}

			$newdata = array('msg_stime_list' => $error_msg, 'err_stime_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal menambahkan Aktivasi Penyajian Makanan.";
			$newdata   = array('msg_stime_list' => $error_msg, 'err_stime_list' => $error);
			$this->session->set_userdata($newdata);
			$this->add_form();
		}
    }

    public function edit_data()
    {
		$error                = 0;
		$error_msg            = "";
		$redirect             = "food_serving_time";
		$data['time_id']    = $_POST['time_id'];
		$data['time_notes'] = $_POST['time_notes'];
		$data['time_date']  = $_POST['time_date'];
		$data['time_time']  = $_POST['time_time'].":00";

		$this->load->model('food_serving_time_model');
        $data['time'] = $this->food_serving_time_model->active_data($data['time_id'])->row();
        $this->session->set_userdata('editing', $data);

		$this->form_validation->set_rules('time_date','Tanggal','required');
		$this->form_validation->set_rules('time_time','Jam','required');
		if($this->form_validation->run())
		{

			$explode           = explode("-", $data['time_date']);
			$data['time_date'] = $explode[2]."-".$explode[1]."-".$explode[0];
			// die("SELECT * FROM bca_food_serving_time WHERE `date` = '".$date."' and id != ".$data['time_id']);
			$exist = $this->food_serving_time_model->data_exist($data['time_date'], $data['time_id'])->num_rows();
			if($exist > 0)
			{
				$error     = 1;
				$error_msg = "Tanggal sudah ada dalam database.";
				$redirect  = "food_serving_time/edit_form/".$data['id'];
			}

			if($error == 0)
			{
				$this->food_serving_time_model->updateData($data['time_id'], $data['time_date'], $data['time_time'], $data['time_notes']);
			
				$log_data_old = "<b>Jam Tapping</b><br />".
								"<b>Keterangan</b>: ".$this->session->userdata('editing')['time']->notes."<br /> ".
								"<b>Tanggal</b>: ".$this->session->userdata('editing')['time']->date."<br />".
								"<b>Jam</b>: ".$this->session->userdata('editing')['time']->time_start;

				$log_data_new = "<b>Jam Tapping</b><br />".
								"<b>Keterangan</b>: ".$data['time_notes']."<br /> ".
								"<b>Tanggal</b>: ".$_POST['time_date']."<br />".
								"<b>Jam</b>: ".$data['time_time'];

				$this->cms_log_activity("edit", $log_data_old, $log_data_new);

				$error     = 0;
				$error_msg = "Jam Tapping berhasil diubah.";
			}

			$this->session->unset_userdata('editing');
			$newdata = array('msg_stime_list' => $error_msg, 'err_stime_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal mengubah Jam Tapping.";
			$newdata   = array('msg_stime_list' => $error_msg, 'err_stime_list' => $error);
			$this->session->unset_userdata('editing');
			$this->session->set_userdata($newdata);
			$this->edit_form($data['time_id']);
		}
    }

    public function remove_data()
	{
		// $id = $this->uri->segment(3);
		$id = $_POST['id'];

		$this->load->model('food_serving_time_model');
		$this->food_serving_time_model->deleteData($id);

		$data = $_POST['data'];

		$this->cms_log_activity("delete", $this->menu_log.":<br />tanggal".$data);
		echo "Jam Tapping tanggal ".$data." telah dihapus.";

		$error = 0;
		$error_msg = "Jam Tapping tanggal ".$data." telah dihapus.";
		$newdata = array('msg_stime_list' => $error_msg, 'err_stime_list' => $error);
		$this->session->set_userdata($newdata);
	}
}

?>