<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Buffet_detail extends BCA_Controller {

	public $menu_log = "Detail Buffet";

	public function index()
	{
		$this->load->model('buffet_detail_model');
		// $data['b_details'] = $this->buffet_detail_model->buffet_detail_list()->result();
		$data['buffets'] = $this->buffet_detail_model->canteen_buffet()->result();

		$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 10)->row();

		$this->load->model('menu_priviledge_model');
    	$data['menu'] = $this->menu_priviledge_model->menu_priv_data(10)->row();
		$this->load->view('buffet_detail/view', $data);
	}

	public function add_form()
    {
    	$this->load->model('buffet_detail_model');
    	$data['buffets'] = $this->buffet_detail_model->canteen_buffet()->result();
    	
    	$this->load->model('menu_priviledge_model');
    	$data['menu'] = $this->menu_priviledge_model->menu_priv_data(10)->row();
    	
        $this->load->view('buffet_detail/add_form', $data);
    }
    
    public function edit_form()
    {
        $data['c_id'] = $this->uri->segment(3);
        $data['buffet_name'] = $this->uri->segment(4);

        $this->load->model('menu_priviledge_model');
    	$data['menu'] = $this->menu_priviledge_model->menu_priv_data(10)->row();

    	$this->load->model('login_model');
		$data['crud'] = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 5)->row();

        $this->load->model('buffet_detail_model');
        $data['b_details'] = $this->buffet_detail_model->buffet_detail_data($data['c_id'])->result();
    	// $data['buffets'] = $this->buffet_detail_model->canteen_buffet()->result();

        $this->load->view('buffet_detail/edit_form', $data);
    }

  //   public function new_data()
  //   {
		// $error             = 0;
		// $error_msg         = "";
		// $redirect          = "buffet_detail";
		// $data['c_id']      = $_POST['c_id'];
		// //$data['item_name'] = $_POST['item_name'];

		// $this->form_validation->set_rules('c_id','Nama Buffet','required');
		// if($this->form_validation->run())
		// {
		// 	$this->load->model('buffet_detail_model');

		// 	if($error == 0)
		// 	{
		// 		foreach ($_POST['item_name'] as $item_name) {
		// 			$this->buffet_detail_model->insertData($data['c_id'], $item_name);
		// 		}
			
		// 		$error     = 0;
		// 		$error_msg = "Makanan Buffet telah ditambahkan.";
		// 	}

		// 	$newdata = array('msg_buffet_list' => $error_msg, 'err_buffet_list' => $error);
		// 	$this->session->set_userdata($newdata);
		// 	redirect($redirect);
		// }
		// else
		// {
		// 	$error     = 1;
		// 	$error_msg = "Gagal menambahkan Makanan Buffet.";
		// 	$newdata   = array('msg_buffet_list' => $error_msg, 'err_buffet_list' => $error);
		// 	$this->session->set_userdata($newdata);
		// 	$this->add_form();
		// }
  //   }

    public function new_data()
    {
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;

    	$buffet_name  = $this->uri->segment(3);
		$data['c_id'] = $_POST['c_id'];
		$redirect     = "buffet_detail/edit_form/".$data['c_id']."/".$buffet_name;
		$insert_count = count($_POST['bItem']);
		$update_count = count($_POST['bItem_id']);

        $this->load->model('buffet_detail_model');
        for($i=0; $i<$insert_count; $i++)
        {
            $data['bItem'] = $_POST['bItem'][$i];
            
            $this->buffet_detail_model->insertData($data['c_id'], $data['bItem']);
        }

        for($i=0; $i<$update_count; $i++)
        {
			$update['id']           = $_POST['bItem_id'][$i];
			$update['bItem_update'] = $_POST['bItem_update'][$i];
           
            $this->buffet_detail_model->updateData($update['id'], $data['c_id'], $update['bItem_update']);
            $this->cms_log_activity("insert", $this->menu_log.": ".$update['bItem_update']);
        }

        $error_msg = "Buffet Detil telah ditambahkan/diedit.";
        $newdata = array('msg_buffet_list' => $error_msg, 'err_buffet_list' => $error);
        $this->session->set_userdata($newdata);
        redirect($redirect);
    }

  //   public function edit_data()
  //   {
		// $error             = 0;
		// $error_msg         = "";
		// $redirect          = "buffet_detail";
		// $data['id']		   = $_POST['id'];
		// $data['c_id']      = $_POST['c_id'];
		// $data['item_name'] = $_POST['item_name'];

		// $this->form_validation->set_rules('c_id','Nama Buffet','required');
		// $this->form_validation->set_rules('item_name','Nama Makanan','required');
		// if($this->form_validation->run())
		// {
		// 	$this->load->model('buffet_detail_model');

		// 	if($error == 0)
		// 	{
		// 		$this->buffet_detail_model->updateData($data['id'], $data['c_id'], $data['item_name']);
			
		// 		$error     = 0;
		// 		$error_msg = "Makanan Buffet berhasil diubah.";			}

		// 	$newdata = array('msg_buffet_list' => $error_msg, 'err_buffet_list' => $error);
		// 	$this->session->set_userdata($newdata);
		// 	redirect($redirect);
		// }
		// else
		// {
		// 	$error     = 1;
		// 	$error_msg = "Gagal mengubah Makanan Buffet.";
		// 	$newdata   = array('msg_buffet_list' => $error_msg, 'err_buffet_list' => $error);
		// 	$this->session->set_userdata($newdata);
		// 	$this->edit_form();
		// }
  //   }

    public function remove_data()
	{
		$id = $this->uri->segment(3);

		$c_id = $this->uri->segment(4);
        $buffet_name = $this->uri->segment(5);
        $item_name = $this->uri->segment(6);
		
		$this->load->model('buffet_detail_model');
		$this->buffet_detail_model->deleteData($id);

		$this->cms_log_activity("delete", $this->menu_log.": ".$item_name);

		$error = 0;
		$error_msg = "Makanan Buffet telah dihapus.";
		$newdata = array('msg_buffet_list' => $error_msg, 'err_buffet_list' => $error);
		$this->session->set_userdata($newdata);
		
		redirect('buffet_detail/edit_form/'.$c_id.'/'.$buffet_name);
	}
}

?>