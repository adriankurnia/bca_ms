<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends BCA_Controller {

	public $menu_log = "Dashboard";

	public function index()
	{
        $this->load->model('general_model');
        $privilege_id = $this->session->userdata('group_priviledge');

        $this->data['getModuleList'] = $this->general_model->select_data2('bca_module_dashboard_tb','bca_group_privilege_id',$privilege_id);
		$this->load->view('dashboard', $this->data);
	}
}

?>
