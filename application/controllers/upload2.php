<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends BCA_Controller {

	public function index()
	{
		$this->load->view('upload/view');
	}

	public function upload_program_file()
	{
		$fulldate = date('Y-m-d H:i:s');
		$filename = $_FILES['updloadProgram']['name'];

		$this->load->library('excel');
		$inputFileName = UPLOAD_PATH.$filename;
		$inputFileType = 'Excel2007';
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader     = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setDelimiter(";"); 
			$objPHPExcel   = $objReader->load($inputFileName);
		} catch (Exception $e) {
			die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
			. '": ' . $e->getMessage());
		}

		$sheet         = $objPHPExcel->getSheet(0);
		$highestRow    = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();
		
		$this->load->model('upload_model');

		for($i = 1; $i<=$highestRow; $i++)
		{
			$program_id    = trim($sheet->getCell('A'.$i));
			$program       = trim($sheet->getCell('I'.$i));
			$subprogram    = trim($sheet->getCell('G'.$i));
			$training      = trim($sheet->getCell('E'.$i));
			$program_begin = explode(".", $sheet->getCell('B'.$i));
			$program_end   = explode(".", $sheet->getCell('C'.$i));
			
			$program_begin = $program_begin[2]."-".$program_begin[1]."-".$program_begin[0];
			$program_end   = $program_end[2]."-".$program_end[1]."-".$program_end[0];
			
			$exist = $this->upload_model->data_exist("bca_program", "p_id", $program_id)->row();
			if(!empty($exist))
			{
				$query = "UPDATE bca_program SET program_name = '".$program."', subprogram_name = '".$subprogram."', training_name = '".$training."', program_begin = '".$program_begin."', program_end = '".$program_end."', edited_date = '".$fulldate."', edited_by = '".$this->session->userdata('username')."' WHERE p_id = '".$program_id."'";
			}
			else
			{
				$query = "INSERT INTO bca_program (p_id, program_name, subprogram_name, training_name, program_begin, program_end, created_date, created_by) VALUES (".$program_id.", '".$program."', '".$subprogram."', '".$training."','".$program_begin."', '".$program_end."', '".$fulldate."','".$this->session->userdata('username')."')";
			}
			$this->db->query($query);
		}

		$error     = 0;
		$error_msg = "Nama Program telah ditambahkan.";
		$newdata   = array('msg_upload_list' => $error_msg, 'err_upload_list' => $error);
		$this->session->set_userdata($newdata);
		redirect("upload");
	}

	public function upload_participant_file()
	{
		$fulldate = date('Y-m-d H:i:s');
		$filename = $_FILES['updloadParticipant']['name'];

		$this->load->library('excel');
		$inputFileName = UPLOAD_PATH.$filename;
		$inputFileType = 'Excel2007';
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader     = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setDelimiter(";"); 
			$objPHPExcel   = $objReader->load($inputFileName);
		} catch (Exception $e) {
			die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
			. '": ' . $e->getMessage());
		}

		$sheet         = $objPHPExcel->getSheet(0);
		$highestRow    = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();
		
		$this->load->model('upload_model');

		for($i = 1; $i<=$highestRow; $i++)
		{
			$e_nip       = trim($this->String_Serialize($sheet->getCell('D'.$i)));
			if(is_numeric($e_nip))
			{
				$nol = "";
				$len = strlen($e_nip);

				if($len < 8)
				{
					for($x=0; $x<8-$len; $x++) 
					{
						$nol .= "0";
					}
					$e_nip = $nol.$e_nip;
				}
			}
			$e_program     = trim($sheet->getCell('A'.$i));
			$program_begin = explode(".", $sheet->getCell('B'.$i));
			$program_end   = explode(".", $sheet->getCell('C'.$i));
			
			$program_begin = $program_begin[2]."-".$program_begin[1]."-".$program_begin[0];
			$program_end   = $program_end[2]."-".$program_end[1]."-".$program_end[0];
			
			$query1 = "INSERT INTO bca_course_participant (co_id, co_start, co_end, e_nip) VALUES ('".$e_program."', '".$program_begin."', '".$program_end."', '".$e_nip."')";
			
			$this->db->query($query1);
		}

		$error     = 0;
		$error_msg = "Perticipant telah ditambahkan.";
		$newdata   = array('msg_upload_list' => $error_msg, 'err_upload_list' => $error);
		$this->session->set_userdata($newdata);
		redirect("upload");
	}

	public function upload_employee_file()
	{
		$fulldate = date('Y-m-d H:i:s');
		$filename = $_FILES['updloadEmployee']['name'];

		$this->load->library('excel');
		$inputFileName = UPLOAD_PATH.$filename;
		$inputFileType = 'Excel2007';
		die($inputFileName);
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader     = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setDelimiter(";"); 
			$objPHPExcel   = $objReader->load($inputFileName);
		} catch (Exception $e) {
			die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
			. '": ' . $e->getMessage());
		}

		$sheet         = $objPHPExcel->getSheet(0);
		$highestRow    = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();

		$this->load->model('upload_model');

		for($i = 1; $i<=$highestRow; $i++)
		{
			$e_nip        = trim($sheet->getCell('A'.$i));
			if(is_numeric($e_nip))
			{
				$nol = "";
				$len = strlen($e_nip);

				if($len < 8)
				{
					for($x=0; $x<8-$len; $x++) 
					{
						$nol .= "0";
					}
					$e_nip = $nol.$e_nip;
				}
			}

			$e_name      = trim($this->String_Serialize($sheet->getCell('B'.$i)));
			$e_eselon    = trim($this->String_Serialize($sheet->getCell('C'.$i)));
			$e_divisi    = trim($this->String_Serialize(substr($sheet->getCell('D'.$i),1)));
			$e_wilayah   = trim($this->String_Serialize($sheet->getCell('E'.$i)));
			$e_work_unit = trim($this->String_Serialize($sheet->getCell('F'.$i)));
			$e_gender    = trim($this->String_Serialize($sheet->getCell('G'.$i)));
			$e_birthdate = explode(".", $sheet->getCell('H'.$i));

			$full_birthdate  = $e_birthdate[2]."-".$e_birthdate[1]."-".$e_birthdate[0];
			
			$exist = $this->upload_model->data_exist("bca_employee", "e_nip", $e_nip)->row();
			if(!empty($exist))
			{
				$query1 = "UPDATE bca_employee SET e_name = '".$e_name."', e_work_unit = '".$e_work_unit."', e_eselon = '".$e_eselon."', e_gender = '".$e_gender."', e_birthdate = '".$full_birthdate."', edited_date = '".$fulldate."', edited_by = '".$this->session->userdata('username')."', e_divisi = '".$e_divisi."', e_wilayah = '".$e_wilayah."' WHERE e_nip = '".$e_nip."'";
			}
			else
			{
				$query1 = "INSERT INTO bca_employee (e_nip, e_name, e_work_unit, e_eselon, e_gender, e_birthdate, created_date, created_by, e_divisi, e_wilayah) VALUES ('".$e_nip."', '".$e_name."', '".$e_work_unit."', '".$e_eselon."', '".$e_gender."','".$full_birthdate."', '".$fulldate."','".$this->session->userdata('username')."', '".$e_divisi."', '".$e_wilayah."')";
			}
			$this->db->query($query1);
		}

		$error     = 0;
		$error_msg = "Karyawan telah ditambahkan.";
		$newdata   = array('msg_upload_list' => $error_msg, 'err_upload_list' => $error);
		$this->session->set_userdata($newdata);
		redirect("upload");
	}

	public function upload_rfid_file()
	{
		$fulldate = date('Y-m-d H:i:s');
		$filename = $_FILES['updloadRfid']['name'];

		$this->load->library('excel');
		$inputFileName = UPLOAD_PATH.$filename;
		$inputFileType = 'Excel2007';
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader     = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setDelimiter(","); 
			$objPHPExcel   = $objReader->load($inputFileName);
		} catch (Exception $e) {
			die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
			. '": ' . $e->getMessage());
		}

		$sheet         = $objPHPExcel->getSheet(0);
		$highestRow    = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();
		
		for($i = 1; $i<=$highestRow; $i++)
		{
			$e_nip        = trim($sheet->getCell('A'.$i));
			if(is_numeric($e_nip))
			{
				$nol = "";
				$len = strlen($e_nip);

				if($len < 8)
				{
					for($x=0; $x<8-$len; $x++) 
					{
						$nol .= "0";
					}
					$e_nip = $nol.$e_nip;
				}
			}

			$e_name       = addslashes($sheet->getCell('B'.$i));
			$e_rfid       = trim($sheet->getCell('C'.$i));
			
			$nol = "";
			$len = strlen($e_rfid);

			if($len < 10)
			{
				for($x=0; $x<10-$len; $x++) 
				{
					$nol .= "0";
				}
				$e_rfid = $nol.$e_rfid;
			}

			$card_issued  = explode(" ", $sheet->getCell('D'.$i));
			$card_expired = explode(" ", $sheet->getCell('E'.$i));

			$card_issued  = $card_issued[0];
			$card_expired = $card_expired[0];
			
			if($e_rfid != NULL || $e_rfid != "" || empty($e_rfid))
			{
				$query = "UPDATE bca_employee SET e_name = '".$e_name."',e_rfid = '".$e_rfid."', card_issued = '".$card_issued."', card_expired = '".$card_expired."', created_date = '".$fulldate."' WHERE e_nip = '".$e_nip."'";
				$this->db->query($query);
			}
		}

		$error     = 0;
		$error_msg = "Data RFID telah ditambahkan.";
		$newdata   = array('msg_upload_list' => $error_msg, 'err_upload_list' => $error);
		$this->session->set_userdata($newdata);
		redirect("upload");
	}

	

}