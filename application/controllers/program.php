<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Program extends BCA_Controller {

	public $menu_log = "Program";

	public function index()
	{
		$this->load->model('program_model');
		$data['programs'] = $this->program_model->program_list()->result();

		$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 8)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(8)->row();

        $this->session->unset_userdata('posting');
        $this->session->unset_userdata('editing');
		$this->load->view('program/view', $data);
	}

	public function add_form()
    {
    	$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(8)->row();
        $this->load->view('program/add_form', $data);
    }
    
    public function edit_form($uid = null)
    {
    	$this->session->unset_userdata('editing');

		if($uid == null) {
        	$id = $this->uri->segment(3);
    	} else {
    		$id = $uid;
    	}

        $this->load->model('program_model');
        $data['program'] = $this->program_model->program_data($id)->row();
        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(8)->row();

        $this->load->view('program/edit_form', $data);
    }

    public function display()
    {
        $id = $this->uri->segment(3);
        $this->load->model('program_model');
        $data['program'] = $this->program_model->program_data($id)->row();
        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(8)->row();

        $this->load->view('program/display', $data);
    }

    public function new_data()
    {
    	// print_r($_POST);die;
		$error                   = 0;
		$error_msg               = "";
		$redirect                = "program";
		$data['program_name']    = $_POST['program_name'];
		// $data['subprogram_name'] = $_POST['subprogram_name'];
		// $data['training_name']   = $_POST['training_name'];
		// $data['program_begin']   = $_POST['program_begin'];
		// $data['program_end']     = $_POST['program_end'];

		$this->session->set_userdata('posting', $data);

		$this->form_validation->set_rules('program_name','Nama Program','required');
		// $this->form_validation->set_rules('subprogram_name','Nama Subprogram','required');
		// $this->form_validation->set_rules('program_begin','Program Mulai','required');
		// $this->form_validation->set_rules('program_end','Program Selesai','required');
		if($this->form_validation->run())
		{
			$this->load->model('program_model');

			if($error == 0)
			{
				$explode               = explode("-", $data['program_begin']);
				$data['program_begin'] = $explode[2]."-".$explode[1]."-".$explode[0];
				
				$explode2              = explode("-", $data['program_end']);
				$data['program_end']   = $explode2[2]."-".$explode2[1]."-".$explode2[0];

				$this->program_model->insertData($data['program_name'], "", "", "", "");
				
				$this->cms_log_activity("insert", "", "Program: ".$data['program_name']);

				$error     = 0;
				$error_msg = "Nama Program telah ditambahkan.";
			}

			$newdata = array('msg_program_list' => $error_msg, 'err_program_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal menambahkan Nama Program.";
			$newdata   = array('msg_program_list' => $error_msg, 'err_program_list' => $error);
			$this->session->set_userdata($newdata);
			$this->add_form();
		}
    }

    public function edit_data()
    {
		$error                   = 0;
		$error_msg               = "";
		$redirect                = "program";
		$data['program_id']      = $_POST['program_id'];
		$data['program_name']    = $_POST['program_name'];
		// $data['subprogram_name'] = $_POST['subprogram_name'];
		// $data['program_begin']   = $_POST['program_begin'];
		// $data['program_end']     = $_POST['program_end'];

		$this->load->model('program_model');
        $data['program'] = $this->program_model->program_data($data['program_id'])->row();
        $this->session->set_userdata('editing', $data);

		$this->form_validation->set_rules('program_name','Nama Program','required');
		// $this->form_validation->set_rules('subprogram_name','Nama Subprogram','required');
		// $this->form_validation->set_rules('program_begin','Program Mulai','required');
		// $this->form_validation->set_rules('program_end','Program Selesai','required');
		if($this->form_validation->run())
		{
			$old = $this->program_model->program_data($data['program_id'])->row();
			if($error == 0)
			{
				$explode               = explode("-", $data['program_begin']);
				$data['program_begin'] = $explode[2]."-".$explode[1]."-".$explode[0];
				
				$explode2              = explode("-", $data['program_end']);
				$data['program_end']   = $explode2[2]."-".$explode2[1]."-".$explode2[0];

				$this->program_model->updateData($data['program_id'], $data['program_name'], "", "", "", "");
			
				$this->load->model('program_model');
				$old = $this->program_model->program_data($this->session->userdata('editing')['program']->p_id)->row();

				$log_data_old = "<b>Program</b>: ".$this->session->userdata('editing')['program']->program_name;
								// "<b>Sub Program</b>: ".$this->session->userdata('editing')['program']->subprogram_name."<br />".
								// "<b>Pelatihan</b>: ".$this->session->userdata('editing')['program']->training_name."<br />".
								// "<b>Program Mulai</b>: ".$this->session->userdata('editing')['program']->program_begin."<br />".
								// "<b>Program Selesai</b>: ".$this->session->userdata('editing')['program']->program_end;

				$new = $this->program_model->program_data($data['program_id'])->row();
				$log_data_new = "<b>Program</b>: ".$data['program_name'];
								// "<b>Sub Program</b>: ".$data['subprogram_name']."<br />".
								// "<b>Pelatihan</b>: ".$data['training_name']."<br />".
								// "<b>Program Mulai</b>: ".$data['program_begin']."<br />".
								// "<b>Program Selesai</b>: ".$data['program_end'];

				$this->cms_log_activity("edit", $log_data_old, $log_data_new);

				$error     = 0;
				$error_msg = "Nama Program berhasil diubah.";
			}

			$this->session->unset_userdata('editing');
			$newdata = array('msg_program_list' => $error_msg, 'err_program_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal mengubah Nama Program.";
			$newdata   = array('msg_program_list' => $error_msg, 'err_program_list' => $error);
			$this->session->unset_userdata('editing');
			$this->session->set_userdata($newdata);
			$this->edit_form($data['program_id']);
		}
    }

    public function remove_data()
	{
		//$id = $this->uri->segment(3);
		$id = $_POST['id'];

		$this->load->model('program_model');
		$this->program_model->deleteData($id);

		//$data = urldecode($this->uri->segment(4));
		$data = $_POST['data'];
		$this->cms_log_activity("delete", $this->menu_log.": ".$data);
		echo $data." telah dihapus.";

		$error = 0;
		$error_msg = $data." telah dihapus.";
		$newdata = array('msg_program_list' => $error_msg, 'err_program_list' => $error);
		$this->session->set_userdata($newdata);
		
		// redirect('program');
	}
	
	public function popup_add_form()
    {
    	$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(8)->row();
        $this->load->view('program/popup_add_form', $data);
    }
}

?>