<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Food_serving_buffet extends BCA_Controller {

	public $menu_log = "Buffet Only";

	public function index()
	{
		$this->load->model('food_serving_buffet_model');
		$data['events'] = $this->food_serving_buffet_model->event_list()->result();

		$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 31)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(31)->row();

        $this->session->unset_userdata('posting');
        $this->session->unset_userdata('editing');
		$this->load->view('food_serving_buffet/view', $data);
	}

	public function add_form()
    {
    	$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(31)->row();
        $this->load->view('food_serving_buffet/add_form', $data);
    }
    
    public function edit_form($uid = null)
    {
    	$this->session->unset_userdata('editing');

        if($uid == null) {
            $id = $this->uri->segment(3);
        } else {
            $id = $uid;
        }

        $this->load->model('food_serving_buffet_model');
        $data['event'] = $this->food_serving_buffet_model->event_data($id)->row();
        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(31)->row();

        $this->load->view('food_serving_buffet/edit_form', $data);
    }

    public function display()
    {
        $id = $this->uri->segment(3);
        $this->load->model('food_serving_buffet_model');
        $data['event'] = $this->food_serving_buffet_model->event_data($id)->row();
        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(31)->row();

        $this->load->view('food_serving_buffet/display', $data);
    }

    public function new_data()
    {
		$error               = 0;
		$error_msg           = "";
		$redirect            = "food_serving_buffet";
		$data['event_name']  = $_POST['event_name'];
		$data['event_start'] = $_POST['event_start'];
		$data['event_end']   = $_POST['event_end'];

		$this->session->set_userdata('posting', $data);
		
		$this->form_validation->set_rules('event_start','Tgl Awa','required');
		$this->form_validation->set_rules('event_end','Tgl Akhir','required');
		if($this->form_validation->run())
		{
			$this->load->model('food_serving_buffet_model');

			$explode               = explode("-", $data['event_start']);
			$data['event_start'] = $explode[2]."-".$explode[1]."-".$explode[0];

			$explode               = explode("-", $data['event_end']);
			$data['event_end']   = $explode[2]."-".$explode[1]."-".$explode[0];

			if(strtotime($data['event_start']) > strtotime($data['event_end']))
			{
				$error     = 1;
				$error_msg = "Tanggal awal lebih besar dari tanggal akhir.";
				$redirect  = "food_serving_buffet/add_form";
			}

			if($error == 0)
			{
				$this->food_serving_buffet_model->insertData($data['event_name'], $data['event_start'], $data['event_end']);
				if($data['serving_status'] == 1) {
					$status = "Aktif";
				} else {
					$status = "Tidak Aktif";
				}
				$this->cms_log_activity("insert", "", $this->menu_log.":<br />".$data['event_start']." s.d ".$data['event_end']);

				$error     = 0;
				$error_msg = "Buffet Only telah ditambahkan.";
			}

			$newdata = array('msg_event_list' => $error_msg, 'err_event_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal menambahkan Buffet Only.";
			$newdata   = array('msg_event_list' => $error_msg, 'err_event_list' => $error);
			$this->session->set_userdata($newdata);
			$this->add_form();
		}
    }

    public function edit_data()
    {
		$error               = 0;
		$error_msg           = "";
		$redirect            = "food_serving_buffet";
		$data['event_id']    = $_POST['event_id'];
		$data['event_name']  = $_POST['event_name'];
		$data['event_start'] = $_POST['event_start'];
		$data['event_end']   = $_POST['event_end'];

		$this->load->model('food_serving_buffet_model');
        $data['event'] = $this->food_serving_buffet_model->event_data($data['event_id'])->row();
        $this->session->set_userdata('editing', $data);

		$this->form_validation->set_rules('event_start','Tgl Awal Libur','required');
		$this->form_validation->set_rules('event_end','Tgl Awal Libur','required');
		if($this->form_validation->run())
		{
			$explode               = explode("-", $data['event_start']);
			$data['event_start'] = $explode[2]."-".$explode[1]."-".$explode[0];

			$explode               = explode("-", $data['event_end']);
			$data['event_end']   = $explode[2]."-".$explode[1]."-".$explode[0];

			if(strtotime($data['event_start']) > strtotime($data['event_end']))
			{
				$error     = 1;
				$error_msg = "Tanggal awal lebih besar dari tanggal akhir.";
				$redirect  = "food_serving_buffet/edit_form/".$data['active_id'];
			}

			if($error == 0)
			{
				$this->food_serving_buffet_model->updateData($data['event_id'], $data['event_name'], $data['event_start'], $data['event_end']);

				$explode = explode("-", $this->session->userdata('editing')['event']->start);
                $oldStart = $explode[2]."-".$explode[1]."-".$explode[0];

                $explode = explode("-", $this->session->userdata('editing')['event']->end);
                $oldEnd = $explode[2]."-".$explode[1]."-".$explode[0];

                if($this->session->userdata('editing')['event']->status == 1) $status = "Aktif";
                else $status = "Tidak Aktif";

				$log_data_old = "<b>Setting Hari Libur</b><br />".
								"<b>Keterangan</b>: ".$this->session->userdata('editing')['event']->event_name."<br /> ".
								"<b>Tgl Awal</b>: ".$oldStart."<br />".
								"<b>Tgl Akhir</b>: ".$oldEnd."<br />";

				$log_data_new = "<b>Setting Hari Libur</b><br />".
								"<b>Keterangan</b>: ".$data['event_name']."<br /> ".
								"<b>Tgl Awal</b>: ".$_POST['event_start']."<br />".
								"<b>Tgl Akhir</b>: ".$_POST['event_end']."<br />";

				$this->cms_log_activity("edit", $log_data_old, $log_data_new);

				$error     = 0;
				$error_msg = "Buffet Only berhasil diubah.";
			}

			$this->session->unset_userdata('editing');
			$newdata = array('msg_event_list' => $error_msg, 'err_event_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal mengubah Buffet Only.";
			$newdata   = array('msg_event_list' => $error_msg, 'err_event_list' => $error);
			$this->session->unset_userdata('editing');
			$this->session->set_userdata($newdata);
			$this->edit_form($data['event_id']);
		}
    }

    public function remove_data()
	{
		// $id = $this->uri->segment(3);
		$id = $_POST['id'];

		$this->load->model('food_serving_buffet_model');
		$this->food_serving_buffet_model->deleteData($id);

		$data = $_POST['data'];
		$explode = explode("/", $data);

		$this->cms_log_activity("delete", $this->menu_log.":<br />tanggal".$explode[0]." s.d ".$explode[1]);
		echo "Buffet Only tanggal ".$explode[0]." s.d ".$explode[1]." telah dihapus.";

		$error = 0;
		$error_msg = "Buffet Only tanggal ".$explode[0]." s.d ".$explode[1]." telah dihapus.";
		$newdata = array('msg_event_list' => $error_msg, 'err_event_list' => $error);
		$this->session->set_userdata($newdata);
		
		// redirect('food_serving_buffet');
	}

	public function get_daily_stock_today()
	{
		$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 31)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(31)->row();
        
		$this->load->model('food_serving_buffet_model');
		$data['servings'] = $this->food_serving_buffet_model->daily_stock_today()->result();
		$this->load->view('food_serving_buffet/remove_view', $data);
	}

	public function remove_view()
	{
		// $id = $this->uri->segment(3);
		$id = $_POST['id'];
		
		$this->load->model('food_serving_buffet_model');
		$this->food_serving_buffet_model->deleteView($id);

		$data = $_POST['data'];

		$this->cms_log_activity("delete", "View tampilan tanggal".date("d-m-Y", time()));
		echo "Tampilan stok ".$data." telah dihapus.";

		$error = 0;
		$error_msg = "Tampilan stok ".$data." telah dihapus.";
		$newdata = array('msg_event_list' => $error_msg, 'err_event_list' => $error);
		$this->session->set_userdata($newdata);
		
		// redirect('food_serving_buffet');
	}
}

?>