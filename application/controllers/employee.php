<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee extends BCA_Controller {

	public $menu_log = "Karyawan";

	public function index()
	{
		// $this->load->model('employee_model');
		// $data['employees'] = $this->employee_model->employee_list()->result();

		$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 21)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(21)->row();

        $this->session->unset_userdata('editing');
		$this->load->view('employee/view', $data);
	}

	public function get_employee_data()
	{
		$id = $_POST['id'];
		$this->load->model('employee_model');
		$res = $this->employee_model->employee_data($id)->row();

		if($res)
		{
			$return = array('ok' => 1, 'data' => $res);
		}
		else
		{
			$return = array('ok' => 0);
		}
		echo json_encode($return);
	}

	public function new_attendance()
	{
		$nip = $_POST['nip'];
		$this->load->model('employee_model');
		$this->employee_model->insert_attendance($nip);
	}

	public function add_form()
    {
    	$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 21)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(21)->row();
        $this->load->view('employee/add_form');
    }
    
    public function edit_form($uid = null)
    {
    	$this->session->unset_userdata('editing');

		if($uid == null) {
        	$id = $this->uri->segment(3);
    	} else {
    		$id = $uid;
    	}

        $this->load->model('employee_model');
        $data['employee'] = $this->employee_model->employee_data($id)->row();

        $this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 21)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(21)->row();

        $this->session->set_userdata('editing', $data);
        $this->load->view('employee/edit_form', $data);
    }

    public function display()
    {
        $id = $this->uri->segment(3);
        $this->load->model('employee_model');
        $data['employee'] = $this->employee_model->employee_data($id)->row();

        $this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 21)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(21)->row();

        $this->load->view('employee/display', $data);
    }

    public function new_employee()
    {
		$error                      = 0;
		$error_msg                  = "";
		$redirect                   = "employee";
		$data['employee_nip']       = $_POST['employee_nip'];
		$data['employee_name']      = $_POST['employee_name'];
		$data['employee_work_unit'] = $_POST['employee_work_unit'];
		$data['employee_rfid']      = $this->String_Serialize($_POST['employee_rfid']);
		$data['employee_birthdate'] = $_POST['employee_birthdate'];
		$data['employee_expired']   = $_POST['employee_expired'];

		$this->form_validation->set_rules('employee_nip','NIP','required');
		$this->form_validation->set_rules('employee_name','Nama','required');
		$this->form_validation->set_rules('employee_work_unit','Unit Kerja','required');
		$this->form_validation->set_rules('employee_rfid','RFID','required');
		$this->form_validation->set_rules('employee_birthdate','Tgl Lahir','required');
		$this->form_validation->set_rules('employee_expired','Kadaluarsa Kartu','required');

		if($this->form_validation->run())
		{
			$this->load->model('employee_model');
			$nip_exist = $this->employee_model->data_exist("e_nip", $data['employee_nip'], "add");
			if($nip_exist > 0)
			{
				$error = 1;
				$error_msg = "NIP sudah terdaftar pada database.";
				$redirect = "employee/add_form";
			}

			$rfid_exist = $this->employee_model->data_exist("e_rfid", $data['employee_rfid'], "add");
			if($rfid_exist > 0)
			{
				$error = 1;
				$error_msg = "RFID sudah terdaftar pada daabase.";
				$redirect = "employee/add_form";
			}

			if($error == 0)
			{
				$explode                    = explode("-", $data['employee_birthdate']);
				$data['employee_birthdate'] = $explode[2]."-".$explode[1]."-".$explode[0];

				$explode                  = explode("-", $data['employee_expired']);
				$data['employee_expired'] = $explode[2]."-".$explode[1]."-".$explode[0];
				$this->employee_model->insert_employee($data['employee_nip'], $data['employee_name'], $data['employee_rfid'], $data['employee_birthdate'], $data['employee_work_unit'], $data['employee_expired']);

				$this->cms_log_activity("insert", "", $this->menu_log.": ".$data['employee_name']);
			
				$error     = 0;
				$error_msg = "Karyawan telah ditambahkan.";
			}

			$newdata = array('msg_employee_list' => $error_msg, 'err_employee_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal menambahkan karyawan.";
			$newdata   = array('msg_employee_list' => $error_msg, 'err_employee_list' => $error);
			$this->session->set_userdata($newdata);
			$this->add_form();
		}
    }

    public function edit_employee()
    {
    	//die(print_r($_POST));
		$error                      = 0;
		$error_msg                  = "";
		$redirect                   = "employee";
		$data['employee_nip']       = $_POST['employee_nip'];
		$data['employee_name']      = $_POST['employee_name'];
		$data['employee_work_unit'] = $_POST['employee_work_unit'];
		$data['employee_rfid']      = $this->String_Serialize($_POST['employee_rfid']);
		// $data['employee_birthdate'] = $_POST['employee_birthdate'];
		$data['employee_expired']   = $_POST['employee_expired'];

		$this->load->model('employee_model');
        $data['employee'] = $this->employee_model->employee_data($data['employee_nip'])->row();
        $this->session->set_userdata('editing', $data);

		$this->form_validation->set_rules('employee_nip','NIP','required');
		$this->form_validation->set_rules('employee_name','Nama','required');
		$this->form_validation->set_rules('employee_work_unit','Unit Kerja','required');
		$this->form_validation->set_rules('employee_rfid','RFID','required');
		// $this->form_validation->set_rules('employee_birthdate','Tgl Lahir','required');
		$this->form_validation->set_rules('employee_expired','Kadaluarsa Kartu','required');

		if($this->form_validation->run())
		{
			$old = $this->employee_model->employee_data($data['employee_nip'])->row();

			$rfid_exist = $this->employee_model->data_exist("e_nip", $data['employee_rfid'], "edit", $data['employee_nip']);
			if($rfid_exist > 0)
			{
				$error = 1;
				$error_msg = "RFID sudah terdaftar pada database.";
				$redirect = "employee/edit_form/".$data['employee_nip'];
			}

			if($error == 0)
			{
				// $explode                    = explode("-", $data['employee_birthdate']);
				// $data['employee_birthdate'] = $explode[2]."-".$explode[1]."-".$explode[0];

				$explode                    = explode("-", $data['employee_expired']);
				$data['employee_expired'] = $explode[2]."-".$explode[1]."-".$explode[0];

				$this->employee_model->update_employee($data['employee_nip'], $data['employee_name'], $data['employee_rfid'], $data['employee_work_unit'], $data['employee_expired']);

				$log_data_old = "<b>Karyawan</b><br />".
								"<b>Nama</b>: ".$this->session->userdata('editing')['employee']->e_name."<br /> ".
								"<b>Unit Kerja</b>: ".$old->program_name."<br />".
								"<b>Kadaluarsa Kartu</b>: ".$this->session->userdata('editing')['employee']->card_expired."<br />".
								"<b>RFID</b>: ".$this->session->userdata('editing')['employee']->e_rfid;

				$log_data_new = "<b>Karyawan</b><br />".
								"<b>Nama</b>: ".$data['employee_name']."<br /> ".
								"<b>Unit Kerja</b>: ".$data['employee_work_unit']."<br />".
								"<b>Kadaluarsa Kartu</b>: ".$data['employee_expired']."<br />".
								"<b>RFID</b>: ".$data['employee_rfid'];

				$this->cms_log_activity("edit", $log_data_old, $log_data_new);
			
				$error     = 0;
				$error_msg = "Karyawan berhasil diubah.";
			}

			$this->session->unset_userdata('editing');
			$newdata = array('msg_employee_list' => $error_msg, 'err_employee_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal mengubah karyawan.";
			$newdata   = array('msg_employee_list' => $error_msg, 'err_employee_list' => $error);
			$this->session->unset_userdata('editing');
			$this->session->set_userdata($newdata);
			$this->edit_form($data['employee_nip']);
		}
    }

    public function remove_employee()
	{
		// $nip = $this->uri->segment(3);
		// $data = urldecode($this->uri->segment(4));
		$nip = $_POST['id'];
		$data = $_POST['data'];
		$this->load->model('employee_model');
		$this->employee_model->delete_employee($nip);

		$this->cms_log_activity("delete", $this->menu_log.": ".$data);
		echo $data." telah dihapus.";
		// $error = 0;
		// $error_msg = "Karyawan telah dihapus.";
		// $newdata = array('msg_employee_list' => $error_msg, 'err_employee_list' => $error);
		// $this->session->set_userdata($newdata);
		
		// redirect('employee');
	}

	public function getData()
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        $this->load->library('Serverside');
        // DB table to use
        $table = 'bca_employee';
         
        // Table's primary key
        $primaryKey = 'e_nip';
         
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
                        array(
                            'db' => 'e_nip',
                            'dt' => 'e_nip',
                            'formatter' => function( $d ) {
                                return '<a href="employee/display/'.$d.'">'.$d.'</a>';
                            }
                        ),
                        array(
                            'db' => 'e_nip',
                            'dt' => 'ACTION',
                            'formatter' => function( $d ) {
                                return '<a class="icmn-pencil" href="employee/edit_form/'.$d.'">';
                            }
                        ),
                        array('db' => 'e_name', 'dt' => 'e_name'),
                        array('db' => 'e_rfid', 'dt' => 'e_rfid'),
                        array('db' => 'e_divisi', 'dt' => 'e_divisi'),
                        array('db' => 'e_wilayah', 'dt' => 'e_wilayah'),
                        array('db' => 'e_work_unit', 'dt' => 'e_work_unit'),
                    );
         
        // SQL server connection information
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db'   => $this->db->database,
            'host' => $this->db->hostname
        );
         
         
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
         
        //require( 'ssp.class.php' );
        $where = "e_category ='EMP'";

        echo json_encode(
            Serverside::complex( $_GET, $sql_details, $table, $primaryKey, $columns, null, $where )
        );
    }
}

?>