<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Food_serving_active extends BCA_Controller {

	public function index()
	{
		$this->load->model('food_serving_active_model');
		$data['actives'] = $this->food_serving_active_model->active_list()->result();

		$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 24)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(24)->row();
		$this->load->view('food_serving_active/view', $data);
	}

	public function add_form()
    {
    	$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(24)->row();
        $this->load->view('food_serving_active/add_form', $data);
    }
    
    public function edit_form()
    {
        $id = $this->uri->segment(3);
        $this->load->model('food_serving_active_model');
        $data['active'] = $this->food_serving_active_model->active_data($id)->row();
        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(24)->row();
        $this->load->view('food_serving_active/edit_form', $data);
    }

    public function new_data()
    {
		$error                = 0;
		$error_msg            = "";
		$redirect             = "food_serving_active";
		
		$data['active_start'] = $_POST['active_start'];
		

		$data['active_end']   = $_POST['active_end'];
		

		$this->form_validation->set_rules('active_start','Tgl Awal','required');
		$this->form_validation->set_rules('active_end','Tgl Akhir','required');
		if($this->form_validation->run())
		{
			$this->load->model('food_serving_active_model');

			$explode              = explode("-", $data['active_start']);
			$data['active_start'] = $explode[2]."-".$explode[1]."-".$explode[0];

			$explode              = explode("-", $data['active_end']);
			$data['active_end']   = $explode[2]."-".$explode[1]."-".$explode[0];

			if(strtotime($data['active_start']) > strtotime($data['active_end']))
			{
				$error     = 1;
				$error_msg = "Tanggal awal lebih besar dari tanggal akhir.";
				$redirect  = "food_serving_active/add_form";
			}

			// $exist = $this->food_serving_active_model->data_exist($data['active_start'], $data['active_end'])->row();
			// if(!empty($exist) || $exist != "" || $exist != null)
			// {
			// 	$error     = 1;
			// 	$error_msg = "Tanggal Aktif sudah ada dalam database.";
			// 	$redirect  = "food_serving_active/add_form";
			// }

			if($error == 0)
			{
				$this->food_serving_active_model->insertData($data['active_start'], $data['active_end']);
			
				$error     = 0;
				$error_msg = "Aktivasi Penyajian Makanan telah ditambahkan.";
			}

			$newdata = array('msg_active_list' => $error_msg, 'err_active_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal menambahkan Aktivasi Penyajian Makanan.";
			$newdata   = array('msg_active_list' => $error_msg, 'err_active_list' => $error);
			$this->session->set_userdata($newdata);
			$this->add_form();
		}
    }

    public function edit_data()
    {
		$error                = 0;
		$error_msg            = "";
		$redirect             = "food_serving_active";
		$data['active_id']    = $_POST['active_id'];
		
		$data['active_start'] = $_POST['active_start'];
		
		
		$data['active_end']   = $_POST['active_end'];
		

		$this->form_validation->set_rules('active_start','Tgl Awal','required');
		$this->form_validation->set_rules('active_end','Tgl Akhir','required');
		if($this->form_validation->run())
		{
			$this->load->model('food_serving_active_model');

			$explode              = explode("-", $data['active_start']);
			$data['active_start'] = $explode[2]."-".$explode[1]."-".$explode[0];

			$explode              = explode("-", $data['active_end']);
			$data['active_end']   = $explode[2]."-".$explode[1]."-".$explode[0];

			if(strtotime($data['active_start']) > strtotime($data['active_end']))
			{
				$error     = 1;
				$error_msg = "Tanggal awal lebih besar dari tanggal akhir.";
				$redirect  = "food_serving_active/edit_form/".$data['active_id'];
			}

			// $exist = $this->food_serving_active_model->data_exist($data['active_start'], $data['active_end'])->row();
			// if(!empty($exist) || $exist != "" || $exist != null)
			// {
			// 	$error     = 1;
			// 	$error_msg = "Tanggal Aktif sudah ada dalam database.";
			// 	$redirect  = "food_serving_active/add_form";
			// }

			if($error == 0)
			{
				$this->food_serving_active_model->updateData($data['active_id'], $data['active_start'], $data['active_end']);
			
				$error     = 0;
				$error_msg = "Aktivasi Penyajian Makanan berhasil diubah.";
			}

			$newdata = array('msg_active_list' => $error_msg, 'err_active_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal mengubah Aktivasi Penyajian Makanan.";
			$newdata   = array('msg_active_list' => $error_msg, 'err_active_list' => $error);
			$this->session->set_userdata($newdata);
			$this->edit_form();
		}
    }

    public function remove_data()
	{
		$id = $this->uri->segment(3);
		
		$this->load->model('food_serving_active_model');
		$this->food_serving_active_model->deleteData($id);

		$error = 0;
		$error_msg = "Aktivasi Penyajian Makanan telah dihapus.";
		$newdata = array('msg_active_list' => $error_msg, 'err_active_list' => $error);
		$this->session->set_userdata($newdata);
		
		redirect('food_serving_active');
	}
}

?>