<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Program extends CI_Controller {
	public function popup_add_form()
    {
		if($_POST)
		{
			$program_name = $this->input->post('program_name');
			$subprogram_name = $this->input->post('subprogram_name');
			$training_name = $this->input->post('training_name');
			$program_begin = $this->input->post('program_begin');
			$program_end = $this->input->post('program_end');
			
			if($program_name != null && $subprogram_name != null && $training_name != null && $program_begin != null && $program_end != null)
			{
				$data = array(
					'program_name' => $program_name,
					'subprogram_name' => $subprogram_name,
					'training_name' => $training_name,
					'program_begin' => $program_begin,
					'program_end' => $program_end,
					'created_date' => date("Y-m-d H:i:s"),
					'created_by' => $this->session->userdata('username')
				);
				
				$this->db->insert('bca_program', $data);
				
				$return = array('ok' => 1, 'message' => "Success");
			}
			else
			{
				$return = array('ok' => 0, 'message' => "Semua Field Wajib Diisi");
			}
		}
		else
		{
			$return = array('ok' => 0, 'message' => "Error POST Method");
		}
		
		echo json_encode($return);
    }
}

?>