<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export_data extends CI_Controller {
	
	//check
	public function __construct() {
		parent::__construct();
		$this->load->library("PHPExcel");

		$this->menu_title = "Export Excel";
	}
	

	public function export_to_excel($type = null){
		if($_POST)
		{
			//membuat objek
			$table_array = $this->input->post('table_array');
			$data = json_decode($table_array);
			
			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold(true);
		
			$row_num = 2;
			
			//var_dump ($data->table_data);
			
			foreach($data->table_data as $row)
			{
				$col_num = 0;
				foreach($row as $key => $column) 
				{
					if($row_num == 2)
					{
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue(chr(65+$col_num).'1', $key);
						
						$objPHPExcel->getActiveSheet()->getColumnDimension(chr(65+$col_num))->setWidth(20);
					}
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col_num, $row_num, $column);
					$col_num++;
				}
				
				$row_num++;
			}
			
			$objPHPExcel->setActiveSheetIndex(0);

			//Set Title
			$objPHPExcel->getActiveSheet()->setTitle('Menu Priviledge');
			

			// Redirect output to a clientâ€™s web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$type.'.xlsx"');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('php://output');
		}
		else
		{
			
		}
	}
}
?>
