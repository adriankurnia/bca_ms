<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Groupmenu_priviledge extends BCA_Controller {

	public $menu_log = "Hak Akses";

	public function index()
	{

		// $data['gm_privs'] = $this->groupmenu_priviledge_model->groupmenu_priv_list()->result();

		$this->load->model('group_priviledge_model');
		$data['g_privs'] = $this->group_priviledge_model->group_priv_list()->result();

		$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 7)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(7)->row();
        $this->session->unset_userdata('posting');
		$this->load->view('groupmenu_priviledge/view', $data);
	}

	public function add_form()
    {
    	$this->load->model('group_priviledge_model');
    	$data['g_privs']  = $this->group_priviledge_model->group_priv_list()->result();

    	$this->load->model('menu_priviledge_model');
    	$data['m_privs']  = $this->menu_priviledge_model->menu_priv_list()->result();

  //   	$this->load->model('groupmenu_priviledge_model');
		// $data['gm_privs'] = $this->groupmenu_priviledge_model->groupmenu_priv_list()->result();
		
		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(7)->row();
        $this->load->view('groupmenu_priviledge/add_form', $data);
    }
    
    public function edit_form()
    {
        $this->load->model('general_model');
        $id = $this->uri->segment(3);
        $this->load->model('group_priviledge_model');
    	$data['g_priv']  = $this->group_priviledge_model->group_priv_data($id)->row();
        $data['getModuleDashboard'] = $this->general_model->select_data('module_tb');
        $data['getModuleList'] = $this->general_model->select_data2('bca_module_dashboard_tb','bca_group_privilege_id',$id);


    	$this->load->model('menu_priviledge_model');
    	$data['m_privs']  = $this->menu_priviledge_model->menu_priv_list()->result();

    	$this->load->model('groupmenu_priviledge_model');
		$data['gm_privs'] = $this->groupmenu_priviledge_model->groupmenu_priv_data($id)->result();
		
		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(7)->row();
        $this->load->view('groupmenu_priviledge/edit_form', $data);
    }

    public function display()
    {
        $this->load->model('general_model');
        $id = $this->uri->segment(3);
        $this->load->model('group_priviledge_model');
    	$data['g_priv']  = $this->group_priviledge_model->group_priv_data($id)->row();
        $data['getModuleDashboard'] = $this->general_model->select_data('module_tb');
        $data['getModuleList'] = $this->general_model->select_data2('bca_module_dashboard_tb','bca_group_privilege_id',$id);

    	$this->load->model('menu_priviledge_model');
    	$data['m_privs']  = $this->menu_priviledge_model->menu_priv_list()->result();

    	$this->load->model('groupmenu_priviledge_model');
		$data['gm_privs'] = $this->groupmenu_priviledge_model->groupmenu_priv_data($id)->result();
		
		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(7)->row();
        $this->load->view('groupmenu_priviledge/display', $data);
    }

    public function priv_validation($group_priviledge)
    {
    	if($group_priviledge == 0)
    	{
    		$this->form_validation->set_message("priv_validation", "Anda harus memilih Grup Admin");
    		return false;
    	}
    	else{
    		return true;
    	}
    }

    public function new_data()
    {
		$error                    = 0;
		$error_msg                = "";
		$redirect                 = "groupmenu_priviledge";
		$data['group_priviledge'] = addslashes($_POST['group_priviledge']);

		$this->session->set_userdata('posting', $data['group_priviledge']);

		$this->form_validation->set_rules('group_priviledge','Grup Admin','required');

		if($this->form_validation->run())
		{
			$this->load->model('group_priviledge_model');
			$this->load->model('menu_priviledge_model');
			$this->load->model('groupmenu_priviledge_model');

			$exist = $this->groupmenu_priviledge_model->same_group_name($data['group_priviledge'])->row();
			if($exist->row > 0)
			{
				$error     = 1;
				$error_msg = "Nama Grup Admin sudah ada dalam daftar.";
				$redirect  = "groupmenu_priviledge/add_form";
			}
			else
			{
				$group_priv_id = $this->group_priviledge_model->insertData($data['group_priviledge']);
				$this->cms_log_activity("insert", "", $this->menu_log.": ".$data['group_priviledge']);

				$menus = $this->menu_priviledge_model->menu_priv_list()->result();
				$fulldate = date('Y-m-d H:i:s');
				foreach ($menus as $menu)
				{	
					$data = array(
						'menu_priv_id'	=> $menu->id,
						'group_priv_id' => $group_priv_id,
						'created_date'  => $fulldate,
						'created_by'    => $this->session->userdata('username')
					);
					$this->db->insert('bca_group_menu_priviledge', $data);
					
					if(array_key_exists('create', $_POST))
					{
						if(in_array($menu->id, $_POST['create']))
						{
							$this->db->query("UPDATE bca_group_menu_priviledge SET `create` = 1 WHERE group_priv_id = ".$group_priv_id." AND menu_priv_id = ".$menu->id);
						}
					}

					if(array_key_exists('read', $_POST))
					{
						if(in_array($menu->id, $_POST['read']))
						{
							$this->db->query("UPDATE bca_group_menu_priviledge SET `read` = 1 WHERE group_priv_id = ".$group_priv_id." AND menu_priv_id = ".$menu->id);
						}
					}

					if(array_key_exists('update', $_POST))
					{
						if(in_array($menu->id, $_POST['update']))
						{
							$this->db->query("UPDATE bca_group_menu_priviledge SET `update` = 1 WHERE group_priv_id = ".$group_priv_id." AND menu_priv_id = ".$menu->id);
						}
					}

					if(array_key_exists('delete', $_POST))
					{
						if(in_array($menu->id, $_POST['delete']))
						{
							$this->db->query("UPDATE bca_group_menu_priviledge SET `delete` = 1 WHERE group_priv_id = ".$group_priv_id." AND menu_priv_id = ".$menu->id);
						}
					}

				}
					
				$error     = 0;
				$error_msg = "Hak Akses telah ditambahkan.";
			}
		
			$newdata = array('msg_groupmenu_list' => $error_msg, 'err_groupmenu_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal menambahkan Hak Akses.";
			$newdata   = array('msg_groupmenu_list' => $error_msg, 'err_groupmenu_list' => $error);
			$this->session->set_userdata($newdata);
			$this->add_form();
		}
    }

    public function edit_data()
    {
    	// echo "<pre>";
    	// print_r($_POST);
    	// echo "</pre>";die;
        //module//

        $this->load->model('general_model');
        $privilege_id = $this->input->post('group_priviledge');
        $module_id = $this->input->post('module_id');
        $this->general_model->delete_data('bca_module_dashboard_tb',array('bca_group_privilege_id'=>$privilege_id));
        if($module_id)foreach($module_id as $list){
            $database = array('bca_group_privilege_id'=>$privilege_id,'module_id'=>$list);
            $this->general_model->insert_data('bca_module_dashboard_tb',$database);
        }

        $data['group_priviledge'] = addslashes($_POST['group_priviledge']);
        $this->cms_log_activity("edit", $this->menu_log.": ".$data['group_priviledge']);
		$this->load->model('groupmenu_priviledge_model');

		$this->groupmenu_priviledge_model->empty_priviledge($data['group_priviledge']);

		foreach ($_POST['create'] as $data) {
			$this->groupmenu_priviledge_model->updateData($data, 'create');
		}
		foreach ($_POST['read'] as $data) {
			$this->groupmenu_priviledge_model->updateData($data, 'read');
		}
		foreach ($_POST['update'] as $data) {
			$this->groupmenu_priviledge_model->updateData($data, 'update');
		}
		foreach ($_POST['delete'] as $data) {
			$this->groupmenu_priviledge_model->updateData($data, 'delete');
		}

		$error     = 0;
		$error_msg = "Hak Akses berhasil diubah.";
		$newdata   = array('msg_groupmenu_list' => $error_msg, 'err_groupmenu_list' => $error);
		$this->session->set_userdata($newdata);
		redirect("groupmenu_priviledge");
    }

	public function remove_data()
	{
		// $id = $this->uri->segment(3);
		// $data = urldecode($this->uri->segment(4));
		
		$id = $_POST['id'];
		$data = $_POST['data'];
		
		$this->load->model('group_priviledge_model');
		$this->group_priviledge_model->deleteData($id);

		$this->db->where('group_priv_id', $id);
		$this->db->delete('bca_group_menu_priviledge');
		$this->cms_log_activity("delete", $this->menu_log.": ".$data);

		echo $data." telah dihapus.";

		$error = 0;
		$error_msg = $data." telah dihapus.";
		$newdata = array('msg_groupmenu_list' => $error_msg, 'err_groupmenu_list' => $error);
		$this->session->set_userdata($newdata);
		
		// redirect('groupmenu_priviledge');
	}

	public function update_value()
	{
		$id    = $_POST['id'];
		$field = $_POST['field'];

		$this->load->model('groupmenu_priviledge_model');
		$value = $this->groupmenu_priviledge_model->groupmenu_priv_data($id)->row();

		if($value->$field == 0)
			$new_value = 1;
		else
			$new_value = 0;

		$this->groupmenu_priviledge_model->updateValue($id, $field, $new_value);
	}
}

?>
