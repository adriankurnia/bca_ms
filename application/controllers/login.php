<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		if($_POST)
		{
			$username = $_POST['username'];
			$password = $_POST['password'];

			$this->load->model('login_model');

			$validate = $this->login_model->post_login($username, $password);

			if($validate)
			{
				$sess_array = array(
					'username'         => $validate->username,
					'loggedin'         => TRUE,
					'group_priviledge' => $validate->group_priv_id
				);
				$this->session->set_userdata($sess_array);
				redirect(base_url()."dashboard");
			}
		}
		$this->load->view('login/view');
	}
	
	public function logout()
	{
		$this->session->unset_userdata('loggedin', 'dashboardDate');
		$this->session->sess_destroy();
		redirect('login');
	}
}