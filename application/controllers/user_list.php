<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_list extends BCA_Controller {

	public $menu_log = "Administrator";

	public function index()
	{
		$this->load->model('user_list_model');
		$data['users'] = $this->user_list_model->user_list()->result();

		$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 4)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(4)->row();

        $this->session->unset_userdata('editing');
        $this->session->unset_userdata('posting');
		$this->load->view('user/view', $data);
	}

	public function add_form()
    {
    	$this->load->model('user_list_model');
        $data['priviledges'] = $this->user_list_model->group_priviledge()->result();
        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(4)->row();
        $this->load->view('user/add_form', $data);
    }
    
    public function edit_form($uid = null)
    {
    	$this->session->unset_userdata('editing');

    	if($uid == null) {
        	$username = urldecode($this->uri->segment(3));
    	} else {
    		$username = $uid;
    	}
        
        $this->load->model('user_list_model');
        $data['user'] = $this->user_list_model->user_data($username)->row();
        $data['priviledges'] = $this->user_list_model->group_priviledge()->result();
        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(4)->row();

        $this->load->view('user/edit_form', $data);
    }

    public function display()
    {
        $username = urldecode($this->uri->segment(3));
        $this->load->model('user_list_model');
        $data['user'] = $this->user_list_model->user_data($username)->row();
        $data['priviledges'] = $this->user_list_model->group_priviledge()->result();
        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(4)->row();

        $this->load->view('user/display', $data);
    }

    public function priv_validation($group_priviledge)
    {
    	if($group_priviledge == 0)
    	{
    		$this->form_validation->set_message("priv_validation", "Anda harus memilih Hak Akses untuk Administrator");
    		return false;
    	}
    	else{
    		return true;
    	}
    }

    public function new_user()
    {
		$error                    = 0;
		$error_msg                = "";
		$redirect                 = "user_list";
		$user['username']         = preg_replace("/\s+/", "_", $_POST['username']);
		$user['userpass']         = $_POST['userpass'];
		$user['group_priviledge'] = $_POST['group_priviledge'];

		$this->session->set_userdata('posting', $user);

		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('userpass','Password','required');
		$this->form_validation->set_rules('group_priviledge','Hak Akses','required|callback_priv_validation');

		if($this->form_validation->run())
		{
			$this->load->model('user_list_model');

			$exist = $this->user_list_model->data_exist($user['username'])->row();
			if($exist->total > 0)
			{
				$error     = 1;
				$error_msg = "Nama Admininistrator sudah ada dalam daftar.";
				$redirect  = "user_list/add_form";
			}

			if($error == 0)
			{
				$user['userpass'] = md5(md5($user['userpass']));

				$this->user_list_model->insert_user($user['username'], $user['userpass'], $user['group_priviledge']);
			
				$this->cms_log_activity("insert", $this->menu_log.": ".$user['username']);

				$error     = 0;
				$error_msg = "Administrator telah ditambahkan.";
			}

			$newdata = array('msg_user_list' => $error_msg, 'err_user_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal menambahkan Administrator.";
			$newdata   = array('msg_user_list' => $error_msg, 'err_user_list' => $error);
			$this->session->set_userdata($newdata);
			$this->add_form();
		}
    }

    public function edit_user()
    {
		$error                    = 0;
		$error_msg                = "";
		$redirect                 = "user_list";
		$user['oldUsername']      = $_POST['oldUsername'];
		$user['username']         = preg_replace("/\s+/", "_", $_POST['username']);
		$user['group_priviledge'] = $_POST['group_priviledge'];
		//print_r($_POST);die();

		$this->load->model('user_list_model');
        $data['user'] = $this->user_list_model->user_data($user['oldUsername'])->row();
        $this->session->set_userdata('editing', $data);

		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('group_priviledge','Hak Akses','required|callback_priv_validation');

		if($_POST['newPass'] != null || !empty($_POST['newPass']) || $_POST['newPass'] != "")
		//if(array_key_exists('newPass', $_POST))
		{
			$user['newPass'] = md5(md5($_POST['newPass']));
			$user['conPass'] = md5(md5($_POST['conPass']));

			$this->form_validation->set_rules('newPass','New Password','required');
			$this->form_validation->set_rules('conPass','Confirm Password','required');
		}
		//print_r($user);die;
		if($this->form_validation->run())
		{
			$exist = $this->user_list_model->data_exist($user['username'], $user['oldUsername'])->row();
			if($exist->total > 0)
			{
				$error     = 1;
				$error_msg = "Nama Admininistrator sudah ada dalam daftar.";
				$redirect  = "user_list/edit_form/".$user['username'];
			}

			if($user['newPass'] != $user['conPass'])
			{
				$error = 1;
				$error_msg = "Password Baru dan Konfirmasi Password tidak sama";
				$redirect = base_url()."user_list/edit_form/".$user['username'];
			}

			if($error == 0)
			{
				if($user['newPass'] != null || !empty($user['newPass']) || $user['newPass'] != "")
				//if(array_key_exists('newPass', $_POST))
				{
					$this->user_list_model->update_user($user['oldUsername'], $user['username'], $user['group_priviledge'], $user['newPass']);
				}
				else
				{
					$this->user_list_model->update_user($user['oldUsername'], $user['username'], $user['group_priviledge']);
				}
				

				$this->load->model('group_priviledge_model');
				$old = $this->group_priviledge_model->group_priv_data($this->session->userdata('editing')['user']->group_priv_id)->row();

				$log_data_old = "<b>Administrator</b><br />".
								"<b>Username</b>: ".$this->session->userdata('editing')['user']->username."<br /> ".
								"<b>Hak Akses</b>: ".$old->group_name;

				$new = $this->group_priviledge_model->group_priv_data($user['group_priviledge'])->row();
				$log_data_new = "<b>Administrator</b><br />".
								"<b>Username</b>: ".$user['username']."<br /> ".
								"<b>Hak Akses</b>: ".$new->group_name;
				if($user['newPass'] != null || !empty($user['newPass']) || $user['newPass'] != "")
				{
					$log_data_new .= "<br /><b>Ubah Password</b>";
				}

				$this->cms_log_activity("edit", $log_data_old, $log_data_new);

				$error     = 0;
				$error_msg = "Administrator berhasil diubah";
			}

			$this->session->unset_userdata('posting');
			$newdata = array('msg_user_list' => $error_msg, 'err_user_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal mengubah Administrator";
			$newdata   = array('msg_user_list' => $error_msg, 'err_user_list' => $error);
			$this->session->unset_userdata('posting');
			$this->session->set_userdata($newdata);
			$this->edit_form($user['oldUsername']);
		}
    }

    public function remove_user()
	{
		// $username = urldecode($this->uri->segment(3));
		
		$username = $_POST['id'];

		$this->load->model('user_list_model');
		$this->user_list_model->delete_user($username);

		$this->cms_log_activity("delete", $this->menu_log.": ".$username);
		echo $username." telah dihapus.";

		$error = 0;
		$error_msg = $username." telah dihapus.";
		$newdata = array('msg_user_list' => $error_msg, 'err_user_list' => $error);
		$this->session->set_userdata($newdata);
		
		// redirect('user_list');
	}
}

?>