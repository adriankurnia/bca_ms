<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Speedr extends UNIPublic_Controller {
	
	//check
	public function __construct() {
		parent::__construct();

		$this->menu_title = "Generator";
	}

	//check
	private function set_header(){
		$this->layout->set_title($this->menu_title);
		$this->data['message'] = $this->session->flashdata('message');
		$this->data['error'] = $this->session->flashdata('error');
	}

	public function generate_controller($database_name, $prefix = null) {
		if($prefix != null)
		{
			$database_name = str_replace($prefix, "", $database_name);
		}
		$prefix_database_name = $prefix . $database_name;

		$database_attributes = $this->db->field_data($prefix_database_name);

		$primary_key = $database_attributes[0]->name;

		$no = 1;
		$add_data_before_post = "";
		foreach($database_attributes as $attribute)
		{
			if($attribute->name == 'created_date' || $attribute->name == 'edited_date' || $attribute->name == 'created_by' || $attribute->name == 'edited_by')
			{
				//Do Not Print
			}
			elseif($attribute->primary_key != 1)
			{
				$add_data_before_post .= '$this->data["'.$attribute->name.'"] = null;'.'// '.$no."\n";
			}
			$no++;
		}

		$no = 1;
		$edit_data_before_post = "";
		foreach($database_attributes as $attribute)
		{
			if($attribute->name == 'created_date' || $attribute->name == 'edited_date' || $attribute->name == 'created_by' || $attribute->name == 'edited_by')
			{
				//Do Not Print
			}
			elseif($attribute->primary_key != 1)
			{
				$edit_data_before_post .= '$this->data["'.$attribute->name.'"] = $content->'.$attribute->name.';'.'// '.$no."\n";
			}
			else
			{
				$edit_data_before_post .= '$this->data["'.$attribute->name.'"] = $id; // '.$no.' [PK]'."\n";
			}
			$no++;
		}

		$no = 1;
		$data_after_post = "";
		foreach($database_attributes as $attribute)
		{
			if($attribute->name == 'created_date' || $attribute->name == 'edited_date' || $attribute->name == 'created_by' || $attribute->name == 'edited_by')
			{
				//Do Not Print
			}
			elseif($attribute->primary_key != 1)
			{
				$data_after_post .= '$this->data["'.$attribute->name.'"] = $this->input->post("'.$attribute->name.'");'.'// '.$no."\n";
			}
			$no++;
		}

		$no = 1;
		$data_validation = "";
		foreach($database_attributes as $attribute)
		{
			if($attribute->name == 'created_date' || $attribute->name == 'edited_date' || $attribute->name == 'created_by' || $attribute->name == 'edited_by')
			{
				//Do Not Print
			}
			elseif($attribute->primary_key != 1)
			{
				$data_validation .= '$this->form_validation->set_rules("'.$attribute->name.'","'.$this->string_to_character($attribute->name).'","required");'.'// '.$no."\n";
			}
			$no++;
		}

		$text = '<?php
	defined("BASEPATH") OR exit("No direct script access allowed");

	class '.$database_name.' extends UNIAdmin_Controller {
		
		//constructor
		public function __construct() {
			parent::__construct();
			$this->load->model("model_'.$database_name.'");

			$this->menu_title = "'.$this->string_to_character($database_name).'";
		}

		//check
		private function set_header(){
			$this->layout->set_title($this->menu_title);
			$this->data["message"] = $this->session->flashdata("message");
			$this->data["error"] = $this->session->flashdata("error");
		}

		//view
		public function index() {
			if($this->read_privilege == 0)
			{
				redirect($this->setting["ADMIN_URL"] . $this->setting["ADMIN_URL_REDIRECT_FORBIDEN"]);
			}

			$this->set_header();
			$this->data["list_'.$database_name.'"] = $this->model_'.$database_name.'->getData()->result();
			$this->layout->view($this->setting["ADMIN_URL"].$this->router->fetch_class()."/view");
		}

		//form
		public function form($id = null) {
			$this->set_header();


			// Add Data
			if($id == null)
			{
				if($this->create_privilege == 0)
				{
					redirect($this->setting["ADMIN_URL"] . $this->setting["ADMIN_URL_REDIRECT_FORBIDEN"]);
				}

				if(empty($data))
				{
					'.$add_data_before_post.'
				}
			}

			// Edit Data
			else
			{
				if($this->update_privilege == 0)
				{
					redirect($this->setting["ADMIN_URL"] . $this->setting["ADMIN_URL_REDIRECT_FORBIDEN"]);
				}
				$this->data["content"] = $this->db
					->where("'.$primary_key.'", $id)
					->get("'.$prefix_database_name.'");

				if($this->data["content"]->num_rows() <= 0)
				{
					$this->session->set_flashdata("error", "Content Not Found");
					redirect($this->setting["ADMIN_URL"].$this->router->fetch_class());
				}

				$content = $this->data["content"]->row();

				'.$edit_data_before_post.'
			}

			if($_POST)
			{
				'.$data_after_post.'
				'.$data_validation.'

				if($this->form_validation->run())
				{	
					if($id == null)
					{
						if($this->create_privilege == 0)
						{
							redirect($this->setting["ADMIN_URL"] . $this->setting["ADMIN_URL_REDIRECT_FORBIDEN"]);
						}

						//$this->db->trans_start();
						
						// Add Data
						$this->model_'.$database_name.'->addData($this->data);
						$this->model_log->addData("Add Data", $this->setting["ADMIN_URL"].$this->router->fetch_class());


						//$this->db->trans_complete();

						$this->session->set_flashdata("message", "Successfully Add New Data");
						redirect($this->setting["ADMIN_URL"].$this->router->fetch_class());
					}
					else
					{
						if($this->update_privilege == 0)
						{
							redirect($this->setting["ADMIN_URL"] . $this->setting["ADMIN_URL_REDIRECT_FORBIDEN"]);
						}

						//$this->db->trans_start();
						
						// Update Data
						$this->model_'.$database_name.'->updateData($this->data);
						$this->model_log->addData("Update Data", $this->setting["ADMIN_URL"].$this->router->fetch_class());

						//$this->db->trans_complete();
						
						$this->session->set_flashdata("message", "Successfully Update Data");
						redirect($this->setting["ADMIN_URL"].$this->router->fetch_class());
					}
				}
			}

			$this->layout->view($this->setting["ADMIN_URL"].$this->router->fetch_class()."/form");
		}

		public function delete() {
			if($this->delete_privilege == 0)
			{
				redirect($this->setting["ADMIN_URL"] . $this->setting["ADMIN_URL_REDIRECT_FORBIDEN"]);
			}

			$id = $_REQUEST["modal"];
			
			$this->db->trans_start();
			$this->model_'.$database_name.'->deleteData($id);
			$this->db->trans_complete();

			$this->session->set_flashdata("message", "Successfully Delete Data");
			redirect($this->setting["ADMIN_URL"].$this->router->fetch_class());
		}

	}
?>';

		write_file("application/controllers/".ADMIN_URL."/".$database_name.".php", $text);
		echo "Controller ".$database_name." Generated \n";
	}

	public function generate_model($database_name, $prefix = null){
		if($prefix != null)
		{
			$database_name = str_replace($prefix, "", $database_name);
		}
		$prefix_database_name = $prefix . $database_name;

		$database_attributes = $this->db->field_data($prefix_database_name);
		$lenght = count($database_attributes);

		$primary_key = $database_attributes[0]->name;
		
		$no = 1;
		$addData = '';
		foreach($database_attributes as $attribute)
		{
			if($attribute->name == 'created_date' || $attribute->name == 'edited_date')
			{
				$addData .= '"'.$attribute->name.'" => $date';
			}
			elseif($attribute->name == 'created_by' || $attribute->name == 'edited_by')
			{
				$addData .= '"'.$attribute->name.'" => $this->session->userdata("admin_id")';
			}
			elseif($attribute->primary_key != 1)
			{
				$addData .= '"'.$attribute->name.'" => $post["'.$attribute->name.'"]'."";
			}
			else
			{
				$addData .= '"'.$attribute->name.'" => ""';
			}

			if($lenght > $no)
			{
				$addData .= ','."\n";
			}
			$no++;
		}

		$no = 1;
		$updateData = '';
		foreach($database_attributes as $attribute)
		{
			if($attribute->name == 'created_date' || $attribute->name == 'edited_date')
			{
				if($attribute->name == 'edited_date')
				{
					$updateData .= '"'.$attribute->name.'" => $date';
				}
			}
			elseif($attribute->name == 'created_by' || $attribute->name == 'edited_by')
			{
				if($attribute->name == 'edited_by')
				{
					$updateData .= '"'.$attribute->name.'" => $this->session->userdata("admin_id")';
				}
			}
			elseif($attribute->primary_key != 1)
			{
				$updateData .= '"'.$attribute->name.'" => $post["'.$attribute->name.'"]';
			}

			if($lenght > $no && $no > 1 && substr($updateData, -2, 1) != ',')
			{
				$updateData .= ','."\n";
			}
			$no++;
		}

		echo substr($updateData, -5);

		$text = '<?php
	class model_'.$database_name.' extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
		}

		public function getData()
		{
			return $this->db->query("SELECT * FROM '.$prefix_database_name.'");
		}

		public function addData($post)
		{
			$date = date("Y-m-d H:i:s");
			
			$data = array(
				'.$addData.'
			);
			
			$this->db->insert("'.$prefix_database_name.'", $data);
		}

		public function updateData($post)
		{
			$date = date("Y-m-d H:i:s");

			$data = array(
				'.$updateData.'
			);

			$this->db->where("'.$primary_key.'", $post["'.$primary_key.'"]);
			$this->db->update("'.$prefix_database_name.'", $data);
		}

		public function deleteData($id)
		{
			$this->db->where("'.$primary_key.'", $id);
			$this->db->delete("'.$prefix_database_name.'");
		}
	}
?>';

		write_file("application/models/model_".$database_name.".php", $text);
		echo "Model ".$database_name." Generated \n";
	}

	public function generate_view($database_name, $prefix = null){
		if($prefix != null)
		{
			$database_name = str_replace($prefix, "", $database_name);
		}
		$prefix_database_name = $prefix . $database_name;

		$database_attributes = $this->db->field_data($prefix_database_name);
		$lenght = count($database_attributes);

		$primary_key = $database_attributes[0]->name;

		if(!is_dir("application/views/".ADMIN_URL."/".$database_name))
		{
			 mkdir('./application/views/'.ADMIN_URL.'/' . $database_name, 0777, TRUE);
			 echo "Directory ".$database_name."[Views] Created \n";
		}
		
		$no = 1;
		$table_header = '';
		foreach($database_attributes as $attribute)
		{
			$table_header .= '<th>'.$this->string_to_character($attribute->name).'</th>'."\n"; 
			$no++;
		}

		$no = 1;
		$table_content = '';
		foreach($database_attributes as $attribute)
		{
			$table_content .= '<td><?php echo $result->'.$attribute->name.'; ?></td>'."\n"; 
			$no++;
		}

		$no = 1;
		$forms = '';
		foreach($database_attributes as $attribute)
		{
			if($attribute->name == 'created_date' || $attribute->name == 'edited_date' || $attribute->name == 'created_by' || $attribute->name == 'edited_by')
			{
				//Do Not Print
			}
			elseif($attribute->primary_key != 1)
			{
				$forms .= 
				'<div class="form-group row">
	                <div class="col-md-3">
	                    <label for="l13">'.$this->string_to_character($attribute->name).'</label>
	                </div>
	                <div class="col-md-9">
	                    <?php 
	                        echo form_input(array("name" => "'.$attribute->name.'", "value" => $'.$attribute->name.', "class" => "form-control", "placeholder" => "'.$this->string_to_character($attribute->name).'"));
	                        echo form_error("'.$attribute->name.'");
	                    ?>
	                </div>
	            </div>'."\n\n";
			}
		}

		$text_view = '<section class="panel">
    <div class="panel-heading">
        <h3>
           List <?php echo $this->menu_title; ?>
        </h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <a href ="<?php echo base_url() . $this->setting['."'ADMIN_URL'".'] . $this->router->fetch_class() ;?>/form" class="btn btn-success margin-inline"> + Add New</a> 
                <div class="margin-bottom-50">
                    <table class="table table-hover nowrap" id="example1" width="100%">
                        <thead>
                        <tr>
                        	'.$table_header.'
                            <th>Action</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            '.$table_header.'
                            <th>Action</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php foreach($list_'.$database_name.' as $result):?>
                        <tr>
                            '.$table_content.'
                            <td>
                                <a href="<?php echo base_url() . $this->setting['."'ADMIN_URL'".'] . $this->router->fetch_class(); ?>/form/<?php echo $result->'.$primary_key.' ?>" class="btn btn-info margin-inline">Edit</a>
                            </td>
                            <td>
                                <button type="button" class="open-deleteModal btn btn-danger margin-inline" data-toggle="modal" data-id="<?php echo $result->'.$primary_key.'; ?>" data-target="#deleteModal">
                                    Delete
                                </button>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->  
<form id="delete" method="post" action="<?php echo base_url(). $this->setting['."'ADMIN_URL'".'] . $this->router->fetch_class(); ?>/delete">
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Delete Item</h4>
                </div>
                <div class="modal-body">
                    Are you sure u want to delete this Item??
                    <br/>
                    <br/>
                    YOU NO LONGER ENABLE TO RETRIEVE THE DATA AFTER IT DELETED
                    <input type="hidden" class="form-control" name="modal" id="modal" value="" />
                </div>
                <div class="modal-footer">
                    <input type="submit" id="submit" value="Delete Item" class="btn btn-primary" />
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- End Modal>

    
<!-- End  -->';

		write_file("application/views/".ADMIN_URL."/".$database_name."/view.php", $text_view);
		echo "View ".$database_name." Generated \n";

		$text_form = '<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3><?php if($this->uri->segment(4) == null) {echo "Add"; } else {echo "Update";} ?> <?php echo $this->menu_title; ?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <?php echo form_open_multipart(current_url());?>
                    	'.$forms.'
				        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" class="btn width-150 btn-primary">Submit</button>
                                    <a href="<?php echo base_url() . $this->setting["ADMIN_URL"] ?><?php echo $this->uri->segment(2); ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                                </div>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->
		';

		write_file("application/views/".ADMIN_URL."/".$database_name."/form.php", $text_form);
		echo "Form ".$database_name." Generated \n";
	}

	public function generate_table($database_name = null, $prefix = null)
	{
		$this->generate_controller($database_name, $prefix);	
		$this->generate_model($database_name, $prefix);
		$this->generate_view($database_name, $prefix);
	}

	public function generate_table_prototype($database_name = null, $prefix = null)
	{
		$data['list_field'] = array();

		if($prefix != null)
		{
			$database_name = str_replace($prefix, "", $database_name);
		}
		$prefix_database_name = $prefix . $database_name;

		$database_attributes = $this->db->field_data($prefix_database_name);
		foreach($database_attributes as $database_attribute)
		{
			if(!$database_attribute->primary_key)
			{
				$data['list_field'][] = $database_attribute->name;
			}
		}

		$primary_key = $database_attributes[0]->name;

		if(empty($data))
		{
			foreach($database_attributes as $database_attribute)
			{
				//check if not primary key
				if(!$database_attribute->primary_key)
				{
					$data[$database_attribute->name];
				}
				
			}

		}

		else
		{

		}



		if($_POST)
		{
			foreach($database_attributes as $database_attribute)
			{
				//check if not primary key
				if(!$database_attribute->primary_key)
				{
					$data[$database_attribute->name] = $this->input->post($database_attribute->name);
				}
			}

			

			//$this->generate_controller($database_name, $prefix);
			//$this->generate_model($database_name, $prefix);
			//$this->generate_view($database_name, $prefix);
		}

		$this->load->view("generator_rad/customize", $data);


	}

	public function generate_all()
	{
		if(empty($data))
		{
			$data['table_prefix'] = null;
		}

		if($_POST)
		{
			$data['table_prefix'] = $this->input->post('table_prefix');

			$tables = $this->db->list_tables();
			foreach($tables as $table)
			{
				//$this->generate_table($table, $data['table_prefix']);
			}		
		}

		$this->load->view("generator_rad/generate_all", $data);
	}


	public function test($database_name){
		$database_attributes = $this->db->field_data($database_name);
		
		foreach($database_attributes as $attribute)
		{
			var_dump($attribute);
			
		}
					
	}
}
