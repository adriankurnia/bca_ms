<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Temp_user extends BCA_Controller {

	public $menu_log = "Peserta";

	public function index()
	{
		// $this->load->model('temp_user_model');
		// $data['temp_users'] = $this->temp_user_model->temp_user_list()->result();

		$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 1)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(1)->row();
		
        $this->session->unset_userdata('posting');
        $this->session->unset_userdata('editing');
		$this->load->view('temp_user/view', $data);
	}

	public function get_temp_user_data()
	{
		$id = $_POST['id'];
		$this->load->model('temp_user_model');
		$res = $this->temp_user_model->temp_user_data($id)->row();

		if($res)
		{
			$return = array('ok' => 1, 'data' => $res);
		}
		else
		{
			$return = array('ok' => 0);
		}
		echo json_encode($return);
	}

	public function get_deleted_temp_user()
	{
		$this->load->model('temp_user_model');
		$data['temp_users'] = $this->temp_user_model->temp_user_deleted()->result();

		$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 1)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(1)->row();
		
		$this->load->view('temp_user/reappear_temp_user', $data);
	}

	public function add_form()
    {
    	$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(1)->row();

        $this->load->model('program_model');
        $data['programs'] = $this->program_model->program_list()->result();

        $this->load->view('temp_user/add_form', $data);
    }

    public function upload_form()
    {
    	$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(1)->row();

        $this->load->view('temp_user/upload_form', $data);
    }
    
    public function edit_form($uid = null)
    {
    	$this->session->unset_userdata('editing');
    	if($uid == null) {
        	$id = urldecode($this->uri->segment(3));
    	} else {
    		$id = urldecode($uid);
    	}

        $this->load->model('temp_user_model');
        $data['temp_user'] = $this->temp_user_model->temp_user_data($id)->row();
        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(1)->row();
        $this->load->model('program_model');
        $data['programs'] = $this->program_model->program_list()->result();
        
        $this->load->view('temp_user/edit_form', $data);
    }

    public function display()
    {
        $id = urldecode($this->uri->segment(3));
        $this->load->model('temp_user_model');
        $data['temp_user'] = $this->temp_user_model->temp_user_data($id)->row();
        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(1)->row();
        $this->load->model('program_model');
        $data['programs'] = $this->program_model->program_list()->result();
        
        $this->load->view('temp_user/display', $data);
    }

    public function prog_validation($temp_user_program)
    {
    	if($temp_user_program == 0)
    	{
    		$this->form_validation->set_message("prog_validation", "Anda harus memilih Program");
    		return false;
    	}
    	else{
    		return true;
    	}
    }

    public function new_temp_user()
    {
		$error                       = 0;
		$error_msg                   = "";
		$redirect                    = "temp_user";
		$client['temp_user_nip']     = trim($_POST['temp_user_nip']);
		$client['temp_user_name']    = $_POST['temp_user_name'];
		$client['temp_user_rfid']    = $this->String_Serialize($_POST['temp_user_rfid']);
		$client['temp_user_expired'] = $_POST['temp_user_expired'];
		$client['temp_user_program'] = $_POST['temp_user_program'];

		$this->session->set_userdata('posting', $client);

		$this->form_validation->set_rules('temp_user_nip','NIP','required');
		$this->form_validation->set_rules('temp_user_name','Nama','required');
		$this->form_validation->set_rules('temp_user_rfid','RFID','required');
		$this->form_validation->set_rules('temp_user_expired','Kadaluarsa Kartu','required');
		$this->form_validation->set_rules('temp_user_program','Program','required|callback_prog_validation');

		if($this->form_validation->run())
		{
			$this->load->model('temp_user_model');
			$client['temp_user_nip'] = "EX_".$client['temp_user_nip'];
			$nip_exist = $this->temp_user_model->data_exist("e_nip", $client['temp_user_nip'], "add");
			if($nip_exist > 0)
			{
				$error = 1;
				$error_msg = "NIP sudah terdaftar pada database.";
				$redirect = "temp_user/add_form";
			}

			// $rfid_exist = $this->temp_user_model->data_exist("e_rfid", $client['temp_user_rfid'], "add");
			// if($rfid_exist > 0)
			// {
			// 	$error = 1;
			// 	$error_msg = "RFID sudah terdaftar pada database.";
			// 	$redirect = "temp_user/add_form";
			// }

			if($error == 0)
			{
				$explode                     = explode("-", $client['temp_user_expired']);
				$client['temp_user_expired'] = $explode[2]."-".$explode[1]."-".$explode[0];
				$this->temp_user_model->insert_temp_user($client['temp_user_nip'], $client['temp_user_name'], $client['temp_user_rfid'], $client['temp_user_expired'],  $client['temp_user_program']);
				
				$this->cms_log_activity("insert", "", $this->menu_log.": ".$client['temp_user_name']);

				$error     = 0;
				$error_msg = "Peserta telah ditambahkan.";
			}

			$newdata = array('msg_temp_user_list' => $error_msg, 'err_temp_user_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal menambahkan Peserta.";
			$newdata   = array('msg_temp_user_list' => $error_msg, 'err_temp_user_list' => $error);
			$this->session->set_userdata($newdata);
			$this->add_form();
		}
    }

    public function edit_temp_user()
    {
		$error                       = 0;
		$error_msg                   = "";
		$redirect                    = "temp_user";
		$client['temp_user_nip']     = urldecode($_POST['temp_user_nip']);
		$client['temp_user_name']    = $_POST['temp_user_name'];
		$client['temp_user_rfid']    = $this->String_Serialize($_POST['temp_user_rfid']);
		$client['temp_user_expired'] = $_POST['temp_user_expired'];
		$client['temp_user_program'] = $_POST['temp_user_program'];

		$this->load->model('temp_user_model');
        $data['temp_user'] = $this->temp_user_model->temp_user_data($client['temp_user_nip'])->row();
        $this->session->set_userdata('editing', $data);

		$this->form_validation->set_rules('temp_user_nip','NIP','required');
		$this->form_validation->set_rules('temp_user_name','Nama','required');
		$this->form_validation->set_rules('temp_user_rfid','RFID','required');
		$this->form_validation->set_rules('temp_user_expired','Kadaluarsa Kartu','required');
		$this->form_validation->set_rules('temp_user_program','Program','required');

		if($this->form_validation->run())
		{
			// $old = $this->temp_user_model->temp_user_data($client['temp_user_nip'])->row();
			// $rfid_exist = $this->temp_user_model->data_exist("t_rfid", $client['temp_user_rfid'], "edit", $client['temp_user_nip']);
			// if($rfid_exist > 0)
			// {
			// 	$error = 1;
			// 	$error_msg = "RFID sudah terdaftar pada database.";
			// 	$redirect = "temp_user/edit_form/".$client['temp_user_nip'];
			// }

			if($error == 0)
			{
				$explode                     = explode("-", $client['temp_user_expired']);
				$client['temp_user_expired'] = $explode[2]."-".$explode[1]."-".$explode[0];
				$this->temp_user_model->update_temp_user($client['temp_user_nip'], $client['temp_user_name'], $client['temp_user_rfid'], $client['temp_user_expired'], $client['temp_user_program']);
			
				$this->load->model('program_model');
				$old = $this->program_model->program_data($this->session->userdata('editing')['temp_user']->e_program)->row();

				$log_data_old = "<b>Peserta</b><br />".
								"<b>Nama</b>: ".$this->session->userdata('editing')['temp_user']->e_name."<br /> ".
								"<b>Program</b>: ".$old->program_name."<br />".
								"<b>Kadaluarsa Kartu</b>: ".$this->session->userdata('editing')['temp_user']->card_expired."<br />".
								"<b>RFID</b>: ".$this->session->userdata('editing')['temp_user']->e_rfid;

				$new = $this->program_model->program_data($client['temp_user_program'])->row();
				$log_data_new = "<b>Peserta</b><br />".
								"<b>Nama</b>: ".$client['temp_user_name']."<br />".
								"<b>Program</b>: ".$new->program_name."<br />".
								"<b>Kadaluarsa Kartu</b>: ".$client['temp_user_expired']."<br />".
								"<b>RFID</b>: ".$client['temp_user_rfid'];

				$this->cms_log_activity("edit", $log_data_old, $log_data_new);

				$error     = 0;
				$error_msg = "Peserta berhasil diubah.";
			}

			$this->session->unset_userdata('posting');
			$newdata = array('msg_temp_user_list' => $error_msg, 'err_temp_user_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal mengubah Peserta.";
			$newdata   = array('msg_temp_user_list' => $error_msg, 'err_temp_user_list' => $error);
			$this->session->unset_userdata('posting');
			$this->session->set_userdata($newdata);
			$this->edit_form($client['temp_user_nip']);
		}
    }

    public function remove_temp_user()
	{
		// $nip = $this->uri->segment(3);
		$nip = $_POST['id'];

		$this->load->model('temp_user_model');
		$this->temp_user_model->delete_temp_user($nip);

		// $data = urldecode($this->uri->segment(4));
		$data = $_POST['data'];
		$this->cms_log_activity("delete", $this->menu_log.": ".$data);
		echo $data." telah dihapus.";

		$error = 0;
		$error_msg = $data." telah dihapus.";
		$newdata = array('msg_temp_user_list' => $error_msg, 'err_temp_user_list' => $error);
		$this->session->set_userdata($newdata);
		
		// redirect('temp_user');
	}

	public function reappear_temp_user()
	{
		// $nip = $this->uri->segment(3);
		$nip = $_POST['id'];
		
		$this->load->model('temp_user_model');
		$this->temp_user_model->update_reappear_temp_user($nip);

		// $data = urldecode($this->uri->segment(4));
		$data = $_POST['data'];
		$this->cms_log_activity("reappeare", $this->menu_log.": ".$data);
		echo $data." telah dimunculkan kembali sebagai peserta.";

		// $error = 0;
		// $error_msg = "Peserta telah dihapus.";
		// $newdata = array('msg_temp_user_list' => $error_msg, 'err_temp_user_list' => $error);
		// $this->session->set_userdata($newdata);
		
		// redirect('temp_user');
	}

	public function upload_file()
	{
		//print_r($_FILES);die;
		//$chk_ext  = explode('.',$fname);
		$total = 0;
		$fulldate = date('Y-m-d H:i:s');
		$filename = $_FILES['updloadData']['tmp_name'];

		$this->load->library('excel');
		$inputFileName = $filename;
		$inputFileType = 'Excel2007';
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader     = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel   = $objReader->load($inputFileName);
		} catch (Exception $e) {
			die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
			. '": ' . $e->getMessage());
		}

		$sheet         = $objPHPExcel->getSheet(0);
		$highestRow    = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();
		
		for($i = 1; $i<=$highestRow; $i++)
		{
			$nip     = 'EX_'.$sheet->getCell('A'.$i);
			$name    = $sheet->getCell('B'.$i);
			$rfid    = $sheet->getCell('C'.$i);
			$expired = explode("-", $sheet->getCell('D'.$i));
			$program = $sheet->getCell('E'.$i);

			$nol = "";
			$len = strlen($rfid);

			if($len < 10)
			{
				for($x=0; $x<10-$len; $x++) 
				{
					$nol .= "0";
				}
				$rfid = $nol.$rfid;
			}
			
			$nip_exist = $this->db->query("SELECT e_nip, e_name FROM bca_employee WHERE e_nip = '".$nip."'")->row();
			if($nip_exist)
			{
				$error_msg .= $nip_exist->e_nip." sudah terdaftar dalam database untuk ".$nip_exist->e_name.". <br/>";
			}
			else
			{
				$program_exist = $this->db->query("SELECT * FROM bca_program WHERE program_name = '".$program."'");
				if($program_exist->num_rows() > 0)
				{
					$res = $program_exist->row();
					$id_program = $res->p_id;
				}
				else
				{
					$fulldate = date('Y-m-d H:i:s');
					// $data = array(
					// 	'program_name' => $program,
					// 	'created_date' => $fulldate,
					// 	'created_by'   => $this->session->userdata('username')
					// );
					// $this->db->insert('bca_program', $data);
					$this->db->query("INSERT INTO `bca_program` (`program_name`, `created_date`, `created_by`) VALUES ('".$program."', '".$fulldate."', '".$this->session->userdata('username')."')");
					$id_program = $this->db->insert_id();
				}
				$query = "INSERT INTO bca_employee (e_nip, e_name, e_rfid, card_expired, e_program, created_date, created_by, e_category) VALUES ('".$nip."', '".$name."', '".$rfid."', '".$expired[2]."-".$expired[1]."-".$expired[0]."', ".$id_program.", '".$fulldate."', '".$this->session->userdata('username')."', 'EXT')";
				$this->db->query($query);
			
				$this->cms_log_activity("upload", $this->menu_log.": ".$name);
				$total++;
			}
		}

		$error     = 0;
		$error_msg .= $total." peserta telah ditambahkan.";
		$newdata   = array('msg_temp_user_list' => $error_msg, 'err_temp_user_list' => $error);
		$this->session->set_userdata($newdata);
		redirect("temp_user");
	}

	public function getData()
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        $this->load->library('Serverside');
        // DB table to use
        //$table = 'bca_employee';
        $table = <<<EOT
 					(SELECT
				      SUBSTR(a.e_nip,4) AS e_nip,
				      a.e_name,
				      a.e_rfid,
				      DATE_FORMAT(a.card_expired, "%d-%m-%Y") AS card_expired,
				      b.program_name
				    FROM bca_employee a
				    LEFT JOIN bca_program b ON a.e_program = b.p_id
					WHERE a.e_category = 'EXT' AND a.deleted = 0) temp
EOT;
        // Table's primary key
        $primaryKey = 'e_nip';
         
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
                        array(
                            'db' => 'e_nip',
                            'dt' => 'e_nip',
                            'formatter' => function( $d ) {
                                return '<a href="temp_user/display/EX_'.$d.'">'.$d.'</a>';
                            }
                        ),
                        array(
                            'db' => 'e_nip',
                            'dt' => 'ACTION',
                            'formatter' => function( $d, $row ) {
                                return '<a class="icmn-pencil" href="temp_user/edit_form/EX_'.$d.'">&nbsp;&nbsp;<a class="icmn-bin" id="EX_'.$d.'" name="'.$row[2].'" onclick="saveChanges(this)">';
                            }
                        ),
                        array('db' => 'e_name', 'dt' => 'e_name'),
                        array('db' => 'e_rfid', 'dt' => 'e_rfid'),
                        array('db' => 'card_expired', 'dt' => 'card_expired'),
                        array('db' => 'program_name', 'dt' => 'program_name'),
                    );
         
        // SQL server connection information
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db'   => $this->db->database,
            'host' => $this->db->hostname
        );
         
         
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
         
        //require( 'ssp.class.php' );

        echo json_encode(
            Serverside::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
        );
    }

    public function training_schedule_view()
    {
    	$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 1)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(1)->row();
		
        $this->session->unset_userdata('editing');
		$this->load->view('temp_user/training_schedule_view', $data);
    }

    public function getTrainingSchedule()
    {
    	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        $this->load->library('Serverside');
        // DB table to use
        //$table = 'bca_employee';
        $table = <<<EOT
 					(SELECT 
 					  id, 
					  co_id,
					  DATE_FORMAT(co_start, "%d-%m-%Y") AS start_date,
					  DATE_FORMAT(co_end, "%d-%m-%Y") AS end_date,
					  e_nip 
					FROM
					  bca_course_participant 
					ORDER BY co_start DESC) temp
EOT;
        // Table's primary key
        $primaryKey = 'id';
         
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
                        array(
                            'db' => 'e_nip',
                            'dt' => 'ACTION',
                            'formatter' => function( $d, $row ) {
                                return '<a class="icmn-pencil" href="temp_user/edit_form/EX_'.$d.'">';
                            }
                        ),
                        array('db' => 'e_name', 'dt' => 'e_name'),
                        array('db' => 'e_rfid', 'dt' => 'e_rfid'),
                        array('db' => 'card_expired', 'dt' => 'card_expired'),
                        array('db' => 'program_name', 'dt' => 'program_name'),
                    );
         
        // SQL server connection information
        $sql_details = array(
            'user' => 'root',
            'pass' => '',
            'db'   => 'bca_ms',
            'host' => 'localhost'
        );
         
         
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
         
        //require( 'ssp.class.php' );

        echo json_encode(
            Serverside::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
        );
    }
}

?>