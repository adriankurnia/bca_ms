<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Server_processing extends BCA_Controller {
    public function index()
    {
        $this->load->view('employee/view2');
    }

    public function getData()
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        $this->load->library('Serverside');
        // DB table to use
        $table = 'bca_employee';
         
        // Table's primary key
        $primaryKey = 'e_nip';
         
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
                        array('db' => 'e_nip', 'dt' => 'e_nip'),
                        array('db' => 'e_name', 'dt' => 'e_name'),
                        array('db' => 'e_rfid', 'dt' => 'e_rfid'),
                        array('db' => 'e_work_unit', 'dt' => 'e_work_unit'),
                    );
         
        // SQL server connection information
        $sql_details = array(
            'user' => 'root',
            'pass' => '',
            'db'   => 'bca_ms',
            'host' => 'localhost'
        );
         
         
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
         
        //require( 'ssp.class.php' );
         
        echo json_encode(
            Serverside::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
        );
    }

}