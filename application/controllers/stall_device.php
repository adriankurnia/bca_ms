<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stall_device extends BCA_Controller {

	public $menu_log = "Perangkat Stall";

	public function index()
	{
		$this->load->model('stall_device_model');
		$data['devices'] = $this->stall_device_model->stall_device_list()->result();

		$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 9)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(9)->row();

        $this->session->unset_userdata('editing');
        $this->session->unset_userdata('posting');
		$this->load->view('stall_device/view', $data);
	}

	public function add_form()
    {
    	$this->load->model('canteen_model');
        $data['food_locations'] = $this->canteen_model->food_location()->result();

        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(9)->row();
        $this->load->view('stall_device/add_form', $data);
    }
    
    public function edit_form($uid = null)
    {
    	$this->session->unset_userdata('editing');

        if($uid == null) {
            $id = $this->uri->segment(3);
        } else {
            $id = $uid;
        }

        $this->load->model('stall_device_model');
        $this->load->model('canteen_model');
		$data['food_locations'] = $this->canteen_model->food_location()->result();
		$data['device']         = $this->stall_device_model->stall_device_data($id)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(9)->row();

        $this->load->view('stall_device/edit_form', $data);
    }

    public function display()
    {
        $id = $this->uri->segment(3);
        $this->load->model('stall_device_model');
        $this->load->model('canteen_model');
		$data['food_locations'] = $this->canteen_model->food_location()->result();
		$data['device']         = $this->stall_device_model->stall_device_data($id)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(9)->row();

        $this->load->view('stall_device/display', $data);
    }

    public function location_validation($loc_id)
    {
    	if($loc_id == 0)
    	{
    		$this->form_validation->set_message("loc_id", "Anda harus memilih lokasi stall");
    		return false;
    	}
    	else{
    		return true;
    	}
    }

    public function new_data()
    {
		$error               = 0;
		$error_msg           = "";
		$redirect            = "stall_device";
		$data['device_name'] = $_POST['device_name'];
		$data['loc_id']      = $_POST['loc_id'];
		$data['mac_addr']    = $_POST['mac_addr'];

		$this->session->set_userdata('posting', $data);

		$this->form_validation->set_rules('device_name','Nama Perangkat','required');
		$this->form_validation->set_rules('loc_id','Lokasi','required|callback_location_validation');
		$this->form_validation->set_rules('mac_addr','Mac Address Perangkat','required');
		if($this->form_validation->run())
		{
			$this->load->model('stall_device_model');

			if(preg_match('/^[a-zA-Z]+[a-zA-Z0-9._ -+]+$/', $data['device_name']))
			{
			    $error = 0;
			}
			else
			{
			    $error = 1;
			    $error_msg = "Nama Perangkat tidak boleh menggunakan simbol/spesial karakter.";
			    $redirect = "stall_device/add_form";
			}

			if($error == 0)
			{
				$data['mac_addr'] = trim(strtoupper($data['mac_addr']));

				$this->stall_device_model->insertData($data['device_name'], $data['loc_id'], $data['mac_addr']);
			
				$this->cms_log_activity("insert", $this->menu_log.": ".$data['device_name']);

				$error     = 0;
				$error_msg = "Perangkat Stall telah ditambahkan.";
			}

			$newdata = array('msg_stalldevice_list' => $error_msg, 'err_stalldevice_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal menambahkan Perangkat Stall.";
			$newdata   = array('msg_stalldevice_list' => $error_msg, 'err_stalldevice_list' => $error);
			$this->session->set_userdata($newdata);
			$this->add_form();
		}
    }

    public function edit_data()
    {
		$error               = 0;
		$error_msg           = "";
		$redirect            = "stall_device";
		$data['s_id']        = $_POST['s_id'];
		$data['device_name'] = $_POST['device_name'];
		$data['loc_id']      = $_POST['loc_id'];
		$data['mac_addr']    = $_POST['mac_addr'];

		$this->load->model('stall_device_model');
		$data['device']         = $this->stall_device_model->stall_device_data($data['s_id'])->row();
		$this->session->set_userdata('editing', $data);

		$this->form_validation->set_rules('device_name','Nama Perangkat','device_name');
		$this->form_validation->set_rules('loc_id','Lokasi','required');
		$this->form_validation->set_rules('mac_addr','Mac Address Perangkat','required');
		if($this->form_validation->run())
		{
			$old = $this->stall_device_model->stall_device_data($data['s_id'])->row();
			if($error == 0)
			{
				$this->stall_device_model->updateData($data['s_id'], $data['device_name'], $data['loc_id'], $data['mac_addr']);
			
				$this->load->model('canteen_location_model');
				$old = $this->canteen_location_model->canteen_location_data($this->session->userdata('editing')['device']->l_id)->row();

				$log_data_old = "<b>Perangkat Stall</b><br />".
								"<b>Nama</b>: ".$this->session->userdata('editing')['device']->device_name."<br /> ".
								"<b>Lokasi</b>: ".$old->l_room."(".$old->l_floor.")"."<br />".
								"<b>MAC Address</b>: ".$this->session->userdata('editing')['device']->mac_addr;

				$new = $this->canteen_location_model->canteen_location_data($data['loc_id'])->row();
				$log_data_new = "<b>Perangkat Stall</b><br />".
								"<b>Nama</b>: ".$data['device_name']."<br /> ".
								"<b>Lokasi</b>: ".$new->l_room."(".$new->l_floor.")"."<br />".
								"<b>MAC Address</b>: ".$data['mac_addr'];

				$this->cms_log_activity("edit", $log_data_old, $log_data_new);
			
				$error     = 0;
				$error_msg = "Perangkat Stall berhasil diubah.";
			}

			$this->session->unset_userdata('editing');
			$newdata = array('msg_stalldevice_list' => $error_msg, 'err_stalldevice_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal mengubah Perangkat Stall.";
			$newdata   = array('msg_stalldevice_list' => $error_msg, 'err_stalldevice_list' => $error);
			$this->session->unset_userdata('editing');
			$this->session->set_userdata($newdata);
			$this->edit_form($data['s_id']);
		}
    }

    public function remove_data()
	{
		// $id = $this->uri->segment(3);
		$id = $_POST['id'];

		$this->load->model('stall_device_model');
		$this->stall_device_model->deleteData($id);

		// $data = urldecode($this->uri->segment(4));
		$data = $_POST['data'];
		$this->cms_log_activity("delete", $this->menu_log.": ".$data);
		echo $data." telah dihapus.";

		$error = 0;
		$error_msg = $data." telah dihapus.";
		$newdata = array('msg_stalldevice_list' => $error_msg, 'err_stalldevice_list' => $error);
		$this->session->set_userdata($newdata);
		
		// redirect('stall_device');
	}
}

?>