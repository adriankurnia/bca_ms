<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cms_log extends BCA_Controller {

	public function index()
	{
		$this->load->model('cms_log_model');
		$data['logs'] = $this->cms_log_model->log_list()->result();

		$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 25)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(25)->row();
		$this->load->view('cms_log/view', $data);
	}
}

?>