<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Food_serving_holiday extends BCA_Controller {

	public $menu_log = "Aktivasi Penyajian Makanan";

	public function index()
	{
		$this->load->model('food_serving_holiday_model');
		$data['holidays'] = $this->food_serving_holiday_model->holiday_list()->result();

		$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 23)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(23)->row();

        $this->session->unset_userdata('posting');
        $this->session->unset_userdata('editing');
		$this->load->view('food_serving_holiday/view', $data);
	}

	public function add_form()
    {
    	$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(23)->row();
        $this->load->view('food_serving_holiday/add_form', $data);
    }
    
    public function edit_form($uid = null)
    {
    	$this->session->unset_userdata('editing');

        if($uid == null) {
            $id = $this->uri->segment(3);
        } else {
            $id = $uid;
        }

        $this->load->model('food_serving_holiday_model');
        $data['holiday'] = $this->food_serving_holiday_model->holiday_data($id)->row();
        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(23)->row();

        $this->load->view('food_serving_holiday/edit_form', $data);
    }

    public function display()
    {
        $id = $this->uri->segment(3);
        $this->load->model('food_serving_holiday_model');
        $data['holiday'] = $this->food_serving_holiday_model->holiday_data($id)->row();
        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(23)->row();

        $this->load->view('food_serving_holiday/display', $data);
    }

    public function new_data()
    {
		$error                  = 0;
		$error_msg              = "";
		$redirect               = "food_serving_holiday";
		$data['holiday_name']   = $_POST['holiday_name'];
		$data['holiday_start']  = $_POST['holiday_start'];
		$data['holiday_end']    = $_POST['holiday_end'];
		$data['serving_status'] = $_POST['serving_status'];

		$this->session->set_userdata('posting', $data);
		
		$this->form_validation->set_rules('holiday_name','Keterangan','required');
		$this->form_validation->set_rules('holiday_start','Tgl Awa','required');
		$this->form_validation->set_rules('holiday_end','Tgl Akhir','required');
		if($this->form_validation->run())
		{
			$this->load->model('food_serving_holiday_model');

			$explode               = explode("-", $data['holiday_start']);
			$data['holiday_start'] = $explode[2]."-".$explode[1]."-".$explode[0];

			$explode               = explode("-", $data['holiday_end']);
			$data['holiday_end']   = $explode[2]."-".$explode[1]."-".$explode[0];

			if(strtotime($data['holiday_start']) > strtotime($data['holiday_end']))
			{
				$error     = 1;
				$error_msg = "Tanggal awal lebih besar dari tanggal akhir.";
				$redirect  = "food_serving_holiday/add_form";
			}

			if($error == 0)
			{
				$this->food_serving_holiday_model->insertData($data['holiday_name'], $data['holiday_start'], $data['holiday_end'], $data['serving_status']);
				if($data['serving_status'] == 1) {
					$status = "Aktif";
				} else {
					$status = "Tidak Aktif";
				}
				$this->cms_log_activity("insert", "", $this->menu_log.":<br />".$data['holiday_start']." s.d ".$data['holiday_end']." ". $status);

				$error     = 0;
				$error_msg = "Setting Hari Libur telah ditambahkan.";
			}

			$newdata = array('msg_holiday_list' => $error_msg, 'err_holiday_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal menambahkan Setting Hari Libur.";
			$newdata   = array('msg_holiday_list' => $error_msg, 'err_holiday_list' => $error);
			$this->session->set_userdata($newdata);
			$this->add_form();
		}
    }

    public function edit_data()
    {
		$error                  = 0;
		$error_msg              = "";
		$redirect               = "food_serving_holiday";
		$data['holiday_id']     = $_POST['holiday_id'];
		$data['holiday_name']   = $_POST['holiday_name'];
		$data['holiday_start']  = $_POST['holiday_start'];
		$data['holiday_end']    = $_POST['holiday_end'];
		$data['serving_status'] = $_POST['serving_status'];

		$this->load->model('food_serving_holiday_model');
        $data['holiday'] = $this->food_serving_holiday_model->holiday_data($data['holiday_id'])->row();
        $this->session->set_userdata('editing', $data);

		$this->form_validation->set_rules('holiday_name','Keterangan','required');
		$this->form_validation->set_rules('holiday_start','Tgl Awal Libur','required');
		$this->form_validation->set_rules('holiday_end','Tgl Awal Libur','required');
		if($this->form_validation->run())
		{
			$explode               = explode("-", $data['holiday_start']);
			$data['holiday_start'] = $explode[2]."-".$explode[1]."-".$explode[0];

			$explode               = explode("-", $data['holiday_end']);
			$data['holiday_end']   = $explode[2]."-".$explode[1]."-".$explode[0];

			if(strtotime($data['holiday_start']) > strtotime($data['holiday_end']))
			{
				$error     = 1;
				$error_msg = "Tanggal awal lebih besar dari tanggal akhir.";
				$redirect  = "food_serving_holiday/edit_form/".$data['active_id'];
			}

			if($error == 0)
			{
				$this->food_serving_holiday_model->updateData($data['holiday_id'], $data['holiday_name'], $data['holiday_start'], $data['holiday_end'], $data['serving_status']);

				$explode = explode("-", $this->session->userdata('editing')['holiday']->start);
                $oldStart = $explode[2]."-".$explode[1]."-".$explode[0];

                $explode = explode("-", $this->session->userdata('editing')['holiday']->end);
                $oldEnd = $explode[2]."-".$explode[1]."-".$explode[0];

                if($this->session->userdata('editing')['holiday']->status == 1) $status = "Aktif";
                else $status = "Tidak Aktif";

				$log_data_old = "<b>Setting Hari Libur</b><br />".
								"<b>Keterangan</b>: ".$this->session->userdata('editing')['holiday']->holiday_name."<br /> ".
								"<b>Tgl Awal</b>: ".$oldStart."<br />".
								"<b>Tgl Akhir</b>: ".$oldEnd."<br />".
								"<b>Status Penyajian</b>: ".$status;

				if($data['serving_status'] == 1) $status = "Aktif";
                else $status = "Tidak Aktif";

				$log_data_new = "<b>Setting Hari Libur</b><br />".
								"<b>Keterangan</b>: ".$data['holiday_name']."<br /> ".
								"<b>Tgl Awal</b>: ".$_POST['holiday_start']."<br />".
								"<b>Tgl Akhir</b>: ".$_POST['holiday_end']."<br />".
								"<b>Status Penyajian</b>: ".$status;

				$this->cms_log_activity("edit", $log_data_old, $log_data_new);

				$error     = 0;
				$error_msg = "Setting Hari Libur berhasil diubah.";
			}

			$this->session->unset_userdata('editing');
			$newdata = array('msg_holiday_list' => $error_msg, 'err_holiday_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal mengubah Setting Hari Libur.";
			$newdata   = array('msg_holiday_list' => $error_msg, 'err_holiday_list' => $error);
			$this->session->unset_userdata('editing');
			$this->session->set_userdata($newdata);
			$this->edit_form($data['holiday_id']);
		}
    }

    public function remove_data()
	{
		// $id = $this->uri->segment(3);
		$id = $_POST['id'];

		$this->load->model('food_serving_holiday_model');
		$this->food_serving_holiday_model->deleteData($id);

		$data = $_POST['data'];
		$explode = explode("/", $data);

		$this->cms_log_activity("delete", $this->menu_log.":<br />tanggal".$explode[0]." s.d ".$explode[1]);
		echo "Setting Hari Libur tanggal ".$explode[0]." s.d ".$explode[1]." telah dihapus.";

		$error = 0;
		$error_msg = "Setting Hari Libur tanggal ".$explode[0]." s.d ".$explode[1]." telah dihapus.";
		$newdata = array('msg_holiday_list' => $error_msg, 'err_holiday_list' => $error);
		$this->session->set_userdata($newdata);
		
		// redirect('food_serving_holiday');
	}
}

?>