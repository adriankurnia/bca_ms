<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Food_serving extends BCA_Controller {

    public $menu_log = "Penyajian Makanan";

	public function index()
	{
        $data['stall_id'] = $this->uri->segment(3);
		$this->load->model('food_serving_model');
		$data['servings'] = $this->food_serving_model->food_serving_data_all($data['stall_id'])->result();

        $this->load->model('login_model');
        $data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 11)->row();

        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(11)->row();
		$this->load->view('food_serving/view', $data);
	}

	public function add_form()
    {
        $this->session->unset_userdata('editing');
        $data['stall_id'] = $this->uri->segment(3);

        $this->load->model('food_serving_model');
        if($_POST)
        {
            if($_POST['date_from'] == "" || $_POST['date_from'] == null || empty($_POST['date_from']) || $_POST['date_to'] == "" || $_POST['date_to'] == null || empty($_POST['date_to']))
            {
                $error     = 1;
                $redirect  = "food_serving/add_form/".$data['stall_id'];
                $error_msg = "Mohon untuk mengisi kedua kolom tanggal untuk melakukan filter.";
                $newdata   = array('msg_serving_list' => $error_msg, 'err_serving_list' => $error);
                $this->session->set_userdata($newdata);
                redirect($redirect);
            }

            $explode = explode("-", $_POST['date_from']);
            $start   = $explode[2]."-".$explode[1]."-".$explode[0];
            
            $explode = explode("-", $_POST['date_to']);
            $end     = $explode[2]."-".$explode[1]."-".$explode[0];

            if(strtotime($start) > strtotime($end))
            {
                $error     = 1;
                $redirect  = "food_serving/add_form/".$data['stall_id'];
                $error_msg = "Tanggal awal lebih besar dari tanggal akhir.";
                $newdata   = array('msg_serving_list' => $error_msg, 'err_serving_list' => $error);
                $this->session->set_userdata($newdata);
                redirect($redirect);
            }
            else
            {
                $start = strtotime("-3 day", strtotime($start));
                $start = date("Y-m-d", $start);

                $end = strtotime("+7 day", strtotime($end));
                $end = date("Y-m-d", $end);
            }

            $data['servings'] = $this->food_serving_model->food_serving_data($data['stall_id'], $start, $end)->result();
        }
        else
        {
            $data['servings'] = $this->food_serving_model->food_serving_data($data['stall_id'])->result();
        }

        $this->load->model('canteen_model');
        $data['food_names'] = $this->canteen_model->canteen_items_list()->result();
        
        $this->load->model('login_model');
        $data['crud'] = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 11)->row();
        
        $this->session->set_userdata('editing', $data['servings']);
        $this->load->view('food_serving/add_form', $data);
    }
    
    // public function edit_form()
    // {
    //     $id = $this->uri->segment(3);
    //     $this->load->model('food_serving_model');
    //     $data['stock'] = $this->food_serving_model->stock_item_data($id)->row();
    //     $data['food_names'] = $this->food_serving_model->food_name()->result();
    //     $this->load->view('stock/edit_form', $data);
    // }

    // public function remove_related_data()
    // {
    //     $mon  = date("m");
    //     $yer  = date("Y");
    //     $s_id = $_POST['s_id'];
    //     $this->load->model('food_serving_model');
    //     $this->food_serving_model->delete_related_data($s_id, $mon, $yer);
    // }

    public function new_serving_food()
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        //print_r($this->session->userdata('editing'));die("asd");
        $data['s_name'] = $_POST['stall_name'];
        $data['s_id']   = $_POST['stall_id'];
        $redirect       = "food_serving/index/".$data['s_id'];
        $insert_count   = count($_POST['item_id']);
        $update_count   = count($_POST['item_id_update']);

        $this->load->model('food_serving_model');
        $this->load->model('canteen_model');

        for($i=0; $i<$insert_count; $i++)
        {
            $data['c_id']       = $_POST['item_id'][$i];
            $data['stock']      = $_POST['item_stock'][$i];
            $fdate              = $_POST['start_date'][$i];
            $explode            = explode("-", $fdate);
            $data['start_date'] = $explode[2]."-".$explode[1]."-".$explode[0];
            
            $fdate              = $_POST['end_date'][$i];
            $explode            = explode("-", $fdate);
            $data['end_date']   = $explode[2]."-".$explode[1]."-".$explode[0];
            $this->food_serving_model->insert_food_serving($data['c_id'], $data['s_id'], $data['stock'], $data['start_date'], $data['end_date']);

            $canteen = $this->canteen_model->canteen_item_data($data['c_id'])->row();

            $this->cms_log_activity("insert", "", $this->menu_log.": ".$data['s_name']." <br />".ucwords($canteen->c_name)." tanggal ".$_POST['start_date'][$i]." s.d ".$_POST['end_date'][$i]);
        }

        for($i=0; $i<$update_count; $i++)
        {
            $update['id']         = $_POST['serving_id'][$i];
            $update['c_id']       = $_POST['item_id_update'][$i];
            $update['stock']      = $_POST['item_stock_update'][$i];
            $fdate                = $_POST['start_date_update'][$i];
            $explode              = explode("-", $fdate);
            $update['start_date'] = $explode[2]."-".$explode[1]."-".$explode[0];
            
            $fdate                = $_POST['end_date_update'][$i];
            $explode              = explode("-", $fdate);
            $update['end_date']   = $explode[2]."-".$explode[1]."-".$explode[0];

            $data2 = $this->food_serving_model->checkChangesData($update['id'], $update['c_id'], $update['start_date'], $update['end_date'], $update['stock'])->row();
            if(empty($data2))
            {
                $this->food_serving_model->update_stock_item($update['id'], $update['c_id'], $update['stock'], $update['start_date'], $update['end_date']);

                $oldname = $this->session->userdata('editing')[$i]->c_name;
                $explode = explode("-", $this->session->userdata('editing')[$i]->start_date);
                $oldStart = $explode[2]."-".$explode[1]."-".$explode[0];

                $explode = explode("-", $this->session->userdata('editing')[$i]->end_date);
                $oldEnd = $explode[2]."-".$explode[1]."-".$explode[0];
                $log_data_old = "<b>Penyajian Makanan: ".ucwords($data['s_name'])."</b><br />".
                                "<b>Nama</b>: ".$oldname."<br /> ".
                                "<b>Stok</b>: ".$this->session->userdata('editing')[$i]->starting_stock."<br />".
                                "<b>Tanggal Mulai</b>: ".$oldStart."<br />".
                                "<b>Tanggal Akhir</b>: ".$oldEnd;

                $new = $this->canteen_model->canteen_item_name($update['c_id'])->row();
                $log_data_new = "<b>Penyajian Makanan: ".ucwords($data['s_name'])."</b><br />".
                                "<b>Nama</b>: ".$new->c_name."<br /> ".
                                "<b>Stok</b>: ".$update['stock']."<br />".
                                "<b>Tanggal Mulai</b>: ".$_POST['start_date_update'][$i]."<br />".
                                "<b>Tanggal Akhir</b>: ".$_POST['end_date_update'][$i];

                $this->cms_log_activity("edit", $log_data_old, $log_data_new);
            }
        }

        $this->session->unset_userdata('editing');

        $error_msg = "Makanan telah ditambahkan/diedit.";
        $newdata = array('msg_serving_list' => $error_msg, 'err_serving_list' => $error);
        $this->session->set_userdata($newdata);
        redirect($redirect);
    }

    public function remove_data()
    {
        // $id = $this->uri->segment(3);
        // $stall_id = $this->uri->segment(4);
        // $data = urldecode($this->uri->segment(5));
        
        $id   = $_POST['id'];
        $data = $_POST['data'];

        $explode = explode("/", $data);

        $this->load->model('food_serving_model');
        $this->food_serving_model->delete_food_serving($id);

        $this->cms_log_activity("delete", $this->menu_log.":<br />tanggal ".$explode[0]." s.d ".$explode[1]);
        echo "Penyajian tanggal".$explode[0]." s.d ".$explode[1]." telah dihapus.";

        $error = 0;
        $error_msg = "Penyajian tanggal ".$explode[0]." s.d ".$explode[1]." telah dihapus.";
        $newdata = array('msg_serving_list' => $error_msg, 'err_serving_list' => $error);
        $this->session->set_userdata($newdata);
        
        // redirect('food_serving/add_form/'.$stall_id);
    }

    public function check_data_exist()
    {
        $row   = $_POST['row'];
        $s_id  = $_POST['s_id'];
        $c_id  = $_POST['c_id'];
        $start = $_POST['start_date'];
        $end   = $_POST['end_date'];

        $this->load->model('food_serving_model');
        $data_row = $this->food_serving_model->check_exist($s_id, $c_id, $start, $end)->num_rows();
        if($data_row > 0)
        {
            $res = $this->food_serving_model->check_exist($s_id, $c_id, $start, $end)->row();
            echo json_encode(array("num" => 1, "stall" => $res->device_name, "row" => $row));
        }
        else
        {
            echo json_encode(array("num" => 0, "stall" => ""));
        }
    }
}

?>