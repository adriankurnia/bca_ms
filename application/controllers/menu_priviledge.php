<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_priviledge extends BCA_Controller {

	public $menu_log = "Menu CMS";

	public function index()
	{
		$this->load->model('menu_priviledge_model');
		$data['m_privs'] = $this->menu_priviledge_model->menu_priv_list()->result();

		$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 5)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(5)->row();
		$this->load->view('menu_priviledge/view', $data);
	}

	public function add_form()
    {
    	$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(5)->row();
        $this->load->view('menu_priviledge/add_form', $data);
    }
    
    public function edit_form()
    {
        $id = $this->uri->segment(3);
        $this->load->model('menu_priviledge_model');
        $data['m_priv'] = $this->menu_priviledge_model->menu_priv_data($id)->row();

        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(5)->row();
        $this->load->view('menu_priviledge/edit_form', $data);
    }

    public function new_data()
    {
		$error                   = 0;
		$error_msg               = "";
		$redirect                = "menu_priviledge";
		$data['menu_name']       = $_POST['menu_name'];
		$data['controller_name'] = $_POST['controller_name'];

		$this->form_validation->set_rules('menu_name','Nama Menu','required');
		$this->form_validation->set_rules('controller_name','Nama Kontroler','required');
		if($this->form_validation->run())
		{
			$this->load->model('menu_priviledge_model');
			$this->load->model('group_priviledge_model');

			if($error == 0)
			{
				$menu_priv_id = $this->menu_priviledge_model->insertData($data['menu_name'], $data['controller_name']);
				$groups = $this->group_priviledge_model->group_priv_list()->result();
				$fulldate = date('Y-m-d H:i:s');
				foreach ($groups as $group)
				{
					if($group->id == 1)
					{
						$data = array(
							'menu_priv_id'	=> $menu_priv_id,
							'group_priv_id' => $group->id,
							'create'		=> 1,
							'read'			=> 1,
							'update'		=> 1,
							'delete'		=> 1,
							'created_date'  => $fulldate,
							'created_by'    => $this->session->userdata('username')
						);
					}
					else
					{
						$data = array(
							'menu_priv_id'	=> $menu_priv_id,
							'group_priv_id' => $group->id,
							'created_date'  => $fulldate,
							'created_by'    => $this->session->userdata('username')
						);
					}
					$this->db->insert('bca_group_menu_priviledge', $data);
				}

				$error     = 0;
				$error_msg = "Menu CMS telah ditambahkan.";
			}

			$newdata = array('msg_menupriv_list' => $error_msg, 'err_menupriv_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal menambahkan Menu CMS.";
			$newdata   = array('msg_menupriv_list' => $error_msg, 'err_menupriv_list' => $error);
			$this->session->set_userdata($newdata);
			$this->add_form();
		}
    }

    public function edit_data()
    {
		$error                   = 0;
		$error_msg               = "";
		$redirect                = "menu_priviledge";
		$data['menu_id']         = $_POST['menu_id'];
		$data['menu_name']       = $_POST['menu_name'];
		$data['controller_name'] = $_POST['controller_name'];

		$this->form_validation->set_rules('menu_name','Nama Menu','required');
		$this->form_validation->set_rules('controller_name','Nama Kontroler','required');
		if($this->form_validation->run())
		{
			$this->load->model('menu_priviledge_model');

			if($error == 0)
			{
				$this->menu_priviledge_model->updateData($data['menu_id'], $data['menu_name'], $data['controller_name']);
			
				$error     = 0;
				$error_msg = "Menu CMS berhasil diubah.";			}

			$newdata = array('msg_menupriv_list' => $error_msg, 'err_menupriv_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal mengubah Menu CMS.";
			$newdata   = array('msg_menupriv_list' => $error_msg, 'err_menupriv_list' => $error);
			$this->session->set_userdata($newdata);
			$this->edit_form();
		}
    }

	public function remove_data()
	{
		$id = $_POST['id'];

		$this->load->model('menu_priviledge_model');
		$this->menu_priviledge_model->deleteData($id);

		$this->db->where('menu_priv_id', $id);
		$this->db->delete('bca_group_menu_priviledge');

		$data = $_POST['data'];
		$this->cms_log_activity("delete", $this->menu_log.": ".$data);
		echo $data." telah dihapus.";

		$error = 0;
		$error_msg = $data." telah dihapus.";
		$newdata = array('msg_menupriv_list' => $error_msg, 'err_menupriv_list' => $error);
		$this->session->set_userdata($newdata);
	}
}

?>