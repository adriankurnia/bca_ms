<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Canteen extends BCA_Controller {

    public $menu_log = "Menu Makanan";

	public function index()
	{
		$this->load->model('canteen_model');
		$data['canteens'] = $this->canteen_model->canteen_items_list()->result();

        $this->load->model('login_model');
        $data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 3)->row();

        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(3)->row();
		
        $this->session->unset_userdata('editing');
        $this->session->unset_userdata('posting');
        $this->load->view('canteen/view', $data);
	}

	public function add_form()
    {
        $this->load->model('canteen_model');
        $data['food_types']     = $this->canteen_model->food_type()->result();
        $data['food_locations'] = $this->canteen_model->food_location()->result();

        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(3)->row();
        $this->load->view('canteen/add_form', $data);
    }
    
    public function edit_form($uid = null)
    {
        $this->session->unset_userdata('editing');
        
        if($uid == null) {
            $id = $this->uri->segment(3);
        } else {
            $id = $uid;
        }

        $this->load->model('canteen_model');
        $data['canteen']        = $this->canteen_model->canteen_item_data($id)->row();
        $data['food_types']     = $this->canteen_model->food_type()->result();
        $data['food_locations'] = $this->canteen_model->food_location()->result();

        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(3)->row();

        $this->load->model('buffet_detail_model');
        $data['b_details'] = $this->buffet_detail_model->buffet_detail_data($id)->result();

        $this->load->view('canteen/edit_form', $data);
    }

    public function display()
    {
        $id = $this->uri->segment(3);
        $this->load->model('canteen_model');
        $data['canteen']        = $this->canteen_model->canteen_item_data($id)->row();
        $data['food_types']     = $this->canteen_model->food_type()->result();
        $data['food_locations'] = $this->canteen_model->food_location()->result();

        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(3)->row();

        $this->load->model('buffet_detail_model');
        $data['b_details'] = $this->buffet_detail_model->buffet_detail_data($id)->result();

        $this->load->view('canteen/display', $data);
    }

    public function type_validation($item_type)
    {
        if($item_type == 0)
        {
            $this->form_validation->set_message("type_validation", "Anda harus memilih Tipe Makanan");
            return false;
        }
        else{
            return true;
        }
    }

    public function sb_validation($item_place)
    {
        if($item_place == 0)
        {
            $this->form_validation->set_message("sb_validation", "Anda harus memilih Stall/Buffet");
            return false;
        }
        else{
            return true;
        }
    }

    public function new_canteen_item()
    {
        $error                 = 0;
        $error_msg             = "";
        $redirect              = "canteen";
        $item['item_name']     = strtolower(trim($_POST['item_name']));
        $item['item_calory']   = $_POST['item_calory'];
        $item['item_fat']      = $_POST['item_fat'];
        $item['item_protein']  = $_POST['item_protein'];
        $item['item_price']    = $_POST['item_price'];
        $item['item_type']     = $_POST['item_type'];
        $item['item_place']    = $_POST['item_place'];
        $file['item_image']    = $_FILES['canteenImage'];

        $this->session->set_userdata('posting', $item);

        $this->form_validation->set_rules('item_name','Nama Makanan','required');
        $this->form_validation->set_rules('item_calory','Kalori','required');
        $this->form_validation->set_rules('item_price','Harga','required');
        $this->form_validation->set_rules('item_type','Tipe Makanan','required|callback_type_validation');
        //$this->form_validation->set_rules('item_place','Stall/Buffet','required|callback_sb_validation');
        if(empty($_FILES['canteenImage']['name']))
        {
            $this->form_validation->set_rules('canteenImage', 'Gambar Makanan', 'required');
        }

        if($this->form_validation->run())
        {
            $this->load->model('canteen_model');          

            $file_name               = preg_replace('/\s+/', '_', $item['item_name']);
            $uploadPath              = 'assets/upload/';
            $config['upload_path']   = $uploadPath;
            $config['allowed_types'] = 'jpg|jpeg|png|bmp';
            $config['file_name']     = $file_name;
            $config['overwrite']     = true;
            $config['max_size']      = 500;
            $config['max_width']     = 1366;
            $config['max_height']    = 768;
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if($this->upload->do_upload('canteenImage'))
            {
                $fileData            = $this->upload->data();
                $upload['file_name'] = $fileData['file_name'];
                $upload['created']   = date("Y-m-d H:i:s");
                $upload['modified']  = date("Y-m-d H:i:s");
                $this->resize_img("assets/food/".$upload['file_name'], 500, 450, "assets/food/resize/".$upload['file_name']);
            }
            else
            {
                $error = 1;
                $error_msg = array('error' => $this->upload->display_errors());
                $error_msg = $error_msg['error'];
                $redirect = "canteen/add_form";
                //$error = array('error' => $this->upload->display_errors());
                //echo "<pre>";print_r($error);echo "</pre>";
            }

            if($error == 0)
            {
                $id = $this->canteen_model->insert_canteen_item($item['item_name'], $item['item_calory'], $item['item_type'], $item['item_place'], $item['item_price'], $upload['file_name'], $item['item_fat'], $item['item_protein']);
                
                $this->cms_log_activity("insert", "", $this->menu_log.": ".$item['item_name']);

                //if($_POST['bItem'] != "" || $_POST['bItem'] != null || !empty($_POST['bItem']))
                if(count($_POST['bItem']) == 1 && $_POST['bItem'][0] == "")
                {

                }
                else
                {
                    $insert_count = count($_POST['bItem']);

                    $this->load->model('buffet_detail_model');
                    for($i=0; $i<$insert_count; $i++)
                    {
                        $data['bItem'] = $_POST['bItem'][$i];
                        
                        $this->buffet_detail_model->insertData($id, $data['bItem']);
                    }
                }

                $error     = 0;
                $error_msg = "Makanan telah ditambahkan.";
            }

            $newdata = array('msg_canteen_list' => $error_msg, 'err_canteen_list' => $error);
            $this->session->set_userdata($newdata);
            redirect($redirect);
        }
        else
        {
            $error     = 1;
            $error_msg = "Gagal menambahkan makanan.";
            $newdata   = array('msg_canteen_list' => $error_msg, 'err_canteen_list' => $error);
            $this->session->set_userdata($newdata);
            $this->add_form();
        }
    }

    public function edit_canteen_item()
    {
        // echo "<pre>";
        // print_r($_POST);
        // echo "</pre>";die;
        $error                 = 0;
        $error_msg             = "";
        $redirect              = "canteen";
        $item['item_id']       = $_POST['item_id'];
        $item['item_name']     = strtolower(trim($_POST['item_name']));
        $item['item_calory']   = $_POST['item_calory'];
        $item['item_fat']      = $_POST['item_fat'];
        $item['item_protein']  = $_POST['item_protein'];
        $item['item_price']    = $_POST['item_price'];
        $item['item_type']     = $_POST['item_type'];
        $item['item_place']    = $_POST['item_place'];
        $item['item_image']    = $_FILES['canteenImage'];

        $this->load->model('canteen_model');
        $data['canteen'] = $this->canteen_model->canteen_item_data($item['item_id'])->row();
        $this->session->set_userdata('editing', $data);

        $this->form_validation->set_rules('item_name','Nama Makanan','required');
        $this->form_validation->set_rules('item_calory','Kalori','required');
        $this->form_validation->set_rules('item_price','Harga','required');

        if($this->form_validation->run())
        {
            if($item['item_type'] == 0)
            {
                $error = 1;
                $error_msg = "Anda harus memilih Tipe Makanan.";
                $redirect = "canteen/edit_form/".$item['item_id'];
            }

            if($item['item_type'] == 1)
            {
                if($item['item_place'] == 0)
                {
                    $error = 1;
                    $error_msg = "Anda harus memilih tempat hidangan utama.";
                    $redirect = "canteen/edit_form/".$item['item_id'];
                }
            }

            if($item['item_image']['error'] == 0)
            {
                $file_name               = preg_replace('/\s+/', '_', $item['item_name']);
                $uploadPath              = 'assets/food/';
                $config['upload_path']   = $uploadPath;
                $config['allowed_types'] = 'jpg|jpeg|png|bmp';
                $config['file_name']     = $file_name;
                $config['overwrite']     = true;
                $config['max_size']      = 500;
                $config['max_width']     = 1366;
                $config['max_height']    = 768;
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('canteenImage'))
                {
                    $fileData            = $this->upload->data();
                    $upload['file_name'] = $fileData['file_name'];
                    $upload['created']   = date("Y-m-d H:i:s");
                    $upload['modified']  = date("Y-m-d H:i:s");
                    $this->resize_img("assets/food/".$upload['file_name'], 500, 450, "assets/food/resize/".$upload['file_name']);
                }
                else
                {
                    $error = 1;
                    $error_msg = array('error' => $this->upload->display_errors());
                    $error_msg = $error_msg['error'];
                    $redirect = "canteen/add_form";
                    //$error = array('error' => $this->upload->display_errors());
                    //echo "<pre>";print_r($error);echo "</pre>";
                }
            } 

            if($error == 0)
            {
                $old = $this->canteen_model->canteen_item_data($item['item_id'])->row();

                if($item['item_image']['error'] == 0)
                {
                    $this->canteen_model->update_canteen_item($item['item_id'] , $item['item_name'], $item['item_calory'], $item['item_type'], $item['item_place'], $item['item_price'], $item['item_fat'], $item['item_protein'], $upload['file_name']);
                }
                else
                {
                    $this->canteen_model->update_canteen_item($item['item_id'] , $item['item_name'], $item['item_calory'], $item['item_type'], $item['item_place'], $item['item_price'], $item['item_fat'], $item['item_protein']);
                }

                if($this->session->userdata('editing')['canteen']->buffet == 1) $place = "Buffet";
                else $place = "Stall";

                $log_data_old = "<b>Menu Makanan</b><br />".
                                "<b>Nama</b>: ".$this->session->userdata('editing')['canteen']->c_name."<br /> ".
                                "<b>Kalori</b>: ".$this->session->userdata('editing')['canteen']->c_calory."<br />".
                                "<b>Tipe</b>: ".$this->session->userdata('editing')['canteen']->f_type."<br />".
                                "<b>Buffet/Stall</b>: ".$place."<br />".
                                "<b>Harga</b>: ".$this->session->userdata('editing')['canteen']->c_price;

                $new = $this->canteen_model->food_type_data($item['item_type'])->row();

                if($item['item_place'] == 1) $place = "Buffet";
                else $place = "Stall";
                $log_data_new = "<b>Menu Makanan</b><br />".
                                "<b>Nama</b>: ".$item['item_name']."<br /> ".
                                "<b>Kalori</b>: ".$item['item_calory']."<br />".
                                "<b>Tipe</b>: ".$new->f_type."<br />".
                                "<b>Buffet/Stall</b>: ".$place."<br />".
                                "<b>Harga</b>: ".$item['item_price'];

                $this->cms_log_activity("edit", $log_data_old, $log_data_new);

                $insert_count = count($_POST['bItem']);
                $update_count = count($_POST['bItem_id']);

                $this->load->model('buffet_detail_model');
                for($i=0; $i<$insert_count; $i++)
                {
                    $data['bItem'] = $_POST['bItem'][$i];
                    
                    $this->buffet_detail_model->insertData($item['item_id'], $data['bItem']);
                }

                for($i=0; $i<$update_count; $i++)
                {
                    $update['id']           = $_POST['bItem_id'][$i];
                    $update['bItem_update'] = $_POST['bItem_update'][$i];
                    
                    if($update['bItem_update'] == "" || empty($update['bItem_update']) || $update['bItem_update'] == null)
                    {
                        $this->buffet_detail_model->deleteData($update['id']);
                    }
                    else
                    {
                        $this->buffet_detail_model->updateData($update['id'], $item['item_id'], $update['bItem_update']);
                    }
                    
                }

                $error     = 0;
                $error_msg = "Makanan berhasil diubah.";
            }

            $this->session->unset_userdata('editing');
            $newdata = array('msg_canteen_list' => $error_msg, 'err_canteen_list' => $error);
            $this->session->set_userdata($newdata);
            redirect($redirect);
        }
        else
        {
            $error     = 1;
            $error_msg = "Gagal mengubah makanan.";
            $newdata   = array('msg_canteen_list' => $error_msg, 'err_canteen_list' => $error);
            $this->session->unset_userdata('editing');
            $this->session->set_userdata($newdata);
            $this->edit_form($item['item_id']);
        }
    }

    public function remove_canteen_item()
    {
        // $id = $this->uri->segment(3);
        // $item_name = urldecode($this->uri->segment(4));

        $id = $_POST['id'];
        $item_name = ucwords($_POST['data']);

        $this->load->model('canteen_model');
        $this->canteen_model->delete_canteen_item($id);

        $this->cms_log_activity("delete", $this->menu_log.": ".$item_name);
        echo $item_name." telah dihapus.";

        $error = 0;
        $error_msg = $item_name." telah dihapus.";
        $newdata = array('msg_canteen_list' => $error_msg, 'err_canteen_list' => $error);
        $this->session->set_userdata($newdata);
        
        // redirect('canteen');
    }

    public function resize_img($source_path, $width, $height, $new_path)
    {
        $this->load->library('image_lib');
        $config['image_library']    = 'gd2';
        $config['source_image']     = $source_path;
        $config['new_image']        = $new_path;
        $config['maintain_ratio']   = false;
        $config['width']            = $width;
        $config['height']           = $height;

        $this->image_lib->clear();      
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }
}

?>