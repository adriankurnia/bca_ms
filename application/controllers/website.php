<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Website extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->server_date = date('Y-m-d');
        //$this->server_date = "2017-11-27";
    }

	public function index()
	{
        $list_location = $this->db->where('deleted',0)->order_by('l_floor', 'ASC')->get('bca_canteen_location');
        if($list_location->num_rows() > 0)
        {
            $data['list_location'] = $list_location->result();
        }

		$this->load->view('website/view', $data);
	}
    public function review(){
        $data['a'] = NULL;
        $this->load->view('website/review', $data);

    }
    public function location($location_id = null)
    {
        $list_location = $this->db->where('deleted',0)->order_by('l_floor', 'ASC')->get('bca_canteen_location');
        if($list_location->num_rows() > 0)
        {
            $data['list_location'] = $list_location->result();
        }

        if($location_id == null)
        {
            redirect('website');
        }
        else
        {
            $location = $this->db->where('l_id', $location_id)->get('bca_canteen_location');
            if($location->num_rows() > 0)
            {
                $data['location'] = strtoupper($location->row()->l_floor . " - " . $location->row()->l_room);

                $this->db->select('bca_daily_stock.id, bca_canteen.c_id, c_name, device_name, l_id, stock');
                $this->db->from('bca_daily_stock');
                $this->db->join('bca_canteen', 'bca_canteen.c_id = bca_daily_stock.c_id');
                $this->db->join('bca_stall_device', 'bca_stall_device.s_id = bca_daily_stock.s_id');
                $this->db->join('bca_canteen_location', 'bca_canteen_location.l_id = bca_stall_device.loc_id');
                $this->db->where('l_id', $location_id);
                $this->db->where('trx_date', $this->server_date);
                $this->db->order_by('bca_daily_stock.s_id', 'ASC');
                $list_foods = $this->db->get();

                if($list_foods->num_rows() > 0)
                {
                    $data['ok'] = 1;
                    $data['list_foods'] = $list_foods->result();
                } 
                else
                {
                    $data['ok'] = 0;
                    $data['message'] = "Tidak Ada Makanan Tersedia Hari Ini.";
                }
            }
            else
            {
                redirect('website');
            }
        }

        $this->load->view('website/location_view', $data);
    }

    public function location_slide()
    {
        $locations = $this->db->get('bca_canteen_location');
        if($locations->num_rows() > 0)
        {
            $locations = $locations->result();
            foreach($locations as $location)
            {
                $loc = strtoupper($location->l_room." (lt. ".$location->l_floor.")");
                if(empty($data['location']))
                {
                     $data['location'] = $loc;
                }

                $data['list_location'] = $locations;

                if($location->l_id != 3)
                {
                    $this->db->select('bca_daily_stock.id, bca_canteen.c_id, c_name, device_name, l_id, stock');
                    $this->db->from('bca_daily_stock');
                    $this->db->join('bca_canteen', 'bca_canteen.c_id = bca_daily_stock.c_id');
                    $this->db->join('bca_stall_device', 'bca_stall_device.s_id = bca_daily_stock.s_id');
                    $this->db->join('bca_canteen_location', 'bca_canteen_location.l_id = bca_stall_device.loc_id');
                    $this->db->where('l_id', $location->l_id);
                    $this->db->where('trx_date', $this->server_date);
                    $this->db->where('buffet !=', 1);
                    $this->db->order_by('bca_daily_stock.s_id', 'ASC');

                    $list_foods = $this->db->get();

                    if($list_foods->num_rows() > 0)
                    {
                        $data['lists'][] = array('location' => $loc, 'list_foods' => $list_foods->result());
                    } 
                    else
                    {
                        $data['lists'][] = array('location' => $loc, 'list_foods' => array());
                    }
                }
                else
                {
                    //Buffet
                    $this->db->select('bca_daily_stock.id, bca_canteen.c_id, c_name, device_name, l_id, stock');
                    $this->db->from('bca_daily_stock');
                    $this->db->join('bca_canteen', 'bca_canteen.c_id = bca_daily_stock.c_id');
                    $this->db->join('bca_stall_device', 'bca_stall_device.s_id = bca_daily_stock.s_id');
                    $this->db->join('bca_canteen_location', 'bca_canteen_location.l_id = bca_stall_device.loc_id');
                    $this->db->join('bca_buffet_detail', 'bca_buffet_detail.c_id = bca_canteen.c_id');
                    $this->db->where('l_id', $location->l_id);
                    $this->db->where('trx_date', $this->server_date);
                    $this->db->where('buffet', 1);
                    $this->db->group_by('c_name');
                    $this->db->order_by('bca_daily_stock.s_id', 'ASC');

                    $list_buffets = $this->db->get();

                    if($list_buffets->num_rows() > 0)
                    {
                        $data['buffets'] = $list_buffets->row()->c_name;
                        $data['c_id'] = $list_buffets->row()->c_id;
                        $data['stocks'] = $list_buffets->result();
                        $data['lists_foods'][] = $this->db->where('c_id', $list_buffets->row()->c_id)->get('bca_buffet_detail')->result();
                        //var_dump($data['lists_foods']);
                    } 
                    else
                    {
                        $data['c_id'] = 0;
                        $data['lists_foods'][] = array();
                    }
                }
            }
        }

        $this->load->view('website/location_slide', $data);
    }

    public function location_buffet($location_id = null)
    {
        $list_location = $this->db->where('deleted',0)->order_by('l_floor', 'ASC')->get('bca_canteen_location');
        if($list_location->num_rows() > 0)
        {
            $data['list_location'] = $list_location->result();
        }

        $location = $this->db->where('l_id', $location_id)->get('bca_canteen_location');
        if($location->num_rows() > 0)
        {
            $data['location'] = strtoupper($location->row()->l_floor . " - " . $location->row()->l_room);

            $this->db->select('bca_daily_stock.id, bca_canteen.c_id, c_name, device_name, l_id, stock');
            $this->db->from('bca_daily_stock');
            $this->db->join('bca_canteen', 'bca_canteen.c_id = bca_daily_stock.c_id');
            $this->db->join('bca_stall_device', 'bca_stall_device.s_id = bca_daily_stock.s_id');
            $this->db->join('bca_canteen_location', 'bca_canteen_location.l_id = bca_stall_device.loc_id');
            $this->db->join('bca_buffet_detail', 'bca_buffet_detail.c_id = bca_canteen.c_id');
            $this->db->where('l_id', $location->row()->l_id);
            $this->db->where('trx_date', $this->server_date);
            $this->db->where('buffet', 1);
            $this->db->group_by('c_name');
            $this->db->order_by('bca_daily_stock.s_id', 'ASC');
            $list_buffets = $this->db->get();
            die(print_r($list_buffets->row()));
            if($list_buffets->num_rows() > 0)
            {
                $data['buffets'] = $list_buffets->row()->c_name;
                $data['c_id'] = $list_buffets->row()->c_id;
                $data['stock'] = $list_buffets->row()->stock;
                $data['lists_foods'][] = $this->db->where('c_id', $list_buffets->row()->c_id)->get('bca_buffet_detail')->result();
            } 
            else
            {
                $data['lists_foods'][] = array();
            }
        }

        $this->load->view('website/location_buffet', $data);
    }

    public function refresh_page($location_id = null)
    {
        $this->db->select('bca_daily_stock.id, bca_canteen.c_id, stock, l_id');
        $this->db->from('bca_daily_stock');
        $this->db->join('bca_canteen', 'bca_canteen.c_id = bca_daily_stock.c_id');
        $this->db->join('bca_stall_device', 'bca_stall_device.s_id = bca_daily_stock.s_id');
        $this->db->join('bca_canteen_location', 'bca_canteen_location.l_id = bca_stall_device.loc_id');
        if($location_id  != null)
        {
            $this->db->where('l_id', $location_id);
        }
        $this->db->where('trx_date', $this->server_date);
        $this->db->order_by('bca_daily_stock.s_id', 'ASC');
        $list_foods = $this->db->get();

        $total_buffet = 0;
        foreach($list_foods->result() as $food)
        {
            if($food->l_id == 3)
            {
                $total_buffet += $food->stock;
            }
        }

        $return = array('ok' => 1, 'data' => $list_foods->result(), 'total_buffet' => $total_buffet, 'message' => 'Found');

        echo json_encode($return);
    }
    
    public function gettaprfid(){
        $this->load->model('general_model');

         $rfid= $this->input->post('rfid');

         $getUserDetail = $this->general_model->select_data_detail2('bca_employee','e_rfid',$rfid,'');
         if($getUserDetail){
             $this->data['getUserDetail']  = $getUserDetail;


             $this->data['resultMakanan'] =   $this->general_model->getMakananByTrx($rfid);
             $content = $this->load->view('website/ajaxrfid',$this->data,TRUE);

            $result = array('result'=>1,'content'=>$content);
         }else{
            $result = array('result'=>0,'message'=>'RFID anda tidak terdaftar pada sistem kami');
         }
         echo json_encode($result);
    }
}

?>
