<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends BCA_Controller {

    /**
     * Load Construct
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('dashboard_model');
          $this->load->model('general_model');
    }
    public function gettotaltapping(){
        $result = array('totalBerhasil'=>$this->dashboard_model->getTotalTrx(1),'totalGagal'=>$this->dashboard_model->getTotalTrx(0));
        echo json_encode($result,TRUE);
    }
    public function getmakananfavorite(){
        $data = $this->dashboard_model->getMakananLaris();

        $labelArray = array();
        $valueArray = array();
        if($data)foreach($data as $list){
            $labelArray[] = ucwords($list->c_name);
            $valueArray[] = $list->total;
        }
        $valueArray = array($valueArray);

        $result = array('label'=>$labelArray,'value'=>$valueArray);
        echo json_encode($result,TRUE);

    }
    public function gettotalmakan(){
         $totalMakan = $this->dashboard_model->getTotalMakan();
        $result = array('total'=>$totalMakan);
        echo json_encode($result,TRUE);
    }
    function getstockbylantai(){
        $data = $this->dashboard_model->getStockByLantai();
        $food_type = $this->dashboard_model->getFoodType();
        $getLantai = $this->dashboard_model->getLantai();
        $label  = array();
        $getDevice = $this->general_model->select_data('bca_stall_device','device_name','asc');
       // pre($getLantai);
        if($getDevice)foreach($getDevice as $list){
            $label[]= $list->device_name;
        }
        $no=0;
        // $colour = array('rgb(255,160,122)','rgb(135,206,235)','rgb(218,165,32)','rgb(50,205,50)','rgb(220,20,60)','rgb(199,0,133)','rgb(11,21,133)','rgb(199,21,10)','rgb(199,21,20)','rgb(199,21,43)','rgb(44,21,12)','rgb(31,21,22)','rgb(199,21,50)','rgb(60,21,133)','rgb(199,21,41)','rgb(199,31,133)','rgb(199,10,133)','rgb(199,9,133)','rgb(5,21,133)','rgb(55,66,161)','rgb(2,21,77)','rgb(10,41,41)','rgb(2,21.111)','rgb(2,21,211)','rgb(2,21,33)','rgb(92,21,133)','rgb(99,21,1)','rgb(2,21,99)','rgb(2,41,1)','rgb(2,0,213)');

        $simpenArray = array();
        $simpenArray2 = array();
        $getBcaFoodServing = $this->dashboard_model->getBcaFoodServing(0);
        if($getBcaFoodServing)foreach($getBcaFoodServing as $list2){
            $dataArrayLantai= array();
             $simpenArray['label'] = $list2->makanan_name;
               $simpenArray['backgroundColor'] = 'rgb('.mt_rand( 0, 255 ).','.mt_rand( 0, 255 ).','.mt_rand( 0, 255 ).')';
                    if($getDevice)foreach($getDevice as $list){
                        $dataArrayLantai[] = isset($data["totalMakanan_".$list->s_id.'_'.make_alias($list2->makanan_name)])? $data["totalMakanan_".$list->s_id.'_'.make_alias($list2->makanan_name)] : 0;
                        //    echo $list->.'<br>'.$list->s_id.'_'.make_alias($list2->makanan_name)."<br>";
                     }
                $simpenArray['data'] = $dataArrayLantai;
                                 $no++;
                $simpenArray2[] = $simpenArray;

            //$simpenArray2[]=$simpenArray;
        }



        $result = array('label'=>$label,'data'=>$simpenArray2);
        echo json_encode($result,TRUE);
    }
     function getgrafikbiaya(){
         $date = date('Y-m-d');
         $monday = date( 'Y-m-d', strtotime('-6 day', strtotime($date)));
         $sunday = $date;
         $dateLabel = $this->getPeriode($monday,$sunday);
         $price = 0;
         $valueArray = array();
         if($dateLabel)foreach($dateLabel as $list){
             $getDataJualan = $this->dashboard_model->getMakananLarisHarga($list);
             if($getDataJualan){
                 $valueArray[]=$getDataJualan;
             }else{
                 $valueArray[] = 0;
             }
         }

        $result = array('label'=>$dateLabel,'value'=>$valueArray);
        echo json_encode($result,TRUE);
    }
     public function getPeriode($start_date,$end_date)
    {
        $begin = new DateTime($start_date);
        $end = new DateTime($end_date);

        $daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
        $newDataArray =  array();
        foreach($daterange as $date){
            $newDataArray[] = $date->format("d-m-Y");
        }
        $newDataArray[] = date('d-m-Y',strtotime($end_date));
        return $newDataArray;
    }

    public function getBar(){

          $this->data['experMode'] =  $experMode = $this->general_model->select_data('ExperienceMode','ExperienceName','asc');
        $jsonData = array();

        $datasets2 = array();
        $datasets = array();
        $no=0;
        $colour = array('rgb(255,160,122)','rgb(135,206,235)','rgb(218,165,32)','rgb(50,205,50)','rgb(220,20,60)','rgb(199,21,133)');
        if($experMode)foreach($experMode as $list){
            $datasets2['label'] = $list->ExperienceName;
            $datasets2['data'] = array($this->dashboard_model->getExperienceDetailByExperienceMode($list->ExperienceModeId));
            $datasets2['backgroundColor'] = $colour[$no];
            $datasets[] = $datasets2;
            $no++;
        }
        $jsonData= $datasets;
        echo json_encode($jsonData,TRUE);

    }
    public function getLastActivity(){
        $start_date = '';$end_date='';$ExperienceModeId=0;
         $this->data['data'] =  $this->dashboard_model->getExperienceDetail($start_date,$end_date,$ExperienceModeId);
        $this->load->view('ajax/last_activity',$this->data);
    }

}

/* End of file ajax.php */
/* Location: ./application/modules/admin/controllers/ajax.php */
