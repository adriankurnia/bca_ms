<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Canteen_location extends BCA_Controller {

    public $menu_log = "Lokasi";

	public function index()
	{
		$this->load->model('canteen_location_model');
		$data['clocations'] = $this->canteen_location_model->canteen_location_list()->result();

        $this->load->model('login_model');
        $data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 2)->row();

        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(2)->row();
		
        $this->session->unset_userdata('editing');
        $this->session->unset_userdata('posting');
        $this->load->view('canteen_location/view', $data);
	}
    
    public function add_form()
    {
        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(2)->row();
        $this->load->view('canteen_location/add_form', $data);
    }

    public function edit_form($uid = null)
    {
        $this->session->unset_userdata('editing');

        if($uid == null) {
            $id = $this->uri->segment(3);
        } else {
            $id = $uid;
        }

        $this->load->model('canteen_location_model');
        $data['clocation'] = $this->canteen_location_model->canteen_location_data($id)->row();

        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(2)->row();

        
        $this->load->view('canteen_location/edit_form', $data);
    }

    public function display()
    {
        $id = $this->uri->segment(3);
        $this->load->model('canteen_location_model');
        $data['clocation'] = $this->canteen_location_model->canteen_location_data($id)->row();

        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(2)->row();

        $this->load->view('canteen_location/display', $data);
    }

    public function new_canteen_location()
    {
        $error         = 0;
        $error_msg     = "";
        $redirect      = "canteen_location";
        $cloc['floor'] = $_POST['floor'];
        $cloc['room']  = $_POST['room'];
        
        $this->form_validation->set_rules('floor','Lantai','required');
        $this->form_validation->set_rules('room','Ruangan','required');

        $this->session->set_userdata('posting', $cloc);

        if($this->form_validation->run())
        {
            $this->load->model('canteen_location_model');

            if($error == 0)
            {
                $this->canteen_location_model->insert_canteen_location($cloc['room'], $cloc['floor']);

                $this->cms_log_activity("insert", "", $this->menu_log.": ".$cloc['room']."(".$cloc['floor'].")");
            
                $error     = 0;
                $error_msg = "Lokasi telah ditambahkan";
            }

            $newdata = array('msg_clocation_list' => $error_msg, 'err_clocation_list' => $error);
            $this->session->set_userdata($newdata);
            redirect($redirect);
        }
        else
        {
            $error     = 1;
            $error_msg = "Gagal menambahkan lokasi";
            $newdata   = array('msg_clocation_list' => $error_msg, 'err_clocation_list' => $error);
            $this->session->set_userdata($newdata);
            $this->add_form();
        }
    }

    public function edit_canteen_location()
    {
        $error         = 0;
        $error_msg     = "";
        $redirect      = "canteen_location";
        $cloc['id']    = $_POST['id'];
        $cloc['floor'] = $_POST['floor'];
        $cloc['room']  = $_POST['room'];

        $this->load->model('canteen_location_model');
        $data['clocation'] = $this->canteen_location_model->canteen_location_data($cloc['id'])->row();
        $this->session->set_userdata('editing', $data);

        $this->form_validation->set_rules('floor','Floor','required');
        $this->form_validation->set_rules('room','Room','required');


        if($this->form_validation->run())
        {
            $this->load->model('canteen_location_model');

            $old = $this->canteen_location_model->canteen_location_data($cloc['id'])->row();

            if($error == 0)
            {
                $this->canteen_location_model->update_canteen_location($cloc['id'], $cloc['room'], $cloc['floor']);

                $log_data_old = "<b>Lokasi</b><br />".
                                "<b>Ruangan</b>: ".$this->session->userdata('editing')['clocation']->l_room."<br /> ".
                                "<b>Lantai</b>: ".$this->session->userdata('editing')['clocation']->l_floor;
                                
                $log_data_new = "<b>Lokasi</b><br />".
                                "<b>Ruangan</b>: ".$cloc['room']."<br /> ".
                                "<b>Lantai</b>: ".$cloc['floor'];

                $this->cms_log_activity("edit", $log_data_old, $log_data_new);
            
                $error     = 0;
                $error_msg = "Lokasi berhasil diubah";
            }

            $this->session->unset_userdata('editing');
            $newdata = array('msg_clocation_list' => $error_msg, 'err_clocation_list' => $error);
            $this->session->set_userdata($newdata);
            redirect($redirect);
        }
        else
        {
            $error     = 1;
            $error_msg = "Gagal mengubah lokasi";
            $newdata   = array('msg_clocation_list' => $error_msg, 'err_clocation_list' => $error);
            $this->session->unset_userdata('editing');
            $this->session->set_userdata($newdata);
            $this->edit_form($cloc['id']);
        }
    }

    public function remove_canteen_location()
    {
        // $id = $this->uri->segment(3);
        // $data = urldecode($this->uri->segment(4));
        
        $id = $_POST['id'];
        $data = $_POST['data'];

        $this->load->model('canteen_location_model');
        $this->canteen_location_model->delete_canteen_location($id);

        $this->cms_log_activity("delete", $this->menu_log.": ".$data);
        echo $data." telah dihapus.";
        
        $error = 0;
        $error_msg = $data." telah dihapus";
        $newdata = array('msg_clocation_list' => $error_msg, 'err_clocation_list' => $error);
        $this->session->set_userdata($newdata);
        
        // redirect('canteen_location');
    }
}

?>