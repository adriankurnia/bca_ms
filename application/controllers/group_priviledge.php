<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Group_priviledge extends BCA_Controller {

	public $menu_log = "Grup Admin";

	public function index()
	{
		$this->load->model('group_priviledge_model');
		$data['g_privs'] = $this->group_priviledge_model->group_priv_list()->result();

		$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 6)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(6)->row();
		$this->load->view('group_priviledge/view', $data);
	}

	public function add_form()
    {
    	$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(6)->row();
        $this->load->view('group_priviledge/add_form', $data);
    }
    
    public function edit_form()
    {
        $id = $this->uri->segment(3);
        $this->load->model('group_priviledge_model');
        $data['g_priv'] = $this->group_priviledge_model->group_priv_data($id)->row();

        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(6)->row();
        $this->load->view('group_priviledge/edit_form', $data);
    }

    public function new_data()
    {
		$error                    = 0;
		$error_msg                = "";
		$redirect                 = "group_priviledge";
		$data['group_name']       = $_POST['group_name'];

		$this->form_validation->set_rules('group_name','Nama Grup','required');
		if($this->form_validation->run())
		{
			$this->load->model('group_priviledge_model');
			$this->load->model('menu_priviledge_model');

			if($error == 0)
			{
				$group_priv_id = $this->group_priviledge_model->insertData($data['group_name']);
				$menus = $this->menu_priviledge_model->menu_priv_list()->result();
				$fulldate = date('Y-m-d H:i:s');
				foreach ($menus as $menu)
				{	
					$data = array(
						'menu_priv_id'	=> $menu->id,
						'group_priv_id' => $group_priv_id,
						'created_date'  => $fulldate,
						'created_by'    => $this->session->userdata('username')
					);
					$this->db->insert('bca_group_menu_priviledge', $data);
				}

				$this->cms_log_activity("insert", "", $this->menu_log.": ".$data['group_name']);
				$error     = 0;
				$error_msg = "Grup Admin telah ditambahkan.";
			}

			$newdata = array('msg_grouppriv_list' => $error_msg, 'err_grouppriv_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal menambahkan Grup Admin.";
			$newdata   = array('msg_grouppriv_list' => $error_msg, 'err_grouppriv_list' => $error);
			$this->session->set_userdata($newdata);
			$this->add_form();
		}
    }

    public function edit_data()
    {
		$error              = 0;
		$error_msg          = "";
		$redirect           = "group_priviledge";
		$data['group_id']   = $_POST['group_id'];
		$data['group_name'] = $_POST['group_name'];

		$this->form_validation->set_rules('group_name','Nama Grup','required');
		if($this->form_validation->run())
		{
			$this->load->model('group_priviledge_model');

			$old = $this->group_priviledge_model->group_priv_data($data['group_id'])->row();

			if($error == 0)
			{
				$this->group_priviledge_model->updateData($data['group_id'], $data['group_name']);

				$this->cms_log_activity("edit", $this->menu_log.": dari ".$old->group_name." menjadi ".$data['group_name']);
				$error     = 0;
				$error_msg = "Grup Admin berhasil diubah.";
			}

			$newdata = array('msg_grouppriv_list' => $error_msg, 'err_grouppriv_list' => $error);
			$this->session->set_userdata($newdata);
			redirect($redirect);
		}
		else
		{
			$error     = 1;
			$error_msg = "Gagal mengubah Grup Admin.";
			$newdata   = array('msg_grouppriv_list' => $error_msg, 'err_grouppriv_list' => $error);
			$this->session->set_userdata($newdata);
			$this->edit_form();
		}
    }

    public function remove_data()
	{
		$id = $this->uri->segment(3);
		$data = urldecode($this->uri->segment(4));
		
		$this->load->model('group_priviledge_model');
		$this->group_priviledge_model->deleteData($id);

		$this->db->where('group_priv_id', $id);
		$this->db->delete('bca_group_menu_priviledge');
		$this->cms_log_activity("delete", $this->menu_log.": ".$data);

		$error = 0;
		$error_msg = "Grup Admin telah dihapus.";
		$newdata = array('msg_grouppriv_list' => $error_msg, 'err_grouppriv_list' => $error);
		$this->session->set_userdata($newdata);
		
		redirect('group_priviledge');
	}
}

?>