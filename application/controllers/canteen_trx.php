<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Canteen_trx extends BCA_Controller {

	public function index()
	{
		// $this->load->model('canteen_trx_model');
		// $data['transactions'] = $this->canteen_trx_model->transaction_list()->result();

		$this->load->model('login_model');
		$data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 19)->row();

		$this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(19)->row();
		$this->load->view('canteen_trx/view', $data);
	}
}

?>