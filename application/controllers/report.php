<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends BCA_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('general_model');
        $this->load->model('report_model');
    }

	public function daily_report()
	{
        $start_date                   = $this->input->get('start_date');
        $end_date                     = $this->input->get('end_date');
        $report_id                    = $this->input->get('report_id');
        $this->data['start_date']     = $start_date;
        $this->data['end_date']       = $end_date;
        $this->data['report_id']      = $report_id;
        $this->data['getTypeMakanan'] = $this->general_model->select_data('bca_food_type','f_id','asc');
        $this->data['getPeriode']     = $this->getPeriode($start_date,$end_date);

        if($start_date != "" && $end_date != "")
        {
            $dataTrx = array();
            $transactions = $this->report_model->summaryTrxByDate($start_date, $end_date );
            foreach ($transactions as $trx) {
                $dataTrx[$trx->trx_date][$trx->trx_item_id][] = $trx->e_id;
            }
            $this->data["summaryTrx"] = $dataTrx;
        }

        if($report_id == 1){
            $this->data['reportContent'] = 'report/dailyreport/daily_canteen';
        }elseif($report_id == 2){
             $this->data['reportContent'] = 'report/dailyreport/daily_canteen_with_program';
        }elseif($report_id == 3){
             $this->data['reportContent'] = 'report/dailyreport/daily_rekap';
        }else{
            $this->data['reportContent'] = NULL;
        }
        $this->data['text_status'] = $text_status = $this->input->get('text_status');
        if($text_status ==1){

            $content=$this->load->view($this->data['reportContent'],$this->data,TRUE);

			$filename = make_alias($start_date.$end_date.$this->data['reportContent']).".xls";
			//prepare to give the user a Save/Open dialog...
			header ("Content-type: application/vnd.ms-excel");
			header ("Content-Disposition: attachment; filename=".$filename);

			//setting the cache expiration to 30 seconds ahead of current time. an IE 8 issue when opening the data directly in the browser without first saving it to a file
			$expiredate = time() + 30;
			$expireheader = "Expires: ".gmdate("D, d M Y G:i:s",$expiredate)." GMT";
			header ($expireheader);

			echo $content;

            exit();



        }

        $this->load->view('report/daily_report',$this->data);

	}

	public function canteen_cost()
	{
        $start_date                   = $this->input->get('start_date');
        $end_date                     = $this->input->get('end_date');
        $report_id                    = $this->input->get('report_id');
        $this->data['start_date']     = $start_date;
        $this->data['end_date']       = $end_date;
        $this->data['report_id']      = $report_id;
        $this->data['getTypeMakanan'] = $this->general_model->select_data('bca_food_type','f_id','asc');
        $this->data['getPeriode']     = $this->getPeriode($start_date,$end_date);

        if($start_date != "" && $end_date != "")
        {
            $dataTrx = array();
            $transactions = $this->report_model->summaryTrxByDate($start_date, $end_date );
            foreach ($transactions as $trx) {
                $dataTrx[$trx->trx_date][$trx->trx_item_id][] = $trx->e_id;
            }
            $this->data["summaryTrx"] = $dataTrx;
        }

        if($report_id == 1){
            $this->data['reportContent'] = 'report/canteen_cost/daily_canteen';
        }elseif($report_id == 2){
             $this->data['reportContent'] = 'report/canteen_cost/daily_canteen_with_program';
        }elseif($report_id == 3){
             $this->data['reportContent'] = 'report/canteen_cost/daily_rekap';
        }else{
            $this->data['reportContent'] = NULL;
        }
        $this->data['text_status'] = $text_status = $this->input->get('text_status');
        if($text_status ==1){

            $content=$this->load->view($this->data['reportContent'],$this->data,TRUE);

			$filename = make_alias($start_date.$end_date.$this->data['reportContent']).".xls";
			//prepare to give the user a Save/Open dialog...
			header ("Content-type: application/vnd.ms-excel");
			header ("Content-Disposition: attachment; filename=".$filename);

			//setting the cache expiration to 30 seconds ahead of current time. an IE 8 issue when opening the data directly in the browser without first saving it to a file
			$expiredate = time() + 30;
			$expireheader = "Expires: ".gmdate("D, d M Y G:i:s",$expiredate)." GMT";
			header ($expireheader);

			echo $content;

            exit();
        }
		$this->load->view('report/canteen_cost',$this->data);
	}

	public function food_ranking()
	{
        $start_date                   = $this->input->get('start_date');
        $end_date                     = $this->input->get('end_date');
        $report_id                    = $this->input->get('report_id');
        $this->data['start_date']     = $start_date;
        $this->data['end_date']       = $end_date;
        $this->data['report_id']      = $report_id;
        $this->data['getTypeMakanan'] = $this->general_model->select_data('bca_food_type','f_id','asc');
        $this->data['getPeriode']     = $this->getPeriode($start_date,$end_date);

        if($report_id == 1){
            $this->data['reportContent'] = 'report/food_ranking/table';
        }else{
            $this->data['reportContent'] = NULL;
        }
         $this->data['text_status'] = $text_status = $this->input->get('text_status');
        if($text_status ==1){

            $content=$this->load->view($this->data['reportContent'],$this->data,TRUE);

			$filename = make_alias($start_date.$end_date.$this->data['reportContent']).".xls";
			//prepare to give the user a Save/Open dialog...
			header ("Content-type: application/vnd.ms-excel");
			header ("Content-Disposition: attachment; filename=".$filename);

			//setting the cache expiration to 30 seconds ahead of current time. an IE 8 issue when opening the data directly in the browser without first saving it to a file
			$expiredate = time() + 30;
			$expireheader = "Expires: ".gmdate("D, d M Y G:i:s",$expiredate)." GMT";
			header ($expireheader);

			echo $content;

            exit();
        }
		$this->load->view('report/food_ranking',$this->data);
	}

	public function calory_meter()
	{
        $start_date               = $this->input->get('start_date');
        $end_date                 = $this->input->get('end_date');
        $nip                      = $this->input->get('nip');
        $report_id                = $this->input->get('report_id');
        $this->data['start_date'] = $start_date;
        $this->data['end_date']   = $end_date;
        $this->data['report_id']  = $report_id;
        $this->data['nip']        = $nip;
        $this->data['getPeriode'] = $this->getPeriode($start_date,$end_date);

        if($report_id == 1){
            $this->data['getVariableDynamic'] = $this->report_model->getTrxByStartDateEndDateGroupByEmployee($start_date,$end_date);
            $this->data['getReportData'] = $this->report_model->getTrxByStartDateEndDate($start_date,$end_date,$nip);
            $this->data['reportContent'] = 'report/calory_meter/table';
        }else{
            $this->data['reportContent'] = NULL;
        }
          $this->data['text_status'] = $text_status = $this->input->get('text_status');
        if($text_status ==1){

            $content=$this->load->view($this->data['reportContent'],$this->data,TRUE);

			$filename = make_alias($start_date.$end_date.$this->data['reportContent']).".xls";
			//prepare to give the user a Save/Open dialog...
			header ("Content-type: application/vnd.ms-excel");
			header ("Content-Disposition: attachment; filename=".$filename);

			//setting the cache expiration to 30 seconds ahead of current time. an IE 8 issue when opening the data directly in the browser without first saving it to a file
			$expiredate = time() + 30;
			$expireheader = "Expires: ".gmdate("D, d M Y G:i:s",$expiredate)." GMT";
			header ($expireheader);

			echo $content;

            exit();
        }
		$this->load->view('report/calory_meter',$this->data);
	}

    public function canteen_trx()
    {
        // $this->load->model('canteen_trx_model');
        // $data['transactions'] = $this->canteen_trx_model->transaction_list()->result();

        $this->load->model('login_model');
        $data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 19)->row();

        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(19)->row();
        $this->load->view('canteen_trx/view2', $data);
    }

    public function cms_log()
    {
        // $this->load->model('cms_log_model');
        // $data['logs'] = $this->cms_log_model->log_list()->result();

        $this->load->model('login_model');
        $data['crud']    = $this->login_model->group_priviledge($this->session->userdata('group_priviledge'), 25)->row();

        $this->load->model('menu_priviledge_model');
        $data['menu'] = $this->menu_priviledge_model->menu_priv_data(25)->row();
        $this->load->view('cms_log/view2', $data);
    }

    public function getPeriode($start_date,$end_date)
    {
        $begin = new DateTime($start_date);
        $end = new DateTime($end_date);

        $daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
        $newDataArray =  array();
        foreach($daterange as $date){
            $newDataArray[] = $date->format("l, d.m.Y");
        }
        $newDataArray[] = date('l, d.m.Y',strtotime($end_date));
        return $newDataArray;
    }

    public function getDateAndTime($full, $type)
    {
        $explode = explode(" ", $full);
        if($type == "date"){
            return $explode[0];
        } else {
            return $explode[1];
        }
    }

    public function getEmployeName($nip)
    {
        $this->load->model('employee_model');
        $res = $this->employee_model->employee_data($nip)->row();
        if($res) {
            return $res->e_name;
        } else {
            return "";
        }
    }

    public function getData()
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        $this->load->library('Serverside');
        // DB table to use
        //$table = 'bca_canteen_trx';
        if(isset($_GET["lowDate"]) && isset($_GET["highDate"]))
        {
            $minDate = $_GET["lowDate"];
            $maxDate = $_GET["highDate"];
            $table = <<<EOT
                    (SELECT trx_id, DATE_FORMAT(DATE(trx_time), "%d-%m-%Y") AS Tanggal, TIME(trx_time) AS Waktu, 
                    CASE 
                        WHEN e_nip LIKE 'EX_%' THEN SUBSTR(e_nip,4)
                        WHEN e_nip THEN e_nip
                    END AS e_nip, e_name, c_name, 
                    case trx_status
                        when 1 then 'Berhasil'
                        when 0 then 'Gagal'
                    end as Status , trx_notes FROM bca_canteen_trx_join_bca_canteen
                    LEFT JOIN bca_employee ON bca_canteen_trx_join_bca_canteen.e_id = bca_employee.e_nip
                    WHERE DATE(trx_time) BETWEEN "$minDate" AND "$maxDate"
                    UNION
                    SELECT trx_id, DATE_FORMAT(DATE(trx_time), "%d-%m-%Y") AS Tanggal, TIME(trx_time) AS Waktu, 
                    CASE 
                        WHEN e_nip LIKE 'EX_%' THEN SUBSTR(e_nip,4)
                        WHEN e_nip THEN e_nip
                    END AS e_nip, e_name, c_name, 
                    case trx_status
                        when 1 then 'Berhasil'
                        when 0 then 'Gagal'
                    end as Status , trx_notes  trx_notes FROM bca_canteen_trx_join_bca_canteen
                    RIGHT JOIN bca_employee ON bca_canteen_trx_join_bca_canteen.e_id = bca_employee.e_nip
                    WHERE trx_id IS NOT NULL AND DATE(trx_time) BETWEEN "$minDate" AND "$maxDate") temp
EOT;
        }
        else
        {
            $table = <<<EOT
                    (SELECT trx_id, DATE_FORMAT(DATE(trx_time), "%d-%m-%Y") AS Tanggal, TIME(trx_time) AS Waktu, 
                    CASE 
                        WHEN e_nip LIKE 'EX_%' THEN SUBSTR(e_nip,4)
                        WHEN e_nip THEN e_nip
                    END AS e_nip, e_name, c_name, 
                    case trx_status
                        when 1 then 'Berhasil'
                        when 0 then 'Gagal'
                    end as Status , trx_notes FROM bca_canteen_trx_join_bca_canteen
                    LEFT JOIN bca_employee ON bca_canteen_trx_join_bca_canteen.e_id = bca_employee.e_nip
                    UNION
                    SELECT trx_id, DATE_FORMAT(DATE(trx_time), "%d-%m-%Y") AS Tanggal, TIME(trx_time) AS Waktu, CASE 
                        WHEN e_nip LIKE 'EX_%' THEN SUBSTR(e_nip,4)
                        WHEN e_nip THEN e_nip
                    END AS e_nip, e_name, c_name, 
                    case trx_status
                        when 1 then 'Berhasil'
                        when 0 then 'Gagal'
                    end as Status , trx_notes  trx_notes FROM bca_canteen_trx_join_bca_canteen
                    RIGHT JOIN bca_employee ON bca_canteen_trx_join_bca_canteen.e_id = bca_employee.e_nip
                    WHERE trx_id IS NOT NULL) temp
EOT;
        }
        // Table's primary key
        $primaryKey = 'trx_id';
         
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
                        array(
                            'db' => 'Tanggal',
                            'dt' => 'tanggal'
                        ),
                        array(
                            'db' => 'Waktu',
                            'dt' => 'waktu'
                        ),
                        array(
                            'db' => 'e_nip',
                            'dt' => 'e_nip'
                        ),
                        array(
                            'db' => 'e_name',
                            'dt' => 'nama'
                        ),
                        array('db' => 'c_name', 'dt' => 'c_name'),
                        array('db' => 'Status',
                              'dt' => 'trx_status'
                        ),
                        array('db' => 'trx_notes', 'dt' => 'trx_notes'),
                    );
         
        // SQL server connection information
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db'   => $this->db->database,
            'host' => $this->db->hostname
        );
         
         
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
         
        //require( 'ssp.class.php' );

        echo json_encode(
            Serverside::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
        );
    }

    public function getData_cmsLog()
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        $this->load->library('Serverside');
        // DB table to use
        if(isset($_GET["lowDate"]) && isset($_GET["highDate"]))
        {
            $minDate = $_GET["lowDate"];
            $maxDate = $_GET["highDate"];
            $table = <<<EOT
                    (SELECT id, activity, notes, became, activity_by, DATE_FORMAT(DATE(activity_time), "%d-%m-%Y") AS tanggal, TIME(activity_time) AS waktu FROM bca_cms_log WHERE DATE(activity_time) BETWEEN "$minDate" AND "$maxDate") temp
EOT;
        }
        else
        {
            $table = <<<EOT
                    (SELECT id, activity, notes, became, activity_by, DATE_FORMAT(DATE(activity_time), "%d-%m-%Y") AS tanggal, TIME(activity_time) AS waktu FROM bca_cms_log) temp
EOT;
        }
        
         
        // Table's primary key
        $primaryKey = 'id';
         
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
                        array('db' => 'activity', 'dt' => 'activity'),
                        array('db' => 'notes', 'dt' => 'notes'),
                        array('db' => 'became', 'dt' => 'became'),
                        array('db' => 'activity_by', 'dt' => 'activity_by'),
                        array('db' => 'tanggal', 'dt' => 'tanggal'),
                        array('db' => 'waktu', 'dt' => 'waktu'),
                    );
         
        // SQL server connection information
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db'   => $this->db->database,
            'host' => $this->db->hostname
        );
         
         
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
         
        //require( 'ssp.class.php' );

        echo json_encode(
            Serverside::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
        );
    }

}
