<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stock extends BCA_Controller {

	public function index()
	{
        $id = 0;
        $data['item_id_stock'] = 0;
        if($this->uri->segment(3))
        {
            $id = $this->uri->segment(3);
            $data['item_id_stock'] = $id;
        }
		$this->load->model('stock_model');
		$data['stocks'] = $this->stock_model->stock_items_list($id)->result();
		$this->load->view('stock/view', $data);
	}

    // public function choose_food()
    // {
    //     $this->load->model('stock_model');
    //     $data['food_names'] = $this->stock_model->food_name()->result();
    //     $this->load->view('stock/choose_food', $data);
    // }

	public function add_form()
    {
        $this->load->model('stock_model');
        $data['food_names'] = $this->stock_model->food_name()->result();
        $this->load->view('stock/add_form', $data);
        // $this->load->view('stock/add_form');
    }
    
    public function edit_form()
    {
        $id = $this->uri->segment(3);
        $this->load->model('stock_model');
        $data['stock'] = $this->stock_model->stock_item_data($id)->row();
        $data['food_names'] = $this->stock_model->food_name()->result();
        $this->load->view('stock/edit_form', $data);
    }

    public function new_stock_item()
    {
        $error           = 0;
        $error_msg       = "";
        $redirect        = "stock";
        $item['item_id'] = $_POST['item_id'];
        $item_dateCount  = count($_POST['item_serving_date']);
        $item_stockCount = count($_POST['item_stock']);

        $this->load->model('stock_model');
        $food_name = $this->stock_model->food_name($item['item_id'])->row();
        $added_servingDate = array();
        $had_input_msg = "Anda sudah pernah input stok ".strtoupper($food_name->c_name)." pada tanggal ";

        if($item_dateCount != $item_stockCount)
        {
            $error = 1;
            $error_msg = "Anda harus menigisi semua field yang diperlukan.";
            $redirect = "stock/add_form";
        }
        else
        {
            $result = $this->stock_model->get_stok_serving_date($item['item_id'])->result();
            if($result)
            {
                foreach ($result as $res) {
                    array_push($added_servingDate, $res->s_serving_date);
                }
            }
            //print_r($added_servingDate);die;
            for($i=0; $i<$item_dateCount; $i++)
            {
                $item['item_stock']        = $_POST['item_stock'][$i];
                $fdate                     = $_POST['item_serving_date'][$i];
                $explode                   = explode("-", $fdate);
                $item['item_serving_date'] = $explode[2]."-".$explode[1]."-".$explode[0];

                if(!in_array($item['item_serving_date'], $added_servingDate))
                {
                    if($error == 0)
                    {
                        $this->stock_model->insert_stock_item($item['item_id'], $item['item_stock'],  $item['item_serving_date']);
                    }
                }
                else
                {
                    $had_input_date .= $fdate.", ";
                }
            }
            if($had_input_date != "")
            {
                $error_msg = $had_input_msg.$had_input_date." silahkan ubah data jika ingin melakukan penambahan stok pada tanggal tersebut.\nData lainnya telah disimpan.";
            }
            else
            {
                $error_msg = "Stok telah ditambahkan.";
            }
            $newdata = array('msg_stock_list' => $error_msg, 'err_stock_list' => $error);
            $this->session->set_userdata($newdata);
            redirect($redirect);
        }

        // $item['item_id']           = $_POST['item_id'];
        // $item['item_stock']        = $_POST['item_stock'];
        // $item['item_serving_date'] = $_POST['item_serving_date'];
        
        // $this->form_validation->set_rules('item_stock','Stock','required');
        // $this->form_validation->set_rules('item_serving_date','Serving Date','required');

        // if($this->form_validation->run())
        // {
        //     $this->load->model('stock_model');
        //     if($item['item_id'] == 0)
        //     {
        //         $error = 1;
        //         $error_msg = "You have to choose Food Name";
        //         $redirect = "stock/add_form";
        //     }

        //     if($error == 0)
        //     {
        //         $this->stock_model->insert_stock_item($item['item_id'], $item['item_stock'],  $item['item_serving_date']);
            
        //         $error     = 0;
        //         $error_msg = "New stock has been added";
        //     }

        //     $newdata = array('msg_stock_list' => $error_msg, 'err_stock_list' => $error);
        //     $this->session->set_userdata($newdata);
        //     redirect($redirect);
        // }
        // else
        // {
        //     $error     = 1;
        //     $error_msg = "Failed to Add stock";
        //     $newdata   = array('msg_stock_list' => $error_msg, 'err_stock_list' => $error);
        //     $this->session->set_userdata($newdata);
        //     $this->add_form();
        // }
    }

    public function edit_stock_item()
    {
        $error                     = 0;
        $error_msg                 = "";
        $redirect                  = "stock";
        $item['stock_id']          = $_POST['stock_id'];
        $item['item_id']           = $_POST['item_id'];
        $item['item_stock']        = $_POST['item_stock'];
        $item['item_serving_date'] = $_POST['item_serving_date'];

        $this->form_validation->set_rules('item_stock','Stok','required');
        $this->form_validation->set_rules('item_serving_date','Tanggal Disajikan','required');

        if($this->form_validation->run())
        {
            $this->load->model('stock_model');
            if($item['item_id'] == 0)
            {
                $error = 1;
                $error_msg = "Anda harus memilih Nama Makanan.";
                $redirect = "stock/edit_form";
            }

            if($error == 0)
            {
                $this->stock_model->update_stock_item($item['stock_id'], $item['item_id'], $item['item_stock'], $item['item_serving_date']);
            
                $error     = 0;
                $error_msg = "Stok berhasil diubah.";
            }

            $newdata = array('msg_stock_list' => $error_msg, 'err_stock_list' => $error);
            $this->session->set_userdata($newdata);
            redirect($redirect);
        }
        else
        {
            $error     = 1;
            $error_msg = "Gagal mengubah stok.";
            $newdata   = array('msg_stock_list' => $error_msg, 'err_stock_list' => $error);
            $this->session->set_userdata($newdata);
            $this->edit_form();
        }
    }

    public function remove_stock_item()
    {
        $id = $this->uri->segment(3);
        
        $this->load->model('stock_model');
        $this->stock_model->delete_stock_item($id);

        $error = 0;
        $error_msg = "Stok telah dihapus.";
        $newdata = array('msg_stock_list' => $error_msg, 'err_stock_list' => $error);
        $this->session->set_userdata($newdata);
        
        redirect('stock');
    }
}

?>