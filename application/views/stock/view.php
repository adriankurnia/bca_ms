<?php $this->load->view('header'); ?>

<section class="page-content">
<div class="page-content-inner">
    <!--  -->
    <section class="panel">
        <div class="panel-heading">
            <?php
                if($item_id_stock != 0)
                {
                    $ci = &get_instance();
                    $ci->load->model('stock_model');
                    $res = $ci->stock_model->food_name($item_id_stock)->row();
            ?>
                    <h3>Daftar Stok <?=strtoupper($res->c_name)?></h3>
            <?php    
                }
                else
                {
            ?>
                    <h3>Daftar Stok Kantin</h3>
            <?php
                }
            ?>
            
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <?php
                        if($item_id_stock != 0)
                        {
                    ?>
                            <form action="<?=base_url()?>stock/add_form" method="post" name="adminForm" id="adminForm">
                                <input type="hidden" name="item_id" value="<?=$item_id_stock?>">
                                <button type="submit" name="submit" class="btn btn-rounded btn-success margin-inline">+ Stok</button>
                            </form>
                    <?php
                        }
                        else
                        {
                    ?>
                            <a class="btn btn-rounded btn-success margin-inline" href="<?=base_url()?>stock/add_form"> + Stok</a>
                    <?php
                        }
                    ?>
                    <!-- <form action="<?=base_url()?>admin/instagram_list/search_by_date" method="post" name="adminForm" id="adminForm"> -->
                    <!-- <div class="margin-bottom-5">
                        Pull Date: 
                        <input class="form-control datepicker-only-init width-200 display-inline-block margin-inline" placeholder="From" type="text" name="date_from">
                        <span class="margin-right-10">—</span>
                        <input class="form-control datepicker-only-init width-200 display-inline-block margin-inline" placeholder="To" type="text" name="date_to">
                        <button type="submit" name="submit" class="btn btn-rounded btn-primary margin-inline">Go</button>
                    </div> -->
                    <?php
						$error		= $this->session->userdata('err_stock_list');
						$error_msg	= $this->session->userdata('msg_stock_list');
						if($this->session->userdata('msg_stock_list'))
						{
							if($error == 0)
							{
								$class = "alert alert-primary";
							}
							else
							{
								$class = "alert alert-warning";
							}
							echo '
								<div class="'.$class.'" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<strong>'.$error_msg.'</strong>
								</div>';
						}
						$this->session->unset_userdata('msg_stock_list');
						$this->session->unset_userdata('err_stock_list');
					?>
                    <div class="margin-bottom-50">
                        <table class="table table-hover nowrap" id="example1" width="100%">
                            <thead>
                            <tr>
                                <th>NO.</th>
                                <th>Nama Makanan</th>
                                <th>Stok</th>
                                <th>Tanggal Disajikan</th>

                                <th>ACTION</th>
                                <th>ACTION</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>NO.</th>
                                <th>Nama Makanan</th>
                                <th>Stok</th>
                                <th>Tanggal Disajikan</th>

                                <th>ACTION</th>
                                <th>ACTION</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            
                            <?php
								if($stocks)
								{
                                    $i = 1;
									foreach($stocks as $stock)
									{
                                        $explode1 = explode(" ", $stock->s_serving_date);
                                        $explode2 = explode("-", $explode1[0]);
                                        $date = $explode2[2]."-".$explode2[1]."-".$explode2[0];
										?>
										<tr>
                                            <td><?=$i?></td>
											<td><?=strtoupper($stock->c_name)?></td>
                                            <td><?=$stock->s_stock?></td>
                                            <td><?=$date?></td>
                                            <td><a class="btn btn-rounded btn-info margin-inline" href="<?=base_url()?>stock/edit_form/<?=$stock->s_id?>">Ubah</td>
                                            <td><a class="btn btn-rounded btn-danger margin-inline" href="<?=base_url()?>stock/remove_stock_item/<?=$stock->s_id?>">Hapus</td>
										 </tr>	
									<?php
                                        $i++;
									}
								}
							?>
                            </tbody>
                        </table>
                    </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </section>
    <!-- End  -->

</div>

<!-- Page Scripts -->
<script>

    $(function(){

        $('#example1').DataTable({
            responsive: true,
            "order": [[ 0, "desc" ]]
        });

        $('#example2').DataTable({
            autoWidth: true,
            scrollX: true,
            fixedColumns: true
        });

        $('#example3').DataTable({
            autoWidth: true,
            scrollX: true,
            fixedColumns: true
        });
    });

    $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD'
        });
</script>

<!-- End Page Scripts -->
</section>
<?php $this->load->view('footer');?>