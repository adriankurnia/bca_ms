<?php $this->load->view('header'); ?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Pilih Menu Makanan</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $error     = $this->session->userdata('err_stock_list');
                    $error_msg = $this->session->userdata('msg_stock_list');
                    if($this->session->userdata('msg_stock_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('err_stock_list');
                    $this->session->unset_userdata('msg_stock_list');
                ?>

                <div id="chooseItem" class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>stock/add_form" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Pilih Makanan</label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" name="item_id" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357">
                                    <option value="0" selected>- Nama Makanan -</option>
                                    <?php
                                        foreach ($food_names as $fn) { ?>
                                            <option value="<?=$fn->c_id?>"><?=strtoupper($fn->c_name)?></option>;
                                        <?php }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Submit</button>
                                    <a href="<?=base_url()?>stock"><button type="button" class="btn btn-default">Cancel</button></a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<script>
    var rowNum = 0;
    function addRow(frm)
    {
        rowNum ++;

        var row = '<div id="rowNum'+rowNum+'"><div class="form-group row"><div class="col-md-4"><select class="form-control" name="item_id[]" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357"><option value="0" selected>- Food Name -</option><?php foreach ($food_names as $fn) { ?><option value="<?=$fn->c_id?>"><?=strtoupper($fn->c_name)?></option>;=<?php }?></select></div><div class="col-md-4"><input id="stock-mask-input" type="text" name="item_stock[]" value="" class="form-control" placeholder="Stok"  /></div><div class="col-md-4"><input type="text" name="item_serving_date[]" class="form-control datepicker-only-init" placeholder="Serving Date" value="" /></div></div><div class="form-group row"><div class="col-md-3"><input type="button" class="btn btn-rounded btn-danger margin-inline" value="Remove" onclick="removeRow('+rowNum+');"></div></div></div>';

        jQuery('#itemRows').append(row);
        showCalender();

    }

    function removeRow(rnum)
    {
        jQuery('#rowNum'+rnum).remove();
    }

    function showCalender()
    {
        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY'
            // format: 'YYYY-MM-DD'
        });
    }

    $(function(){
        
        $('#summernote').summernote({
            height: 350
        });

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY'
            // format: 'YYYY-MM-DD'
        });

        $('#stock-mask-input').mask('0000000', {placeholder: "Stock"});
    })
</script>
<?php $this->load->view('footer');?>