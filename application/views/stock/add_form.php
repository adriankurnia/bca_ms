<?php
    $this->load->view('header');

    if($_POST)
    {
        $ci = &get_instance();
        $ci->load->model('stock_model');
        $res = $ci->stock_model->food_name($_POST['item_id'])->row();
?>
        <section class="page-content">
        <div class="page-content-inner">

        <!-- Basic Form Elements -->
        <section class="panel">
            <div class="panel-heading">
                <h3>Tambah Stok <?=strtoupper($res->c_name)?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <?php
                            // $error     = $this->session->userdata('err_stock_list');
                            // $error_msg = $this->session->userdata('msg_stock_list');
                            // if($this->session->userdata('msg_stock_list'))
                            // {
                            //     if($error == 0)
                            //     {
                            //         $class = "alert alert-primary";
                            //     }
                            //     else
                            //     {
                            //         $class = "alert alert-warning";
                            //     }
                            //     echo '
                            //         <div class="'.$class.'" role="alert">
                            //             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            //                 <span aria-hidden="true">&times;</span>
                            //             </button>
                            //             <strong>'.$error_msg.'</strong>
                            //         </div>';
                            // }
                            // $this->session->unset_userdata('err_stock_list');
                            // $this->session->unset_userdata('msg_stock_list');
                        ?>
                        <div class="margin-bottom-50">
                            <br />
                            <!-- Horizontal Form -->
                            <form action="<?=base_url()?>stock/new_stock_item" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                                <input type="hidden" name="item_id" value=<?=$_POST['item_id']?>>
                                <div class="form-group row">
                                    <!-- <div class="col-md-4">Name</div> -->
                                    <div class="col-md-6">Tanggal Disajikan</div>
                                    <div class="col-md-5">Stok</div>
                                </div>
                                <div  id="itemRows">
                                    <div class="form-group row">
                                        <!-- <div class="col-md-4">
                                            <select class="form-control" name="item_id[]" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357">
                                                <option value="0" selected>- Food Name -</option>
                                                <?php
                                                    foreach ($food_names as $fn) { ?>
                                                        <option value="<?=$fn->c_id?>"><?=strtoupper($fn->c_name)?></option>;
                                                    <?php }
                                                ?>
                                            </select>
                                        </div> -->
                                        <div class="col-md-6">
                                            <input type="text" name="item_serving_date[]" class="form-control datepicker-only-init" placeholder="Tanggal Disajikan" value="" />
                                        </div>
                                        <div class="col-md-5">
                                            <input id="stock-mask-input" type="text" name="item_stock[]" value="" class="form-control" placeholder="Stok"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input onclick="addRow(this.form);" type="button" class="btn btn-rounded btn-info margin-inline" value="Tambah Stok lain" />
                                    </div>
                                </div>
                                
                                <!-- <div class="form-group row">
                                    <div class="col-md-3">
                                        <label class="form-control-label" for="l0">Name</label>
                                    </div>
                                    <div class="col-md-9">
                                        <select class="form-control" name="item_id" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357">
                                            <option value="0" selected>- Food Name -</option>
                                            <?php
                                                foreach ($food_names as $fn) { ?>
                                                    <option value="<?=$fn->c_id?>"><?=strtoupper($fn->c_name)?></option>;
                                                <?php }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label class="form-control-label" for="l0">Stock</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input id="stock-mask-input" type="text" name="item_stock" value="" class="form-control" placeholder="Stok"  />
                                        <?php echo form_error('item_stock');?>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label class="form-control-label" for="l0">Serving Date</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="item_serving_date" id="datepicker-only-init" class="form-control datepicker-only-init" placeholder="Serving Date" value="" />
                                        <?php echo form_error('item_serving_date');?>
                                    </div>
                                </div> -->

                                <div class="form-actions">
                                    <div class="form-group row">
                                        <div class="col-md-9 col-md-offset-3">
                                            <button type="submit" name="submit" class="btn width-150 btn-primary">Kirim</button>
                                            <a href="<?=base_url()?>stock"><button type="button" class="btn btn-default">Batal</button></a>
                                        </div>
                                    </div>
                                </div>
                            </form><!-- End Horizontal Form -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End -->    
        </div>

        </section>
<?php
    }
    else
    {
?>
        <section class="page-content">
        <div class="page-content-inner">

        <!-- Basic Form Elements -->
        <section class="panel">
            <div class="panel-heading">
                <h3>Pilih Menu Makanan</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <?php
                            $error     = $this->session->userdata('err_stock_list');
                            $error_msg = $this->session->userdata('msg_stock_list');
                            if($this->session->userdata('msg_stock_list'))
                            {
                                if($error == 0)
                                {
                                    $class = "alert alert-primary";
                                }
                                else
                                {
                                    $class = "alert alert-warning";
                                }
                                echo '
                                    <div class="'.$class.'" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <strong>'.$error_msg.'</strong>
                                    </div>';
                            }
                            $this->session->unset_userdata('err_stock_list');
                            $this->session->unset_userdata('msg_stock_list');
                        ?>

                        <div id="chooseItem" class="margin-bottom-50">
                            <br />
                            <!-- Horizontal Form -->
                            <form action="<?=base_url()?>stock/add_form" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label class="form-control-label" for="l0">Pilih Makanan</label>
                                    </div>
                                    <div class="col-md-9">
                                        <select class="form-control" name="item_id" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357">
                                            <option value="0" selected>- Nama Makanan -</option>
                                            <?php
                                                foreach ($food_names as $fn) { ?>
                                                    <option value="<?=$fn->c_id?>"><?=strtoupper($fn->c_name)?></option>;
                                                <?php }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <div class="form-group row">
                                        <div class="col-md-9 col-md-offset-3">
                                            <button type="submit" name="submit" class="btn width-150 btn-primary">Kirim</button>
                                            <a href="<?=base_url()?>stock"><button type="button" class="btn btn-default">Batal</button></a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End -->    
        </div>

        </section>
<?php
    }
?>
<script>
    var rowNum = 0;
    function addRow(frm)
    {
        rowNum ++;

        // var row = '<div id="rowNum'+rowNum+'"><div class="form-group row"><div class="col-md-6"><input id="stock-mask-input" type="text" name="item_stock[]" value="" class="form-control" placeholder="Stok"  /></div><div class="col-md-5"><input type="text" name="item_serving_date[]" class="form-control datepicker-only-init" placeholder="Serving Date" value="" /></div></div><div class="form-group row"><div class="col-md-3"><input type="button" class="btn btn-rounded btn-danger margin-inline" value="Remove" onclick="removeRow('+rowNum+');"></div></div></div>';

        var row = '<div id="rowNum'+rowNum+'"><div class="form-group row"><div class="col-md-6"><input type="text" name="item_serving_date[]" class="form-control datepicker-only-init" placeholder="Tanggal Disajikan" value="" /></div><div class="col-md-5"><input id="stock-mask-input" type="text" name="item_stock[]" value="" class="form-control" placeholder="Stok"  /></div><div class="col-md-1"><button type="button" class="btn btn-rounded btn-danger margin-inline icmn-cross" onclick="removeRow('+rowNum+');"></button></div></div></div>';

        jQuery('#itemRows').append(row);
        showCalender();

    }

    function removeRow(rnum)
    {
        jQuery('#rowNum'+rnum).remove();
    }

    function showCalender()
    {
        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY'
            // format: 'YYYY-MM-DD'
        });
    }

    $(function(){
        
        $('#summernote').summernote({
            height: 350
        });

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY'
            // format: 'YYYY-MM-DD'
        });

        $('#stock-mask-input').mask('0000000', {placeholder: "Stock"});
    })
</script>
<?php $this->load->view('footer');?>