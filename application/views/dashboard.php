<?php $this->load->view('header'); ?>
<section class="page-content">
<div class="page-content-inner">

    <!-- Documentation -->
    <section class="panel panel-with-borders">
        <div class="panel-heading">
            <h2>
                <small class="pull-right font-size-14 display-block margin-top-5"></small>
             Dashboard
            </h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <!-- Berhasil/gagal jumlah tapping -->
                <div class="row">
                <div class="col-md-12 idtem1 idtem">
                    <h5>Stok makanan (Main, desert , drink) untuk di setiap lantai</h5>
                   <canvas id="myChart1"></canvas>
                 </div>

                 <!-- Jumlah peserta yang sudah makan -->
                <div class="col-md-6 idtem2 idtem">
                    <h5>Jumlah peserta yang sudah makan</h5>
                     <div class="" id="myChart2"></div>
                 </div>
                </div>
                <div class="row">
                <div class="col-md-6 idtem3 idtem">
                    <h5>Makanan favorit di hari ini</h5>
                     <div class="" id="myChart3"></div>
                 </div>
                    <div class="col-md-6 idtem4 idtem">
                     <canvas class="" id="myChart4"></canvas>
                 </div>


                </div>
                <div class="row idtem5 idtem">
                     <div class="col-md-12">
                       <canvas id="myChart5"></canvas>
                    </div>
                </div>
            </div>


        </div>
    </section>
    <!-- End Documentation -->

</div>
</section>



<script>
function reloadChart(){
<?php
    $zxcAa = array();
        if($getModuleList)foreach($getModuleList as $list){
            $zxcAa[] = $list->module_id;
        }

    $newDataZz = array(1,2,3,4,5);
        if($newDataZz)foreach($newDataZz as $zzzList){
            if(!in_array($zzzList,$zxcAa)){?>
              $(".idtem<?php echo $zzzList?>").hide();
<?php
            }
        }

    if($getModuleList)foreach($getModuleList as $list){
        if($list->module_id == 1){
        ?>
        $.getJSON("<?php echo site_url('ajax/getstockbylantai');?>", function (result) {
            var ctx = document.getElementById('myChart1').getContext('2d');
            var myChart = new Chart(ctx,
                            {
                                type: 'horizontalBar',
                                data: {
                                    labels: result.label,
                                    datasets: result.data
                                },
                                options: 
                                {
                                    showAllTooltips: true,
                                    legend: {
                                        display: true,
                                        position:'top',
                                    },
                                    tooltips: 
                                    {
                                        callbacks: 
                                        {
                                            title: function(tooltipItems, data) 
                                            {
                                                return '';
                                            },
                                            label: function(tooltipItem, data) 
                                            {
                                                var datasetLabel = 'ss';
                                                var label        = data.labels[tooltipItem.index];
                                                var data_label   = data.datasets[tooltipItem.datasetIndex];
                                                console.log(data_label);

                                                if(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] > 0)
                                                {
                                                    return data_label.label + '  ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                                } 
                                                else 
                                                {
                                                    return 0 ;
                                                }
                                            }
                                        }
                                    }
                                }
                            });
        });
        <?php }else  if($list->module_id == 2){?>

        $.getJSON("<?php echo site_url('ajax/gettotalmakan');?>", function (result) {
                new Chartist.Bar("#myChart2", {
                        labels: ["<?php echo date('d-m-Y')?> ("+result.total+")"],
                        series: [
                            [result.total],
                        ]
                    }, {
                        stackBars: !0,
                        axisY: {
                        },
                        plugins: [
                            Chartist.plugins.tooltip()
                        ]
                    }).on("draw", function(data) {
                        "bar" === data.type && data.element.attr({
                            style: "stroke-width: 50px"
                        })
                    });
              });
        <?php }else  if($list->module_id == 3){?>

        $.getJSON("<?php echo site_url('ajax/getmakananfavorite');?>", function (result) {
            new Chartist.Line("#myChart3", {
                labels: result.label,
                series: result.value
            }, {
                low: 0,
                showArea: true,
                plugins: [
                    Chartist.plugins.tooltip()
                ]
            });
         });
        <?php }else  if($list->module_id == 4){?>

        $.getJSON("<?php echo site_url('ajax/gettotaltapping');?>", function (result) {

                var ctx = document.getElementById("myChart4").getContext('2d');
                    var myChart = new Chart(ctx, {
                      type: 'pie',
                      data: {
                        labels: ["Berhasil ("+result.totalBerhasil+")", "Gagal("+result.totalGagal+")"],
                        datasets: [{
                          backgroundColor: [
                            "#2ecc71",
                            "#3498db"
                          ],
                          data: [result.totalBerhasil,result.totalGagal]
                        }]
                      },options: {
                      title: {
                        display: true,
                        text: 'Tapping Status'
                      }},
                    });

        });
        <?php }else  if($list->module_id == 5){?>

        $.getJSON("<?php echo site_url('ajax/getgrafikbiaya');?>", function (result) {
            var barCtx = document.getElementById('myChart5').getContext('2d');

            var dataBar = {
                labels: result.label,
                datasets: [
                    {
                        label: 'Biaya Konsumsi',
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)',
                            '#3498db'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)',
                            '#3498db'
                        ],

                        data: result.value
                    }
                ]
            };

            new Chart(barCtx, {
                type: "bar",
                data: dataBar,
                options: {
                    scales: {
                        xAxes: [{
                            stacked: true
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }
            });

        });
        <?php }?>






    <?php }else{ ?>
            $(".idtem").hide();
    <?php } ?>
}

    function Loading(){
            reloadChart();

    }
    Loading();

    setInterval(function() {
       Loading();
    }, 90000);


 Chart.pluginService.register({
            beforeRender: function(chart) {
                if (chart.config.options.showAllTooltips) {
                    // create an array of tooltips
                    // we can't use the chart tooltip because there is only one tooltip per chart
                    chart.pluginTooltips = [];
                    chart.config.data.datasets.forEach(function(dataset, i) {
                        chart.getDatasetMeta(i).data.forEach(function(sector, j) {
                            chart.pluginTooltips.push(new Chart.Tooltip({
                                _chart: chart.chart,
                                _chartInstance: chart,
                                _data: chart.data,
                                _options: chart.options.tooltips,
                                _active: [sector]
                            }, chart));
                        });
                    });

                    // turn off normal tooltips
                    chart.options.tooltips.enabled = false;
                }
            },
            afterDraw: function(chart, easing) {
                if (chart.config.options.showAllTooltips) {
                    // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
                    if (!chart.allTooltipsOnce) {
                        if (easing !== 1)
                            return;
                        chart.allTooltipsOnce = true;
                    }

                    // turn on tooltips
                    chart.options.tooltips.enabled = true;
                    Chart.helpers.each(chart.pluginTooltips, function(tooltip) {
                        tooltip.initialize();
                        tooltip.update(); // broken line
                        // we don't actually need this since we are not animating tooltips
                        tooltip.pivot();
                        tooltip.transition(easing).draw();
                    });
                    chart.options.tooltips.enabled = false;

                }
            }
        });


</script>

<?php $this->load->view('footer');?>
