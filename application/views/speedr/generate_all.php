<?php echo form_open_multipart(current_url());?>
	<div class="form-group row">
		<div class="col-md-3">
			<label for="l13">Table Prefix</label>
		</div>
		<div class="col-md-9">
			<?php 
				echo form_input(array("name" => "table_prefix", "value" => $table_prefix, "class" => "form-control"));
				echo form_error("table_prefix");
			?>
		</div>
	</div>

	<div class="form-actions">
		<div class="form-group row">
			<div class="col-md-9 col-md-offset-3">
				<button type="submit" class="btn width-150 btn-primary">Submit</button>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>