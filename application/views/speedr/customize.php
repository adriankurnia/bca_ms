<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Clean UI Admin Template</title>

    <link href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>common/img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
    <link href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>common/img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
    <link href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>common/img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
    <link href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>common/img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
    <link href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>common/img/favicon.png" rel="icon" type="image/png">
    <link href="favicon.ico" rel="shortcut icon">

    <!-- HTML5 shim and Respond.js for < IE9 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Vendors Styles -->
    <!-- v1.0.0 -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/jscrollpane/style/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/ladda/dist/ladda-themeless.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/cleanhtmlaudioplayer/src/player.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/cleanhtmlvideoplayer/src/player.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/bootstrap-sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/summernote/dist/summernote.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/owl.carousel/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/ionrangeslider/css/ion.rangeSlider.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/datatables/media/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/c3/c3.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/chartist/dist/chartist.min.css">
    <!-- v.1.4.0 -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/nprogress/nprogress.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/jquery-steps/demo/css/jquery.steps.css">
    <!-- v.1.4.2 -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/bootstrap-select/dist/css/bootstrap-select.min.css">

    <!-- Clean UI Styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . ASSETS_BACKEND_URL; ?>common/css/source/main.css">

    <!-- Vendors Scripts -->
    <!-- v1.0.0 -->
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/tether/dist/js/tether.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/jscrollpane/script/jquery.jscrollpane.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/spin.js/spin.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/ladda/dist/ladda.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/html5-form-validation/dist/jquery.validation.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/jquery-typeahead/dist/jquery.typeahead.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/autosize/dist/autosize.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/bootstrap-show-password/bootstrap-show-password.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/cleanhtmlaudioplayer/src/jquery.cleanaudioplayer.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/cleanhtmlvideoplayer/src/jquery.cleanvideoplayer.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/summernote/dist/summernote.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/ionrangeslider/js/ion.rangeSlider.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/nestable/jquery.nestable.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/datatables/media/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/datatables-responsive/js/dataTables.responsive.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/editable-table/mindmup-editabletable.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/d3/d3.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/c3/c3.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/chartist/dist/chartist.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/peity/jquery.peity.min.js"></script>
    <!-- v1.0.1 -->
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/chartist-plugin-tooltip/dist/chartist-plugin-tooltip.min.js"></script>
    <!-- v1.1.1 -->
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/gsap/src/minified/TweenMax.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/hackertyper/hackertyper.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/jquery-countTo/jquery.countTo.js"></script>
    <!-- v1.4.0 -->
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/nprogress/nprogress.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/jquery-steps/build/jquery.steps.min.js"></script>
    <!-- v1.4.2 -->
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>vendors/chart.js/src/Chart.bundle.min.js"></script>

    <!-- Clean UI Scripts -->
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>common/js/common.js"></script>
    <script src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>common/js/demo.temp.js"></script>

<!-- Page Scripts -->

</head>
<body class="theme-default">
<nav class="left-menu" left-menu>
    <div class="logo-container">
        <a href="<?php echo base_url() . ADMIN_URL_DASHBOARD; ?>" class="logo">
            <img src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>common/img/logo.png" alt="Clean UI Admin Template" />
            <img class="logo-inverse" src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>common/img/logo-inverse.png" alt="Clean UI Admin Template" />
        </a>
    </div>
    <div class="left-menu-inner scroll-pane">

        <?php /*
        <ul class="left-menu-list left-menu-list-root list-unstyled">
            <li class="left-menu-list<?php if($this->router->fetch_class() == 'dashboard') { echo "-active"; } ?>">
                <a class="left-menu-link" href="<?php echo base_url() . ADMIN_URL_DASHBOARD; ?>">
                    <i class="left-menu-link-icon icmn-home2"><!-- --></i>
                    <span class="menu-top-hidden">Dashboard</span>
                </a>
            </li>
            <li class="left-menu-list-separator"><!-- --></li>

            <!-- LIST MENU -->
            <li class="left-menu-list-submenu<?php if($this->router->fetch_class() == 'booking') { echo " left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-files-empty2"><!-- --></i>
                    Booking Page
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->router->fetch_class() == 'booking') {echo " style='display:block;'"; }?> >
                    <li>
                        <a class="left-menu-link" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_BOOKING']; ?>">
                            List Booking
                        </a>
                    </li>
                    <li>
                        <a class="left-menu-link" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_BOOKING']; ?>form">
                            Add Booking
                        </a>
                    </li>
                </ul>
            </li>

            <li class="left-menu-list-separator"><!-- --></li>

            <li class="left-menu-list-submenu<?php if($this->router->fetch_class() == 'room') { echo " left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-files-empty2"><!-- --></i>
                    Room Page
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->router->fetch_class() == 'room') {echo " style='display:block;'"; }?> >
                    <li>
                        <a class="left-menu-link" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_ROOM']; ?>">
                            List Room
                        </a>
                    </li>
                    <li>
                        <a class="left-menu-link" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_ROOM']; ?>form">
                            Add Room
                        </a>
                    </li>
                </ul>
            </li>

            <li class="left-menu-list-submenu<?php if($this->router->fetch_class() == 'room_facility') { echo " left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-files-empty2"><!-- --></i>
                    Room Facility
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->router->fetch_class() == 'room_facility') {echo " style='display:block;'"; }?> >
                    <li>
                        <a class="left-menu-link" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_ROOM_FACILITY']; ?>">
                            List Room Facility
                        </a>
                    </li>
                    <li>
                        <a class="left-menu-link" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_ROOM_FACILITY']; ?>form">
                            Add Room Facility
                        </a>
                    </li>
                </ul>
            </li>

            <li class="left-menu-list-submenu<?php if($this->router->fetch_class() == 'room_display') { echo " left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-files-empty2"><!-- --></i>
                    Room Display
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->router->fetch_class() == 'room_display') {echo " style='display:block;'"; }?> >
                    <li>
                        <a class="left-menu-link" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_ROOM_DISPLAY']; ?>">
                            List Room Display
                        </a>
                    </li>
                </ul>
            </li>

            <li class="left-menu-list-separator"><!-- --></li>

            <li class="left-menu-list-submenu<?php if($this->router->fetch_class() == 'receptionist') { echo " left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-files-empty2"><!-- --></i>
                    Receptionist Page
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->router->fetch_class() == 'receptionist') {echo " style='display:block;'"; }?> >
                    <li>
                        <a class="left-menu-link" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_RECEPTIONIST']; ?>">
                            List Receptionist
                        </a>
                    </li>
                    <li>
                        <a class="left-menu-link" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_RECEPTIONIST']; ?>form">
                            Add Receptionist
                        </a>
                    </li>
                </ul>
            </li>

            <li class="left-menu-list-submenu<?php if($this->router->fetch_class() == 'receptionist_display') { echo " left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-files-empty2"><!-- --></i>
                    Receptionist Display
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->router->fetch_class() == 'receptionist_display') {echo " style='display:block;'"; }?> >
                    <li>
                        <a class="left-menu-link" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_RECEPTIONIST_DISPLAY']; ?>">
                            List Receptionist Display
                        </a>
                    </li>
                </ul>
            </li>

            <li class="left-menu-list-separator"><!-- --></li>

            <li class="left-menu-list-submenu<?php if($this->router->fetch_class() == 'device') { echo " left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-files-empty2"><!-- --></i>
                    Device
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->router->fetch_class() == 'device') {echo " style='display:block;'"; }?> >
                    <li>
                        <a class="left-menu-link" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_DEVICE']; ?>">
                            List Device
                        </a>
                    </li>
                </ul>
            </li>

            <li class="left-menu-list-separator"><!-- --></li>

            <li class="left-menu-list-submenu<?php if($this->router->fetch_class() == 'member') { echo " left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-files-empty2"><!-- --></i>
                    Member Page
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->router->fetch_class() == 'member') {echo " style='display:block;'"; }?> >
                    <li>
                        <a class="left-menu-link" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_MEMBER']; ?>">
                            List Member
                        </a>
                    </li>
                    <li>
                        <a class="left-menu-link" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_MEMBER']; ?>form">
                            Add Member
                        </a>
                    </li>
                </ul>
            </li>

            <li class="left-menu-list-separator"><!-- --></li>

            <li class="left-menu-list-submenu<?php if($this->router->fetch_class() == 'font_family') { echo " left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-files-empty2"><!-- --></i>
                    Font Family Page
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->router->fetch_class() == 'font_family') {echo " style='display:block;'"; }?> >
                    <li>
                        <a class="left-menu-link" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_FONT_FAMILY']; ?>">
                            List Font Family
                        </a>
                    </li>
                    <li>
                        <a class="left-menu-link" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_FONT_FAMILY']; ?>form">
                            Add Font Family
                        </a>
                    </li>
                </ul>
            </li>

            <li class="left-menu-list-submenu<?php if($this->router->fetch_class() == 'font_size') { echo " left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-files-empty2"><!-- --></i>
                    Font Size Page
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->router->fetch_class() == 'font_size') {echo " style='display:block;'"; }?> >
                    <li>
                        <a class="left-menu-link" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_FONT_SIZE']; ?>">
                            List Font Size
                        </a>
                    </li>
                    <li>
                        <a class="left-menu-link" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_FONT_SIZE']; ?>form">
                            Add Font Size
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        */?>
    </div>
</nav>
<nav class="top-menu">
    <div class="menu-icon-container hidden-md-up">
        <div class="animate-menu-button left-menu-toggle">
            <div><!-- --></div>
        </div>
    </div>
    <div class="menu">
        <?php /*
        <div class="menu-user-block">
            <div class="dropdown dropdown-avatar">
                <a href="javascript: void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="avatar" href="javascript:void(0);">
                        <img src="<?php echo base_url() . ASSETS_BACKEND_URL; ?>common/img/temp/avatars/1.jpg" alt="Alternative text to the image">
                    </span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="" role="menu">
                    <a class="dropdown-item" href="#"><i class="dropdown-icon icmn-user"></i> Profile</a>
                    <div class="dropdown-divider"></div>
                    <div class="dropdown-header">Menu</div>
                    <a class="dropdown-item" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_ADMINISTRATOR']; ?>form/<?php echo $this->session->userdata('admin_privilege_id'); ?>"><i class="dropdown-icon icmn-circle-right"></i> Change Password</a>
                    <?php if($privileges['privilege'] == 1):?>
                    <a class="dropdown-item" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_PRIVILEGE']; ?>"><i class="dropdown-icon icmn-circle-right"></i> Privilege</a>
                    <?php endif;?>
                    <?php if($privileges['administrator'] == 1):?>
                    <a class="dropdown-item" href="<?php echo base_url() . $this->setting['ADMIN_URL'] . $this->setting['ADMIN_URL_ADMINISTRATOR']; ?>"><i class="dropdown-icon icmn-circle-right"></i> Administrator</a>
                    <?php endif;?>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?php echo base_url() . ADMIN_URL_LOGOUT; ?>"><i class="dropdown-icon icmn-exit"></i> Logout</a>
                </ul>
            </div>
        </div>
        <div class="menu-info-block">
            ADMIN PANEL
        </div>
        */ ?>
    </div>
</nav>

<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3><?php if($this->uri->segment(4) == null) {echo "Add"; } else {echo "Update";} ?> <?php echo $this->menu_title; ?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <?php echo form_open_multipart(current_url());?>
                        <?php foreach($list_field as $field):?>
                        <div class="form-group row" id="field">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0"><?php echo $field; ?></label>
                            </div>
                            <div class="col-md-3">
                                 <select class="form-control" id="l13" id="attribute" name="<?php echo $field; ?>">
                                    <option value="0" class="option" <?php if($field == 0) { echo "selected='selected'"; } ?> >Simple Text Box</option>
                                    <option value="1" class="option" <?php if($field == 1) { echo "selected='selected'"; } ?> >Text Editor</option>
                                    <option value="2" class="option" <?php if($field == 1) { echo "selected='selected'"; } ?> >Dropdown Select</option>
                                    <option value="3" class="option" <?php if($field == 1) { echo "selected='selected'"; } ?> >Upload Image</option>
                                    <option value="4" class="option" <?php if($field == 1) { echo "selected='selected'"; } ?> >Radio</option>
                                    <option value="5" class="option" <?php if($field == 1) { echo "selected='selected'"; } ?> >Checkbox</option>
                                </select>
                            </div>
                            <div class="col-md-6" id="additional_attribute">
                            </div>
                        </div>
                        <?php endforeach; ?>

                        
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" class="btn width-150 btn-primary">Submit</button>
                                    <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                                </div>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->

</div>

</section>



<div class="main-backdrop"><!-- --></div>

<script>
    $(function(){
         $("#attribute").change(function () {

            //var end = this.value;
            //var firstDropVal = $('#pick').val();
            $("#additional_attribute").html('<select class="form-control" id="l13" id="attribute" name="additonal">' +
                    '<option value="0" class="option">Simple Text Box</option>' +
                    '<option value="1" class="option">Text Editor</option>' +
                '</select>');
        });
    });
 </script>

<!-- DATA TABLES -->
<script>
    $(function(){

        $('#example1').DataTable({
            responsive: true
        });

        $('#example2').DataTable({
            autoWidth: true,
            scrollX: true,
            fixedColumns: true
        });

        $('#example3').DataTable({
            autoWidth: true,
            scrollX: true,
            fixedColumns: true
        });
    });
</script>

<!-- SUMMERNOTE -->
<script>
    $(function() {
        $('#summernote').summernote({
            height: 350
        });
        $('#summernote2').summernote({
            height: 350
        });
    });
</script>

<!-- DATEPICKER -->
<script>
    $(function(){

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            defaultDate: "<?php echo date('j F Y'); ?>",
            format: 'D MMMM YYYY'
        });

    })
</script>

<!-- DELETE MODAL -->
<script>
    //Modal For Delete
    $(document).on('click', '.open-deleteModal', function () {
         var deleteId = $(this).data('id');
         $('.modal-body #modal').val( deleteId );
    });
</script>


<!-- End Page Scripts -->


</body>
</html>
