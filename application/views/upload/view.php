<?php $this->load->view('header'); ?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Upload File(s) From SAP/BPAM</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
               <!-- <div class="col-md-6">
                    <label class="form-control-label" for="l0">
                        File yang bisa diupload adalah <b>.csv, .xls, .xlxs</b> <br />
                        Format Cells untuk semua kolom harus <b>Text</b><br />
                        Format isi sebagai berikut:
                            <ul>
                                <li>Kolom 1 => NIP</li>
                                <li>Kolom 2 => Nama Perserta</li>
                                <li>Kolom 3 => RFID</li>
                                <li>Kolom 4 => Kadaluarsa Kartu (dd-mm-yyyy)</li>
                                <li>Kolom 5 => Program</li>
                            </ul>
                    </label>
                </div>
                <div class="col-md-6">
                    <label>Contoh:</label>
                    <img src="<?=base_url()."assets/example.png"?>">
                </div> -->
                <div class="form-group row"></div>
                <?php
                    $error     = $this->session->userdata('err_upload_list');
                    $error_msg = $this->session->userdata('msg_upload_list');
                    if($this->session->userdata('msg_upload_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('err_upload_list');
                    $this->session->unset_userdata('msg_upload_list');
                ?>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    
                    <div class="col-lg-12">
                        <form action="<?=base_url()?>upload/upload_program_file" method="post" enctype="multipart/form-data">
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Upload Program</label>
                                </div>
                                <div class="col-md-8">
                                   <input type="file" class="form-control" name="updloadProgram" />
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-12">
                        <form action="<?=base_url()?>upload/upload_employee_file" method="post" enctype="multipart/form-data">
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Upload Karyawan</label>
                                </div>
                                <div class="col-md-8">
                                   <input type="file" class="form-control" name="updloadEmployee" />
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-12">
                        <form action="<?=base_url()?>upload/upload_participant_file" method="post" enctype="multipart/form-data">
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Upload Peserta</label>
                                </div>
                                <div class="col-md-8">
                                   <input type="file" class="form-control" name="updloadParticipant" />
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-12">
                        <form action="<?=base_url()?>upload/upload_rfid_file" method="post" enctype="multipart/form-data">
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Upload RFID Data</label>
                                </div>
                                <div class="col-md-8">
                                   <input type="file" class="form-control" name="updloadRfid" />
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- <div class="col-lg-12">
                        <form action="<?=base_url()?>upload/upload_trainer_file" method="post" enctype="multipart/form-data">
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Upload Trainer</label>
                                </div>
                                <div class="col-md-8">
                                   <input type="file" class="form-control" name="updloadTrainer" />
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Upload</button>
                                </div>
                            </div>
                        </form>
                    </div> -->
                    <div class="col-lg-6">
                        <br /><br />
                        <label>Contoh File RFID:</label>
                        <img src="<?=base_url()."assets/rfidExample.png"?>">
                        <br /><br /><label>Tidak perlu ada header dan save dengan format <b>CSV</b> (Comma Delimited)</label>
                    </div>
                    <div class="col-lg-6"></div>
                <!-- End Horizontal Form-->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<script>
    $(function(){
        
        $('#summernote').summernote({
            height: 350
        });
        
        $('.datepicker-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD HH:mm:ss'
        });

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY'
        });

    })
</script>
<?php $this->load->view('footer');?>