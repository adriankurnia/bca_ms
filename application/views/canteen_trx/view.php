<?php $this->load->view('header'); ?>

<section class="page-content">
<div class="page-content-inner">
    <!--  -->
    <section class="panel">
        <div class="panel-heading">
            <h3>Laporan <?=$menu->menu_name?></h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
					<form action="<?php echo base_url() . 'api/export_data/export_to_excel/canteen_trx'?>" method="post" name="form_excel" id="form_excel">
						<input type="hidden" name="table_array" id="table_array" value="">
					</form>
					<button type="button" class="btn btn-rounded btn-info margin-inline" onclick="export_to_excel()">Export to Excel</button>
                    <!-- <a class="btn btn-rounded btn-success margin-inline" href="<?=base_url()?>training_transaction/add_form"> + <?=$menu->menu_name?></a> -->
                    <!-- <form action="<?=base_url()?>temP-user/search_by_date" method="post" name="adminForm" id="adminForm"> -->
                    <!-- <div class="margin-bottom-5">
                        Pull Date: 
                        <input class="form-control datepicker-only-init width-200 display-inline-block margin-inline" placeholder="From" type="text" name="date_from">
                        <span class="margin-right-10">—</span>
                        <input class="form-control datepicker-only-init width-200 display-inline-block margin-inline" placeholder="To" type="text" name="date_to">
                        <button type="submit" name="submit" class="btn btn-rounded btn-primary margin-inline">Go</button>
                    </div> -->
                    <?php
						$error		= $this->session->userdata('err_transaction_list');
						$error_msg	= $this->session->userdata('msg_transaction_list');
						if($this->session->userdata('msg_transaction_list'))
						{
							if($error == 0)
							{
								$class = "alert alert-primary";
							}
							else
							{
								$class = "alert alert-warning";
							}
							echo '
								<div class="'.$class.'" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<strong>'.$error_msg.'</strong>
								</div>';
						}
						$this->session->unset_userdata('msg_transaction_list');
						$this->session->unset_userdata('err_transaction_list');

						$ci = &get_instance();
		                $ci->load->model('employee_model');
					?>
                    <div class="margin-bottom-50">
                        <table class="table table-hover nowrap" id="table_view" width="100%">
                            <thead>
							<tr>
                                <th style="width:5%"></th>
                                <th style="width:15%" class="filterhead_dropdown">Tanggal</th>
                                <th style="width:15%" class="filterhead_dropdown">Waktu</th>
                                <th class="filterhead_text">NIP</th>
                                <th class="filterhead_text">Nama</th>
                                <th class="filterhead_text">Makanan</th>
                                <th class="filterhead_text">Status</th>
                                <th class="filterhead_text">Keterangan</th>

                                <?php
                                    // if($crud->update == 1) {
                                    //     echo "<th>ACTION</th>";
                                    // }

                                    // if($crud->delete == 1) {
                                    //     echo "<th>ACTION</th>";
                                    // }
                                ?>
                            </tr>
                            <tr id="table_header">
                                <th style="width:5%">NO.</th>
                                <th style="width:15%">Tanggal</th>
                                <th style="width:15%"">Waktu</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Makanan</th>
                                <th>Status</th>
                                <th>Keterangan</th>

                                <?php
                                    // if($crud->update == 1) {
                                    //     echo "<th>ACTION</th>";
                                    // }

                                    // if($crud->delete == 1) {
                                    //     echo "<th>ACTION</th>";
                                    // }
                                ?>
                            </tr>
                            </thead>
                            <!-- <tfoot>
                            <tr>
                                <th style="width:5%">NO.</th>
                                <th style="width:15%">Tanggal</th>
                                <th style="width:15%"">Waktu</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Makanan</th>
                                <th>Status</th>

                                <?php
                                    // if($crud->update == 1) {
                                    //     echo '<th>ACTION</th>';
                                    // }

                                    // if($crud->delete == 1) {
                                    //     echo '<th>ACTION</th>';
                                    // }
                                ?>
                            </tr>
                            </tfoot> -->
                            <tbody id="table_body">
                            
                            <?php
								if($transactions)
								{
                                    $i = 1;
									foreach($transactions as $trx)
									{
										$explode1 = explode(" ", $trx->trx_time);
										$explode2 = explode("-", $explode1[0]);
										$t_date   = $explode2[2]."-".$explode2[1]."-".$explode2[0];
										$t_time   = $explode1[1];

                                        if($trx->e_id != "")
                                		{
                                			$res = $ci->employee_model->employee_data($trx->e_id)->row();
                                			if($res) $emp_name = $res->e_name;
                                			else $emp_name = "";
                                		}
                                		else
                                		{
                                			$emp_name = "";
                                		}

										?>
										<tr>
                                            <td><?=$i?></td>
											<td><?=$t_date?></td>
                                            <td><?=$t_time?></td>
                                            <td><?=$trx->e_id?></td>
                                            <td><?=$emp_name?></td>
                                            <td><?=ucwords($trx->c_name)?></td>
                                            <td><?=($trx->trx_status == 1)?"Berhasil":"Gagal"?></td>
                                            <td><?=ucwords($trx->trx_notes)?></td>
                                            <?php
                                                // if($crud->update == 1) {
                                                //     echo '<td><a class="btn btn-rounded btn-info margin-inline" href="training_transaction/edit_form/'.$trx->p_id.'">Ubah</td>';
                                                // }

                                                // if($crud->delete == 1) {
                                                //     echo '<td><a class="btn btn-rounded btn-danger margin-inline" href="training_transaction/remove_data/'.$trx->p_id.'">Hapus</td>';
                                                // }
                                            ?>
										 </tr>	
									<?php
                                        $i++;
									}
								}
							?>
                            </tbody>
                        </table>
                    </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </section>
    <!-- End  -->

</div>

<!-- Page Scripts -->
<script>
    $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD'
        });
</script>

<script>
    // DataTable Custom
	$(document).ready(function() {
		
		var table = $('#table_view').DataTable({
			"scrollX": true,
            "scrollY": 350,
			"order": [[ 0, "asc" ]],
            "aLengthMenu": [
                [25, 50, 100, 200, -1],
                [25, 50, 100, 200, "All"]
            ],
			"language": {
				"search": "Search All:"
			}
		});
		
		$('.filterhead_text').each( function () {
			var title = $('#table_view thead th').eq( $(this).index() ).text();
			$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" style="width:70%" />' );
		});
		
		//disable repetitive 
		var table_index = [];
		$('.filterhead_dropdown').each( function (i) {
			var column_count = $(this).index();
			if(table_index.includes(column_count))
			{
				//Exist
				
			}
			else
			{
				//Push
				table_index.push(column_count);
				$(this).html( '<select id="dropdown_'+column_count+'" class="form-control"><option value="" ></option></select>' );
				table.column(column_count).data().unique().sort().each( function ( d, j ) {
					$('#dropdown_'+column_count).append( '<option value="'+d+'">'+d+'</option>' )
				});
			}
			
		});
		
		// Hide Table
        //var column = table.column(0).visible(false);
		
		//Initialize table
		table.draw();
		
		// Apply the filter

		$(".filterhead_text input").on( 'keyup change', function () {
			table
				.column( $(this).parent().index()+':visible' )
				.search( this.value )
				.draw();
		});
		
		$(".filterhead_dropdown select").on( 'change', function () {
			table
				.column( $(this).parent().index()+':visible' )
				.search( this.value )
				.draw();
		});
	});
	
	function export_to_excel()
	{
		var myTableHeader = [];
		var myTableArray = "";
		myTableArray += '{"table_data":[';
		
		$("table thead tr#table_header").each(function( index ) {
			if(index == 0)
			{
				var tableData = $(this).find('th');
				
				if (tableData.length > 0) {
					var column_num = 0;
					tableData.each(function() { 
						if(column_num < 8)
						{
							myTableHeader.push($(this).text());
							column_num++;
						}
						
					});
				}
			}
		});
		
		console.log(myTableHeader);
		
		$("table #table_body tr").each(function() {
			var arrayOfThisRow = "";
			var tableData = $(this).find('td');
			
			arrayOfThisRow += "{";
			if (tableData.length > 0) {
				var column_num = 0;
				tableData.each(function() { 
					if(column_num < 8)
					{
						arrayOfThisRow += '"' + myTableHeader[column_num] + '"' + ':' + '"' + $(this).text() + '"' + ','; 
						column_num++;
					}
				});
			}
			
			//Remove the last coma
			arrayOfThisRow = arrayOfThisRow.slice(0, -1);

			arrayOfThisRow += "},";
			
			myTableArray += arrayOfThisRow;
		});
		
		//Remove the last coma
		myTableArray = myTableArray.slice(0, -1);
		
		myTableArray += "]}";
		
		console.log(myTableArray);
		
		$("#table_array").val(myTableArray);
		
		$("form").submit();
	}
</script>

<!-- End Page Scripts -->
</section>
<?php $this->load->view('footer');?>