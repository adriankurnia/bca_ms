<?php 
    $this->load->view('header');
    // print_r($this->session->userdata('editing'));die("asd");
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Ubah <?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $error      = $this->session->userdata('err_holiday_list');
                    $error_msg  = $this->session->userdata('msg_holiday_list');
                    if($this->session->userdata('msg_holiday_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('msg_holiday_list');
                    $this->session->unset_userdata('err_holiday_list');
                ?>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>food_serving_holiday/edit_data" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Keterangan <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="hidden" name="holiday_id" value="<?=$holiday->id?>">
                                <input type="text" name="holiday_name" value="<?=$holiday->holiday_name?>" class="form-control" placeholder="Keterangan"  />
                                <div class="error_msg"><?php echo form_error('holiday_name');?></div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tgl Awal <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <?php
                                    $explode1 = explode(" ", $holiday->start);
                                    $explode2 = explode("-", $explode1[0]);
                                    $date = $explode2[2]."-".$explode2[1]."-".$explode2[0];
                                ?>
                                <input type="text" value="<?=$date?>" name="holiday_start" class="form-control datepicker-only-init" placeholder="Tgl Awal" />
                                <div class="error_msg"><?php echo form_error('holiday_start');?></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tgl Akhir <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <?php
                                    $explode1 = explode(" ", $holiday->end);
                                    $explode2 = explode("-", $explode1[0]);
                                    $date = $explode2[2]."-".$explode2[1]."-".$explode2[0];
                                ?>
                                <input type="text" value="<?=$date?>" name="holiday_end" class="form-control datepicker-only-init" placeholder="Tgl Akhir" />
                                <div class="error_msg"><?php echo form_error('holiday_end');?></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Status Penyajian <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" id="serving_status" name="serving_status" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357">
                                    <option value="0" <?=($holiday->status==0)?"selected":""?>>Tidak Aktif</option>;
                                    <option value="1" <?=($holiday->status==1)?"selected":""?>>Aktif</option>;
                                </select>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Kirim</button>
                                    <a href="<?=base_url()?>food_serving_holiday"><button type="button" class="btn btn-default">Batal</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<script>
    $(function(){
        
        $('#summernote').summernote({
            height: 350
        });
        
        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY'
        });

    })
</script>
<?php $this->load->view('footer');?>