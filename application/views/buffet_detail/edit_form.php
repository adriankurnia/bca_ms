<?php $this->load->view('header'); ?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3><?=$menu->menu_name?> <?=strtoupper(urldecode($buffet_name))?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $error      = $this->session->userdata('err_buffet_list');
                    $error_msg  = $this->session->userdata('msg_buffet_list');
                    if($this->session->userdata('msg_buffet_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('msg_buffet_list');
                    $this->session->unset_userdata('err_buffet_list');
                ?>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form id="form_submit" action="<?=base_url()?>buffet_detail/new_data/<?=$buffet_name?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <input type="hidden" name="c_id" value="<?=$c_id?>">
                        <!-- <input type="hidden" name="stall_id" value="<?=$stall_id?>"> -->
                        <div class="form-group row">
                            <div class="col-md-6">Nama Makanan</div>
                            <!-- <div class="col-md-2">Stok</div>
                            <div class="col-md-3">Tanggal Awal</div>
                            <div class="col-md-3">Tanggal Akhir</div> -->
                        </div>
                        <div  id="itemRows">
                            <?php
                                $rowNum = 0;
                                foreach($b_details as $bd)
                                {
                                    // $fdate         = $fs->start_date;
                                    // $explode       = explode("-", $fdate);
                                    // $starting_date = $explode[2]."-".$explode[1]."-".$explode[0];
                                    
                                    // $fdate         = $fs->end_date;
                                    // $explode       = explode("-", $fdate);
                                    // $ending_date   = $explode[2]."-".$explode[1]."-".$explode[0];
                            ?>
                                <div id="rowNum<?=$rowNum?>">
                                    <div class="form-group row">
                                        <!-- <div class="col-md-3">
                                            <input type="hidden" name="serving_id[]" value="<?=$fs->id?>">
                                            <select class="form-control" name="item_id_update[]" id="itemid_<?=$rowNum?>" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357" <?=($crud->update == 0)?"readonly":""?>>
                                                <option value="0" selected>- Nama Makanan -</option>
                                                <?php
                                                    foreach ($food_names as $fn) { ?>
                                                        <option value="<?=$fn->c_id?>" <?=($fs->c_id == $fn->c_id)?"selected":""?>><?=strtoupper($fn->c_name)?></option>;
                                                    <?php }
                                                ?>
                                            </select>
                                        </div> -->
                                        <div class="col-md-6">
                                            <input type="hidden" name="bItem_id[]" value="<?=$bd->id?>">
                                            <input id="bItem_<?=$rowNum?>" class="form-control" type="text" name="bItem_update[]" value="<?=strtoupper($bd->item_name)?>" placeholder="Nama Makanan" <?=($crud->update == 0)?"readonly":""?> />
                                        </div>
                                        <!-- <div class="col-md-3">
                                            <input id="startdate_<?=$rowNum?>" type="text" name="start_date_update[]" class="form-control datepicker-only-init" placeholder="Tanggal Awal" value="<?=$starting_date?>" <?=($crud->update == 0)?"readonly":""?> />
                                        </div>
                                        <div class="col-md-3">
                                            <input id="enddate_<?=$rowNum?>" type="text" name="end_date_update[]" class="form-control datepicker-only-init" placeholder="Tanggal Akhir" value="<?=$ending_date?>" <?=($crud->update == 0)?"readonly":""?> />
                                        </div> -->
                                        <?php
                                            if($crud->delete == 1)
                                            {
                                                ?>
                                                <div class="col-md-1">
                                                    <a href="<?=base_url()?>buffet_detail/remove_data/<?=$bd->id?>/<?=$c_id?>/<?=$buffet_name?>/<?=$bd->item_name?>"><button type="button" class="btn btn-rounded btn-danger margin-inline">Hapus</button></a>
                                                </div>
                                            <?php
                                            }
                                        ?>
                                        <div class="col-md-12 error" style="color: red"></div>
                                    </div>
                                </div>
                            <?php
                                $rowNum++;
                                }
                            ?>
                        </div>

                            <!-- <form id="itemRows" action="<?=base_url()?>food_serving/new_serving_food" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                                
                            </form> -->
                        <?php
                            if($crud->create == 1)
                            {
                                ?>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input onclick="addRow(this.form);" type="button" class="btn btn-rounded btn-info margin-inline" value="Tambah" />
                            </div>
                        </div>
                        <?php 
                            }
                        ?>

                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <?php
                                        if($crud->update == 1 || $crud->create == 1)
                                        {
                                    ?>
                                            <button type="submit" name="submit" class="btn width-150 btn-primary">Kirim</button>
                                    <?php 
                                        } 
                                    ?>
                                    <a href="<?=base_url()?>canteen"><button type="button" class="btn btn-default">Batal</button></a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<script>
    var rowNum = <?=$rowNum?>;
    var error = 0;
    var error2 = 0;
    function addRow(frm)
    {
        var row = '<div id="rowNum'+rowNum+'"><div class="form-group row"><div class="col-md-6"><input type="text" id="bItem_'+rowNum+'" name="bItem[]" class="form-control" placeholder="Nama Makanan" value="" /></div><div class="col-md-1" style="height: 40px"><button type="button" class="btn btn-rounded btn-danger margin-inline icmn-cross" onclick="removeRow('+rowNum+');"></button></div><div class="col-md-12 error" style="color: red"></div></div></div>';

        jQuery('#itemRows').append(row);
        showCalender();
        rowNum++;
    }

    function removeRow(rnum)
    {
        jQuery('#rowNum'+rnum).remove();
        rowNum--;
    }

    function showCalender()
    {
        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY',
            // format: 'YYYY-MM-DD'
        });
    }

    $(function(){
        
        $('#summernote').summernote({
            height: 350
        });

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY'
            //disabledDates: ['2017-10-01', '2017-10-03'],
            // format: 'YYYY-MM-DD'
        });
    })

</script>
<?php $this->load->view('footer');?>