<?php $this->load->view('header'); ?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Tambah <?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
				<?php
                    $error      = $this->session->userdata('err_menupriv_list');
                    $error_msg  = $this->session->userdata('msg_menupriv_list');
                    if($this->session->userdata('msg_menupriv_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('msg_menupriv_list');
                    $this->session->unset_userdata('err_menupriv_list');
                ?>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>buffet_detail/new_data" method="post" accept-charset="utf-8" enctype="multipart/form-data">
						<div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Nama Buffet</label>
                            </div>
                            <div class="col-md-8">
                                <select class="form-control" id="c_id" name="c_id" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357">
                                    <option value="0">- Nama Buffet -</option>
                                    <?php
                                        foreach ($buffets as $b) { ?>
                                            <option value="<?=$b->c_id?>"><?=$b->c_name?></option>;
                                        <?php }
                                    ?>
                                </select>
                                <div class="error_msg"><?php echo form_error('c_id');?></div>
                            </div>
                        </div>
                        <div id="itemRows">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="form-control-label" for="l0">Nama Makanan</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" name="item_name[]" value="" class="form-control" placeholder="Nama Makanan"  />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input onclick="addRow(this.form);" type="button" class="btn btn-rounded btn-info margin-inline" value="Tambah Makanan Lain" />
                            </div>
                        </div>

				        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Kirim</button>
                                    <a href="<?=base_url()?>buffet_detail"><button type="button" class="btn btn-default">Batal</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<script>
    $(function(){
		
		$('#summernote').summernote({
            height: 350
        });
		
        $('.datepicker-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
			format: 'YYYY-MM-DD HH:mm:ss'
        });

    })

    var rowNum = 0;
    function addRow(frm)
    {
        rowNum ++;

        var row = '<div id="rowNum'+rowNum+'"><div class="form-group row"><div class="col-md-3"><label class="form-control-label" for="l0">Nama Makanan</label></div><div class="col-md-8"><input type="text" name="item_name[]" value="" class="form-control" placeholder="Nama Makanan"  /></div><div class="col-md-1"><button type="button" class="btn btn-rounded btn-danger margin-inline icmn-cross" onclick="removeRow('+rowNum+');"></button></div></div></div>';

        jQuery('#itemRows').append(row);
    }

    function removeRow(rnum)
    {
        jQuery('#rowNum'+rnum).remove();
    }

</script>
<?php $this->load->view('footer');?>