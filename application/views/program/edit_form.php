<?php
    $this->load->view('header');
    // print_r($this->session->userdata('editing'));die("asd");
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Ubah <?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $error      = $this->session->userdata('err_program_list');
                    $error_msg  = $this->session->userdata('msg_program_list');
                    if($this->session->userdata('msg_program_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('msg_program_list');
                    $this->session->unset_userdata('err_program_list');
                ?>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>program/edit_data" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Nama Program <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="hidden" name="program_id" value="<?=$program->p_id?>">
                                <input type="text" name="program_name" value="<?= $program->program_name ?>" class="form-control" placeholder="Nama Program"  />
                                <div class="error_msg"><?php echo form_error('program_name');?></div>
                            </div>
                        </div>

                        <!-- <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Subprogram Name</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" id="subprogram_name" name="subprogram_name" value="<?= $program->subprogram_name ?>" class="form-control" placeholder="Nama Sub Program"  />
                                <div class="error_msg"><?php echo form_error('subprogram_name');?></div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Pelatihan</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" id="training_name" name="training_name" value="<?= $program->training_name ?>" class="form-control" placeholder="Nama Pelatihan"  />
                                <div class="error_msg"><?php echo form_error('training_name');?></div>
                            </div>
                        </div> -->
                        
                        <!-- <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Program Mulai</label>
                            </div>
                            <div class="col-md-9">
                                <?php
                                if($program->program_begin != null || $program->program_begin != "" || !empty($program->program_begin))
                                    {
                                        $explode = explode("-", $program->program_begin);
                                        $date = $explode[2]."-".$explode[1]."-".$explode[0];
                                    }
                                    else
                                    {
                                        $date = "";
                                    }
                                ?>
                                <input type="text" id="program_begin" name="program_begin" value="<?= $date ?>" class="form-control datepicker-only-init" placeholder="Program Mulai" />
                                <div class="error_msg"><?php echo form_error('program_begin');?></div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Program Selesai</label>
                            </div>
                            <div class="col-md-9">
                                <?php
                                if($program->program_end != null || $program->program_end != "" || !empty($program->program_end))
                                    {
                                        $explode = explode("-", $program->program_end);
                                        $date = $explode[2]."-".$explode[1]."-".$explode[0];
                                    }
                                    else
                                    {
                                        $date = "";
                                    }
                                ?>
                                <input type="text" id="program_end" name="program_end" value="<?= $date ?>" class="form-control datepicker-only-init" placeholder="Program Selesai" />
                                <div class="error_msg"><?php echo form_error('program_end');?></div>
                            </div>
                        </div> -->

                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Kirim</button>
                                    <a href="<?=base_url()?>program"><button type="button" class="btn btn-default">Batal</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<script>

    function showPass()
    {
        if($('#changePass').is(':checked'))
        {
            $('#password_change').show();
        }
        else
        {
            $('#password_change').hide();
        }
    }

    $(document).ready(function(){
        $('#password_change').show();
    });

    $(function(){
        
        $('#summernote').summernote({
            height: 350
        });
        
        $('.datepicker-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD HH:mm:ss'
        });

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY'
        });

    })
</script>
<?php $this->load->view('footer');?>