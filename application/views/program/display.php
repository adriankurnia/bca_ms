<?php 
    $this->load->view('headerDisplay');
    // print_r($this->session->userdata('editing'));die("asd");
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3><?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>employee/edit_employee" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Nama Program</label>
                            </div>
                            <div class="col-md-9">
                                <input type="hidden" name="program_id" value="<?=$program->p_id?>">
                                <input type="text" name="program_name" value="<?= $program->program_name ?>" class="form-control" placeholder="Nama Program" disabled />
                            </div>
                        </div>

                        <!-- <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Subprogram Name</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" id="subprogram_name" name="subprogram_name" value="<?= $program->subprogram_name ?>" class="form-control" placeholder="Nama Sub Program" disabled />
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Pelatihan</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" id="training_name" name="training_name" value="<?= $program->training_name ?>" class="form-control" placeholder="Nama Pelatihan" disabled />
                            </div>
                        </div> -->
                        
                        <!-- <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Program Mulai</label>
                            </div>
                            <div class="col-md-9">
                                <?php
                                if($program->program_begin != null || $program->program_begin != "" || !empty($program->program_begin))
                                    {
                                        $explode = explode("-", $program->program_begin);
                                        $date = $explode[2]."-".$explode[1]."-".$explode[0];
                                    }
                                    else
                                    {
                                        $date = "";
                                    }
                                ?>
                                <input type="text" id="program_begin" name="program_begin" value="<?= $date ?>" class="form-control datepicker-only-init" placeholder="Program Mulai" disabled />
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Program Selesai</label>
                            </div>
                            <div class="col-md-9">
                                <?php
                                if($program->program_end != null || $program->program_end != "" || !empty($program->program_end))
                                    {
                                        $explode = explode("-", $program->program_end);
                                        $date = $explode[2]."-".$explode[1]."-".$explode[0];
                                    }
                                    else
                                    {
                                        $date = "";
                                    }
                                ?>
                                <input type="text" id="program_end" name="program_end" value="<?= $date ?>" class="form-control datepicker-only-init" placeholder="Program Selesai" disabled />
                            </div>
                        </div> -->
                        
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <a href="<?=base_url()?>program"><button type="button" class="btn btn-default">Kembali</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>
</section>

<?php $this->load->view('footer');?>