<?php 
    $this->load->view('headerDisplay');
    // print_r($this->session->userdata('editing'));die("asd");
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3><?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>employee/edit_employee" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Keterangan</label>
                            </div>
                            <div class="col-md-9">
                                <input type="hidden" name="event_id" value="<?=$event->id?>">
                                <input type="text" name="event_name" value="<?=$event->event_name?>" class="form-control" placeholder="Keterangan" disabled />
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tgl Awal</label>
                            </div>
                            <div class="col-md-9">
                                <?php
                                    $explode1 = explode(" ", $event->start);
                                    $explode2 = explode("-", $explode1[0]);
                                    $date = $explode2[2]."-".$explode2[1]."-".$explode2[0];
                                ?>
                                <input type="text" value="<?=$date?>" name="event_start" class="form-control datepicker-only-init" placeholder="Tgl Awal" disabled />
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tgl Akhir</label>
                            </div>
                            <div class="col-md-9">
                                <?php
                                    $explode1 = explode(" ", $event->end);
                                    $explode2 = explode("-", $explode1[0]);
                                    $date = $explode2[2]."-".$explode2[1]."-".$explode2[0];
                                ?>
                                <input type="text" value="<?=$date?>" name="event_end" class="form-control datepicker-only-init" placeholder="Tgl Akhir" disabled />
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <a href="<?=base_url()?>food_serving_buffet"><button type="button" class="btn btn-default">Kembali</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<?php $this->load->view('footer');?>