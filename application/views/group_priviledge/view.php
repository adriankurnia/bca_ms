<?php $this->load->view('header'); ?>

<section class="page-content">
<div class="page-content-inner">
    <!--  -->
    <section class="panel">
        <div class="panel-heading">
            <h3><?=$menu->menu_name?></h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <?php
                    if($crud->create == 1)
                    {
                        ?>
                    <a class="btn btn-rounded btn-success margin-inline" href="<?=base_url()?>group_priviledge/add_form"> + <?=$menu->menu_name?></a>
                    <?php } ?>
                    <!-- <form action="<?=base_url()?>temP-user/search_by_date" method="post" name="adminForm" id="adminForm"> -->
                    <!-- <div class="margin-bottom-5">
                        Pull Date: 
                        <input class="form-control datepicker-only-init width-200 display-inline-block margin-inline" placeholder="From" type="text" name="date_from">
                        <span class="margin-right-10">—</span>
                        <input class="form-control datepicker-only-init width-200 display-inline-block margin-inline" placeholder="To" type="text" name="date_to">
                        <button type="submit" name="submit" class="btn btn-rounded btn-primary margin-inline">Go</button>
                    </div> -->
                    <?php
						$error		= $this->session->userdata('err_grouppriv_list');
						$error_msg	= $this->session->userdata('msg_grouppriv_list');
						if($this->session->userdata('msg_grouppriv_list'))
						{
							if($error == 0)
							{
								$class = "alert alert-primary";
							}
							else
							{
								$class = "alert alert-warning";
							}
							echo '
								<div class="'.$class.'" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<strong>'.$error_msg.'</strong>
								</div>';
						}
						$this->session->unset_userdata('msg_grouppriv_list');
						$this->session->unset_userdata('err_grouppriv_list');
					?>
                    <div class="margin-bottom-50">
                        <table class="table table-hover nowrap" id="example1" width="100%">
                            <thead>
                            <tr>
                                <th>NO.</th>
                                <th>Nama Grup</th>
                                <th>Tanggal Dibuat</th>
                                <th>Dibuat Oleh</th>
                                <th>Tanggal Diedit</th>
                                <th>Diedit Oleh</th>

                                <?php
                                    if($crud->update == 1 || $crud->delete == 1) {
                                        echo "<th>ACTION</th>";
                                    }

                                    // if($crud->delete == 1) {
                                    //     echo "<th>ACTION</th>";
                                    // }
                                ?>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>NO.</th>
                                <th>Nama Grup</th>
                                <th>Tanggal Dibuat</th>
                                <th>Dibuat Oleh</th>
                                <th>Tanggal Diedit</th>
                                <th>Diedit Oleh</th>

                                <?php
                                    if($crud->update == 1 || $crud->delete == 1) {
                                        echo "<th>ACTION</th>";
                                    }

                                    // if($crud->delete == 1) {
                                    //     echo "<th>ACTION</th>";
                                    // }
                                ?>
                            </tr>
                            </tfoot>
                            <tbody>
                            
                            <?php
								if($g_privs)
								{
                                    $i = 1;
									foreach($g_privs as $priv)
									{
                                        if($priv->id != 1)
                                        {
                                            $explode1 = explode(" ", $priv->created_date);
                                            $explode2 = explode("-", $explode1[0]);
                                            $c_date = $explode2[2]."-".$explode2[1]."-".$explode2[0];

                                            if($priv->edited_date != null || $priv->edited_date != "" || !empty($priv->edited_date))
                                            {
                                                $explode1 = explode(" ", $priv->edited_date);
                                                $explode2 = explode("-", $explode1[0]);
                                                $e_date = $explode2[2]."-".$explode2[1]."-".$explode2[0];
                                            }
                                            else
                                            {
                                                $e_date = null;
                                            }
    										?>
    										<tr>
                                                <td><?=$i?></td>
    											<td><?=$priv->group_name?></td>
                                                <td><?=$c_date?></td>
                                                <td><?=$priv->created_by?></td>
                                                <td><?=$e_date?></td>
                                                <td><?=$priv->edited_by?></td>
                                                <td>
                                                <?php
                                                    if($crud->update == 1) {
                                                        echo '<a class="btn btn-rounded btn-info margin-inline icmn-pencil" href="group_priviledge/edit_form/'.$priv->id.'">';
                                                    }

                                                    if($crud->delete == 1) {
                                                        echo '<a class="btn btn-rounded btn-danger margin-inline icmn-bin" href="group_priviledge/remove_data/'.$priv->id.'/'.$priv->group_name.'">';
                                                    }
                                                ?>
                                            </td>
    										 </tr>	
    									<?php
                                            $i++;
    									}
                                    }
								}
							?>
                            </tbody>
                        </table>
                    </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </section>
    <!-- End  -->

</div>

<!-- Page Scripts -->
<script>

    $(function(){

        $('#example1').DataTable({
            responsive: true,
            "order": [[ 0, "desc" ]]
        });

        $('#example2').DataTable({
            autoWidth: true,
            scrollX: true,
            fixedColumns: true
        });

        $('#example3').DataTable({
            autoWidth: true,
            scrollX: true,
            fixedColumns: true
        });
    });

    $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD'
        });
</script>

<!-- End Page Scripts -->
</section>
<?php $this->load->view('footer');?>