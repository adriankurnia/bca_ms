<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>BCA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/fontface.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/swiper.min.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/project.css" rel="stylesheet">

	<script>
		var monthNames = [ "Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" ];
	</script>

</head>

<body>



<!-- List -->
<div id="proj-logo"><img src="<?php echo base_url() . 'assets/frontend/'; ?>media/logo.svg" alt="" /></div>

<div id="proj-day"></div>
<div id="proj-time"></div>
    <style>
        #proj-buffet:after{
            content: none;!important;
        }
    </style>
<div id="proj-buffet">

    <form style="position: absolute; left: -1000px;"  id="form_submit" method="POST" enctype="multipart/form-data" action="#" onsubmit="doCheckRfid();return false;">
    <input type="text" name="rfid" id="rfid" value="" autofocus>
  </form>
    <span id="changeContent">

    </span>

</div>
<!-- End List -->

<!-- JS -->
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/jquery.min.js"></script>
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/swiper.min.js"></script>
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/project.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script>
	function doCheckRfid(){

		$.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>website/gettaprfid",
            dataType: "json",
            cache: false,
            data:$("#form_submit").serialize(),
            success: function(temp)
            {
                console.log(temp);
                    if(temp.result == 1){
                        $("#changeContent").empty().html(temp.content);

                    }else{
                         $("#changeContent").empty();
                        alert(temp.message);

                    }
                $("#rfid").val('');

            }
        });
    }
</script>

</body>
</html>
