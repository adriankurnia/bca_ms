<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>BCA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/fontface.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/swiper.min.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/project.css" rel="stylesheet">
	
	<script>
		var monthNames = [ "Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" ];
	</script>
	
</head>

<body>



<!-- List -->
<div id="proj-logo"><img src="<?php echo base_url() . 'assets/frontend/'; ?>media/logo.svg" alt="" /></div>
<div id="proj-room" class="buffet">
	<div id="proj-roomname">
        <?php if(!empty($buffets)) {?>
            <?php echo $buffets; ?><br />
        <?php 
            }
            else
            {
                echo "Tidak Ada Penyajian Makanan";
            }
        ?>
    </div>
    
</div>
<div id="proj-day"></div>
<div id="proj-time"></div>
<div id="proj-buffet">
	<div class="proj-buffet-ribbon top"><img src="<?php echo base_url() . 'assets/frontend/'; ?>media/ribbon-top.png" alt="" /></div>
	<div class="proj-buffet-ribbon bottom"><img src="<?php echo base_url() . 'assets/frontend/'; ?>media/ribbon-bottom.png" alt="" /></div>
	
	<div class="proj-buffet left">
		<div class="proj-table">
			<div class="proj-tablecell middle">
				<?php 
					if(!empty($lists_foods))
					{
					foreach($lists_foods as $foods): ?>
					<?php foreach($foods as $food):?>
						<div class="proj-buffet">
							<div class="proj-buffetname"><?php echo ucwords(strtolower($food->item_name)); ?></div>
						</div>
					<?php endforeach;?>
					<?php endforeach; }?>
        	</div>
        </div>
	</div>
	<div class="proj-buffet right">
		<div class="proj-table">
			<div class="proj-tablecell middle">
				<div class="proj-buffet-stok-title">Stok</div>
				<div class="proj-buffet-stok" id="stock_<?php echo $c_id;?>"><?=$stock?></div>
        	</div>
        </div>
	</div>
</div>
<!-- End List -->

<!-- JS -->
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/jquery.min.js"></script>
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/swiper.min.js"></script>
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/project.js"></script>
	
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script>
	$( document ).ready(function() {
	    setInterval(function () {
	    refreshPage();
	  }, 10000);
	});

	function refreshPage()
	{
		var location_id = <?php echo $this->uri->segment(3); ?>

		$.ajax({
        type: "GET",
        url: "<?php echo base_url(); ?>website/refresh_page/"+location_id,
        dataType: "json",
        cache: false,
        success: function(response)
        {
            console.log(response);

            if(response['ok'] == '1')
            {
            	for(var key in response['data'])
                {
                	if (response['data'].hasOwnProperty(key)) 
                    {
                    	//Update Stock
                    	if(response['data'][key]['stock'] <= 0)
                    	{
                            //alert("habis");
                    		$("#stock_"+response['data'][key]['c_id']).html('-');
                    	}
                    	else
                    	{
                    		$("#stock_"+response['data'][key]['c_id']).html(response['data'][key]['stock']);
                    	}
                    }
                }
            }
        }
    });
	}
</script>

</body>
</html>
