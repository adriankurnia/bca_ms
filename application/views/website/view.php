<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>BCA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/fontface.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/swiper.min.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/project.css" rel="stylesheet">
	
	<script>
		var monthNames = [ "Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" ];
	</script>
	
</head>

<body>



<!-- List -->
<div id="proj-logo"><img src="<?php echo base_url() . 'assets/frontend/'; ?>media/logo.svg" alt="" /></div>
<div id="proj-room">
	<div id="proj-roomname">PILIH LOKASI</div>
	<select id="location">
		<?php foreach($list_location as $location): ?>
			<option value="<?php echo $location->l_id?>"><?php echo strtoupper($location->l_floor . " - " . $location->l_room); ?></option>
		<?php endforeach; ?>
	</select>
</div>
<div id="proj-day"></div>
<div id="proj-time"></div>
<div id="proj-list">Silahkan Pilih Lokasi Terlebih Dahulu</div>
<!-- End List -->



    
<!-- JS -->
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/jquery.min.js"></script>
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/swiper.min.js"></script>
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/project.js"></script>
	
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script>
	var base_url = "<?php echo base_url(); ?>";

	$('select#location').on('change', function() {
	  window.location = base_url+"website/location/"+this.value;
	})
</script>

</body>
</html>
