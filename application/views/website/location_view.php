<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>BCA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/fontface.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/swiper.min.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/project.css" rel="stylesheet">
	
	<script>
		var monthNames = [ "Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" ];
	</script>
	
</head>

<body>



<!-- List -->
<div id="proj-logo"><img src="<?php echo base_url() . 'assets/frontend/'; ?>media/logo.svg" alt="" /></div>
<div id="proj-room">
	<div id="proj-roomname"><?php echo $location; ?></div>
	<select id="location">
		<?php foreach($list_location as $location): ?>
			<option value="<?php echo $location->l_id?>" <?php if($this->uri->segment(3) == $location->l_id) { echo "selected"; } ?>><?php echo strtoupper($location->l_floor . " - " . $location->l_room); ?></option>
		<?php endforeach; ?>
	</select>
</div>
<div id="proj-day"></div>
<div id="proj-time"></div>
<div id="proj-list">
	<?php if($ok == 0): ?>
		<?php echo $message; ?>
	<?php elseif($ok == 1): ?>
		<?php foreach($list_foods as $food): ?>
			<div id="status_<?php echo $food->id; ?>" class="proj-list <?php if($food->stock <= 0) { echo "oos"; } elseif($food->stock <= 10) { echo "warning"; }?>">
				<div class="proj-list-name"><?php echo strtoupper($food->c_name . " - " . $food->device_name); ?></div>
				<div id="stock_<?php echo $food->id; ?>" class="proj-list-qty"><?php echo $food->stock; ?></div>
			</div>
		<?php endforeach;?>
	<?php endif; ?>
</div>
<!-- End List -->

<!-- JS -->
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/jquery.min.js"></script>
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/swiper.min.js"></script>
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/project.js"></script>
	
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script>
	$( document ).ready(function() {
	    setInterval(function () {
	    refreshPage();
	  }, 10000);
	});

	function refreshPage()
	{
		var location_id = <?php echo $this->uri->segment(3); ?>

		$.ajax({
        type: "GET",
        url: "<?php echo base_url(); ?>website/refresh_page/"+location_id,
        dataType: "json",
        cache: false,
        success: function(response)
        {
            console.log(response);

            if(response['ok'] == '1')
            {
            	for(var key in response['data'])
                {
                	if (response['data'].hasOwnProperty(key)) 
                    {
                    	//Update Stock
                    	if(response['data'][key]['stock'] <= 0)
                    	{
                    		$("#stock_"+response['data'][key]['id']).html('-');
                    	}
                    	else
                    	{
                    		$("#stock_"+response['data'][key]['id']).html(response['data'][key]['stock']);
                    	}
                    	
                    	//Update Status
                    	if(response['data'][key]['stock'] <= 0)
                    	{
                    		$("#status_"+response['data'][key]['id']).removeClass();
                    		$("#status_"+response['data'][key]['id']).addClass("proj-list oos");
                    	}
                    	else if(response['data'][key]['stock'] <= 10)
                    	{
                    		$("#status_"+response['data'][key]['id']).removeClass();
                    		$("#status_"+response['data'][key]['id']).addClass("proj-list warning");
                    	}
                    	else
                    	{
                    		$("#status_"+response['data'][key]['id']).removeClass();
                    		$("#status_"+response['data'][key]['id']).addClass("proj-list");
                    	}
                    }
                }
            }
        }
    });
	}
</script>


<script>
	var base_url = "<?php echo base_url(); ?>";

	$('select#location').on('change', function() {
	  window.location = base_url+"website/location/"+this.value;
	})
</script>

</body>
</html>
