<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>BCA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/fontface.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/swiper.min.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/frontend/'; ?>css/project.css" rel="stylesheet">
	
	<script>
		var monthNames = [ "Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" ];
	</script>
	
</head>

<body>


<?php
    $ci = &get_instance();
    $ci->load->model('food_serving_buffet_model');
    $buffet_only = 0;
    $date_now = date("Y-m-d",time());
    $result = $ci->food_serving_buffet_model->event_time($date_now)->row();
    if($result != null)
    {
        $buffet_only = 1;
        $start       = $result->start;
        $end         = $result->end;
    }
?>

<!-- List -->
<div id="proj-logo"><img src="<?php echo base_url() . 'assets/frontend/'; ?>media/logo.svg" alt="" /></div>
<div id="proj-room">
	<div id="proj-roomname">
        <?php 
            //echo $location;
            if($buffet_only == 1)
            {
                $unix_start = strtotime($start);
                $unix_end = strtotime($end) + 23*3600 + 59*60 + 59;
                if(time() >= $unix_start && time() <= $unix_end) echo "Buffet";
                else echo $location;
            }
            else echo $location;
        ?>
    </div>
    
    <select id="location">
        <?php
            $index = 0; $liveStock = 0;
            foreach($stocks as $res)
            {
                $liveStock += $res->stock;
            }
        ?>
        <?php foreach($list_location as $location): ?>
            <?php $index++; ?>
            <option value="<?php echo $index; ?>"><?php echo strtoupper($location->l_room." (lt. ".$location->l_floor.")"); ?></option>
        <?php endforeach; ?>
    </select>
    
</div>
<div id="proj-day"></div>
<div id="proj-time"></div>


<div id="proj-list">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <?php 
                if($buffet_only == 0)
                {
                foreach($lists as $locations): 
            ?>
                    <div class="swiper-slide">
                    	
                        <?php if(!empty($locations['list_foods'])): ?>
                            
                                <?php foreach($locations['list_foods'] as $food): ?>
                    			<div id="status_<?php echo $food->id; ?>" class="proj-list <?php if($food->stock <= 0) { echo "oos"; } elseif($food->stock <= 10) { echo "warning"; }?>">
                    				<div class="proj-list-name"><?php echo strtoupper($food->c_name); ?></div>
                    				<div id="stock_<?php echo $food->id; ?>" class="proj-list-qty"><?php echo $food->stock; ?></div>
                    			</div>
                                <?php endforeach;?>
                            
                        <?php else: ?>
                            <?php echo "Tidak Ada Makanan di: " . $locations['location']; ?>
                        <?php endif; ?>
                    
                    </div>
            <?php 
                endforeach;
                if(!empty($lists_foods)): ?>
                    <div class="swiper-slide">
                        <div id="proj-buffet">
                            <div class="proj-buffet-ribbon top"><img src="<?php echo base_url() . 'assets/frontend/'; ?>media/ribbon-top.png" alt="" /></div>
                            <div class="proj-buffet-ribbon bottom"><img src="<?php echo base_url() . 'assets/frontend/'; ?>media/ribbon-bottom.png" alt="" /></div>

                            <div class="proj-buffet left">
                                <div class="proj-table">
                                    <div class="proj-tablecell middle">
                                        <?php 
                                            if(!empty($lists_foods))
                                            {
                                            foreach($lists_foods as $foods): ?>
                                            <?php foreach($foods as $food):?>
                                                <div class="proj-buffet">
                                                    <div class="proj-buffetname" style="font-size: 50px"><?php echo $food->item_name; ?></div>
                                                </div>
                                            <?php endforeach;?>
                                            <?php endforeach; }?>
                                    </div>
                                </div>
                            </div>
                            <div class="proj-buffet right">
                                <div class="proj-table">
                                    <div class="proj-tablecell middle">
                                        <div class="proj-buffet-stok-title">Stok</div>
                                        <?php 
                                        if($c_id!=0)
                                        { ?>
                                        <div class="proj-buffet-stok" id="total_buffet"><?=$liveStock;?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php 
                endif;
                }
                else
                {

                if(!empty($lists_foods)): ?>
                    <div class="swiper-slide">
                    	<div id="proj-buffet">
    						<div class="proj-buffet-ribbon top"><img src="<?php echo base_url() . 'assets/frontend/'; ?>media/ribbon-top.png" alt="" /></div>
    						<div class="proj-buffet-ribbon bottom"><img src="<?php echo base_url() . 'assets/frontend/'; ?>media/ribbon-bottom.png" alt="" /></div>

    						<div class="proj-buffet left">
    							<div class="proj-table">
    								<div class="proj-tablecell middle">
    									<?php 
    										if(!empty($lists_foods))
    										{
    										foreach($lists_foods as $foods): ?>
    										<?php foreach($foods as $food):?>
    											<div class="proj-buffet">
    												<div class="proj-buffetname" style="font-size: 50px"><?php echo $food->item_name; ?></div>
    											</div>
    										<?php endforeach;?>
    										<?php endforeach; }?>
    								</div>
    							</div>
    						</div>
    						<div class="proj-buffet right">
    							<div class="proj-table">
    								<div class="proj-tablecell middle">
    									<div class="proj-buffet-stok-title">Stok</div>
                                        <?php 
                                        if($c_id!=0)
                                        { ?>
    									<div class="proj-buffet-stok" id="total_buffet"><?=$liveStock;?></div>
                                        <?php } ?>
    								</div>
    							</div>
    						</div>
                        </div>
                    </div>
            <?php 
                endif; 
                }
            ?>
        </div>
    </div>
</div>
<!-- End List -->

<!-- JS -->
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/jquery.min.js"></script>
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/swiper.min.js"></script>
<script src="<?php echo base_url() . 'assets/frontend/'; ?>js/project.js"></script>
	
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script>
	$( document ).ready(function() {
	    setInterval(function () {
	    refreshPage();
	  }, 3000 );

        refreshAt(10, 45, 0);
        refreshAt(11, 0, 0);
        refreshAt(11, 15, 0);
        refreshAt(11, 30, 0);
        refreshAt(11, 45, 0);
        refreshAt(12, 0, 0);
        refreshAt(12, 15, 0);
        refreshAt(12, 30, 0);
        refreshAt(12, 45, 0);
        refreshAt(13, 0, 0);
        refreshAt(13, 15, 0);
        refreshAt(13, 30, 0);
        refreshAt(13, 45, 0);
        refreshAt(14, 0, 0);
	});

	function refreshPage()
	{
		$.ajax({
        type: "GET",
        url: "<?php echo base_url(); ?>website/refresh_page/",
        dataType: "json",
        cache: false,
        success: function(response)
        {
            console.log(response);

            if(response['ok'] == '1')
            {
                //Update Buffet Stock
                if(response['total_buffet'] <= 0)
                {
                    $("#total_buffet").html('-');
                }
                else
                {
                    $("#total_buffet").html(response['total_buffet']);
                }

            	for(var key in response['data'])
                {
                	if (response['data'].hasOwnProperty(key)) 
                    {
                    	//Update Stock
                    	if(response['data'][key]['stock'] <= 0)
                    	{
                    		$("#stock_"+response['data'][key]['id']).html('-');
                    	}
                    	else
                    	{
                    		$("#stock_"+response['data'][key]['id']).html(response['data'][key]['stock']);
                    	}
                    	
                    	//Update Status
                    	if(response['data'][key]['stock'] <= 0)
                    	{
                    		$("#status_"+response['data'][key]['id']).removeClass();
                    		$("#status_"+response['data'][key]['id']).addClass("proj-list oos");
                    	}
                    	else if(response['data'][key]['stock'] <= 10)
                    	{
                    		$("#status_"+response['data'][key]['id']).removeClass();
                    		$("#status_"+response['data'][key]['id']).addClass("proj-list warning");
                    	}
                    	else
                    	{
                    		$("#status_"+response['data'][key]['id']).removeClass();
                    		$("#status_"+response['data'][key]['id']).addClass("proj-list");
                    	}
                    }
                }
            }
        }
    });
	}
</script>


<script>
    /* Swiper
    ============================== */
    var swiper = new Swiper('#proj-list .swiper-container', {
        autoplay: 7000,
        width: 1920
    });

    swiper.on('SlideChangeStart', function (e) {
        $("#proj-roomname").html($("#location option:eq("+e.activeIndex+")").text());
    });


	$('select#location').on('change', function() {
        $("#location").html(this.text);
        var index = this.value;

        console.log(index);

        swiper.slideTo((index-1),1000,false);
	})


    function refreshAt(hours, minutes, seconds)
    {
        var now = new Date();
        var then = new Date();

        if(now.getHours() > hours ||
           (now.getHours() == hours && now.getMinutes() > minutes) ||
            now.getHours() == hours && now.getMinutes() == minutes && now.getSeconds() >= seconds) {
            then.setDate(now.getDate() + 1);
        }
        then.setHours(hours);
        then.setMinutes(minutes);
        then.setSeconds(seconds);

        var timeout = (then.getTime() - now.getTime());
        setTimeout(function() { window.location.reload(true); }, timeout);
    }
</script>

</body>
</html>
