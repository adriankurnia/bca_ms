<?php 
    $this->load->view('header');
    // print_r($this->session->userdata('editing'));die("asd");
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Ubah <?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $error     = $this->session->userdata('err_user_list');
                    $error_msg = $this->session->userdata('msg_user_list');
                    if($this->session->userdata('msg_user_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('err_user_list');
                    $this->session->unset_userdata('msg_user_list');
                ?>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>user_list/edit_user" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Username <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="hidden" name="oldUsername" value="<?= $user->username ?>" />
                                <input type="text" name="username" value="<?= $user->username ?>" class="form-control" placeholder="Username" />
                                <div class="error_msg"><?php echo form_error('username');?></div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Pilih Hak Akses <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" name="group_priviledge" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357">
                                    <?php
                                        foreach ($priviledges as $p) { 
                                            if($p->id != 1) { ?>
                                            <option value="<?=$p->id?>" <?=($user->group_priv_id == $p->id)?"selected":""?>><?=$p->group_name?></option>;
                                        <?php } }
                                    ?>
                                </select>
                                <div class="error_msg"><?php echo form_error('group_priviledge');?></div>
                            </div>
                        </div>
                        
                        <!-- <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Ubah Password?</label>
                            </div>
                            <div class="col-md-9">
                                <input type="checkbox" id="changePass" name="changePass" onchange="showPass()">
                            </div>
                        </div>
                        <div id="password_change">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="form-control-label" for="l0">Password Lama</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="hidden" name="currentPass" value="<?=$user->userpass?>">
                                    <input type="password" name="oldPass" value="" class="form-control" placeholder="Password Lama"  />
                                    <div class="error_msg"><?php echo form_error('oldPass');?></div>
                                </div>
                            </div> -->
                            
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="form-control-label" for="l0">Password Baru</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="password" name="newPass" class="form-control" placeholder="Password Baru" value="" />
                                    <div class="error_msg"><?php echo form_error('newPass');?></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="form-control-label" for="l0">Konfirmasi Password</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="password" name="conPass" value="" class="form-control" placeholder="Konfirmasi Password"  />
                                    <div class="error_msg"><?php echo form_error('conPass');?></div>
                                </div>
                            </div>
                        <!-- </div> -->
                        
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Kirim</button>
                                    <a href="<?=base_url()?>user_list"><button type="button" class="btn btn-default">Batal</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<script>

    function showPass()
    {
        if($('#changePass').is(':checked'))
        {
            $('#password_change').show();
        }
        else
        {
            $('#password_change').hide();
        }
    }

    $(document).ready(function(){
        $('#password_change').show();
    });

    $(function(){
        
        $('#summernote').summernote({
            height: 350
        });
        
        $('.datepicker-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD HH:mm:ss'
        });

    })
</script>
<?php $this->load->view('footer');?>