<?php 
    $this->load->view('headerDisplay');
    // print_r($this->session->userdata('editing'));die("asd");
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3><?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>user_list/edit_user" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Username</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="username" value="<?= $user->username ?>" class="form-control" placeholder="Username" disabled />
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Pilih Hak Akses</label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" name="group_priviledge" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357" disabled>
                                    <?php
                                        foreach ($priviledges as $p) { 
                                            if($p->id != 1) { ?>
                                            <option value="<?=$p->id?>" <?=($user->group_priv_id == $p->id)?"selected":""?>><?=$p->group_name?></option>;
                                        <?php } }
                                    ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <a href="<?=base_url()?>user_list"><button type="button" class="btn btn-default">Kembali</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>
<script>
    function close_window()
    {
        close();
    }
</script>
</section>

<?php $this->load->view('footer');?>