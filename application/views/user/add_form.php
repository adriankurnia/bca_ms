<?php $this->load->view('header'); ?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Tambah <?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
				<?php
                    $error     = $this->session->userdata('err_user_list');
                    $error_msg = $this->session->userdata('msg_user_list');
                    if($this->session->userdata('msg_user_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('err_user_list');
                    $this->session->unset_userdata('msg_user_list');
                ?>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>user_list/new_user" method="post" accept-charset="utf-8" enctype="multipart/form-data">
						<div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Username <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="username" 
                                        value="<?php 
                                                if($this->session->userdata('posting')) { 
                                                    echo $this->session->userdata('posting')['username'];
                                                } else { 
                                                    null;
                                                }
                                            ?>" class="form-control" placeholder="Username"  />
                            	<div class="error_msg"><?php echo form_error('username');?></div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Password <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="password" name="userpass" 
                                        value="<?php 
                                                if($this->session->userdata('posting')) { 
                                                    echo $this->session->userdata('posting')['userpass'];
                                                } else { 
                                                    null;
                                                }
                                            ?>" class="form-control" placeholder="Password"  />
                                <div class="error_msg"><?php echo form_error('userpass');?></div>
                            </div>
                        </div>
						
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Pilih Hak Akses <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" name="group_priviledge" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357">
                                    <option value="0">- Nama Hak Akses -</option>
                                    <?php
                                        foreach ($priviledges as $p) { 
                                            if($p->id != 1) { ?>
                                            <option value="<?=$p->id?>" 
                                                <?php
                                                    if($this->session->userdata('posting')) { 
                                                        if($this->session->userdata('posting')['group_priviledge'] == $p->id)
                                                            echo "Selected";
                                                        else{
                                                            null;
                                                        }
                                                    }
                                                ?>
                                                ><?=ucwords($p->group_name)?></option>;
                                        <?php } }
                                    ?>
                                </select>
                                <div class="error_msg"><?php echo form_error('group_priviledge');?></div>
                            </div>
                        </div>
                        
				        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Kirim</button>
                                    <a href="<?=base_url()?>user_list"><button type="button" class="btn btn-default">Batal</button></a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <?php $this->session->unset_userdata('posting'); ?><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<script>
    $(function(){
		
		$('#summernote').summernote({
            height: 350
        });
		
        $('.datepicker-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
			format: 'YYYY-MM-DD HH:mm:ss'
        });

    })
</script>
<?php $this->load->view('footer');?>