<?php $this->load->view('header'); ?>

<section class="page-content">
<div class="page-content-inner">
    <!--  -->
    <section class="panel">
        <div class="panel-heading">
            <h3>Daftar <?=$menu->menu_name?></h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <?php
                    if($crud->create == 1)
                    {
                        ?>
					<form action="<?php echo base_url() . 'api/export_data/export_to_excel/temp_user'?>" method="post" name="form_excel" id="form_excel">
						<input type="hidden" name="table_array" id="table_array" value="">
					</form>
                    <a class="btn btn-rounded btn-success margin-inline" href="<?=base_url()?>temp_user/add_form"> + <?=$menu->menu_name?></a>
                    <a class="btn btn-rounded btn-success margin-inline" href="<?=base_url()?>temp_user/upload_form"> Upload <?=$menu->menu_name?></a>
					<button type="button" class="btn btn-rounded btn-info margin-inline" onclick="export_to_excel()">Export to Excel</button>
                    <?php } ?>
                    <?php
						$error		= $this->session->userdata('err_temp_user_list');
						$error_msg	= $this->session->userdata('msg_temp_user_list');
						if($this->session->userdata('msg_temp_user_list'))
						{
							if($error == 0)
							{
								$class = "alert alert-primary";
							}
							else
							{
								$class = "alert alert-warning";
							}
							echo '
								<div class="'.$class.'" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<strong>'.$error_msg.'</strong>
								</div>';
						}
						$this->session->unset_userdata('msg_temp_user_list');
						$this->session->unset_userdata('err_temp_user_list');
					?>
                    <div class="margin-bottom-50">
                        <table class="table table-hover nowrap" id="table_view" width="100%">
                            <thead>
                            <tr>
                                <th></th>
                                <?php
                                    if($crud->update == 1 || $crud->delete == 1) {
                                        echo "<th></th>";
                                    }
                                ?>
                                <th class="filterhead_text">NIP</th>
                                <th class="filterhead_text">Nama</th>
                                <th class="filterhead_text">Program</th>
                                <th class="filterhead_text">Kadaluarsa Kartu</th>
                                
                            </tr>
                            <tr id="table_header">
                                <th>NO.</th>
                                <?php
                                    if($crud->update == 1 || $crud->delete == 1) {
                                        echo "<th>ACTION</th>";
                                    }
                                ?>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Program</th>
                                <th>Kadaluarsa Kartu</th>
                            </tr>
                            </thead>
                            <tbody id="table_body">
                            
                            </tbody>
                        </table>
                    </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </section>
    <!-- End  -->

</div>

<!-- Page Scripts -->
<!-- <script src="<?=base_url()?>assets/js/auto-logout.js"></script> -->
<script>
    $('.datepicker-only-init').datetimepicker({
		widgetPositioning: {
			horizontal: 'left'
		},
		icons: {
			time: "fa fa-clock-o",
			date: "fa fa-calendar",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
			up: "fa fa-arrow-up",
			down: "fa fa-arrow-down"
		},
		format: 'YYYY-MM-DD'
	});
</script>

<script>
    // DataTable Custom
	$(document).ready(function() {
		var crud = <?=$crud->update?>;
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        if(<?=$crud->update?> == 1 || <?=$crud->delete?> == 1)
        {
    		var table = $('#table_view').DataTable({
    			"processing": true,
                "serverSide": true,
                "ajax": "<?=base_url()."temp_user/getData"?>",
    			"columns": [
                            {
                                "data": null,
                                "orderable": false
                            },
                            {"data": "ACTION"},
                            {"data": "e_nip"},
                            {"data": "e_name"},
                            {"data": "program_name"},
                            {"data": "card_expired"}
                        ],
    			"scrollX": true,
                "scrollY": 350,
    			"order": [[ 1, "asc" ]],
    			"aLengthMenu": [
                    [25, 50, 100, 200, -1],
                    [25, 50, 100, 200, "All"]
                ],
                "iDisplayLength": 50,
    			"language": {
    				"search": "Search All:"
    			},
    			"rowCallback": function (row, data, iDisplayIndex) {
                            var info = this.fnPagingInfo();
                            var page = info.iPage;
                            var length = info.iLength;
                            var index = page * length + (iDisplayIndex + 1);
                            $('td:eq(0)', row).html(index);
                },initComplete: function () {
                    $('.filterhead_text').each( function () {
                        var title = $('#table_view thead th').eq( $(this).index() ).text();
                        $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
                    });
                    
                    //disable repetitive 
                    var table_index = [];
                    $('.filterhead_dropdown').each( function (i) {
                        var column_count = $(this).index();
                        if(table_index.includes(column_count))
                        {
                            //Exist
                            
                        }
                        else
                        {
                            //Push
                            table_index.push(column_count);
                            $(this).html( '<select id="dropdown_'+column_count+'" class="form-control"><option value="" ></option></select>' );
                            table.column(column_count).data().unique().sort().each( function ( d, j ) {
                                $('#dropdown_'+column_count).append( '<option value="'+d+'">'+d+'</option>' )
                            });
                        }
                        
                    });

                    table.draw();
                    

                    $(".filterhead_text input").on( 'keyup change', function () {
                        table
                            .column( $(this).parent().index()+':visible' )
                            .search( this.value )
                            .draw();
                    });
                    
                    $(".filterhead_dropdown select").on( 'change', function () {
                        table
                            .column( $(this).parent().index()+':visible' )
                            .search( this.value )
                            .draw();
                    });
                }
    		});
        }
        else
        {
            var table = $('#table_view').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "<?=base_url()."temp_user/getData"?>",
                "columns": [
                            {
                                "data": null,
                                "orderable": false
                            },
                            {"data": "e_nip"},
                            {"data": "e_name"},
                            {"data": "program_name"},
                            {"data": "card_expired"}
                        ],
                "scrollX": true,
                "scrollY": 350,
                "order": [[ 1, "asc" ]],
                "aLengthMenu": [
                    [25, 50, 100, 200, -1],
                    [25, 50, 100, 200, "All"]
                ],
                "iDisplayLength": 50,
                "language": {
                    "search": "Search All:"
                },
                "rowCallback": function (row, data, iDisplayIndex) {
                            var info = this.fnPagingInfo();
                            var page = info.iPage;
                            var length = info.iLength;
                            var index = page * length + (iDisplayIndex + 1);
                            $('td:eq(0)', row).html(index);
                },initComplete: function () {
                    $('.filterhead_text').each( function () {
                        var title = $('#table_view thead th').eq( $(this).index() ).text();
                        $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
                    });
                    
                    //disable repetitive 
                    var table_index = [];
                    $('.filterhead_dropdown').each( function (i) {
                        var column_count = $(this).index();
                        if(table_index.includes(column_count))
                        {
                            //Exist
                            
                        }
                        else
                        {
                            //Push
                            table_index.push(column_count);
                            $(this).html( '<select id="dropdown_'+column_count+'" class="form-control"><option value="" ></option></select>' );
                            table.column(column_count).data().unique().sort().each( function ( d, j ) {
                                $('#dropdown_'+column_count).append( '<option value="'+d+'">'+d+'</option>' )
                            });
                        }
                        
                    });

                    table.draw();
                    

                    $(".filterhead_text input").on( 'keyup change', function () {
                        table
                            .column( $(this).parent().index()+':visible' )
                            .search( this.value )
                            .draw();
                    });
                    
                    $(".filterhead_dropdown select").on( 'change', function () {
                        table
                            .column( $(this).parent().index()+':visible' )
                            .search( this.value )
                            .draw();
                    });
                }
            });
        }
		
	});
	
	function export_to_excel()
	{
		var myTableHeader = [];
		var myTableArray = "";
		myTableArray += '{"table_data":[';
		
		$("table thead tr#table_header").each(function( index ) {
			if(index == 0)
			{
				var tableData = $(this).find('th');
				
				if (tableData.length > 0) {
					var column_num = 0;
					tableData.each(function() { 
						if(column_num < 6)
						{
							if(column_num == 1)
                            {
                                column_num = 2;
                            }
                            else
                            {
                                myTableHeader.push($(this).text());
                                column_num++;
                            }
						}
						
					});
				}
			}
		});
		
		console.log(myTableHeader);
		
		$("table #table_body tr").each(function() {
			var arrayOfThisRow = "";
			var tableData = $(this).find('td');
			
			arrayOfThisRow += "{";
			if (tableData.length > 0) {
				var column_num = 0;
				tableData.each(function() { 
					if(column_num < 6)
					{
						if(column_num == 1)
                        {
                            column_num = 2;
                        }
                        else
                        {
                            if(column_num == 0)
                            {
                                column_num = 1;
                            }
                            else
                            {
                                arrayOfThisRow += '"' + myTableHeader[column_num-1] + '"' + ':' + '"' + $(this).text() + '"' + ',';
                                column_num++;
                            }
                        }
					}
				});
			}
			
			//Remove the last coma
			arrayOfThisRow = arrayOfThisRow.slice(0, -1);

			arrayOfThisRow += "},";
			
			myTableArray += arrayOfThisRow;
		});
		
		//Remove the last coma
		myTableArray = myTableArray.slice(0, -1);
		
		myTableArray += "]}";
		
		console.log(myTableArray);
		
		$("#table_array").val(myTableArray);
		
		$("form").submit();
	}

	function saveChanges(object)
    {
        var id = object.id;
        var data = object.name;
        //alert(id);
        var box = confirm("Anda yakin mau menghapus "+data+"?");

        if(box == true)
        {
            $.ajax({
                url: "<?= base_url() ?>temp_user/remove_temp_user",
                type: 'POST',
                data: {
                        "id": id,
                        "data": data,
                    },
                error: function(e){
                    //alert("error");
                },
                success: function(response){

                    alert(response);

                    setTimeout(function(){ window.location.reload(); }, 0);
                } 
            });
        }
    }
</script>

<!-- End Page Scripts -->
</section>
<?php $this->load->view('footer');?>