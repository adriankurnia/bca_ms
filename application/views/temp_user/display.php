<?php 
    $this->load->view('headerDisplay');
    // print_r($this->session->userdata('editing'));die("asd");
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3><?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>employee/edit_employee" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">NIP</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="temp_user_nip" value="<?= $temp_user->e_nip ?>" class="form-control" placeholder="NIP" disabled />
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Nama</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="temp_user_name" value="<?= $temp_user->e_name ?>" class="form-control" placeholder="Nama" disabled />
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Program</label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" id="temp_user_program" name="temp_user_program" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357" disabled >
                                    <option value="0">-</option>
                                    <?php
                                        foreach ($programs as $p) { ?>
                                            <option value="<?=$p->p_id?>" <?=($temp_user->e_program == $p->p_id)?"selected":""?>><?=$p->program_name?></option>;
                                        <?php }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Kadaluarsa Kartu</label>
                            </div>
                            <div class="col-md-9">
                                <?php
                                    if($temp_user->card_expired != "" || !empty($temp_user->card_expired) || $temp_user->card_expired != null)
                                    {
                                        $explode1 = explode(" ", $temp_user->card_expired);
                                        $explode2 = explode("-", $explode1[0]);
                                        $date = $explode2[2]."-".$explode2[1]."-".$explode2[0];
                                    }
                                    else
                                    {
                                        $date = "";
                                    }
                                ?>
                                <input type="text" name="temp_user_expired" id="datepicker-init" class="form-control datepicker-only-init" placeholder="Kadaluarsa Kartu" value="<?= $date ?>" disabled />
                            </div>
                        </div>

                        <?php 
                            // if($temp_user->card_expired == "" || empty($temp_user->card_expired) || $temp_user->card_expired = null)
                            // { 
                            ?>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">RFID</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="temp_user_rfid" value="<?= $temp_user->e_rfid ?>" class="form-control" placeholder="Silahkan Tap Kartu" disabled />
                                <?php echo form_error('temp_user_rfid');?>
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <a href="<?=base_url()?>temp_user""><button type="button" class="btn btn-default">Kembali</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<?php $this->load->view('footer');?>