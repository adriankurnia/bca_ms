<?php
    $this->load->view('header');
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Tambah <?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
				<?php
                    $error     = $this->session->userdata('err_temp_user_list');
                    $error_msg = $this->session->userdata('msg_temp_user_list');
                    if($this->session->userdata('msg_temp_user_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('err_temp_user_list');
                    $this->session->unset_userdata('msg_temp_user_list');
                ?>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>temp_user/new_temp_user" method="post" accept-charset="utf-8" enctype="multipart/form-data">
					
						<div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">NIP <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="temp_user_nip" id="temp_user_nip" 
                                    value="<?php 
                                                if($this->session->userdata('posting')) { 
                                                    echo $this->session->userdata('posting')['temp_user_nip'];
                                                } else { 
                                                    null;
                                                }
                                            ?>" 
                                    class="form-control" placeholder="NIP"  />
                            	<div class="error_msg"><?php echo form_error('temp_user_nip');?></div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Nama <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="temp_user_name" id="temp_user_name" 
                                    value="<?php 
                                                if($this->session->userdata('posting')) { 
                                                    echo $this->session->userdata('posting')['temp_user_name'];
                                                } else { 
                                                    null;
                                                }
                                            ?>" class="form-control" placeholder="Nama"  />
                                <div class="error_msg"><?php echo form_error('temp_user_name');?></div>
                            </div>
                        </div>
						
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Program <label class="mandatory">*</label></label>
                                <!-- <a href="#" onclick="popup_button()">(+ Add New)</a> -->
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" id="temp_user_program" name="temp_user_program" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357">
                                    <option value="0">- Nama Program -</option>
                                    <?php
                                        foreach ($programs as $p) { ?>
                                            <option value="<?=$p->p_id?>"
                                                <?php
                                                    if($this->session->userdata('posting')) { 
                                                        if($this->session->userdata('posting')['temp_user_program'] == $p->p_id)
                                                            echo "Selected";
                                                        else{
                                                            null;
                                                        }
                                                    }
                                                ?>
                                                ><?=$p->program_name?></option>;
                                        <?php }
                                    ?>
                                </select>
                                <div class="error_msg"><?php echo form_error('temp_user_program');?></div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Kadaluarsa Kartu <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="temp_user_expired" id="temp_user_expired" 
                                    value="<?php 
                                                if($this->session->userdata('posting')) { 
                                                    echo $this->session->userdata('posting')['temp_user_expired'];
                                                } else { 
                                                    null;
                                                }
                                            ?>" class="form-control datepicker-only-init" placeholder="Kadaluarsa Kartu" />
                                <div class="error_msg"><?php echo form_error('temp_user_expired');?></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">RFID <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="temp_user_rfid" id="temp_user_rfid" 
                                    value="<?php 
                                                if($this->session->userdata('posting')) { 
                                                    echo $this->session->userdata('posting')['temp_user_rfid'];
                                                } else { 
                                                    null;
                                                }
                                            ?>" class="form-control" placeholder="Silahkan Tap Kartu"  />
                                <div class="error_msg"><?php echo form_error('temp_user_rfid');?></div>
                            </div>
                        </div>
                        
				        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Kirim</button>
                                    <a href="<?=base_url()?>temp_user"><button type="button" class="btn btn-default">Batal</button></a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <?php $this->session->unset_userdata('posting'); ?><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<script>
    $(function(){
		
		$('#summernote').summernote({
            height: 350
        });
		
        $('.datepicker-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
			format: 'YYYY-MM-DD HH:mm:ss'
        });

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY'
        });

    })
</script>

<script>
	var win = null;
	function popup_button()
	{ 
		win = window.open("<?php echo base_url() . 'program/popup_add_form'; ?>", 'targetWindow', 'toolbar=no, width=800, height=600');
		win.focus();
		
		var timer = setInterval(function() {   
    		if(win.closed) {  
    			clearInterval(timer);  
    			location.reload();
    		}  
	    }, 1000); 
	}
</script>


<?php $this->load->view('footer');?>