<?php $this->load->view('header'); ?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Upload <?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
               <div class="col-md-6">
                    <label class="form-control-label" for="l0">
                        File yang bisa diupload adalah <b>.xls, .xlxs</b> <br />
                        Format Cells untuk semua kolom harus <b>Text</b><br />
                        Format isi sebagai berikut:
                            <ul>
                                <li>Kolom 1 => NIP</li>
                                <li>Kolom 2 => Nama Perserta</li>
                                <li>Kolom 3 => RFID</li>
                                <li>Kolom 4 => Kadaluarsa Kartu (dd-mm-yyyy)</li>
                                <li>Kolom 5 => Program</li>
                            </ul>
                    </label>
                </div>
                <div class="col-md-6">
                    <label>Contoh:</label>
                    <img src="<?=base_url()."assets/example.png"?>"></div>
                <div class="form-group row"></div>
				<?php
                    $error     = $this->session->userdata('err_temp_user_list');
                    $error_msg = $this->session->userdata('msg_temp_user_list');
                    if($this->session->userdata('msg_temp_user_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('err_temp_user_list');
                    $this->session->unset_userdata('msg_temp_user_list');
                ?>
                <!-- <div class="progress">
                    <div class="bar"></div >
                    <div class="percent">0%</div >
                </div> -->

                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <div class="col-lg-12">
                    <form id="uploadPeserta" action="<?=base_url()?>temp_user/upload_file" method="post" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Upload Peserta</label>
                            </div>
                            <div class="col-md-8">
                               <input type="file" class="form-control" name="updloadData" />
                            </div>
                            <div class="col-md-2">
                                <button type="submit" name="submit" class="btn width-150 btn-primary">Upload</button>
                            </div>
                        </div>
                    </form>
                </div><!-- End Horizontal Form-->
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>
<!-- <style>
    .progress { position:relative; width:1025px; height:29px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
    .bar { background-color: #B4F5B4; width:0%; height:29px; border-radius: 3px; }
    .percent { position:absolute; display:inline-block; top:3px; left:48%; }
</style> -->
<script src="http://malsup.github.com/jquery.form.js"></script>
<script>
// (function() {
    
// var bar     = $('.bar');
// var percent = $('.percent');
// var status  = $('#status');
   
// $('#uploadPeserta').ajaxForm({
//     beforeSend: function() {
//         status.empty();
//         var percentVal = '0%';
//         bar.width(percentVal)
//         percent.html(percentVal);
//     },
//     uploadProgress: function(event, position, total, percentComplete) {
//         var percentVal = percentComplete + '%';
//         bar.width(percentVal)
//         percent.html(percentVal);
//         //console.log(percentVal, position, total);
//     },
//     success: function() {
//         var percentVal = '100%';
//         bar.width(percentVal)
//         percent.html(percentVal);
//     },
//     complete: function(xhr) {
//         status.html(xhr.responseText);
//     }
// }); 

// })();       
</script>
<script>
    $(function(){
		
		$('#summernote').summernote({
            height: 350
        });
		
        $('.datepicker-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
			format: 'YYYY-MM-DD HH:mm:ss'
        });

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY'
        });

    })
</script>
<?php $this->load->view('footer');?>