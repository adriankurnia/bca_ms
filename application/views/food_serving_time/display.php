<?php 
    $this->load->view('headerDisplay');
    // print_r($this->session->userdata('editing'));die("asd");
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3><?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <!-- <form action="<?=base_url()?>employee/edit_employee" method="post" accept-charset="utf-8" enctype="multipart/form-data"> -->
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Keterangan</label>
                            </div>
                            <div class="col-md-9">
                                <input type="hidden" name="time_id" value="<?=$time->id?>">
                                <input type="text" name="time_notes" class="form-control" value="<?=$time->notes?>" placeholder="Keterangan" disabled />
                                <?php echo form_error('time_notes');?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tanggal <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="time_date" class="form-control datepicker-only-init" value="<?=$time->date?>" placeholder="Tanggal" disabled />
                                <?php echo form_error('time_date');?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Jam <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="time_time" class="form-control timepicker-init" value="<?=$time->time_start?>" placeholder="Jam" disabled />
                                <?php echo form_error('time_time');?>
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <a href="<?=base_url()?>food_serving_time"><button type="button" class="btn btn-default">Kembali</button></a>
                                </div>
                            </div>
                        </div>
                    <!-- </form>End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<?php $this->load->view('footer');?>