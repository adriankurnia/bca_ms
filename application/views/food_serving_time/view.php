<?php $this->load->view('header'); ?>

<section class="page-content">
<div class="page-content-inner">
    <!--  -->
    <section class="panel">
        <div class="panel-heading">
            <h3>Daftar <?=$menu->menu_name?></h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <h5><?=$menu->menu_name?> Default <?= $default->default_time ?> &nbsp;<a class="icmn-pencil" href="food_serving_time/default_form"></a></h5>
                    <?php
                    if($crud->create == 1)
                    {
                        ?>
					<form action="<?php echo base_url() . 'api/export_data/export_to_excel/food_serving_time'?>" method="post" name="form_excel" id="form_excel">
						<input type="hidden" name="table_array" id="table_array" value="">
					</form>
                    <a class="btn btn-rounded btn-success margin-inline" href="<?=base_url()?>food_serving_time/add_form"> + <?=$menu->menu_name?></a>
					<button type="button" class="btn btn-rounded btn-info margin-inline" onclick="export_to_excel()">Export to Excel</button>
                    <?php }
						$error		= $this->session->userdata('err_stime_list');
						$error_msg	= $this->session->userdata('msg_stime_list');
						if($this->session->userdata('msg_stime_list'))
						{
							if($error == 0)
							{
								$class = "alert alert-primary";
							}
							else
							{
								$class = "alert alert-warning";
							}
							echo '
								<div class="'.$class.'" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<strong>'.$error_msg.'</strong>
								</div>';
						}
						$this->session->unset_userdata('msg_stime_list');
						$this->session->unset_userdata('err_stime_list');
					?>
                    <div class="margin-bottom-50">
                        <table class="table table-hover nowrap" id="table_view" width="100%">
                            <thead>
							<tr>
                                <th style="width:5%"></th>
                                <?php
                                    if($crud->update == 1 || $crud->delete == 1) {
                                        echo "<th style='width:10%'></th>";
                                    }
                                ?>
                                <th class="filterhead_text">Keterangan</th>
                                <th class="filterhead_dropdown"></th>
                                <th class="filterhead_dropdown">Waktu Tapping</th>

                                
                            </tr>
                            <tr id="table_header">
                                <th style="width:5%">NO.</th>
                                <?php
                                    if($crud->update == 1 || $crud->delete == 1) {
                                        echo "<th style='width:10%'>ACTION</th>";
                                    }
                                ?>
                                <th>Keterangan</th>
                                <th>Tanggal</th>
                                <th>Waktu Tapping</th>
                            </tr>
                            </thead>
                            <tbody id="table_body">
                            
                            <?php
								if($times)
								{
                                    $i = 1;
									foreach($times as $time)
									{
                                        $explode1 = explode(" ", $time->date);
                                        $explode2 = explode("-", $explode1[0]);
                                        $c_date = $explode2[2]."-".$explode2[1]."-".$explode2[0];
                                        
										?>
										<tr>
                                            <td><?=$i?></td>
                                            <?php
                                                if($crud->update == 1 && $crud->delete == 1) {
                                                    if($time->date >= date("Y-m-d", time()))
                                                    {
                                                        echo '<td><a class="icmn-pencil" href="food_serving_time/edit_form/'.$time->id.'">&nbsp;&nbsp;<a class="icmn-bin" id="'.$time->id.'" name="'.$c_date.'" onclick="saveChanges(this)"></td>';
                                                    }
                                                    else
                                                    {
                                                        echo '<td></td>';
                                                    }
                                                }
                                                else if($crud->update == 1 && $crud->delete == 0) {
                                                    if($time->date >= date("Y-m-d", time()))
                                                    {
                                                        echo '<td><a class="icmn-pencil" href="food_serving_time/edit_form/'.$time->id.'"></td>';
                                                    }
                                                    else
                                                    {
                                                        echo '<td></td>';
                                                    }
                                                }
                                                else if($crud->delete == 1 && $crud->update == 0) {
                                                    if($time->date >= date("Y-m-d", time()))
                                                    {
                                                        echo '<td><a class="icmn-bin" id="'.$time->id.'" name="'.$c_date.'" onclick="saveChanges(this)"></td>';
                                                    }
                                                    else
                                                    {
                                                        echo '<td></td>';
                                                    }
                                                }
                                                else
                                                {
                                                    echo '';
                                                }
                                            ?>
                                            <td><a href="food_serving_time/display/<?=$time->id?>"><?=$time->notes?></a></td>
                                            <td><?=$c_date?></td>
                                            <td><?=$time->time_start?></td>
                                            
										</tr>	
									<?php
                                        $i++;
									}
								}
							?>
                            </tbody>
                        </table>
                    </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </section>
    <!-- End  -->

</div>

<!-- Page Scripts -->
<!-- <script src="<?=base_url()?>assets/js/auto-logout.js"></script> -->
<script>
    $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD'
        });
</script>

<script>
    // DataTable Custom
	$(document).ready(function() {
		
		var table = $('#table_view').DataTable({
			"scrollX": true,
			"scrollY": 350,
			"order": [[ 0, "asc" ]],
			"aLengthMenu": [
                [25, 50, 100, 200, -1],
                [25, 50, 100, 200, "All"]
            ],
			"language": {
				"search": "Search All:"
			}
		});
		
		$('.filterhead_text').each( function () {
			var title = $('#table_view thead th').eq( $(this).index() ).text();
			$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
		});
		
		//disable repetitive 
		var table_index = [];
		$('.filterhead_dropdown').each( function (i) {
			var column_count = $(this).index();
			if(table_index.includes(column_count))
			{
				//Exist
				
			}
			else
			{
				//Push
				table_index.push(column_count);
				$(this).html( '<select id="dropdown_'+column_count+'" class="form-control"><option value="" ></option></select>' );
				table.column(column_count).data().unique().sort().each( function ( d, j ) {
					$('#dropdown_'+column_count).append( '<option value="'+d+'">'+d+'</option>' )
				});
			}
			
		});
		
		// Hide Table
        //var column = table.column(0).visible(false);
		
		//Initialize table
		table.draw();
		
		// Apply the filter

		$(".filterhead_text input").on( 'keyup change', function () {
			table
				.column( $(this).parent().index()+':visible' )
				.search( this.value )
				.draw();
		});
		
		$(".filterhead_dropdown select").on( 'change', function () {
			table
				.column( $(this).parent().index()+':visible' )
				.search( this.value )
				.draw();
		});
	});
	
	function export_to_excel()
	{
		var myTableHeader = [];
		var myTableArray = "";
		myTableArray += '{"table_data":[';
		
		$("table thead tr#table_header").each(function( index ) {
			if(index == 0)
			{
				var tableData = $(this).find('th');
				
				if (tableData.length > 0) {
					var column_num = 0;
					tableData.each(function() { 
						if(column_num < 5)
						{
							if(column_num == 1)
                            {
                                column_num = 2;
                            }
                            else
                            {
                                myTableHeader.push($(this).text());
                                column_num++;
                            }
						}
						
					});
				}
			}
		});
		
		console.log(myTableHeader);
		
		$("table #table_body tr").each(function() {
			var arrayOfThisRow = "";
			var tableData = $(this).find('td');
			
			arrayOfThisRow += "{";
			if (tableData.length > 0) {
				var column_num = 0;
				tableData.each(function() { 
					if(column_num < 5)
					{
						if(column_num == 1)
                        {
                            column_num = 2;
                        }
                        else
                        {
                            if(column_num == 0)
                            {
                                column_num = 1;
                            }
                            else
                            {
                                arrayOfThisRow += '"' + myTableHeader[column_num-1] + '"' + ':' + '"' + $(this).text().toString() + '"' + ',';
                                column_num++;
                            }
                        }
					}
				});
			}
			
			//Remove the last coma
			arrayOfThisRow = arrayOfThisRow.slice(0, -1);

			arrayOfThisRow += "},";
			
			myTableArray += arrayOfThisRow;
		});
		
		//Remove the last coma
		myTableArray = myTableArray.slice(0, -1);
		
		myTableArray += "]}";
		
		console.log(myTableArray);
		
		$("#table_array").val(myTableArray);
		
		$("form").submit();
	}

	function saveChanges(object)
    {
        var id = object.id;
        var data = object.name;
        // alert(id);
        var box = confirm("Anda yakin mau menghapus Waktu Tapping tanggal "+data+"?");

        if(box == true)
        {
            $.ajax({
                url: "<?= base_url() ?>food_serving_time/remove_data",
                type: 'POST',
                data: {
                        "id": id,
                        "data": data,
                    },
                error: function(e){
                    //alert("error");
                },
                success: function(response){

                    alert(response);

                    setTimeout(function(){ window.location.reload(); }, 0);
                } 
            });
        }
    }
</script>

<!-- End Page Scripts -->
</section>
<?php $this->load->view('footer');?>