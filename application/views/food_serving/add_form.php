<?php 
    $this->load->view('header');
    //print_r($this->session->userdata('editing')['servings']);die("qwer");
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <?php
            $ci = &get_instance();
            $ci->load->model('stall_device_model');
            $name = $ci->stall_device_model->stall_device_data($stall_id)->row();
        ?>
        <h3>Tambah Penyajian di <?=$name->device_name?> (<?=trim($name->l_room)?>)<br />Lantai <?=$name->l_floor?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $error      = $this->session->userdata('err_serving_list');
                    $error_msg  = $this->session->userdata('msg_serving_list');
                    if($this->session->userdata('msg_serving_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('msg_serving_list');
                    $this->session->unset_userdata('err_serving_list');
                ?>
                <form action="<?=base_url()?>food_serving/add_form/<?=$stall_id?>" method="post" name="adminForm" id="adminForm">
                    <div class="margin-bottom-5">
                        Filter: 
                        <input class="form-control datepicker-only-init width-200 display-inline-block margin-inline" placeholder="Tanggal Awal" type="text" name="date_from">
                        <span class="margin-right-10">—</span>
                        <input class="form-control datepicker-only-init width-200 display-inline-block margin-inline" placeholder="Tanggal Akhir" type="text" name="date_to">
                        <button type="submit" name="submit" class="btn btn-rounded btn-success margin-inline">Go</button>
                    </div>
                </form>

                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form id="form_submit" action="<?=base_url()?>food_serving/new_serving_food" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <input type="hidden" name="stall_name" value="<?=$name->device_name?>">
                        <input type="hidden" name="stall_id" value="<?=$stall_id?>">
                        <div class="form-group row">
                            <div class="col-md-3">Nama Makanan</div>
                            <div class="col-md-2">Stok</div>
                            <div class="col-md-3">Tanggal Awal</div>
                            <div class="col-md-3">Tanggal Akhir</div>
                        </div>
                        <div  id="itemRows">
                            <?php
                                $rowNum = 0;
                                foreach($servings as $fs)
                                {
                                    $fdate         = $fs->start_date;
                                    $explode       = explode("-", $fdate);
                                    $starting_date = $explode[2]."-".$explode[1]."-".$explode[0];
                                    
                                    $fdate         = $fs->end_date;
                                    $explode       = explode("-", $fdate);
                                    $ending_date   = $explode[2]."-".$explode[1]."-".$explode[0];

                            ?>
                                <div id="rowNum<?=$rowNum?>">
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <input type="hidden" name="serving_id[]" value="<?=$fs->id?>">
                                            <?php
                                                if($crud->update == 0 || $fs->end_date < date("Y-m-d", time()))
                                                {
                                                    echo '<input type="hidden" id="hiditemid_'.$rowNum.'" value="'.$fs->c_name.'">';   
                                                    echo '<input type="hidden" name="item_id_update[]" value="'.$fs->c_id.'">';
                                                } 
                                            ?>
                                            <select class="form-control" name="item_id_update[]" id="itemid_<?=$rowNum?>" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357" <?=($crud->update == 0 || $fs->end_date < date("Y-m-d", time()))?"disabled":""?>>
                                                <option value="0" selected>- Nama Makanan -</option>
                                                <?php
                                                    foreach ($food_names as $fn) { ?>
                                                        <option value="<?=$fn->c_id?>" <?=($fs->c_id == $fn->c_id)?"selected":""?>><?=ucwords($fn->c_name)?></option>;
                                                    <?php }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <?php
                                                if($crud->update == 0 || $fs->end_date < date("Y-m-d", time()))
                                                {
                                                    echo '<input type="hidden" name="item_stock_update[]" id="itemstock_'.$rowNum.'" value="'.$fs->starting_stock.'">';
                                                } 
                                            ?>
                                            <input id="itemstock_<?=$rowNum?>" class="form-control stock-mask-input" type="text" name="item_stock_update[]" value="<?=$fs->starting_stock?>" placeholder="Stok" <?=($crud->update == 0 || $fs->end_date < date("Y-m-d", time()))?"disabled":""?> />
                                        </div>
                                        <div class="col-md-3">
                                            <?php
                                                if($crud->update == 0 || $fs->end_date < date("Y-m-d", time()))
                                                {
                                                    echo '<input type="hidden" name="start_date_update[]" id="startdate_'.$rowNum.'" value="'.$starting_date.'">';
                                                } 
                                            ?>
                                            <input id="startdate_<?=$rowNum?>" type="text" name="start_date_update[]" class="form-control datepicker-only-init" placeholder="Tanggal Awal" value="<?=$starting_date?>" <?=($crud->update == 0 || $fs->end_date < date("Y-m-d", time()))?"disabled":""?> />
                                        </div>
                                        <div class="col-md-3">
                                            <?php
                                                if($crud->update == 0 || $fs->end_date < date("Y-m-d", time()))
                                                {
                                                    echo '<input type="hidden" name="end_date_update[]" id="enddate_'.$rowNum.'" value="'.$ending_date.'">';
                                                } 
                                            ?>
                                            <input id="enddate_<?=$rowNum?>" type="text" name="end_date_update[]" class="form-control datepicker-only-init" placeholder="Tanggal Akhir" value="<?=$ending_date?>" <?=($crud->update == 0 || $fs->end_date < date("Y-m-d", time()))?"disabled":""?> />
                                        </div>
                                        <?php
                                            if($crud->delete == 1)
                                            {
                                                if($fs->start_date >= date("Y-m-d", time()))
                                                {
                                                ?>
                                                <div class="col-md-1">
                                                    <a id="<?=$fs->id?>" name="<?=$starting_date."/".$ending_date?>" onclick="saveChanges(this)"><button type="button" class="btn btn-rounded btn-danger margin-inline icmn-bin"></button></a>
                                                </div>
                                            <?php
                                                }
                                            }
                                        ?>
                                        <div class="col-md-12 error" style="color: red"></div>
                                    </div>
                                </div>
                            <?php
                                $rowNum++;
                                }
                            ?>
                        </div>

                            <!-- <form id="itemRows" action="<?=base_url()?>food_serving/new_serving_food" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                                
                            </form> -->
                        <?php
                            if($crud->create == 1)
                            {
                                ?>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input onclick="addRow(this.form);" type="button" class="btn btn-rounded btn-info margin-inline" value="Tambah" />
                            </div>
                        </div>
                        <?php 
                            }
                        ?>

                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <!-- <button type="submit" name="submit" class="btn width-150 btn-primary">Kirim</button> -->
                                    <?php
                                        if($crud->update == 1 || $crud->create == 1)
                                        {
                                            ?>
                                    <button id="btn_submit" type="button" name="button_click" onclick="date_validate()" class="btn width-150 btn-primary">Kirim</button>
                                    <?php 
                                        } 
                                    ?>
                                    <a href="<?=base_url()?>food_serving/index/<?=$stall_id?>"><button type="button" class="btn btn-default">Batal</button></a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
        
    
</section>
<!-- End -->    
</div>

</section>

<script>
    var rowNum = <?=$rowNum?>;
    var error = 0;
    var error2 = 0;
    function addRow(frm)
    {
        var row = '<div id="rowNum'+rowNum+'"><div class="form-group row"><div class="col-md-3"><select class="form-control" name="item_id[]" id="itemid_'+rowNum+'" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357"><option value="0" selected>- Nama Makanan -</option><?php foreach ($food_names as $fn) { ?><option value="<?=$fn->c_id?>"><?=ucwords($fn->c_name)?></option>;<?php } ?></select></div><div class="col-md-2"><input id="itemstock_'+rowNum+'" class="stock-mask-input form-control" type="text" name="item_stock[]" value="" placeholder="Stok"  /></div><div class="col-md-3"><input type="text" id="startdate_'+rowNum+'" name="start_date[]" class="form-control datepicker-only-init-tanggal" placeholder="Tanggal Awal" value="" /></div><div class="col-md-3"><input type="text" name="end_date[]" id="enddate_'+rowNum+'" class="form-control datepicker-only-init-tanggal" placeholder="Tanggal Akhir" value="" /></div><div class="col-md-1" style="height: 40px"><button type="button" class="btn btn-rounded btn-danger margin-inline icmn-cross" onclick="removeRow('+rowNum+');"></button></div><div class="col-md-12 error" style="color: red"></div></div></div>';

        jQuery('#itemRows').append(row);
        showCalender();
        showTanggal();
        rowNum++;
    }

    function removeRow(rnum)
    {
        jQuery('#rowNum'+rnum).remove();
        rowNum--;
    }

    function showCalender()
    {
        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY',
            // format: 'YYYY-MM-DD'
        });
    }

    function showTanggal()
    {
        $('.datepicker-only-init-tanggal').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY',
            minDate: '<?= date("Y-m-d"); ?>'
            // format: 'YYYY-MM-DD'
        });
    }

    $(function(){
        
        $('#summernote').summernote({
            height: 350
        });

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY'
            //disabledDates: ['2017-10-01', '2017-10-03'],
            // format: 'YYYY-MM-DD'
        });

        // 
        $('.stock-mask-input').mask('0000000', {placeholder: "Stok"});
    })

    function date_validate()
    {
        $("#btn_submit").attr("disabled", true);
        error = 0;
        error2 = 0;
        $(".error").html("");
        for(var i=0; i<rowNum-1; i++)
        {
            for(var j=0; j<rowNum-1; j++)
            {
                if(i != j+1)
                {
                    var item_id      = $("#itemid_"+i).val();
                    var item_stock   = $("#itemstock_"+i).val();
                    var start_date   = $("#startdate_"+i).val();
                    var end_date     = $("#enddate_"+i).val();
                    
                    var item_id2     = $("#itemid_"+(j+1)).val();
                    var item_stock2  = $("#itemstock_"+(j+1)).val();
                    var start_date2  = $("#startdate_"+(j+1)).val();
                    var end_date2    = $("#enddate_"+(j+1)).val();
                    
                    var split_start  = start_date.split("-");
                    var split_start2 = start_date2.split("-");
                    
                    var split_end    = end_date.split("-");
                    var split_end2   = end_date2.split("-");
                    
                    var date1_start  = new Date(split_start[2], '0'+(split_start[1]-1), split_start[0]); //Year, Month, Date
                    var date2_start  = new Date(split_start2[2], '0'+(split_start2[1]-1), split_start2[0]);
                    
                    var date1_end    = new Date(split_end[2], '0'+(split_end[1]-1), split_end[0]);
                    var date2_end    = new Date(split_end2[2], '0'+(split_end2[1]-1), split_end2[0]);

                    if(date2_start > date2_end)
                    {
                        error++;
                        $("#rowNum"+(j+1)+" .error").html("Tanggal Akhir tidak boleh lebih kecil dari Tanggal Awal");
                    }

                    if((date1_start <= date2_end) && (date1_end >= date2_start))
                    {
                        error++;
                        var food = $("#itemid_"+i+" option:selected").text();
                        if(food == "" || food == null)
                        {
                            food = $("#hiditemid_"+i).val();
                        }
                        var food2 = $("#itemid_"+(j+1)+" option:selected").text();
                        if(food2 == "" || food == null)
                        {
                            food2 = $("#hiditemid_"+j+1).val();
                        }
                        $("#rowNum"+(j+1)+" .error").html("Tanggal penyajian "+food2+" overlap dengan "+food);
                    }
                }
            }
        }

        if(error == 0)
        {
            validate_db();
            setTimeout(function(){
                if(error2 == 0)
                {
                    insertData();
                }
                else
                {
                    $("#btn_submit").attr("disabled", false);
                }
            }, 3000);
        }
        else
        {
            $("#btn_submit").attr("disabled", false);
        }
    }

    function validate_db()
    {
        for(var i=0; i<rowNum; i++)
        {
            var exist   = 0;
            var c_id    = $("#itemid_"+i).val();
            var s_date  = $("#startdate_"+i).val();
            var e_date  = $("#enddate_"+i).val();
            
            var split_s = s_date.split("-");
            var split_e = e_date.split("-");
            
            var date1_s = split_s[2]+"-"+split_s[1]+"-"+split_s[0];
            var date1_e = split_e[2]+"-"+split_e[1]+"-"+split_e[0];
            
            $.ajax({
                url: "<?= base_url() ?>food_serving/check_data_exist",
                type: "POST",
                data: {
                        "row": i,
                        "s_id": <?=$stall_id?>,
                        "c_id": c_id,
                        "start_date": date1_s,
                        "end_date": date1_e,
                    },
                dataType: "JSON",
                error: function(e){
                    //alert(e);
                },
                success: function(response){
                    //alert(response);
                    if(response.num == 1)
                    {
                        error2++;
                        // var food = $("#itemid_"+(response.row+1)+" option:selected").text();
                        // $("#rowNum"+(response.row+1)+" .error").html("Penyajian "+food+" sudah ada di "+response.stall+" pada tanggal yang sama.");
                        var food = $("#itemid_"+(response.row)+" option:selected").text();
                        $("#rowNum"+(response.row)+" .error").html("Penyajian "+food+" sudah ada di "+response.stall+" pada tanggal yang sama.");
                    }
                }
            });
        }
    }

    function insertData()
    {
        $("#form_submit").submit();
    }
</script>

<script>
    // DataTable Custom
    $(document).ready(function() {
        
        var table = $('#table_view').DataTable({
            "scrollX": true,
            "scrollY": 350,
            "order": [[ 2, "asc" ]],
            "aLengthMenu": [
                [25, 50, 100, 200, -1],
                [25, 50, 100, 200, "All"]
            ],
            "language": {
                "search": "Search All:"
            }
        });
        
        $('.filterhead_text').each( function () {
            var title = $('#table_view thead th').eq( $(this).index() ).text();
            $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
        });
        
        //disable repetitive 
        var table_index = [];
        $('.filterhead_dropdown').each( function (i) {
            var column_count = $(this).index();
            if(table_index.includes(column_count))
            {
                //Exist
                
            }
            else
            {
                //Push
                table_index.push(column_count);
                $(this).html( '<select id="dropdown_'+column_count+'" class="form-control"><option value="" ></option></select>' );
                table.column(column_count).data().unique().sort().each( function ( d, j ) {
                    $('#dropdown_'+column_count).append( '<option value="'+d+'">'+d+'</option>' )
                });
            }
            
        });
        
        // Hide Table
        //var column = table.column(0).visible(false);
        
        //Initialize table
        table.draw();
        
        // Apply the filter

        $(".filterhead_text input").on( 'keyup change', function () {
            table
                .column( $(this).parent().index()+':visible' )
                .search( this.value )
                .draw();
        });
        
        $(".filterhead_dropdown select").on( 'change', function () {
            table
                .column( $(this).parent().index()+':visible' )
                .search( this.value )
                .draw();
        });
    });
    
    function export_to_excel()
    {
        var myTableHeader = [];
        var myTableArray = "";
        myTableArray += '{"table_data":[';
        
        $("table thead tr#table_header").each(function( index ) {
            if(index == 0)
            {
                var tableData = $(this).find('th');
                
                if (tableData.length > 0) {
                    var column_num = 0;
                    tableData.each(function() { 
                        if(column_num < 4)
                        {
                            myTableHeader.push($(this).text());
                            column_num++;
                        }
                    });
                }
            }
        });
        
        console.log(myTableHeader);
        
        $("table #table_body tr").each(function() {
            var arrayOfThisRow = "";
            var tableData = $(this).find('td');
            
            arrayOfThisRow += "{";
            if (tableData.length > 0) {
                var column_num = 0;
                tableData.each(function() { 
                    if(column_num < 4)
                    {
                        arrayOfThisRow += '"' + myTableHeader[column_num] + '"' + ':' + '"' + $(this).text() + '"' + ',';
                        column_num++;  
                    }
                });
            }
            
            //Remove the last coma
            arrayOfThisRow = arrayOfThisRow.slice(0, -1);

            arrayOfThisRow += "},";
            
            myTableArray += arrayOfThisRow;
        });
        
        //Remove the last coma
        myTableArray = myTableArray.slice(0, -1);
        
        myTableArray += "]}";
        
        console.log(myTableArray);
        
        $("#table_array").val(myTableArray);
        
        $("form#myForm").submit();
    }

    function saveChanges(object)
    {
        var id = object.id;
        var data = object.name;
        var res = data.split("/");
        //alert(id);
        var box = confirm("Anda yakin mau menghapus penyajian tanggal "+res[0]+" s.d "+res[1]+"?");

        if(box == true)
        {
            $.ajax({
                url: "<?= base_url() ?>food_serving/remove_data",
                type: 'POST',
                data: {
                        "id": id,
                        "data": data,
                    },
                error: function(e){
                    //alert("error");
                },
                success: function(response){

                    alert(response);

                    setTimeout(function(){ window.location.reload(); }, 0);
                } 
            });
        }
    }
</script>
<?php $this->load->view('footer');?>