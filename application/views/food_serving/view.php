<?php $this->load->view('header'); ?>

<section class="page-content">
<div class="page-content-inner">
    <!--  -->
    <section class="panel">
        <div class="panel-heading">
            <?php
            $ci = &get_instance();
            $ci->load->model('stall_device_model');
            $name = $ci->stall_device_model->stall_device_data($stall_id)->row();
        ?>
        <h3>Penyajian di <?=$name->device_name?> (<?=trim($name->l_room)?>)<br />Lantai <?=$name->l_floor?></h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <form id="myForm" action="<?php echo base_url() . 'api/export_data/export_to_excel/food_serving'?>" method="post" name="form_excel" id="form_excel">
                        <input type="hidden" name="table_array" id="table_array" value="">
                    </form>
                    <a class="btn btn-rounded btn-success margin-inline" href="<?=base_url()?>food_serving/add_form/<?=$stall_id?>"> + <?=$menu->menu_name?></a>
                    <button type="button" class="btn btn-rounded btn-info margin-inline" onclick="export_to_excel()">Export to Excel</button>
                    <?php
                        $error      = $this->session->userdata('err_serving_list');
                        $error_msg  = $this->session->userdata('msg_serving_list');
                        if($this->session->userdata('msg_serving_list'))
                        {
                            if($error == 0)
                            {
                                $class = "alert alert-primary";
                            }
                            else
                            {
                                $class = "alert alert-warning";
                            }
                            echo '
                                <div class="'.$class.'" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>'.$error_msg.'</strong>
                                </div>';
                        }
                        $this->session->unset_userdata('msg_serving_list');
                        $this->session->unset_userdata('err_serving_list');
                    ?>
                    <div class="margin-bottom-50">
                        <table class="table table-hover nowrap" id="table_view" width="100%">
                            <thead>
                                <tr>
                                    <!-- <th></th> -->
                                    <?php
                                        // if($crud->update == 1 || $crud->delete == 1) {
                                        //     echo "<th style='width:15%'></th>";
                                        // }
                                    ?>
                                    <th class="filterhead_text">Nama Makanan</th>
                                    <th class="filterhead_text">Stok</th>
                                    <th class="filterhead_dropdown">Tanggal Awal</th>
                                    <th class="filterhead_dropdown">Tanggal Akhir</th>
                                </tr>
                                <tr id="table_header">
                                    <!-- <th>NO.</th> -->
                                    <?php
                                        // if($crud->update == 1 || $crud->delete == 1) {
                                        //     echo "<th style='width:15%'>ACTION</th>";
                                        // }
                                    ?>
                                    <th>Nama Makanan</th>
                                    <th>Stok</th>
                                    <th>Tanggal Awal</th>
                                    <th>Tanggal Akhir</th>
                                </tr>
                                </thead>
                            </thead>
                            <tbody id="table_body">
                                <?php 
                                    // $i = 1;
                                    foreach($servings as $fs): ?>
                                    <tr>
                                        <!-- <td><?=$i?></td> -->
                                        <?php
                                            // if($crud->update == 1) {
                                            //     if($fs->start_date > date("Y-m-d", time())) {
                                            //     echo '<td><a class="icmn-pencil" href="canteen_location/edit_form/'.$fs->id.'">&nbsp;&nbsp;';
                                            //     }
                                            // }

                                            // if($crud->delete == 1) {
                                            //     if($fs->start_date > date("Y-m-d", time())) {
                                            //     echo '<a class="icmn-bin" id="'.$fs->id.'" onclick="saveChanges(this)"></td>';
                                            //     }
                                            // }
                                        ?>

                                        <?php
                                            // if($crud->update == 1 && $crud->delete == 1) {
                                            //     if($fs->start_date >= date("Y-m-d", time()))
                                            //     {
                                            //         echo '<td><a class="icmn-pencil" href="canteen_location/edit_form/'.$fs->id.'">&nbsp;&nbsp;<a class="icmn-bin" id="'.$holiday->id.'" name="'.$c_date.'/'.$e_date.'" onclick="saveChanges(this)"></td>';
                                            //     }
                                            //     else
                                            //     {
                                            //         echo '<td></td>';
                                            //     }
                                            // }
                                            // else if($crud->update == 1 && $crud->delete == 0) {
                                            //     if($fs->start_date >= date("Y-m-d", time()))
                                            //     {
                                            //         echo '<td><a class="icmn-pencil" href="canteen_location/edit_form/'.$fs->id.'"></td>';
                                            //     }
                                            //     else
                                            //     {
                                            //         echo '<td></td>';
                                            //     }
                                            // }
                                            // else if($crud->delete == 1 && $crud->update == 0) {
                                            //     if($fs->start_date >= date("Y-m-d", time()))
                                            //     {
                                            //         echo '<td><a class="icmn-bin" id="'.$fs->id.'"></td>';
                                            //     }
                                            //     else
                                            //     {
                                            //         echo '<td></td>';
                                            //     }
                                            // }
                                            // else
                                            // {
                                            //     echo '';
                                            // }
                                        ?>
                                        <td><?php echo ucwords($this->db->where('c_id', $fs->c_id)->get('bca_canteen')->row()->c_name); ?></td>
                                        <td><?php echo $fs->starting_stock; ?></td>
                                        <td><?php echo date('d-m-Y', strtotime( $fs->start_date )); ?></td>
                                        <td><?php echo date('d-m-Y', strtotime( $fs->end_date )); ?></td>
                                    </tr>
                                <?php
                                    // $i++;
                                    endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </section>
    <!-- End  -->

</div>

<!-- Page Scripts -->
<script>
    
    $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD'
        });

    // DataTable Custom
    $(document).ready(function() {
        
        var table = $('#table_view').DataTable({
            "scrollX": true,
            "scrollY": 350,
            "order": [[ 2, "asc" ]],
            "aLengthMenu": [
                [25, 50, 100, 200, -1],
                [25, 50, 100, 200, "All"]
            ],
            "iDisplayLength": -1,
            "language": {
                "search": "Search All:"
            }
        });
        
        $('.filterhead_text').each( function () {
            var title = $('#table_view thead th').eq( $(this).index() ).text();
            $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
        });
        
        //disable repetitive 
        var table_index = [];
        $('.filterhead_dropdown').each( function (i) {
            var column_count = $(this).index();
            if(table_index.includes(column_count))
            {
                //Exist
                
            }
            else
            {
                //Push
                table_index.push(column_count);
                $(this).html( '<select id="dropdown_'+column_count+'" class="form-control"><option value="" ></option></select>' );
                table.column(column_count).data().unique().sort().each( function ( d, j ) {
                    $('#dropdown_'+column_count).append( '<option value="'+d+'">'+d+'</option>' )
                });
            }
            
        });
        
        // Hide Table
        //var column = table.column(0).visible(false);
        
        //Initialize table
        table.draw();
        
        // Apply the filter

        $(".filterhead_text input").on( 'keyup change', function () {
            table
                .column( $(this).parent().index()+':visible' )
                .search( this.value )
                .draw();
        });
        
        $(".filterhead_dropdown select").on( 'change', function () {
            table
                .column( $(this).parent().index()+':visible' )
                .search( this.value )
                .draw();
        });
    });
    
    function export_to_excel()
    {
        var myTableHeader = [];
        var myTableArray = "";
        myTableArray += '{"table_data":[';
        
        $("table thead tr#table_header").each(function( index ) {
            if(index == 0)
            {
                var tableData = $(this).find('th');
                
                if (tableData.length > 0) {
                    var column_num = 0;
                    tableData.each(function() { 
                        if(column_num < 4)
                        {
                            myTableHeader.push($(this).text());
                            column_num++;
                        }
                        
                    });
                }
            }
        });
        
        console.log(myTableHeader);
        
        $("table #table_body tr").each(function() {
            var arrayOfThisRow = "";
            var tableData = $(this).find('td');
            
            arrayOfThisRow += "{";
            if (tableData.length > 0) {
                var column_num = 0;
                tableData.each(function() { 
                    if(column_num < 4)
                    {
                        arrayOfThisRow += '"' + myTableHeader[column_num] + '"' + ':' + '"' + $(this).text() + '"' + ','; 
                        column_num++;
                    }
                });
            }
            
            //Remove the last coma
            arrayOfThisRow = arrayOfThisRow.slice(0, -1);

            arrayOfThisRow += "},";
            
            myTableArray += arrayOfThisRow;
        });
        
        //Remove the last coma
        myTableArray = myTableArray.slice(0, -1);
        
        myTableArray += "]}";
        
        console.log(myTableArray);
        
        $("#table_array").val(myTableArray);
        
        $("form").submit();
    }

</script>
<!-- End Page Scripts -->
</section>
<?php $this->load->view('footer');?>