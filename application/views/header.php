<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>BCA Kantin Management Sistem</title>

    <link href="<?= base_url() ?>assets/backend/assets/common/img/favicon.png" rel="icon" type="image/png">

    <!-- HTML5 shim and Respond.js for < IE9 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Vendors Styles -->
    <!-- v1.0.0 -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/jscrollpane/style/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/ladda/dist/ladda-themeless.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/fullcalendar/dist/fullcalendar.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/cleanhtmlaudioplayer/src/player.css"> -->
    <!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/cleanhtmlvideoplayer/src/player.css"> -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/summernote/dist/summernote.css"> -->
    <!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/owl.carousel/dist/assets/owl.carousel.min.css"> -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/ionrangeslider/css/ion.rangeSlider.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/datatables/media/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/c3/c3.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/chartist/dist/chartist.min.css">
    <!-- v1.4.0 -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/nprogress/nprogress.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/jquery-steps/demo/css/jquery.steps.css">
    <!-- v1.4.2 -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css">
    <!-- v1.7.0 -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/dropify/dist/css/dropify.min.css">

    <!-- Clean UI Styles -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/common/css/source/main.css">

    <!-- Custom -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/common/css/custom.css">

    <!-- Fixed Header -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/datatables-fixedheader/css/fixedHeader.bootstrap4.min.css">

    <!-- jQuery-ui -->
    <!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/jquery-ui/jquery-ui.min.css"> -->

    <!-- Vendors Scripts -->
    <!-- v1.0.0 -->
    <script src="<?= base_url() ?>assets/backend/assets/vendors/jquery/jquery.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/tether/dist/js/tether.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/jscrollpane/script/jquery.jscrollpane.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/spin.js/spin.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/ladda/dist/ladda.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/html5-form-validation/dist/jquery.validation.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/jquery-typeahead/dist/jquery.typeahead.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/autosize/dist/autosize.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/bootstrap-show-password/bootstrap-show-password.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/moment/min/moment.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
    <!-- <script src="<?= base_url() ?>assets/backend/assets/vendors/cleanhtmlaudioplayer/src/jquery.cleanaudioplayer.js"></script> -->
    <!-- <script src="<?= base_url() ?>assets/backend/assets/vendors/cleanhtmlvideoplayer/src/jquery.cleanvideoplayer.js"></script> -->
    <script src="<?= base_url() ?>assets/backend/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/summernote/dist/summernote.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/ionrangeslider/js/ion.rangeSlider.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/nestable/jquery.nestable.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/datatables/media/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/datatables-responsive/js/dataTables.responsive.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/editable-table/mindmup-editabletable.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/d3/d3.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/c3/c3.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/chartist/dist/chartist.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/peity/jquery.peity.min.js"></script>
    <!-- v1.0.1 -->
    <script src="<?= base_url() ?>assets/backend/assets/vendors/chartist-plugin-tooltip/dist/chartist-plugin-tooltip.min.js"></script>
    <!-- v1.1.1 -->
    <script src="<?= base_url() ?>assets/backend/assets/vendors/gsap/src/minified/TweenMax.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/hackertyper/hackertyper.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/jquery-countTo/jquery.countTo.js"></script>
    <!-- v1.4.0 -->
    <script src="<?= base_url() ?>assets/backend/assets/vendors/nprogress/nprogress.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/jquery-steps/build/jquery.steps.min.js"></script>
    <!-- v1.4.2 -->
    <script src="<?= base_url() ?>assets/backend/assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/chart.js/src/Chart.bundle.min.js"></script>
    <!-- v1.7.0 -->
    <script src="<?= base_url() ?>assets/backend/assets/vendors/dropify/dist/js/dropify.min.js"></script>

    <!-- Clean UI Scripts -->
    <script src="<?= base_url() ?>assets/backend/assets/common/js/common.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/common/js/demo.temp.js"></script>
    
    <!-- Lightbox -->
    <!-- <script src="<?= base_url() ?>assets/backend/assets/common/js/html5lightbox/html5lightbox.js?>"></script> -->

    <!-- Fixed Header -->
    <script src="<?= base_url() ?>assets/backend/assets/vendors/datatables-fixedheader/js/dataTables.fixedHeader.min.js"></script>

    <!-- jQuery-ui -->
    <!-- <script src="<?= base_url() ?>assets/jquery-ui/jquery-ui.min.js"></script> -->

    <!-- Code Fandy - DELETE MODAL -->
    <script>
        //Modal For Delete
        $(document).on('click', '.open-deleteModal', function () {
             var deleteId = $(this).data('id');
             $('.modal-body #modal').val( deleteId );
        });
    </script>
    <script>
        // Add the following into your HEAD section
        var timer = 0;
        var logout_time = <?=LOGOUT_TIME?>;
        function set_interval() {
          // the interval 'timer' is set as soon as the page loads
          timer = setInterval("auto_logout()", logout_time);
          // the figure '10000' above indicates how many milliseconds the timer be set to.
          // Eg: to set it to 5 mins, calculate 5min = 5x60 = 300 sec = 300,000 millisec.
          // So set it to 300000
        }

        function reset_interval() {
          //resets the timer. The timer is reset on each of the below events:
          // 1. mousemove   2. mouseclick   3. key press 4. scroliing
          //first step: clear the existing timer

          if (timer != 0) {
            clearInterval(timer);
            timer = 0;
            // second step: implement the timer again
            timer = setInterval("auto_logout()", logout_time);
            // completed the reset of the timer
          }
        }

        function auto_logout() {
          // this function will redirect the user to the logout script
          window.location.href = '<?=base_url()?>login/logout';
        }
    </script>
</head>

<body class="theme-default" onload="set_interval()" onmousemove="reset_interval()" onclick="reset_interval()" onkeypress="reset_interval()" onscroll="reset_interval()">
<nav class="left-menu" left-menu>
    <div class="logo-container">
        <a href="<?=base_url()?>dashboard" class="logo">
            <img src="<?= base_url() ?>assets/backend/assets/common/img/logo.png" alt="Clean UI Admin Template" />
            <img class="logo-inverse" src="<?= base_url() ?>assets/backend/assets/common/img/logo-inverse.png" alt="Clean UI Admin Template" />
        </a>
    </div>
    <div class="left-menu-inner scroll-pane">
        <ul class="left-menu-list left-menu-list-root list-unstyled">
            <li class="left-menu-list-active">
                <a class="left-menu-link" href="<?=base_url()?>dashboard">
                    <i class="left-menu-link-icon icmn-home2"></i>
                    <span class="menu-top-hidden">Dashboard</span>
                </a>
            </li>
            <?php
                $ci = &get_instance();
                $ci->load->model('login_model');

                //Upload Files
                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 22)->row();
                if($res)
                {
                if($res->read == 1 && $res->create == 1)
                {
            ?>
            <li class="left-menu-list-submenu <?php if($this->uri->segment(1) == 'upload') { echo "left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-upload"><!-- --></i>
                    <?= $res->menu_name ?>
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'upload') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>upload">
                            Upload
                        </a>
                    </li>
                </ul>
                <?php } ?>
            </li>
            <?php
                }

                //Menu CMS
                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 5)->row();
                if($res)
                {
                if($res->read == 1)
                {
            ?>
            <li class="left-menu-list-submenu <?php if($this->uri->segment(1) == 'menu_priviledge') { echo "left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-menu2"><!-- --></i>
                    <?= $res->menu_name ?>
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'menu_priviledge') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>menu_priviledge">
                            Daftar <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php if($res->create == 1) { ?>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'menu_priviledge') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>menu_priviledge/add_form">
                            Tambah <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php } } ?>
            </li>
            <?php
                }

                //Hak Akses
                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 7)->row();
                if($res)
                {
                if($res->read == 1)
                {
            ?>
            <li class="left-menu-list-submenu <?php if($this->uri->segment(1) == 'groupmenu_priviledge') { echo "left-menu-list-opened"; } ?>" >
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-accessibility"><!-- --></i>
                    <?= $res->menu_name ?>
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'groupmenu_priviledge') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>groupmenu_priviledge">
                            Daftar <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php if($res->create == 1) { ?>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'groupmenu_priviledge') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>groupmenu_priviledge/add_form">
                            Tambah <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php } } ?>
            </li>
            <?php
                }

                //Administrator
                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 4)->row();
                if($res)
                {
                if($res->read == 1)
                {
            ?>
            <li class="left-menu-list-submenu <?php if($this->uri->segment(1) == 'user_list') { echo "left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-users"><!-- --></i>
                    <?= $res->menu_name ?>
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'user_list') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>user_list">
                            Daftar <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php if($res->create == 1) { ?>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'user_list') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>user_list/add_form">
                            Tambah <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php } } ?>
            </li>
            <?php
                }

                

                //Karyawan
                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 21)->row();
                if($res)
                {
                if($res->read == 1)
                {  
            ?>
            <li class="left-menu-list-submenu <?php if($this->uri->segment(1) == 'employee') { echo "left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-man-woman"></i>
                    <?= $res->menu_name ?>
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'employee') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>employee">
                            Daftar <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php if($res->create == 1) { ?>
                <!-- <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'employee') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>employee/add_form">
                            Tambah <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul> -->
                <?php } } ?>
            </li>
            <?php
                }

                //Pengguna Sementara
                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 1)->row();
                if($res)
                {
                if($res->read == 1)
                {
            ?>
            <li class="left-menu-list-submenu <?php if($this->uri->segment(1) == 'temp_user') { echo "left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-man-woman"><!-- --></i>
                    <?= $res->menu_name ?>
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'program' || $this->uri->segment(1) == 'temp_user') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>temp_user">
                            Daftar <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php if($res->create == 1) { ?>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'program' || $this->uri->segment(1) == 'temp_user') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>temp_user/add_form">
                            Tambah <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'program' || $this->uri->segment(1) == 'temp_user') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>temp_user/upload_form">
                            Upload <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'program' || $this->uri->segment(1) == 'temp_user') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>temp_user/get_deleted_temp_user">
                            Munculkan <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <!-- <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'program' || $this->uri->segment(1) == 'temp_user') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>temp_user/get_training_schedule">
                            Training Scedule <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul> -->
                <?php }
                    $resProg = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 8)->row();
                    if($res)
                    {
                    if($resProg->read == 1)
                    {
                ?>
                        <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'program' || $this->uri->segment(1) == 'temp_user') { echo "style='display:block;'"; } ?>>
                            <li>
                                <a class="left-menu-link" href="<?=base_url()?>program">
                                    Daftar Program
                                </a>
                            </li>
                        </ul>
                        <?php 
                        if($resProg->create == 1)
                        {
                        ?>
                        <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'program' || $this->uri->segment(1) == 'temp_user') { echo "style='display:block;'"; } ?>>
                            <li>
                                <a class="left-menu-link" href="<?=base_url()?>program/add_form">
                                    Tambah Program
                                </a>
                            </li>
                        </ul>
                <?php   } 
                    } 
                }
            } ?>
            </li>
            <?php
                }

                //Lokasi Stall
                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 2)->row();
                if($res)
                {
                if($res->read == 1)
                {
            ?>
            <li class="left-menu-list-submenu <?php if($this->uri->segment(1) == 'canteen_location') { echo "left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-location2"><!-- --></i>
                    <?= $res->menu_name ?>
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'canteen_location') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>canteen_location">
                            Daftar <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php if($res->create == 1) { ?>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'canteen_location') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>canteen_location/add_form">
                            Tambah <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php } } ?>
            </li>
            <?php
                }

                //Menu Makanan
                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 3)->row();
                if($res)
                {
                if($res->read == 1)
                {
            ?>
            <li class="left-menu-list-submenu <?php if($this->uri->segment(1) == 'canteen') { echo "left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-menu3"><!-- --></i>
                    <?= $res->menu_name ?>
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'canteen') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>canteen">
                            Daftar <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php if($res->create == 1) { ?>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'canteen') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>canteen/add_form">
                            Tambah <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php } } ?>
            </li>
            <?php
                }

                //Perangkat Stall
                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 9)->row();
                if($res)
                {
                if($res->read == 1)
                {
            ?>
            <li class="left-menu-list-submenu <?php if($this->uri->segment(1) == 'stall_device') { echo "left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-tablet"><!-- --></i>
                    <?= $res->menu_name ?>
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'stall_device') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>stall_device">
                            Daftar <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php if($res->create == 1) { ?>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'stall_device') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>stall_device/add_form">
                            Tambah <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php } } ?>
            </li>
            <?php
                }

                //Penyajian Makanan
                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 11)->row();
                if($res)
                {
                if($res->read == 1)
                {
            ?>
            <li class="left-menu-list-submenu <?php if($this->uri->segment(1) == 'food_serving') { echo "left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-spoon-knife"><!-- --></i>
                    <?= $res->menu_name ?>
                </a>
                <!-- <ul class="left-menu-list list-unstyled">
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>food_serving">
                            Daftar <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul> -->
                <?php if($res->read == 1) {
                        $model = &get_instance();
                        $model->load->model('stall_device_model');
                        $stall = $model->stall_device_model->stall_device_list()->result();
                        foreach ($stall as $s) { ?>
                            <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'food_serving') { echo "style='display:block;'"; } ?>>
                                <li class="left-menu-list-submenu left-menu-list-opened">
                                    <!-- <a class="left-menu-link" href="<?=base_url()?>food_serving/add_form/<?=$s->s_id?>">
                                        <?= $s->device_name ?>
                                    </a> -->
                                    <a class="left-menu-link" href="javascript: void(0);"><i class="icmn-tablet"><!-- --></i><?= $s->device_name ?></a>
                                    <ul class="left-menu-list list-unstyled"<?php if($this->uri->segment(3) == $s->s_id) { echo "style='display:block;'"; } ?>>
                                        <li>
                                            <a class="left-menu-link" href="<?=base_url()?>food_serving/index/<?=$s->s_id?>">Daftar Penyajian</a>
                                        </li>
                                        <li>
                                            <a class="left-menu-link" href="<?=base_url()?>food_serving/add_form/<?=$s->s_id?>">Tambah Penyajian</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        <?php
                            }
                        }
                    }
                ?>
            </li>
            <?php
                }

                // Jam Tapping
                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 26)->row();
                if($res)
                {
                if($res->read == 1)
                {
            ?>
            
            <li class="left-menu-list-submenu <?php if($this->uri->segment(1) == 'food_serving_time') { echo "left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-clock"></i>
                    <?= $res->menu_name ?>
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'food_serving_time') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>food_serving_time">
                            Setting <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php if($res->create == 1) { ?>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'food_serving_time') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>food_serving_time/add_form">
                            Tambah <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php } } ?>
            </li>
            <?php 
                }

                // Setting Hari Libur
                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 23)->row();
                if($res)
                {
                if($res->read == 1)
                {
            ?>
            
            <li class="left-menu-list-submenu <?php if($this->uri->segment(1) == 'food_serving_holiday') { echo "left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-calendar2"></i>
                    <?= $res->menu_name ?>
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'food_serving_holiday') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>food_serving_holiday">
                            Daftar <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php if($res->create == 1) { ?>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'food_serving_holiday') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>food_serving_holiday/add_form">
                            Tambah <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php } } ?>
            </li>
            <?php 
                }

                 // Semua Buffet, Tampilan Stok
                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 31)->row();
                if($res)
                {
                if($res->read == 1)
                {
            ?>
            
            <li class="left-menu-list-submenu <?php if($this->uri->segment(1) == 'food_serving_buffet') { echo "left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-display"></i>
                    <?= $res->menu_name ?>
                </a>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'food_serving_buffet') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>food_serving_buffet">
                            Daftar <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php if($res->create == 1) { ?>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'food_serving_buffet') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>food_serving_buffet/add_form">
                            Tambah <?= $res->menu_name ?>
                        </a>
                    </li>
                </ul>
                <?php } ?> 
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'food_serving_buffet') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>food_serving_buffet/get_daily_stock_today">
                            Delete Tampilan Stok
                        </a>
                    </li>
                </ul>
                <?php } ?>
            </li>
            <?php 
                }

                // Laporan
                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 25)->row();
                if($res)
                {
                    $audit_t = $res->read;
                }

                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 26)->row();
                if($res)
                {
                    $serv_t = $res->read;
                }

                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 27)->row();
                if($res)
                {
                    $daily_r = $res->read;
                }

                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 28)->row();
                if($res)
                {
                    $canteen_c = $res->read;
                }

                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 29)->row();
                if($res)
                {
                    $food_r = $res->read;
                }

                $res = $ci->login_model->group_priviledge($this->session->userdata('group_priviledge'), 30)->row();
                if($res)
                {
                    $calory_m = $res->read;
                }


                //if($res->read == 1)
                if($audit_t == 1 || $serv_t == 1 || $daily_r == 1 || $canteen_c == 1 || $food_r == 1 || $calory_m == 1)
                {
            ?>

            <li class="left-menu-list-submenu <?php if($this->uri->segment(1) == 'report') { echo "left-menu-list-opened"; } ?>">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-drawer"></i>
                    Laporan
                </a>
                <?php if($audit_t == 1) { ?>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'report') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>report/cms_log">
                            Laporan Audit Trail
                        </a>
                    </li>
                </ul>
                <?php }
                if($serv_t == 1) { ?>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'report') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>report/canteen_trx">
                            Laporan Transaksi Tapping
                        </a>
                    </li>
                </ul>
                <?php }
                if($daily_r == 1) { ?>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'report') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>report/daily_report">
                            Laporan Harian Kantin
                        </a>
                    </li>
                </ul>
                <?php }
                if($canteen_c == 1) { ?>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'report') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>report/canteen_cost">
                            Laporan Biaya Kantin
                        </a>
                    </li>
                </ul>
                <?php }
                if($food_r == 1) { ?>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'report') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>report/food_ranking">
                            Laporan Peringkat Makanan
                        </a>
                    </li>
                </ul>
                <?php }
                if($calory_m == 1) { ?>
                <ul class="left-menu-list list-unstyled" <?php if($this->uri->segment(1) == 'report') { echo "style='display:block;'"; } ?>>
                    <li>
                        <a class="left-menu-link" href="<?=base_url()?>report/calory_meter">
                            Laporan Pengukuran Kalori
                        </a>
                    </li>
                </ul>
                <?php } ?>
            </li>
            <?php  }  ?>
            <li class="left-menu-list-separator"><!-- --></li>
        </ul>
    </div>
</nav>
<nav class="top-menu">
    <div class="menu-icon-container hidden-md-up">
        <div class="animate-menu-button left-menu-toggle">
            <div><!-- --></div>
        </div>
    </div>
    <div class="menu">
        <div class="menu-user-block">
            <div class="dropdown dropdown-avatar">
                <a href="javascript: void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="avatar" href="javascript:void(0);">
                        <img src="<?=base_url()."assets/backend/assets/common/img/favicon_.png"?>" alt="Alternative text to the image">
                    </span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="" role="menu">
                    <a class="dropdown-item" href="<?=base_url()?>login/logout"><i class="dropdown-icon icmn-exit"></i> Logout</a>
                </ul>
            </div>
        </div>
    </div>
</nav>
