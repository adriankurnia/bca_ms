
<style>
table {
    text-align:center;!important;
}
</style>
<section class="page-content">
<div class="page-content-inner">
    <!--  -->
    <section class="panel">
        <div class="panel-heading">
            <h3>Laporan Pengukuran Kalori<br>

            Periode : <?php echo $start_date. ' s.d '. $end_date;?>
            </h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">

                    <div class="margin-bottom-50">


                        <?php
                        $grandTotalKalori = 0 ;
                        $grandTotalLemak = 0 ;
                        $grandTotalProtein = 0 ;
                        if($getVariableDynamic)foreach($getVariableDynamic as $list){
                            ${"subTotalKalori_".$list->e_id} =0;
                            ${"subTotalLemak_".$list->e_id} =0;
                            ${"subTotalProtein_".$list->e_id}=0;
                        }
?>


                        <br>
                        <div class="table-responsive">
                        <table border="1" width="100%">
                            <thead>
                                <tr align="center" style="background-color:#0060af; color: white">
                                    <td><b>Tanggal</td>
                                    <td><b>NIP</td>
                                    <td><b>Nama</td>
                                    <td><b>Nama Pelatihan</td>
                                    <td><b>Menu</td>
                                    <td><b>Gender</td>
                                    <td><b>Kalori</td>
                                    <td><b>Lemak</td>
                                    <td><b>Protein</td>
                                    <td><b>ESL-PKT</td>
                                    <td><b>Divisi/Wilayah</td>
                                    <td><b>Unit Kerja</td>
                                    <td><b>Tgl Lahir</td>
                                    <td><b>Umur</td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=0; $defaultzId = 0; $temp = 0; $totalData = count($getReportData);?>
                            <?php 
                                if($getReportData)foreach($getReportData as $list)
                                {
                                    if($no == 1){
                                        if($temp != $list->e_id)
                                        {
                                            $defaultzId = $temp;
                                        }
                                        else
                                        {
                                            $defaultzId = $list->e_id;
                                        }
                                    }
                                    else if($no == 0)
                                    {
                                        $defaultzId = $list->e_id;
                                    }
                                    
                                    $train_name = "";
                                    $tgl_lahir  = "";
                                    $divwil     = "";

                                    if($list->tanggal_lahir)
                                    {
                                        $tgl_lahir = indonesian_date($list->tanggal_lahir,'d.m.Y');
                                    }

                                    if($list->e_divisi && $list->e_wilayah)
                                    {
                                        $divwil = $list->e_divisi."/".$list->e_wilayah;
                                    }

                                    $res = $this->report_model->getParticipant($list->e_id, $list->trx_time)->row();
                                    if(!empty($res->co_id))
                                    {
                                        $co_id = $res->co_id;
                                        $res2 = $this->report_model->getProgram($co_id)->row();
                                        if(!empty($res2))
                                        {
                                            $train_name   = $res2->training_name;
                                        }
                                    }
                                ?>

                                    <?php 
                                    if($defaultzId != $list->e_id) { ?>
                                        <tr style="background-color:#0093d6; color: white"">
                                            <td colspan="5" style="text-align: right">Subtotal</td>
                                            <td>&nbsp;</td>
                                            <td><?php echo ${"subTotalKalori_".$defaultzId};?></td>
                                            <td><?php echo ${"subTotalProtein_".$defaultzId};?></td>
                                            <td><?php echo ${"subTotalLemak_".$defaultzId};?></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    <?php 
                                        $no=0; 
                                    }?>

                                    <tr>
                                        <td><?php echo indonesian_date($list->trx_time,'d-m-Y')?></td>
                                        <td>
                                            <?php 
                                              if(strpos($list->e_id, "EX_") !== false)
                                              {
                                                echo substr($list->e_id, 3);
                                              }
                                              else
                                              {
                                                echo $list->e_id;
                                              }
                                        ?>
                                        </td>
                                        <td><?php echo $list->e_name?></td>
                                        <td><?php echo ucwords($train_name)?></td>
                                        <td><?php echo ucwords($list->makanan_name)?></td>
                                        <td><?php echo $list->e_gender?></td>
                                        <td><?php
                                            ${"subTotalKalori_".$list->e_id}+=$list->calory;
                                            $grandTotalKalori+=$list->calory;
                                            echo $list->calory?></td>

                                        <td><?php
                                            ${"subTotalProtein_".$list->e_id}+=$list->protein;
                                            $grandTotalProtein+=$list->protein;
                                            echo $list->protein;?></td>

                                        <td><?php
                                            ${"subTotalLemak_".$list->e_id}+=$list->fat;
                                            $grandTotalLemak+=$list->fat;
                                            echo $list->fat;?></td>

                                        <td><?php echo $list->e_eselon?></td>
                                        <td><?php echo $divwil?></td>
                                        <td><?php echo $list->unitkerja?></td>
                                        <td><?php echo $tgl_lahir?></td>
                                        <td><?php echo $list->age?></td>
                                    </tr>
                                <?php 
                                    $temp = $list->e_id;
                                    $no++;
                                } ?>
                                
                                <tfoot>
                                <?php if($getReportData) { ?>

                                <tr style="background-color:#0093d6; color: white">
                                    <td colspan="5" style="text-align: right" ><b>Subtotal</td>
                                    <td>&nbsp;</td>
                                    <td><?php echo ${"subTotalKalori_".$list->e_id};?></td>
                                    <td><?php echo ${"subTotalProtein_".$list->e_id};?></td>
                                    <td><?php echo ${"subTotalLemak_".$list->e_id};?></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr style="background-color:#0093d6; color: white">
                                    <td colspan="5" style="text-align: right" ><b>GrandTotal</td>
                                    <td>&nbsp;</td>
                                    <td><b><?php echo $grandTotalKalori;?></td>
                                    <td><b><?php echo $grandTotalProtein;?></td>
                                    <td><?php echo $grandTotalLemak;?></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                <?php }else{ ?>
                                    <tr>
                                        <td colspan="14"><b> Tidak ada data yang tersedia pada saat ini.</td>
                                    </tr>

                                <?php }?>
                                </tfoot>
                            </tbody>

                        </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End  -->

</div>

<!-- End Page Scripts -->
</section>
