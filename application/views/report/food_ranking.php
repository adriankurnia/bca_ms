<?php $this->load->view('header'); ?>
<section class="page-content">
<div class="page-content-inner">
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Laporan Peringkat Makanan</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $error     = $this->session->userdata('err_dreport_list');
                    $error_msg = $this->session->userdata('msg_dreport_list');
                    if($this->session->userdata('msg_dreport_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('err_dreport_list');
                    $this->session->unset_userdata('msg_dreport_list');
                ?>
                <?php if(!$reportContent){?>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="" method="GET" accept-charset="utf-8" enctype="multipart/form-data" id="form_report">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tanggal Awal</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="start_date" class="form-control datepicker-only-init" placeholder="Tanggal Mulai" value="<?php echo $start_date?>" />
                                <?php echo form_error('start_date');?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tanggal Akhir</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="end_date" value="<?php echo $end_date;?>" class="form-control datepicker-only-init" placeholder="Tanggal Akhir" />
                                <?php echo form_error('end_date');?>
                            </div>
                        </div>
                        <input type="hidden" name="report_id" value="1">

                        <input type="hidden" value="<?php echo $text_status?>" name="text_status" id="text_status">
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <a href="#" onclick="changeStatus(0);" class="btn btn-rounded btn-primary">Kirim</a>
                                    <!-- <a href="#" onclick="changeStatus(1);" class="btn btn-rounded btn-default">Export to Excel</a> -->
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
                <?php }else{?>
                 <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="" method="GET" accept-charset="utf-8" enctype="multipart/form-data" id="form_report" style="display:none;">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tanggal Awal</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="start_date" class="form-control datepicker-only-init" placeholder="Tanggal Mulai" value="<?php echo $start_date?>" />
                                <?php echo form_error('start_date');?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tanggal Akhir</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="end_date" value="<?php echo $end_date;?>" class="form-control datepicker-only-init" placeholder="Tanggal Akhir" />
                                <?php echo form_error('end_date');?>
                            </div>
                        </div>
                        <input type="hidden" name="report_id" value="1">

                        <input type="hidden" value="<?php echo $text_status?>" name="text_status" id="text_status">

                    </form><!-- End Horizontal Form -->
                </div>
                <?php }?>
                <?php
                    if($reportContent) {
                        $this->load->view($reportContent);
                        echo
                            '<div>
                                <div class="form-group">
                                    <div class="col-md-9">
                                       <a href="#" onclick="changeStatus(1);" class="btn btn-rounded btn-success">Export to Excel</a>
                                       <a href="'.base_url().'report/food_ranking" class="btn btn-rounded btn-default">Kembali</a>
                                    </div>
                                </div>
                            </div>';
                    }
                ?>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<script>

    function showPlace()
    {
        if($('#item_type').val() == 1)
        {
            $('#place').show();
        }
        else
        {
            $('#place').hide();
        }
    }

    $(document).ready(function(){
        $('#place').hide();
    });

    $(function(){
        
        $('#summernote').summernote({
            height: 350
        });
        
        $('.datepicker-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD HH:mm:ss'
        });

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY'
            // format: 'YYYY-MM-DD'
        });
    })

    function changeStatus(dataId){
    $("#text_status").val(dataId);
    $("#form_report").submit();
}
</script>
<?php $this->load->view('footer');?>
