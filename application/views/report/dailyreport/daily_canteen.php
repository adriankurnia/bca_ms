
<style>
table {
    text-align:center;!important;
}
</style>
<section class="page-content">
<div class="page-content-inner">
    <!--  -->
    <section class="panel">
        <div class="panel-heading">
            <h3>Laporan Harian Kantin<br>

            Periode : <?php echo $start_date. ' s.d '. $end_date;?>
            </h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">

                    <div class="margin-bottom-50">
                        <?php if($getPeriode)foreach($getPeriode as $newDate){?>

                        <?php
                            $stallColSpan     = 0;
                            $buffetColSpan    = 0;
                            $minumanColspan   = 0;
                            $dessertColSpan   = 0;
                            $totalStallJual   = 0;
                            $totalBuffetJual  = 0;
                            $totalMinumanJual = 0;
                            $totalDesertJual  = 0;
                            // echo "<pre>";
                            // print_r($summaryTrx['2018-04-03']);
                            // echo "</pre>";
                            
                        ?>


                        <br>
                        <?php

                            $nama_hari = array(
                                "Monday"    => "Senin",
                                "Tuesday"   => "Selasa",
                                "Wednesday" => "Rabu",
                                "Thursday"  => "Kamis",
                                "Friday"    => "Jumat",
                                "Saturday"  => "Sabtu",
                                "Sunday"    => "Minggu"
                            );
                            
                            $explode  = explode(",", $newDate);
                            $hari     = $nama_hari[$explode[0]];
                            $explode  = explode(".", trim($explode[1]));
                            $tanggal  = $explode[0]."-".$explode[1]."-".$explode[2];
                            $tanggal2 = $explode[2]."-".$explode[1]."-".$explode[0];
                        ?>
                        <?=$hari.", ".$tanggal?> 
                        <div class="table-responsive">
                        <table border="1" width="100%">
                            <thead>
                                <tr align="center" style="background-color:#0060af; color: white">
                                    <td rowspan="3"><b>No.</td>
                                    <td rowspan="3"><b>NIP</td>
                                    <td rowspan="3"><b>Nama</td>
                                    <td rowspan="3"><b>Program</td>
                                    <td rowspan="3"><b>Sub Program</td>
                                    <td rowspan="3"><b>Nama Pelatihan</td>
                                    <?php if($getTypeMakanan)foreach($getTypeMakanan as $list){

                                    ${'getTotalByType'.$list->f_id}          = $this->report_model->getMakananByFilter($list->f_id,0,$newDate);
                                    ${'getTotalMakananByStall'.$list->f_id}  = $this->report_model->getMakananByFilter($list->f_id,2,$newDate);
                                    ${'getTotalMakananByBuffet'.$list->f_id} = $this->report_model->getMakananByFilter($list->f_id,1,$newDate);
                                    ${'categorySpan_'.$list->f_id}           = count(${'getTotalByType'.$list->f_id});

                                    ?>

                                    <?php

                                    $totalMakanannStall =  count( ${'getTotalMakananByStall1'});
                                    $stallColSpan = $totalMakanannStall;
                                    $totalMakanannBuffet =  count( ${'getTotalMakananByBuffet1'});
                                    $buffetColSpan =$totalMakanannBuffet; 
                                    ?>

                                    <?php

                                    if($list->f_id == 1 && ${'categorySpan_'.$list->f_id} == 0){
                                        ${'categorySpan_'.$list->f_id}=2;

                                    }
                                    if($list->f_id == 1 && $stallColSpan > 0 && $buffetColSpan == 0){
                                        $buffetColSpan =1;
                                        ${'categorySpan_'.$list->f_id}+=$buffetColSpan;
                                    }
                                    if($list->f_id == 1 && $stallColSpan == 0 && $buffetColSpan > 0){
                                        $stallColSpan =1;
                                        ${'categorySpan_'.$list->f_id}+=$stallColSpan;
                                    }



                                    ?>

                                    <td rowspan="<?php if($list->f_id == 1)echo '1';else echo'2';?>" colspan="<?php echo ${'categorySpan_'.$list->f_id}  ?>"><b><?php echo $list->f_type?></td>
                            <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
<!--
                                    <td rowspan="2" colspan="">HIDANGAN PENUTUP</td>
                                    <td rowspan="2" colspan="">MINUMAN</td>
-->
                                </tr>
                                <tr align="center" style="background-color:#0060af; color: white">


                                    <td colspan="<?php echo $totalMakanannStall;?>"><b>Stall</td>

                                    <td colspan="<?php echo $totalMakanannBuffet ;?>"><b>Buffet</td>

                                </tr>

                                <tr align="center" style="background-color:#0060af; color: white">
                                    <?php if($getTotalMakananByStall1)foreach($getTotalMakananByStall1 as $list1000){?>
                                    <td><b><?php echo ucwords($list1000->c_name)?></td>
                                 <?php }else{ ?>
                                     <td></td>

                                    <?php } ?>
                                    <?php if($getTotalMakananByBuffet1)foreach($getTotalMakananByBuffet1 as $list1000){?>
                                    <td><b><?php echo ucwords($list1000->c_name)?></td>
                                    <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                    <?php if($getTotalByType2)foreach($getTotalByType2 as $list1000){?>
                                    <td><b><?php echo ucwords($list1000->c_name)?></td>
                              <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                    <?php if($getTotalByType3)foreach($getTotalByType3 as $list1000){?>
                                    <td><b><?php echo ucwords($list1000->c_name)?></td>
                                 <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                </tr>
                                 <tr align="center" style="background-color:#0093d6; color: white">
                                     <td colspan="6" style="text-align:right;"><b>Stock &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <?php if($getTotalMakananByStall1)foreach($getTotalMakananByStall1 as $list1000){?>
                                    <td><b><?php echo numberformat($list1000->starting_stock)?></td>
                                      <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                    <?php if($getTotalMakananByBuffet1)foreach($getTotalMakananByBuffet1 as $list1000){?>
                                    <td><b><?php echo numberformat($list1000->starting_stock)?></td>
                                     <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                    <?php if($getTotalByType2)foreach($getTotalByType2 as $list1000){?>
                                    <td><b><?php echo numberformat($list1000->starting_stock)?></td>
                                    <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                    <?php if($getTotalByType3)foreach($getTotalByType3 as $list1000){?>
                                    <td><b><?php echo numberformat($list1000->starting_stock)?></td>
                                     <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>

                                </tr>

                            </thead>
                            <tbody>
                                <?php $getTrxByDate = $this->report_model->getTrxByDate($newDate);?>
                                <?php
                                // echo "<pre>";
                                // print_r($getTrxByDate);
                                // echo "</pre>";die;
                                $no=1;
                                if($getTrxByDate)foreach($getTrxByDate as $list10000z){
                                  $ex = 0;
                                  if(is_numeric($list10000z->e_id))
                                  {
                                    $res = $this->report_model->getParticipant($list10000z->e_id, $newDate)->row();
                                    if(!empty($res)){ 
                                      $co_id = $res->co_id;
                                      $res2 = $this->report_model->getProgram($co_id)->row();
                                      if(!empty($res2)){
                                        $prog_name    = $res2->program_name;
                                        $subprog_name = $res2->subprogram_name;
                                        $train_name   = $res2->training_name;
                                      }
                                      else
                                      {
                                        $co_id        = "";
                                        $prog_name    = "";
                                        $subprog_name = "";
                                        $train_name   = "";
                                      }
                                    }
                                    else 
                                    { 
                                      $co_id        = "";
                                      $prog_name    = "";
                                      $subprog_name = "";
                                      $train_name   = "";
                                    }
                                  }
                                  else if(!is_numeric($list10000z->e_id))
                                  {
                                      $ex = 1;
                                      $co_id        = "";
                                      $prog_name    = $list10000z->program_name;
                                      $subprog_name = "";
                                      $train_name   = "";
                                  }
                                  else
                                  {
                                    //$co_id        = "";
                                    $prog_name    = "";
                                    $subprog_name = "";
                                    $train_name   = "";
                                  }
                                  ?>
                                <tr>
                                  <td><?php echo $no++;?></td>
                                  <td><?php 
                                          if(strpos($list10000z->e_id, "EX_") !== false)
                                          {
                                            echo substr($list10000z->e_id, 3);
                                          }
                                          else
                                          {
                                            echo $list10000z->e_id;
                                          }
                                      ?>
                                  </td>
                                  <td><?php echo $list10000z->e_name;?></td>
                                  <td><?php echo $prog_name; ?></td>
                                  <td><?php echo $subprog_name; ?></td>
                                  <td><?php echo $train_name; ?></td>
                                      <?php if($getTotalMakananByStall1)foreach($getTotalMakananByStall1 as $list1000){
                                    // $totalBelanjaHariIni = $this->report_model->getTrxByDateCustomerItem($newDate,$list10000z->e_id,$list1000->c_id);
                                    // $totalStallJual+=$totalBelanjaHariIni;
                                        if(array_key_exists($list1000->c_id, $summaryTrx[$tanggal2]))
                                        {
                                          if(in_array($list10000z->e_id, $summaryTrx[$tanggal2][$list1000->c_id]))
                                          {
                                            $totalBelanjaHariIni = 1;
                                            $totalStallJual+=$totalBelanjaHariIni;
                                          }
                                          else
                                          {
                                            $totalBelanjaHariIni = 0;
                                          }
                                        }
                                        else
                                        {
                                          $totalBelanjaHariIni = 0;
                                        }
                                    ?>
                                    <td><?php echo numberformat($totalBelanjaHariIni);?></td>
                               <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                      <?php if($getTotalMakananByBuffet1)foreach($getTotalMakananByBuffet1 as $list1000){
                                    // $totalBelanjaHariIni = $this->report_model->getTrxByDateCustomerItem($newDate,$list10000z->e_id,$list1000->c_id);
                                    // $totalBuffetJual+=$totalBelanjaHariIni;
                                        if(array_key_exists($list1000->c_id, $summaryTrx[$tanggal2]))
                                        {
                                          if(in_array($list10000z->e_id, $summaryTrx[$tanggal2][$list1000->c_id]))
                                          {
                                            $totalBelanjaHariIni = 1;
                                            $totalBuffetJual+=$totalBelanjaHariIni;
                                          }
                                          else
                                          {
                                            $totalBelanjaHariIni = 0;
                                          }
                                        }
                                        else
                                        {
                                          $totalBelanjaHariIni = 0;
                                        }
                                    ?>
                                    <td><?php echo numberformat($totalBelanjaHariIni);?></td>
                                  <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>

                                  <?php if($getTotalByType2)foreach($getTotalByType2 as $list1000){
                                    // $totalBelanjaHariIni = $this->report_model->getTrxByDateCustomerItem($newDate,$list10000z->e_id,$list1000->c_id);
                                    if(array_key_exists($list1000->c_id, $summaryTrx[$tanggal2]))
                                    {
                                      if(in_array($list10000z->e_id, $summaryTrx[$tanggal2][$list1000->c_id]))
                                      {
                                        $totalBelanjaHariIni = 1;
                                      }
                                      else
                                      {
                                        $totalBelanjaHariIni = 0;
                                      }
                                    }
                                    else
                                    {
                                      $totalBelanjaHariIni = 0;
                                    }
                                    ?>
                                    <td><?php echo numberformat($totalBelanjaHariIni);?></td>
                                    <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                    <?php if($getTotalByType3)foreach($getTotalByType3 as $list1000){
                                      // $totalBelanjaHariIni = $this->report_model->getTrxByDateCustomerItem($newDate,$list10000z->e_id,$list1000->c_id);
                                      if(array_key_exists($list1000->c_id, $summaryTrx[$tanggal2]))
                                      {
                                        if(in_array($list10000z->e_id, $summaryTrx[$tanggal2][$list1000->c_id]))
                                        {
                                          $totalBelanjaHariIni = 1;
                                        }
                                        else
                                        {
                                          $totalBelanjaHariIni = 0;
                                        }
                                      }
                                      else
                                      {
                                        $totalBelanjaHariIni = 0;
                                      }
                                    ?>
                                    <td><?php echo numberformat($totalBelanjaHariIni);;?></td>
                                     <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>

                                </tr>
                                <?php } ?>

                                <tr style="background-color:#0093d6; color: white">
                                <td rowspan="3" colspan="2"><b>Total</td>
                                <td  colspan="4"  style="text-align:right;"><b>PerItem &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                     <?php if($getTotalMakananByStall1)foreach($getTotalMakananByStall1 as $list1000){



$totalBelanjaHariIni = $this->report_model->getTrxByDateCustomerItem($newDate,0,$list1000->c_id);
                                       ${'sisaStock_'.$list1000->c_id} = $totalBelanjaHariIni;
                                    ?>
                                    <td><?php echo numberformat($totalBelanjaHariIni)?></td>
                                   <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                      <?php if($getTotalMakananByBuffet1)foreach($getTotalMakananByBuffet1 as $list1000){
                                    $totalBelanjaHariIni = $this->report_model->getTrxByDateCustomerItem($newDate,0,$list1000->c_id);
                                        ${'sisaStock_'.$list1000->c_id} = $totalBelanjaHariIni;
                                    ?>
                                    <td><?php echo numberformat($totalBelanjaHariIni)?></td>
                                   <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>

                                  <?php if($getTotalByType2)foreach($getTotalByType2 as $list1000){
                                    $totalBelanjaHariIni = $this->report_model->getTrxByDateCustomerItem($newDate,0,$list1000->c_id);
                                    $totalMinumanJual+=$totalBelanjaHariIni;
                                        ${'sisaStock_'.$list1000->c_id} = $totalBelanjaHariIni;
                                    ?>
                                    <td><?php echo numberformat($totalBelanjaHariIni)?></td>
                                    <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                    <?php if($getTotalByType3)foreach($getTotalByType3 as $list1000){
                                      $totalBelanjaHariIni = $this->report_model->getTrxByDateCustomerItem($newDate,0,$list1000->c_id);
                                     $totalDesertJual+=$totalBelanjaHariIni;
                                        ${'sisaStock_'.$list1000->c_id} = $totalBelanjaHariIni;
                                    ?>
                                    <td><?php echo numberformat($totalBelanjaHariIni);?></td>
                                     <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                </tr>
                                <tr style="background-color:#0093d6; color: white">
                                     <td colspan="4"  style="text-align:right;"><b>Stall / Buffet &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <?php if($stallColSpan > 0 ){?>
                                     <td colspan="<?php echo $stallColSpan;?>"><?php echo numberformat($totalStallJual)?></td>
                                <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                    <?php if($buffetColSpan){?>
                                     <td colspan="<?php echo $buffetColSpan;?>"><?php echo numberformat($totalBuffetJual);?></td>
                             <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>

                                        <?php
                                    $minumanColspan = ${"categorySpan_2"};
$dessertColSpan = ${"categorySpan_3"};;
                                    ?>
                                     <td colspan="<?php echo $minumanColspan;?>" style="text-align:center;"> -</td>
                                     <td colspan="<?php echo $dessertColSpan;?>" style="text-align:center;"> -</td>

                                </tr>
                                <tr style="background-color:#0093d6; color: white">
                                 <td  colspan="4" style="text-align:right;"><b>Perkategori &nbsp;&nbsp;&nbsp;&nbsp;</td>

                                         <?php if($getTypeMakanan)foreach($getTypeMakanan as $list){
                                    if($list->f_id == 1){
                                        ${'totalPerTypeMakanan_'.$list->f_id}= $totalStallJual+$totalBuffetJual;
                                    }elseif($list->f_id == 2){
                                        ${'totalPerTypeMakanan_'.$list->f_id}= $totalMinumanJual;
                                    }elseif($list->f_id == 3){
                                        ${'totalPerTypeMakanan_'.$list->f_id}= $totalDesertJual;
                                    }


                                    ?>


                                    <td colspan="<?php echo ${'categorySpan_'.$list->f_id};?>"><?php echo numberformat(${'totalPerTypeMakanan_'.$list->f_id});?></td>
                                    <?php } ?>





                                </tr>
                                <tr style="background-color:#0093d6; color: white">
                                <td colspan="6" style="text-align:right;"><b>Stok Lebih &nbsp;&nbsp;&nbsp;&nbsp;</td>

                                     <?php if($getTotalMakananByStall1)foreach($getTotalMakananByStall1 as $list1000){?>
                                    <td colspan=""><b><?php echo numberformat($list1000->starting_stock - ${'sisaStock_'.$list1000->c_id})?></td>
                                    <?php }else{ ?>
                                     <td colspan="1">&nbsp;</td>

                                    <?php } ?>
                                    <?php if($getTotalMakananByBuffet1)foreach($getTotalMakananByBuffet1 as $list1000){?>
                                   <td><b><?php echo numberformat($list1000->starting_stock - ${'sisaStock_'.$list1000->c_id})?>
                                 <?php }else{ ?>
                                     <td></td>

                                    <?php } ?>
                                    <?php if($getTotalByType2)foreach($getTotalByType2 as $list1000){?>
                            <td><b><?php echo numberformat($list1000->starting_stock - ${'sisaStock_'.$list1000->c_id})?>
                                    <?php }else{ ?>
                                     <td></td>

                                    <?php } ?>
                                    <?php if($getTotalByType3)foreach($getTotalByType3 as $list1000){?>
                                  <td><b><?php echo numberformat($list1000->starting_stock - ${'sisaStock_'.$list1000->c_id})?>
                                     <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>




                                </tr>
                            </tbody>

                        </table>
                        <?php } ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End  -->

</div>

<!-- End Page Scripts -->
</section>
