
<style>
table {
    text-align:center;!important;
}
</style>
<section class="page-content">
<div class="page-content-inner">
    <!--  -->
    <section class="panel">
        <div class="panel-heading">
            <h3>Laporan Rekap Harian Kantin<br>

            Periode : <?php echo $start_date. ' s.d '. $end_date;?>
            </h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="margin-bottom-50">

                        <?php
                            $stallColSpan     = 0;
                            $buffetColSpan    = 0;
                            $minumanColspan   = 0;
                            $dessertColSpan   = 0;
                            $totalStallJual   = 0;
                            $totalBuffetJual  = 0 ;
                            $totalMinumanJual = 0;
                            $totalDesertJual  = 0;
                        ?>

                        <br>
                        <div class="table-responsive">
                        <table border="1" width="100%">
                            <thead>
                                <tr align="center" style="background-color:#0060af; color: white">
                                    <td rowspan="3" colspan="3"><b>Tanggal</td>

                                    <?php if($getTypeMakanan)foreach($getTypeMakanan as $list){

                                    ${'getTotalByType'.$list->f_id}          = $this->report_model->getMakananByFilterStartDateEndDate($list->f_id,0,$start_date,$end_date);
                                    ${'getTotalMakananByStall'.$list->f_id}  = $this->report_model->getMakananByFilterStartDateEndDate($list->f_id,2,$start_date,$end_date);
                                    ${'getTotalMakananByBuffet'.$list->f_id} = $this->report_model->getMakananByFilterStartDateEndDate($list->f_id,1,$start_date,$end_date);
                                    ${'categorySpan_'.$list->f_id}           = count(${'getTotalByType'.$list->f_id});
                                    
                                    // echo "<pre>";
                                    // print_r($getTotalMakananByStall1);echo "</pre>";die;
                                    $totalMakanannStall  = count( ${'getTotalMakananByStall1'});
                                    $stallColSpan        = $totalMakanannStall;
                                    $totalMakanannBuffet = count( ${'getTotalMakananByBuffet1'});
                                    $buffetColSpan       = $totalMakanannBuffet; 
                                    
                                    if($list->f_id == 1 && ${'categorySpan_'.$list->f_id} == 0){
                                        ${'categorySpan_'.$list->f_id}=2;
                                    }

                                    if($list->f_id == 1 && $stallColSpan > 0 && $buffetColSpan == 0){
                                        $buffetColSpan =1;
                                        ${'categorySpan_'.$list->f_id}+=$buffetColSpan;
                                    }

                                    if($list->f_id == 1 && $stallColSpan == 0 && $buffetColSpan > 0){
                                        $stallColSpan =1;
                                        ${'categorySpan_'.$list->f_id}+=$stallColSpan;
                                    }

                                    ?>

                                    <td rowspan="<?php if($list->f_id == 1)echo '1';else echo'2';?>" colspan="<?php echo ${'categorySpan_'.$list->f_id}  ?>"><b><?php echo $list->f_type?></td>
                                    <?php 
                                    } else { 
                                    ?>
                                    <td>&nbsp;</td>

                                    <?php } ?>
<!--
                                    <td rowspan="2" colspan="">HIDANGAN PENUTUP</td>
                                    <td rowspan="2" colspan="">MINUMAN</td>
-->
                                </tr>

                                <tr align="center" style="background-color:#0060af; color: white">
                                    <td colspan="<?php echo $totalMakanannStall;?>"><b>Stall</td>
                                    <td colspan="<?php echo $totalMakanannBuffet ;?>"><b>Buffet</td>
                                </tr>

                                <tr align="center" style="background-color:#0060af; color: white">
                                    <?php if($getTotalMakananByStall1)foreach($getTotalMakananByStall1 as $list1000){
                                        ${"totalStockSemua_".$list1000->c_id} = 0;
                                        ${"sisaStock_".$list1000->c_id}       = 0;
                                    ?>
                                    <td><b><?php echo ucwords($list1000->c_name)?></td>
                                    <?php 
                                    }else{ 
                                    ?>
                                    <td></td>
                                    <?php } ?>

                                    <?php if($getTotalMakananByBuffet1)foreach($getTotalMakananByBuffet1 as $list1000){
                                        ${"totalStockSemua_".$list1000->c_id} = 0;
                                        ${"sisaStock_".$list1000->c_id}       = 0;
                                    ?>
                                    <td><b><?php echo ucwords($list1000->c_name)?></td>
                                    <?php }else{ ?>
                                    <td>&nbsp;</td>
                                    <?php } ?>

                                    <?php if($getTotalByType2)foreach($getTotalByType2 as $list1000){
                                        ${"totalStockSemua_".$list1000->c_id} = 0;
                                        ${"sisaStock_".$list1000->c_id}       = 0;
                                    ?>
                                    <td><b><?php echo ucwords($list1000->c_name)?></td>
                                    <?php }else{ ?>
                                    <td>&nbsp;</td>
                                    <?php } ?>

                                    <?php if($getTotalByType3)foreach($getTotalByType3 as $list1000){
                                        ${"totalStockSemua_".$list1000->c_id} = 0;
                                        ${"sisaStock_".$list1000->c_id}       = 0;
                                    ?>
                                    <td><b><?php echo $list1000->c_name?></td>
                                    <?php }else{ ?>
                                    <td>&nbsp;</td>
                                    <?php } ?>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $no = 1;
                                if($getPeriode)foreach($getPeriode as $nnewDate){?>
                                <tr>
                                <td colspan="3">
                                    <?php
                                        $nama_hari = array(
                                            "Monday"    => "Senin",
                                            "Tuesday"   => "Selasa",
                                            "Wednesday" => "Rabu",
                                            "Thursday"  => "Kamis",
                                            "Friday"    => "Jumat",
                                            "Saturday"  => "Sabtu",
                                            "Sunday"    => "Minggu"
                                        );

                                        $explode = explode(",", $nnewDate);
                                        $hari    = $nama_hari[$explode[0]];
                                        $explode = explode(".", trim($explode[1]));
                                        $tanggal = $explode[0]."-".$explode[1]."-".$explode[2];
                                        echo $hari.", ".$tanggal;
                                    ?>
                                </td>

                                <?php if($getTotalMakananByStall1)foreach($getTotalMakananByStall1 as $list1000){
                                    $totalBelanjaHariIni = $this->report_model->getTrxByDateProgramItemDaily($nnewDate,$list1000->c_id);
                                    // echo "c_id:".$list1000->c_id;
                                    // echo "totalBelanja: ".$totalBelanjaHariIni;
                                    // echo "startingStock: ".$list1000->starting_stock;
                                    $serving = 0;
                                    $serving = $this->report_model->checkServing($nnewDate, $list1000->c_id);
                                    if($serving->total > 0)
                                    {
                                      ${"totalStockSemua_".$list1000->c_id}+=$serving->starting_stock;
                                    }

                                    $totalStallJual+=$totalBelanjaHariIni;
                                    ?>
                                    <td><?php echo numberformat($totalBelanjaHariIni);?></td>
                               <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                      <?php if($getTotalMakananByBuffet1)foreach($getTotalMakananByBuffet1 as $list1000){
                                    $totalBelanjaHariIni = $this->report_model->getTrxByDateProgramItemDaily($nnewDate,$list1000->c_id);
                                    $totalBuffetJual+=$totalBelanjaHariIni;
                                    $serving = 0;
                                    $serving = $this->report_model->checkServing($nnewDate, $list1000->c_id);
                                    if($serving->total > 0)
                                    {
                                      ${"totalStockSemua_".$list1000->c_id}+=$serving->starting_stock;
                                    }

                                    ?>
                                    <td><?php echo numberformat($totalBelanjaHariIni);?></td>
                                  <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>

                                  <?php if($getTotalByType2)foreach($getTotalByType2 as $list1000){
                                    $totalBelanjaHariIni = $this->report_model->getTrxByDateProgramItemDaily($nnewDate,$list1000->c_id);
                                             $totalMinumanJual+=$totalBelanjaHariIni;
                                             $serving = 0;
                                    $serving = $this->report_model->checkServing($nnewDate, $list1000->c_id);
                                    if($serving->total > 0)
                                    {
                                      ${"totalStockSemua_".$list1000->c_id}+=$serving->starting_stock;
                                    }

                                    ?>
                                    <td><?php echo numberformat($totalBelanjaHariIni);?></td>
                                    <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                    <?php if($getTotalByType3)foreach($getTotalByType3 as $list1000){
                                      $totalBelanjaHariIni = $this->report_model->getTrxByDateProgramItemDaily($nnewDate,$list1000->c_id);
                                        $totalDesertJual+=$totalBelanjaHariIni;
                                        $serving = 0;
                                    $serving = $this->report_model->checkServing($nnewDate, $list1000->c_id);
                                    if($serving->total > 0)
                                    {
                                      ${"totalStockSemua_".$list1000->c_id}+=$serving->starting_stock;
                                    }

                                    ?>
                                    <td><?php echo numberformat($totalBelanjaHariIni);?></td>
                                     <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>

                                </tr>
                                <?php } ?>

                                <tr style="background-color:#0093d6; color: white">
                                <td rowspan="3" colspan="1"><b>Total</td>
                                <td  colspan="2"  style="text-align:right;"><b>PerItem &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                     <?php if($getTotalMakananByStall1)foreach($getTotalMakananByStall1 as $list1000){



$totalBelanjaHariIni = $this->report_model->getTrxByDateProgramItemDailyStartDateEndDate($start_date,$end_date,$list1000->c_id);
                                       ${'sisaStock_'.$list1000->c_id}+= $totalBelanjaHariIni;
                                    ?>
                                    <td><?php echo numberformat($totalBelanjaHariIni);?></td>
                                   <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                      <?php if($getTotalMakananByBuffet1)foreach($getTotalMakananByBuffet1 as $list1000){
                                    $totalBelanjaHariIni = $this->report_model->getTrxByDateProgramItemDailyStartDateEndDate($start_date,$end_date,$list1000->c_id);
                                        ${'sisaStock_'.$list1000->c_id}+= $totalBelanjaHariIni;
                                    ?>
                                    <td><?php echo numberformat($totalBelanjaHariIni);?></td>
                                   <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>

                                  <?php if($getTotalByType2)foreach($getTotalByType2 as $list1000){
                                    $totalBelanjaHariIni = $this->report_model->getTrxByDateProgramItemDailyStartDateEndDate($start_date,$end_date,$list1000->c_id);

                                        ${'sisaStock_'.$list1000->c_id}+= $totalBelanjaHariIni;
                                    ?>
                                    <td><?php echo numberformat($totalBelanjaHariIni);?></td>
                                    <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                    <?php if($getTotalByType3)foreach($getTotalByType3 as $list1000){
                                      $totalBelanjaHariIni = $this->report_model->getTrxByDateProgramItemDailyStartDateEndDate($start_date,$end_date,$list1000->c_id);

                                        ${'sisaStock_'.$list1000->c_id}+= $totalBelanjaHariIni;
                                    ?>
                                    <td><?php echo numberformat($totalBelanjaHariIni);;?></td>
                                     <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                </tr>
                                <tr style="background-color:#0093d6; color: white">
                                     <td colspan="2"  style="text-align:right;"><b>Stall / Buffet &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <?php if($stallColSpan > 0 ){?>
                                     <td colspan="<?php echo $stallColSpan;?>"><?php echo numberformat($totalStallJual);?></td>
                                <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>
                                    <?php if($buffetColSpan){?>
                                     <td colspan="<?php echo $buffetColSpan;?>"><?php echo numberformat($totalBuffetJual);?></td>
                             <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>

                                        <?php
                                    $minumanColspan = ${"categorySpan_2"};
$dessertColSpan = ${"categorySpan_3"};;
                                    ?>
                                     <td colspan="<?php echo $minumanColspan;?>" style="text-align:center;"> -</td>
                                     <td colspan="<?php echo $dessertColSpan;?>" style="text-align:center;"> -</td>

                                </tr>
                                <tr style="background-color:#0093d6; color: white">
                                 <td  colspan="2" style="text-align:right;"><b>Perkategori &nbsp;&nbsp;&nbsp;&nbsp;</td>

                                         <?php if($getTypeMakanan)foreach($getTypeMakanan as $list){
                                    if($list->f_id == 1){
                                        ${'totalPerTypeMakanan_'.$list->f_id}= $totalStallJual+$totalBuffetJual;
                                    }elseif($list->f_id == 2){
                                        ${'totalPerTypeMakanan_'.$list->f_id}= $totalMinumanJual;
                                    }elseif($list->f_id == 3){
                                        ${'totalPerTypeMakanan_'.$list->f_id}= $totalDesertJual;
                                    }


                                    ?>


                                    <td colspan="<?php echo ${'categorySpan_'.$list->f_id};?>"><?php echo numberformat (${'totalPerTypeMakanan_'.$list->f_id});?></td>
                                    <?php } ?>





                                </tr>
                                <tr style="background-color:#0093d6; color: white">
                                <td colspan="3" style="text-align:right;"><b>Stok Lebih &nbsp;&nbsp;&nbsp;&nbsp;</td>

                                     <?php if($getTotalMakananByStall1)foreach($getTotalMakananByStall1 as $list1000){?>
                                    <td colspan=""><b><?php echo numberformat(${"totalStockSemua_".$list1000->c_id} - ${'sisaStock_'.$list1000->c_id})?></td>
                                    <?php }else{ ?>
                                     <td colspan="1">&nbsp;</td>

                                    <?php } ?>
                                    <?php if($getTotalMakananByBuffet1)foreach($getTotalMakananByBuffet1 as $list1000){?>
                                   <td><b><?php echo numberformat(${"totalStockSemua_".$list1000->c_id} - ${'sisaStock_'.$list1000->c_id})?>
                                 <?php }else{ ?>
                                     <td></td>

                                    <?php } ?>
                                    <?php if($getTotalByType2)foreach($getTotalByType2 as $list1000){?>
                            <td><b><?php echo numberformat(${"totalStockSemua_".$list1000->c_id} - ${'sisaStock_'.$list1000->c_id})?>
                                    <?php }else{ ?>
                                     <td></td>

                                    <?php } ?>
                                    <?php if($getTotalByType3)foreach($getTotalByType3 as $list1000){?>
                                  <td><b><?php echo numberformat(${"totalStockSemua_".$list1000->c_id}- ${'sisaStock_'.$list1000->c_id})?>
                                     <?php }else{ ?>
                                     <td>&nbsp;</td>

                                    <?php } ?>




                                </tr>
                            </tbody>

                        </table>
                    	</div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End  -->

</div>

<!-- End Page Scripts -->
</section>
