
<style>
table {
    text-align:center;!important;
}
</style>
<section class="page-content">
<div class="page-content-inner">
    <!--  -->
    <section class="panel">
        <div class="panel-heading">
            <h3>Laporan Peringkat Makanan Kantin<br>

            Periode : <?php echo $start_date. ' s.d '. $end_date;?>
            </h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">

                    <div class="margin-bottom-50">


                        <?php
$stallColSpan = 0;
$buffetColSpan = 0;
$minumanColspan = 0;
$dessertColSpan = 0;
$totalStallJual = 0;
$totalBuffetJual = 0 ;
$totalMinumanJual = 0;
$totalDesertJual = 0;
$forDataLoopingArray = array();
?>


                        <br>
                        <div class="table-responsive">
                        <table border="1" width="100%">
                            <thead>
                                <tr align="center" style="background-color:#0060af; color: white">
                                    <td ><b>Peringkat</td>
                                    <td  colspan=""><b>Nama Menu</td>
                                    <td  colspan=""><b>Stok</td>
                                    <td  colspan=""><b>Sisa Stok</td>
                                    <td><b>Jam Tap Terakhir</td>



                                    <?php if($getTypeMakanan)foreach($getTypeMakanan as $list){

                                    ${'getTotalByType'.$list->f_id}  = $this->report_model->getMakananByFilterStartDateEndDate($list->f_id,0,$start_date,$end_date);
                                    ${'getTotalMakananByStall'.$list->f_id} = $this->report_model->getMakananByFilterStartDateEndDate($list->f_id,2,$start_date,$end_date);
                                     ${'getTotalMakananByBuffet'.$list->f_id} = $this->report_model->getMakananByFilterStartDateEndDate($list->f_id,1,$start_date,$end_date);
                                         ${'categorySpan_'.$list->f_id} =  count(${'getTotalByType'.$list->f_id});

                                    ?>

                                     <?php

                                    $totalMakanannStall =  count( ${'getTotalMakananByStall1'});
                                    $stallColSpan = $totalMakanannStall;
                                    $totalMakanannBuffet =  count( ${'getTotalMakananByBuffet1'});
                                    $buffetColSpan =$totalMakanannBuffet; ?>

                                    <?php

                                    if($list->f_id == 1 && ${'categorySpan_'.$list->f_id} == 0){
                                        ${'categorySpan_'.$list->f_id}=2;

                                    }
                                    if($list->f_id == 1 && $stallColSpan > 0 && $buffetColSpan == 0){
                                        $buffetColSpan =1;
                                        ${'categorySpan_'.$list->f_id}+=$buffetColSpan;
                                    }
                                    if($list->f_id == 1 && $stallColSpan == 0 && $buffetColSpan > 0){
                                        $stallColSpan =1;
                                        ${'categorySpan_'.$list->f_id}+=$stallColSpan;
                                    }



                                    ?>


                            <?php }?>
                                </tr>

                                 <?php if($getTotalMakananByStall1)foreach($getTotalMakananByStall1 as $list1000){

                                     ${"totalStockSemua_".$list1000->c_id}=0;
                                     ${"sisaStock_".$list1000->c_id}=0;
                                    ?>

                                 <?php }else{ ?>


                                    <?php } ?>
                                    <?php if($getTotalMakananByBuffet1)foreach($getTotalMakananByBuffet1 as $list1000){
                                      ${"totalStockSemua_".$list1000->c_id}=0;
                                      ${"sisaStock_".$list1000->c_id}=0;
                                    ?>

                                    <?php }else{ ?>


                                    <?php } ?>
                                    <?php if($getTotalByType2)foreach($getTotalByType2 as $list1000){

                                      ${"totalStockSemua_".$list1000->c_id}=0;
                                      ${"sisaStock_".$list1000->c_id}=0;?>

                              <?php }else{ ?>

                                    <?php } ?>
                                    <?php if($getTotalByType3)foreach($getTotalByType3 as $list1000){
                                      ${"totalStockSemua_".$list1000->c_id}=0;
                                      ${"sisaStock_".$list1000->c_id}=0;
                                    ?>

                                 <?php }else{ ?>

                                    <?php } ?>

                            </thead>
                            <tbody>
<?php if($getTotalMakananByStall1)foreach($getTotalMakananByStall1 as $list1000){
$totalBelanjaHariIni = $this->report_model->getTrxByDateProgramItemDailyStartDateEndDate($start_date,$end_date,$list1000->c_id);

                                    ${"totalStockSemua_".$list1000->c_id}+=$list1000->starting_stock;
                                      ${"sisaStock_".$list1000->c_id}+=$totalBelanjaHariIni;
                                    $totalStallJual+=$totalBelanjaHariIni;
                                    ?>

                               <?php }else{ ?>

                                    <?php } ?>
                                      <?php if($getTotalMakananByBuffet1)foreach($getTotalMakananByBuffet1 as $list1000){
                                    $totalBelanjaHariIni = $this->report_model->getTrxByDateProgramItemDailyStartDateEndDate($start_date,$end_date,$list1000->c_id);
                                    $totalBuffetJual+=$totalBelanjaHariIni;
                                     ${"totalStockSemua_".$list1000->c_id}+=$list1000->starting_stock;
                                      ${"sisaStock_".$list1000->c_id}+=$totalBelanjaHariIni;
                                    ?>

                                  <?php }else{ ?>

                                    <?php } ?>

                                  <?php if($getTotalByType2)foreach($getTotalByType2 as $list1000){
                                    $totalBelanjaHariIni = $this->report_model->getTrxByDateProgramItemDailyStartDateEndDate($start_date,$end_date,$list1000->c_id);
                                             $totalMinumanJual+=$totalBelanjaHariIni;
                                     ${"totalStockSemua_".$list1000->c_id}+=$list1000->starting_stock;
                                      ${"sisaStock_".$list1000->c_id}+=$totalBelanjaHariIni;
                                    ?>

                                    <?php }else{ ?>


                                    <?php } ?>
                                    <?php if($getTotalByType3)foreach($getTotalByType3 as $list1000){
                                      $totalBelanjaHariIni = $this->report_model->getTrxByDateProgramItemDailyStartDateEndDate($start_date,$end_date,$list1000->c_id);
                                        $totalDesertJual+=$totalBelanjaHariIni;
                                     ${"totalStockSemua_".$list1000->c_id}+=$list1000->starting_stock;
                                      ${"sisaStock_".$list1000->c_id}+=$totalBelanjaHariIni;
                                    ?>

                                     <?php }?>








                                     <?php if($getTotalMakananByStall1)foreach($getTotalMakananByStall1 as $list1000){
      $forDataLoopingArray[$list1000->c_id]['id'] = $list1000->c_id;

                                      $forDataLoopingArray[$list1000->c_id]['totalStock'] = ${"totalStockSemua_".$list1000->c_id};
                                      $forDataLoopingArray[$list1000->c_id]['sisaStock'] = ${"sisaStock_".$list1000->c_id};
                                        $forDataLoopingArray[$list1000->c_id]['makananName'] =ucwords($list1000->c_name);


                                      ?>

                                    <?php }?>
                                    <?php if($getTotalMakananByBuffet1)foreach($getTotalMakananByBuffet1 as $list1000){
      $forDataLoopingArray[$list1000->c_id]['id'] = $list1000->c_id;

                                        $forDataLoopingArray[$list1000->c_id]['totalStock'] = ${"totalStockSemua_".$list1000->c_id};
                                      $forDataLoopingArray[$list1000->c_id]['sisaStock'] = ${"sisaStock_".$list1000->c_id};
                                        $forDataLoopingArray[$list1000->c_id]['makananName'] =ucwords($list1000->c_name);
                                      ?>

                                 <?php }else{ ?>


                                    <?php } ?>
                                    <?php if($getTotalByType2)foreach($getTotalByType2 as $list1000){
      $forDataLoopingArray[$list1000->c_id]['id'] = $list1000->c_id;
                                        $forDataLoopingArray[$list1000->c_id]['totalStock'] = ${"totalStockSemua_".$list1000->c_id};
                                      $forDataLoopingArray[$list1000->c_id]['sisaStock'] = ${"sisaStock_".$list1000->c_id};
                                        $forDataLoopingArray[$list1000->c_id]['makananName'] =ucwords($list1000->c_name);
                                      ?>

                                    <?php }?>
                                    <?php if($getTotalByType3)foreach($getTotalByType3 as $list1000){
                                         $forDataLoopingArray[$list1000->c_id]['id'] = $list1000->c_id;
                                        $forDataLoopingArray[$list1000->c_id]['totalStock'] = ${"totalStockSemua_".$list1000->c_id};
                                      $forDataLoopingArray[$list1000->c_id]['sisaStock'] = ${"sisaStock_".$list1000->c_id};
                                        $forDataLoopingArray[$list1000->c_id]['makananName'] =ucwords($list1000->c_name);
                                      ?>

                                     <?php }else{ ?>


                                    <?php } ?>






                            <?php


                                function build_sorter($key) {
    return function ($a, $b) use ($key) {
        return strnatcmp( $b[$key],$a[$key]);
    };
}

usort($forDataLoopingArray, build_sorter('sisaStock'));

// die(print_r($forDataLoopingArray));
                                $no=1; if($forDataLoopingArray)foreach($forDataLoopingArray as $list){?>
                                    <tr>
                                        <td><?php echo $no++;?></td>
                                        <td> <?php echo $list['makananName'];?></td>
                                        <td> <?php echo numberformat($list['totalStock']);?></td>
                                        <td> <?php echo numberformat($list['totalStock'] - $list['sisaStock']);?></td>
                                        <td> <?php echo find_latest_date($list['id']);?></td>
                                    </tr>
                            <?php } ?>

                            </tbody>

                        </table>
                      </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End  -->

</div>

<!-- End Page Scripts -->
</section>
