
<style>
table {
    text-align:center;!important;
}
</style>
<section class="page-content">
<div class="page-content-inner">
    <!--  -->
    <section class="panel">
        <div class="panel-heading">
            <h3>Laporan Peringkat Makanan Kantin<br>

            Periode : <?php echo $start_date. ' s.d '. $end_date;?>
            </h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="margin-bottom-50">
                        <div class="table-responsive">
                        <table border="1" width="100%">
                            <thead>
                                <tr align="center" style="background-color:#0060af; color: white">
                                    <td><b>Peringkat</td>
                                    <td><b>Nama Menu</td>
                                    <td><b>Stok</td>
                                    <td><b>Sisa Stok</td>
                                    <td><b>Jam Tap Terakhir</td>

                            <?php
                                $explode = explode("-", $start_date);
                                $explode2 = explode("-", $end_date);
                                $formated_start = $explode[2]."-".$explode[1]."-".$explode[0];
                                $formated_end = $explode2[2]."-".$explode2[1]."-".$explode2[0];
                                // die("statrt: ".$formated_start."||end: ".$formated_end);
                                $transactions = $this->report_model->getSuccessTrx($formated_start, $formated_end)->result();
                                if($transactions)
                                {
                                  foreach ($transactions as $trx)
                                  {
                                    $loop_start = $formated_start;
                                    // echo "item: ".$trx->trx_item_id.";";
                                    $stok = 0;
                                    $sisaStok = 0;

                                    while (strtotime($loop_start) <= strtotime($formated_end)) 
                                    {
                                      $res = $this->report_model->getStartingStock($loop_start, $trx->trx_item_id)->row();
                                      if($res) {$stok += $res->starting_stock;}
                                      // echo $stok.",";
                                      $res2 = $this->report_model->getSisaStock($loop_start, $trx->trx_item_id)->row();
                                      if($res2) {$sisaStok = $sisaStok + ($res->starting_stock - $res2->stock);}
                                      // echo $sisaStok."|";
                                      $loop_start = date ("Y-m-d", strtotime("+1 day", strtotime($loop_start)));
                                    }
                                    $foods[] = array("id" => $trx->trx_item_id, "makananName" => $trx->c_name, "total_stok" => $stok, "sisa_stok" => $sisaStok);
                                  }
                                }
                              
                                if($transactions)
                                {
                                    function build_sorter($key) {
                                      return function ($a, $b) use ($key) {
                                          return strnatcmp( $b[$key],$a[$key]);
                                      };
                                    }

                                    usort($foods, build_sorter('sisa_stok'));



                                $no=1; 
                                if($foods)foreach($foods as $list){ ?>
                                <tr>
                                    <td><?php echo $no++;?></td>
                                    <td><?php echo ucwords($list['makananName']);?></td>
                                    <td><?php echo numberformat($list['total_stok']);?></td>
                                    <td><?php echo numberformat($list['total_stok'] - $list['sisa_stok']);?></td>
                                    <td><?php echo find_latest_date($list['id']);?></td>
                                </tr>
                            <?php } }?>

                            </tbody>

                        </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End  -->

</div>

<!-- End Page Scripts -->
</section>
