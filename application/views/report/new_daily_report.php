<?php $this->load->view('header'); ?>

<section class="page-content">
<div class="page-content-inner">
        <style>
        thead{
           background-color:bisque;
        }
    </style>
    <!--  -->
    <section class="panel">
        <div class="panel-heading">
            <h3>Laporan Harian Kantin BCA Learning Institute</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
					<?php
                        $error		= $this->session->userdata('err_dreport_list');
						$error_msg	= $this->session->userdata('msg_dreport_list');
						if($this->session->userdata('msg_dreport_list'))
						{
							if($error == 0)
							{
								$class = "alert alert-primary";
							}
							else
							{
								$class = "alert alert-warning";
							}
							echo '
								<div class="'.$class.'" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<strong>'.$error_msg.'</strong>
								</div>';
						}
						$this->session->unset_userdata('msg_dreport_list');
						$this->session->unset_userdata('err_dreport_list');

                        //echo "<pre>";
                        foreach ($result as $date)
                        {
                            //print_r($date);
                            // foreach ($date as $data) {
                            //     print_r($data);
                            // }
                            $span1 = array();
                            foreach ($date['ftype_span'] as $span) {
                                array_push($span1, $span->span);
                            }

                            $span2 = array();
                            foreach ($date['buffet_span'] as $span) {
                                array_push($span2, $span->total);
                            }
                        
                        //echo "</pre>";
                        //die;

                        // $span1 = array();
                        // foreach ($ftype_span as $span) {
                        //     array_push($span1, $span->span);
                        // }

                        // $span2 = array();
                        // foreach ($buffet_span as $span) {
                        //     array_push($span2, $span->total);
                        // }
					?>
                    <div class="margin-bottom-50">
                        <table border="1" width="100%">
                            <thead>
                                <tr align="center">
                                    <td rowspan="3">NO.</td>
                                    <td rowspan="3">NIP</td>
                                    <td rowspan="3">NAMA</td>
                                    <td rowspan="3">PROGRAM</td>
                                    <!-- <td rowspan="3">SUB PROGRAM</td>
                                    <td rowspan="3">NAMA PELATIHAN</td> -->
                                    <td colspan="<?=$span1[0]?>">HIDANGAN UTAMA</td>
                                    <td rowspan="2" colspan="<?=$span1[1]?>">HIDANGAN PENUTUP</td>
                                    <td rowspan="2" colspan="<?=$span1[2]?>">MINUMAN</td>
                                </tr>
                                <tr align="center">
                                    <td colspan="<?=$span2[1]?>">STALL</td>
                                    <td colspan="<?=$span2[0]?>">BUFFET</td>
                                </tr>
                                <tr align="center">
                                    <?php
                                        $utama   = array();
                                        $minuman = array();
                                        $penutup = array();
                                        foreach ($date['main_courses'] as $main) {
                                            array_push($utama, $main->c_id);
                                            echo '<td>'.strtoupper($main->name).'</td>';
                                        }
                                        foreach ($date['desserts'] as $dessert) {
                                            array_push($penutup, $dessert->c_id);
                                            echo '<td>'.strtoupper($dessert->name).'</td>';
                                        }
                                        foreach ($date['drinks'] as $drink) {
                                            array_push($minuman, $drink->c_id);
                                            echo '<td>'.strtoupper($drink->name).'</td>';
                                        }
                                    ?>
                                </tr>
                            </thead>
                                <?php
                                    $td_utama   = $span1[0];
                                    $td_minuman = $span1[2];
                                    $td_penutup = $span1[1];
                                    $number = 1;
                                    foreach($date['transaction'] as $res)
                                    {
                                        echo '<tr>';
                                        echo '<td>'.$number.'</td>';
                                        echo '<td>'.$res->e_id.'</td>';
                                        echo '<td>'.$res->e_name.'</td>';
                                        echo '<td>'.$res->program_name.'</td>';
                                        // echo '<td>'.$result->e_word_unit.'</td>';
                                        // echo '<td>'.$result->e_course_id.'</td>';

                                        $key_utama = array_search($res->utama, $utama);
                                        for($i=0; $i<$td_utama; $i++)
                                        {
                                            if($key_utama == $i) {
                                                echo '<td>1</td>';
                                            } else {
                                                echo '<td>0</td>';
                                            }
                                        }

                                        $key_penutup = array_search($res->penutup, $penutup);
                                        for($i=0; $i<$td_penutup; $i++)
                                        {
                                            if($key_penutup == $i) {
                                                echo '<td>1</td>';
                                            } else {
                                                echo '<td>0</td>';
                                            }
                                        }

                                        $key_minuman = array_search($res->minuman, $minuman);
                                        for($i=0; $i<$td_minuman; $i++)
                                        {
                                            if($key_minuman == $i) {
                                                echo '<td>1</td>';
                                            } else {
                                                echo '<td>0</td>';
                                            }
                                        }
                                        echo '</tr>';
                                        $number++;
                                    }
                                ?>
                        </table>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <!-- End  -->

</div>

<!-- Page Scripts -->
<script>

    $(function(){

        $('#example1').DataTable({
            responsive: true,
            "order": [[ 0, "desc" ]]
        });

        $('#example2').DataTable({
            autoWidth: true,
            scrollX: true,
            fixedColumns: true
        });

        $('#example3').DataTable({
            autoWidth: true,
            scrollX: true,
            fixedColumns: true
        });
    });

    function saveChanges(object)
    {
        var type = object.innerHTML;
        var id_elem = object.id;

        if(type == "Show")
        {
            var box = confirm("Tampilkan Gambar ini di web?");
        }
        else
        {
            var box = confirm("Hilangkan Gambar ini dari web?");
        }

        if(box == true)
        {
            var field = object.name;
            var id    = object.value;

            $.ajax({
                url: "<?= base_url() ?>admin/instagram_list/update_value",
                type: 'POST',
                data: {
                        "id": id,
                        "field": field,
                    },
                error: function(e){
                    //alert("error");
                },
                success: function(response){
                    if(type == "Show") {
                        var class_val = "btn btn-rounded btn-warning margin-inline";
                        var display   = "Hide";
                    }
                    else {
                        var class_val = "btn btn-rounded btn-primary margin-inline";
                        var display   = "Show";
                    }

                    document.getElementById(id_elem).setAttribute("class", class_val);
                    document.getElementById(id_elem).innerHTML = display;
                }
            });
        }
    }

    function changeThumbnail(object)
    {
        var type = object.innerHTML;
        var id_elem = object.id;

        if(type == "Active")
        {
            var box = confirm("Matikan thumbnail?");
        }
        else
        {
            var box = confirm("Jadikan Gambar ini thumbnail?");
        }

        if(box == true)
        {
            var field = object.name;
            var id    = object.value;

            $.ajax({
                url: "<?= base_url() ?>admin/instagram_list/update_value",
                type: 'POST',
                data: {
                        "id": id,
                        "field": field,
                    },
                error: function(e){
                    //alert("error");
                },
                success: function(response){
                    if(type == "Active") {
                        var class_val = "btn btn-rounded btn-warning margin-inline";
                        var display   = "InActive";
                    }
                    else {
                        var class_val = "btn btn-rounded btn-primary margin-inline";
                        var display   = "Active";
                    }

                    document.getElementById(id_elem).setAttribute("class", class_val);
                    document.getElementById(id_elem).innerHTML = display;

                    window.location.reload();
                }
            });
        }
    }

    $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD'
        });
</script>

<!-- End Page Scripts -->
</section>
<?php $this->load->view('footer');?>
