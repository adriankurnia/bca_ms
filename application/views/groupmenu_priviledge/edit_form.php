<?php $this->load->view('header'); ?>
<?php
$newArray = array();
if($getModuleList)foreach($getModuleList as $list){
    $newArray[]= $list->module_id;
}
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Ubah <?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $error     = $this->session->userdata('err_temp_user_list');
                    $error_msg = $this->session->userdata('msg_temp_user_list');
                    if($this->session->userdata('msg_temp_user_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('err_temp_user_list');
                    $this->session->unset_userdata('msg_temp_user_list');
                ?>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>groupmenu_priviledge/edit_data" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Grup Admin <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="hidden" name="group_priviledge" value="<?=$g_priv->id?>">
                                <input type="text" value="<?=$g_priv->group_name?>" class="form-control" readonly />
                            </div>
                        </div>
                        <!-- <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Check All</label>
                            </div>
                            <div class="col-md-1">
                                <label><input type="checkbox" id="checkAll" value="" class="form-control" /></label>
                            </div>
                        </div> -->
                        <table class="table table-hover nowrap" id="example1" width="100%">
                            <thead>
                                <tr>
                                    <th>Nama Menu</th>
                                    <th><input type="checkbox" id="checkAllCreate" value="" />Create</th>
                                    <th><input type="checkbox" id="checkAllRead" value="" />Read</th>
                                    <th><input type="checkbox" id="checkAllUpdate" value="" />Update</th>
                                    <th><input type="checkbox" id="checkAllDelete" value="" />Delete</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Nama Menu</th>
                                    <th>Create</th>
                                    <th>Read</th>
                                    <th>Update</th>
                                    <th>Delete</th>
                                </tr>
                            </tfoot>
                        <?php
                            foreach ($gm_privs as $data) { ?>
                                <tr>
                                    <td><?=$data->menu_name?></td>
                                    
                                    <td><input type="checkbox" class="create" name="create[]" value="<?=$data->id?>" <?=($data->create == 1)?"checked":""?> id="Create<?=$data->id?>" onchange="checkRead(this)"></td>
                                    <td><input type="checkbox" class="read" name="read[]" value="<?=$data->id?>" <?=($data->read == 1)?"checked":""?> id="Read<?=$data->id?>"></td>
                                    <td><input type="checkbox" class="update" name="update[]" value="<?=$data->id?>" <?=($data->update == 1)?"checked":""?> id="Update<?=$data->id?>" onchange="checkRead(this)"></td>
                                    <td><input type="checkbox" class="delete" name="delete[]" value="<?=$data->id?>" <?=($data->delete == 1)?"checked":""?> id="Delete<?=$data->id?>" onchange="checkRead(this)"></td>
                                 </tr>
                            <?php }
                        ?>
                        </table>
                        <table  class="table table-hover nowrap">
                            <thead>
                                <tr>
                                    <td>Dashboard Module</td>
                                </tr>
                                <tr>
                                    <td>Name</td><td>Show</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($getModuleDashboard)foreach($getModuleDashboard as $list){?>
                                <tr>
                                    <td><?php echo $list->name;?></td>
                                    <td><label><input type="checkbox" name="module_id[]" <?php if(in_array($list->id,$newArray))echo 'checked';?> value="<?php echo $list->id;?>" > Show</label></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Kirim</button>
                                    <a href="<?=base_url()?>groupmenu_priviledge"><button type="button" class="btn btn-default">Batal</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<script>
    $(document).ready(function(){
        if ($('.create:checked').length == $('.create').length) {
               $("#checkAllCreate").not(this).prop('checked', true);
            }
        if ($('.read:checked').length == $('.read').length) {
               $("#checkAllRead").not(this).prop('checked', true);
            }
        if ($('.update:checked').length == $('.update').length) {
               $("#checkAllUpdate").not(this).prop('checked', true);
            }
        if ($('.delete:checked').length == $('.delete').length) {
               $("#checkAllDelete").not(this).prop('checked', true);
            }
    });

    $("#checkAllCreate").click(function () {
        $("input[class='create']").not(this).prop('checked', this.checked);
        $("input[class='read']").not(this).prop('checked', this.checked);
        $("#checkAllRead").not(this).prop('checked', this.checked);
    });
    $("#checkAllRead").click(function () {
        $("input[class='read']").not(this).prop('checked', this.checked);
    });
    $("#checkAllUpdate").click(function () {
        $("input[class='update']").not(this).prop('checked', this.checked);
        $("input[class='read']").not(this).prop('checked', this.checked);
        $("#checkAllRead").not(this).prop('checked', this.checked);
    });
    $("#checkAllDelete").click(function () {
        $("input[class='delete']").not(this).prop('checked', this.checked);
        $("input[class='read']").not(this).prop('checked', this.checked);
        $("#checkAllRead").not(this).prop('checked', this.checked);
    });
    $(".create").click(function () {
        $("#checkAllCreate").not(this).prop('checked', false);
        $(".create").change(function(){
            if ($('.create:checked').length == $('.create').length) {
               $("#checkAllCreate").not(this).prop('checked', true);
            }
        });
    });
    $(".read").click(function () {
        $("#checkAllRead").not(this).prop('checked', false);
        $(".read").change(function(){
            if ($('.read:checked').length == $('.read').length) {
               $("#checkAllRead").not(this).prop('checked', true);
            }
        });
    });
    $(".update").click(function () {
        $("#checkAllUpdate").not(this).prop('checked', false);
        $(".update").change(function(){
            if ($('.update:checked').length == $('.update').length) {
               $("#checkAllUpdate").not(this).prop('checked', true);
            }
        });
    });
    $(".delete").click(function () {
        $("#checkAllDelete").not(this).prop('checked', false);
        $(".delete").change(function(){
            if ($('.delete:checked').length == $('.delete').length) {
               $("#checkAllDelete").not(this).prop('checked', true);
            }
        });
    });
    
    function checkRead(object)
    {
        var id = object.value;
        var prop = object.id;
        if($('input[id="'+prop+'"]:checked').length > 0)
        {
            document.getElementById("Read"+id).checked = true;
        }
        else
        {
            document.getElementById("Read"+id).checked = flase;
        }
    }

    $(function(){
        
        $('#summernote').summernote({
            height: 350
        });
        
        $('.datepicker-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD HH:mm:ss'
        });

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY'
        });
    })

    function saveChanges(object)
    {
        var id    = object.value;
        var field = object.name;
        $.ajax({
            url: "<?= base_url() ?>groupmenu_priviledge/update_value",
            type: 'POST',
            data: {
                    "id": id,
                    "field": field,
                },
            error: function(e){
                //alert("error");
            },
            success: function(response){
                //alert("success");
            }
        });
    }
</script>
<?php $this->load->view('footer');?>
