<?php $this->load->view('header'); ?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Tambah <?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
				<?php
                    $error     = $this->session->userdata('err_groupmenu_list');
                    $error_msg = $this->session->userdata('msg_groupmenu_list');
                    if($this->session->userdata('msg_groupmenu_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('err_groupmenu_list');
                    $this->session->unset_userdata('msg_groupmenu_list');
                ?>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>groupmenu_priviledge/new_data" method="post" accept-charset="utf-8" enctype="multipart/form-data">
						<div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Grup Admin <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="group_priviledge" id="group_priviledge" 
                                        value="<?php 
                                                if($this->session->userdata('posting')) { 
                                                    echo $this->session->userdata('posting')['group_priviledge'];
                                                } else { 
                                                    null;
                                                }
                                            ?>" class="form-control" placeholder="Grup Admin" onKeyUp="checkGroupAdmin()" />
                                <?php echo form_error('group_priviledge');?>
                            </div>
                        </div>
                        <!-- <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Check All</label>
                            </div>
                            <div class="col-md-1">
                                <label><input type="checkbox" id="checkAll" value="" class="form-control" /></label>
                            </div>
                        </div> -->
                        <div id="menuTable">
                            <table class="table table-hover nowrap" id="example1" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nama Menu</th>
                                        <th><input type="checkbox" id="checkAllCreate" value="" />Create</th>
                                        <th><input type="checkbox" id="checkAllRead" value="" />Read</th>
                                        <th><input type="checkbox" id="checkAllUpdate" value="" />Update</th>
                                        <th><input type="checkbox" id="checkAllDelete" value="" />Delete</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Nama Menu</th>
                                        <th>Create</th>
                                        <th>Read</th>
                                        <th>Update</th>
                                        <th>Delete</th>
                                    </tr>
                                </tfoot>
                            <?php
                                $group_id = "";
                                foreach ($m_privs as $data) { ?>
                                    <tr>
                                        <td><?=$data->menu_name?></td>
                                        <!-- <td><input type="checkbox" name="crud[]" value="create_<?=$data->id?>"></td>
                                        <td><input type="checkbox" name="crud[]" value="read_<?=$data->id?>"></td>
                                        <td><input type="checkbox" name="crud[]" value="update_<?=$data->id?>"></td>
                                        <td><input type="checkbox" name="crud[]" value="delete_<?=$data->id?>"></td> -->

                                        <td><input class="create" type="checkbox" name="create[]" value="<?=$data->id?>" id="Create<?=$data->id?>" onchange="checkRead(this)"></td>
                                        <td><input class="read" type="checkbox" name="read[]" value="<?=$data->id?>" id="Read<?=$data->id?>"></td>
                                        <td><input class="update" type="checkbox" name="update[]" value="<?=$data->id?>" id="Update<?=$data->id?>" onchange="checkRead(this)"></td>
                                        <td><input class="delete" type="checkbox" name="delete[]" value="<?=$data->id?>" id="Delete<?=$data->id?>" onchange="checkRead(this)"></td>
                                     </tr>
                                <?php }
                            ?>
                            </table>
                        </div>
				        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Kirim</button>
                                    <a href="<?=base_url()?>groupmenu_priviledge"><button type="button" class="btn btn-default">Batal</button></a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <?php $this->session->unset_userdata('posting'); ?><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<script>

    $(document).ready(function(){
        $('#menuTable').hide();
    });

    $("#checkAllCreate").click(function () {
        $("input[class='create']").not(this).prop('checked', this.checked);
        $("input[class='read']").not(this).prop('checked', this.checked);
        $("#checkAllRead").not(this).prop('checked', this.checked);
    });
    $("#checkAllRead").click(function () {
        $("input[class='read']").not(this).prop('checked', this.checked);
    });
    $("#checkAllUpdate").click(function () {
        $("input[class='update']").not(this).prop('checked', this.checked);
        $("input[class='read']").not(this).prop('checked', this.checked);
        $("#checkAllRead").not(this).prop('checked', this.checked);
    });
    $("#checkAllDelete").click(function () {
        $("input[class='delete']").not(this).prop('checked', this.checked);
        $("input[class='read']").not(this).prop('checked', this.checked);
        $("#checkAllRead").not(this).prop('checked', this.checked);
    });
    $(".create").click(function () {
        $("#checkAllCreate").not(this).prop('checked', false);
        if ($('.create:checked').length == $('.create').length) {
           $("#checkAllCreate").not(this).prop('checked', true);
        }
    });
    $(".read").click(function () {
        $("#checkAllRead").not(this).prop('checked', false);
        if ($('.read:checked').length == $('.read').length) {
           $("#checkAllRead").not(this).prop('checked', true);
        }
    });
    $(".update").click(function () {
        $("#checkAllUpdate").not(this).prop('checked', false);
        if ($('.update:checked').length == $('.update').length) {
           $("#checkAllUpdate").not(this).prop('checked', true);
        }
    });
    $(".delete").click(function () {
        $("#checkAllDelete").not(this).prop('checked', false);
        if ($('.delete:checked').length == $('.delete').length) {
           $("#checkAllDelete").not(this).prop('checked', true);
        }
    });
    
    function checkRead(object)
    {
        var id = object.value;
        var prop = object.id;
        if($('input[id="'+prop+'"]:checked').length > 0)
        {
            document.getElementById("Read"+id).checked = true;
        }
        else
        {
            document.getElementById("Read"+id).checked = flase;
        }
    }

    function checkGroupAdmin()
    {
        var grup = $("#group_priviledge").val();
        if(grup.length >= 2)
        {
            $("#menuTable").show();
        }
        else
        {
            $("#menuTable").hide();
        }
    }

    $(function(){
		
		$('#summernote').summernote({
            height: 350
        });
		
        $('.datepicker-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
			format: 'YYYY-MM-DD HH:mm:ss'
        });

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY'
        });
    })

</script>
<?php $this->load->view('footer');?>