<?php 
    $this->load->view('headerDisplay');
    $newArray = array();
    if($getModuleList)foreach($getModuleList as $list){
        $newArray[]= $list->module_id;
    }
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3><?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>groupmenu_priviledge/edit_data" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Grup Admin</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" value="<?=$g_priv->group_name?>" class="form-control" disabled />
                            </div>
                        </div>
                        <table class="table table-hover nowrap" id="example1" width="100%">
                            <thead>
                                <tr>
                                    <th>Nama Menu</th>
                                    <th>Create</th>
                                    <th>Read</th>
                                    <th>Update</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Nama Menu</th>
                                    <th>Create</th>
                                    <th>Read</th>
                                    <th>Update</th>
                                    <th>Delete</th>
                                </tr>
                            </tfoot>
                        <?php
                            foreach ($gm_privs as $data) { ?>
                                <tr>
                                    <td><?=$data->menu_name?></td>
                                    
                                    <td><input type="checkbox" class="create" name="create[]" value="<?=$data->id?>" <?=($data->create == 1)?"checked":""?> id="Create<?=$data->id?>" disabled ></td>
                                    <td><input type="checkbox" class="read" name="read[]" value="<?=$data->id?>" <?=($data->read == 1)?"checked":""?> id="Read<?=$data->id?>" disabled></td>
                                    <td><input type="checkbox" class="update" name="update[]" value="<?=$data->id?>" <?=($data->update == 1)?"checked":""?> id="Update<?=$data->id?>" disabled></td>
                                    <td><input type="checkbox" class="delete" name="delete[]" value="<?=$data->id?>" <?=($data->delete == 1)?"checked":""?> id="Delete<?=$data->id?>" disabled></td>
                                 </tr>
                            <?php }
                        ?>
                        </table>
                        <table  class="table table-hover nowrap">
                        <thead>
                        <tr>
                            <td>Dashboard Module</td>
                        </tr>
                        <tr>
                        <td>Name</td><td>Show</td>
                        </tr>
                        </thead>
                        <tbody>
                            <?php if($getModuleDashboard)foreach($getModuleDashboard as $list){?>
                            <tr>
                            <td><?php echo $list->name;?></td>
                            <td><label><input type="checkbox" name="module_id[]" <?php if(in_array($list->id,$newArray))echo 'checked';?> value="<?php echo $list->id;?>" disabled > Show</label></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        </table>
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <a href="<?=base_url()?>groupmenu_priviledge"><button type="button" class="btn btn-default">Kembali</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<?php $this->load->view('footer');?>