<?php 
    $this->load->view('header');
    // print_r($this->session->userdata('editing'));die("asd");
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Ubah <?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $error     = $this->session->userdata('err_clocation_list');
                    $error_msg = $this->session->userdata('msg_clocation_list');
                    if($this->session->userdata('msg_clocation_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('err_clocation_list');
                    $this->session->unset_userdata('msg_clocation_list');
                ?>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>canteen_location/edit_canteen_location" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Ruangan <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="hidden" name="id" value="<?=$clocation->l_id?>">
                                <input type="text" name="room" value="<?=$clocation->l_room?>" class="form-control" placeholder="Nama Ruangan"  />
                                <div class="error_msg"><?php echo form_error('room');?></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Lantai <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="floor" value="<?=$clocation->l_floor?>" class="form-control" placeholder="Lantai"  />
                                <div class="error_msg"><?php echo form_error('floor');?></div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Kirim</button>
                                    <a href="<?=base_url()?>canteen_location"><button type="button" class="btn btn-default">Batal</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<script>
    $(function(){
        
        $('#summernote').summernote({
            height: 350
        });
        
        $('.datepicker-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD HH:mm:ss'
        });
    })
</script>
<?php $this->load->view('footer');?>