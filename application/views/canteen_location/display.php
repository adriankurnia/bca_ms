<?php 
    $this->load->view('headerDisplay');
    // print_r($this->session->userdata('editing'));die("asd");
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3><?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>employee/edit_employee" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Ruangan</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="room" value="<?=$clocation->l_room?>" class="form-control" placeholder="Nama Ruangan" disabled />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Lantai</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="floor" value="<?=$clocation->l_floor?>" class="form-control" placeholder="Lantai" disabled />
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <a href="<?=base_url()?>canteen_location"><button type="button" class="btn btn-default">Kembali</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<?php $this->load->view('footer');?>