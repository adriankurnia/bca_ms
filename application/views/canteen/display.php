<?php 
    $this->load->view('headerDisplay');
    // print_r($this->session->userdata('editing'));die("asd");
    $rowNum = 1;
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3><?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>employee/edit_employee" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Nama</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="item_name" value="<?= ucwords($canteen->c_name) ?>" class="form-control" placeholder="Nama Makanan" disabled />
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Kalori</label>
                            </div>
                            <div class="col-md-9">
                                <input id="cal-mask-input" type="text" name="item_calory" value="<?= $canteen->c_calory ?>" class="form-control" placeholder="Kalori" disabled />
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Lemak</label>
                            </div>
                            <div class="col-md-9">
                                <input id="fat-mask-input" type="text" name="item_fat" value="<?= $canteen->c_fat ?>" class="form-control" placeholder="Lemak" disabled />
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Protein</label>
                            </div>
                            <div class="col-md-9">
                                <input id="protein-mask-input" type="text" name="item_protein" value="<?= $canteen->c_protein ?>" class="form-control" placeholder="Protein" disabled />
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tipe</label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" id="item_type" name="item_type" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357" disabled >
                                    <?php
                                        foreach ($food_types as $ft) { ?>
                                            <option value="<?=$ft->f_id?>" <?=($canteen->c_type == $ft->f_id)?"selected":""?>><?=$ft->f_type?></option>;
                                        <?php }
                                    ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group row" id="place">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Stall/Buffet</label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" id="item_place" name="item_place" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357" disabled>
                                    <option value="0" <?=($canteen->buffet == 0)?"selected":""?>>- Tempat Hidangan Utama -</option>;
                                    <option value="1" <?=($canteen->buffet == 1)?"selected":""?>>Buffet</option>;
                                    <option value="2" <?=($canteen->buffet == 2)?"selected":""?>>Stall</option>;
                                </select>
                            </div>
                        </div>

                        <div id="buffet_detail">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="form-control-label" for="l0">Makanan Buffet</label>
                                </div>
                            <?php
                            foreach($b_details as $bd)
                            {
                            ?>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-8">
                                        <input id="bItem_<?=$rowNum?>" class="form-control" type="text" name="bItem_update[]" value="<?=ucwords($bd->item_name)?>" placeholder="Makanan Buffet" disabled />
                                    </div>
                                    <?php
                                    if($rowNum == 1)
                                    {
                                    ?>
                                    <!-- <div class="col-md-1" style="height: 40px">
                                        <button onclick="addRow(this.form);" type="button" class="btn btn-rounded btn-info margin-inline icmn-plus" ></button>
                                    </div> -->
                                    <?php } ?>
                            <?php 
                            // $rowNum++;
                            } ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Gambar</label>
                            </div>
                            <div class="col-md-9">
                               <img src="<?= base_url() ?>assets/food/resize/<?= $canteen->image ?>" width="300px" height="180px">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Harga</label>
                            </div>
                            <div class="col-md-9">
                                <input id="price-mask-input" type="text" name="item_price" value="<?= $canteen->c_price ?>" class="form-control" placeholder="Harga" disabled />
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <a href="<?=base_url()?>canteen" onclick="close_window()"><button type="button" class="btn btn-default">Kembali</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>
<script>
    function close_window()
    {
        close();
    }
</script>
</section>

<?php $this->load->view('footer');?>