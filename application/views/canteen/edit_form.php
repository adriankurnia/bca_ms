<?php
    $this->load->view('header');
    // print_r($this->session->userdata('editing'));die("asd");
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Ubah <?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $error     = $this->session->userdata('err_canteen_list');
                    $error_msg = $this->session->userdata('msg_canteen_list');
                    if($this->session->userdata('msg_canteen_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('err_canteen_list');
                    $this->session->unset_userdata('msg_canteen_list');

                    $rowNum = 1;
                ?>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>canteen/edit_canteen_item" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Nama <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="hidden" name="item_id" value="<?= $canteen->c_id ?>">
                                <input type="text" name="item_name" value="<?= ucwords($canteen->c_name) ?>" class="form-control" placeholder="Nama Makanan" />
                                <div class="error_msg"><?php echo form_error('item_name');?></div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Kalori <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input id="cal-mask-input" type="text" name="item_calory" value="<?= $canteen->c_calory ?>" class="form-control" placeholder="Kalori"  />
                                <div class="error_msg"><?php echo form_error('item_calory');?></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Lemak</label>
                            </div>
                            <div class="col-md-9">
                                <input id="fat-mask-input" type="text" name="item_fat" value="<?= $canteen->c_fat ?>" class="form-control" placeholder="Lemak"  />
                                <div class="error_msg"><?php echo form_error('item_fat');?></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Protein</label>
                            </div>
                            <div class="col-md-9">
                                <input id="protein-mask-input" type="text" name="item_protein" value="<?= $canteen->c_protein ?>" class="form-control" placeholder="Protein"  />
                                <div class="error_msg"><?php echo form_error('item_protein');?></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tipe <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" id="item_type" name="item_type" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357" onchange="showPlace()">
                                    <?php
                                        foreach ($food_types as $ft) { ?>
                                            <option value="<?=$ft->f_id?>" <?=($canteen->c_type == $ft->f_id)?"selected":""?>><?=$ft->f_type?></option>;
                                        <?php }
                                    ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group row" id="place">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Stall/Buffet</label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" id="item_place" name="item_place" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357" onchange="showBuffetDetail()">
                                    <option value="0" <?=($canteen->buffet == 0)?"selected":""?>>- Tempat Hidangan Utama -</option>;
                                    <option value="1" <?=($canteen->buffet == 1)?"selected":""?>>Buffet</option>;
                                    <option value="2" <?=($canteen->buffet == 2)?"selected":""?>>Stall</option>;
                                </select>
                            </div>
                        </div>

                        <div id="buffet_detail">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="form-control-label" for="l0">Makanan Buffet</label>
                                </div>
                            <?php
                            foreach($b_details as $bd)
                            {
                            ?>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-8">
                                        <input type="hidden" name="bItem_id[]" value="<?=$bd->id?>">
                                        <input id="bItem_<?=$rowNum?>" class="form-control" type="text" name="bItem_update[]" value="<?=ucwords($bd->item_name)?>" placeholder="Makanan Buffet" />
                                    </div>
                                    <?php
                                    if($rowNum == 1)
                                    {
                                    ?>
                                    <div class="col-md-1" style="height: 40px">
                                        <button onclick="addRow(this.form);" type="button" class="btn btn-rounded btn-info margin-inline icmn-plus" ></button>
                                    </div>
                                    <?php } ?>
                                    <div class="col-md-12 error" style="color: red"></div>
                            <?php 
                            $rowNum++;
                            } ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Gambar (.jpg/.jpeg/.png/.bmp) <label class="mandatory">*</label> <br /> Ukuran maksimum 500KB</label>
                            </div>
                            <div class="col-md-9">
                               <input type="file" class="form-control" name="canteenImage" />
                               <img src="<?= base_url() ?>assets/food/resize/<?= $canteen->image ?>" width="300px" height="180px">
                               <div class="error_msg"><?php echo form_error('canteenImage');?></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Harga <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input id="price-mask-input" type="text" name="item_price" value="<?= $canteen->c_price ?>" class="form-control" placeholder="Harga"  />
                                <div class="error_msg"><?php echo form_error('item_price');?></div>
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Kirim</button>
                                    <a href="<?=base_url()?>canteen"><button type="button" class="btn btn-default">Batal</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<script>
    var rowNum = <?=$rowNum?>;

    $(document).ready(function(){
        if(<?=$canteen->c_type?> == 1 && <?=$canteen->buffet?> == 1)
        {
            // alert("place show");
            $('#place').show();
            $('#buffet_detail').show();
        }
        else if(<?=$canteen->c_type?> == 1 && (<?=$canteen->buffet?> == 2 || <?=$canteen->buffet?> == 0))
        {
            // alert("place hide 1");
            $('#place').show();
            $('#buffet_detail').hide();
        }
        else
        {
            // alert("place hide 2");
            $('#place').hide();
            $('#buffet_detail').hide();
        }
    });

    function addRow(frm)
    {
        var row = '<div id="rowNum'+rowNum+'"><div class="form-group row"><div class="col-md-3"></div><div class="col-md-8"><input type="text" id="bItem_'+rowNum+'" name="bItem[]" class="form-control" placeholder="Nama Makanan" value="" /></div><div class="col-md-1" style="height: 40px"><button type="button" class="btn btn-rounded btn-danger margin-inline icmn-cross" onclick="removeRow('+rowNum+');"></button></div><div class="col-md-12 error" style="color: red"></div></div></div>';

        jQuery('#buffet_detail').append(row);
        rowNum++;
    }

    function removeRow(rnum)
    {
        jQuery('#rowNum'+rnum).remove();
        rowNum--;
    }

    function showPlace()
    {
        if($('#item_type').val() == 1)
        {
            $('#place').show();
        }
        else
        {
            $('#place').hide();
        }
    }

    function showBuffetDetail()
    {
        if($('#item_place').val() == 1)
        {
            $('#buffet_detail').show();
        }
        else
        {
            $('#buffet_detail').hide();
        }
    }

    $(function(){
        
        $('#summernote').summernote({
            height: 350
        });
        
        $('.datepicker-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD HH:mm:ss'
        });

        $('#cal-mask-input').mask('0000', {placeholder: "Kalori"});
        $('#price-mask-input').mask('0000000', {placeholder: "Harga"});
    })
</script>
<?php $this->load->view('footer');?>