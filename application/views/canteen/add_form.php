<?php $this->load->view('header'); ?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Tambah <?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $error     = $this->session->userdata('err_canteen_list');
                    $error_msg = $this->session->userdata('msg_canteen_list');
                    if($this->session->userdata('msg_canteen_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('err_canteen_list');
                    $this->session->unset_userdata('msg_canteen_list');

                    $rowNum = 1;
                ?>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>canteen/new_canteen_item" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Nama <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="item_name" 
                                        value="<?php 
                                                if($this->session->userdata('posting')) { 
                                                    echo $this->session->userdata('posting')['item_name'];
                                                } else { 
                                                    null;
                                                }
                                            ?>" class="form-control" placeholder="Nama Makanan" />
                                <div class="error_msg"><?php echo form_error('item_name');?></div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Kalori <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input id="cal-mask-input" type="text" name="item_calory" 
                                        value="<?php 
                                                if($this->session->userdata('posting')) { 
                                                    echo $this->session->userdata('posting')['item_calory'];
                                                } else { 
                                                    null;
                                                }
                                            ?>" class="form-control" placeholder="Kalori"  />
                                <div class="error_msg"><?php echo form_error('item_calory');?></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Lemak</label>
                            </div>
                            <div class="col-md-9">
                                <input id="fat-mask-input" type="text" name="item_fat" 
                                        value="<?php 
                                                if($this->session->userdata('posting')) { 
                                                    echo $this->session->userdata('posting')['item_fat'];
                                                } else { 
                                                    null;
                                                }
                                            ?>" class="form-control" placeholder="Lemak"  />
                                <div class="error_msg"><?php echo form_error('item_fat');?></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Protein</label>
                            </div>
                            <div class="col-md-9">
                                <input id="protein-mask-input" type="text" name="item_protein" 
                                        value="<?php 
                                                if($this->session->userdata('posting')) { 
                                                    echo $this->session->userdata('posting')['item_protein'];
                                                } else { 
                                                    null;
                                                }
                                            ?>" class="form-control" placeholder="Protein"  />
                                <div class="error_msg"><?php echo form_error('item_protein');?></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tipe <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" id="item_type" name="item_type" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357" onchange="showPlace()">
                                    <option value="0">- Tipe Makanan -</option>
                                    <?php
                                        foreach ($food_types as $ft) { ?>
                                            <option value="<?=$ft->f_id?>"
                                                <?php
                                                    if($this->session->userdata('posting')) { 
                                                        if($this->session->userdata('posting')['item_type'] == $ft->f_id)
                                                            echo "Selected";
                                                        else{
                                                            null;
                                                        }
                                                    }
                                                ?>
                                                ><?=$ft->f_type?></option>;
                                        <?php }
                                    ?>
                                </select>
                                <div class="error_msg"><?php echo form_error('item_type');?></div>
                            </div>
                        </div>
                        
                        <div class="form-group row" id="place">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Stall/Buffet</label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" id="item_place" name="item_place" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357" onchange="showBuffetDetail()">
                                    <option value="0">- Stall/Buffet -</option>;
                                    <option value="1"
                                    <?php
                                        if($this->session->userdata('posting')) { 
                                            if($this->session->userdata('posting')['item_place'] == 1)
                                                echo "Selected";
                                            else{
                                                null;
                                            }
                                        }
                                    ?>
                                    >Buffet</option>;
                                    <option value="2"
                                    <?php
                                        if($this->session->userdata('posting')) { 
                                            if($this->session->userdata('posting')['item_place'] == 2)
                                                echo "Selected";
                                            else{
                                                null;
                                            }
                                        }
                                    ?>
                                    >Stall</option>;
                                </select>
                                <div class="error_msg"><?php echo form_error('item_place');?></div>
                            </div>
                        </div>

                        <div id="buffet_detail">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="form-control-label" for="l0">Detail Buffet</label>
                                </div>
                                <div class="col-md-8">
                                    <input id="bItem_<?=$rowNum?>" class="form-control" type="text" name="bItem[]" value="" placeholder="Nama Makanan" />
                                </div>
                                <div class="col-md-1" style="height: 40px">
                                    <button onclick="addRow(this.form);" type="button" class="btn btn-rounded btn-info margin-inline icmn-plus" ></button>
                                </div>
                                <div class="error_msg"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Gambar (.jpg/.jpeg/.png) <label class="mandatory">*</label><br />Ukuran file max 500KB<br />1024 x 768</label>
                            </div>
                            <div class="col-md-9">
                               <input type="file" class="form-control" name="canteenImage" />
                               <div class="error_msg"><?php echo form_error('canteenImage');?></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Harga <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input id="price-mask-input" type="text" name="item_price" 
                                        value="<?php 
                                                if($this->session->userdata('posting')) { 
                                                    echo $this->session->userdata('posting')['item_price'];
                                                } else { 
                                                    null;
                                                }
                                            ?>" class="form-control" placeholder="Harga"  />
                                <div class="error_msg"><?php echo form_error('item_price');?></div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Kirim</button>
                                    <a href="<?=base_url()?>canteen"><button type="button" class="btn btn-default">Batal</button></a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <?php $this->session->unset_userdata('posting'); ?><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<script>

    var rowNum = <?=$rowNum?>;
    function addRow(frm)
    {
        var row = '<div id="rowNum'+rowNum+'"><div class="form-group row"><div class="col-md-3"></div><div class="col-md-8"><input type="text" id="bItem_'+rowNum+'" name="bItem[]" class="form-control" placeholder="Nama Makanan" value="" /></div><div class="col-md-1" style="height: 40px"><button type="button" class="btn btn-rounded btn-danger margin-inline icmn-cross" onclick="removeRow('+rowNum+');"></button></div><div class="col-md-12 error" style="color: red"></div></div></div>';

        jQuery('#buffet_detail').append(row);
        rowNum++;
    }

    function removeRow(rnum)
    {
        jQuery('#rowNum'+rnum).remove();
        rowNum--;
    }

    function showPlace()
    {
        if($('#item_type').val() == 1)
        {
            $('#place').show();
        }
        else
        {
            $('#place').hide();
        }
    }

    function showBuffetDetail()
    {
        if($('#item_place').val() == 1 && $('#item_type').val() == 1)
        {
            $('#buffet_detail').show();
        }
        else
        {
            $('#buffet_detail').hide();
        }
    }

    $(document).ready(function(){
        $('#place').hide();
        $('#buffet_detail').hide();
    });

    $(function(){
        
        $('#summernote').summernote({
            height: 350
        });
        
        $('.datepicker-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD HH:mm:ss'
        });

        $('#cal-mask-input').mask('0000', {placeholder: "Kalori"});
        $('#fat-mask-input').mask('0000', {placeholder: "Lemak"});
        $('#protein-mask-input').mask('0000', {placeholder: "Protein"});
        $('#price-mask-input').mask('0000000', {placeholder: "Harga"});
    })
</script>
<?php $this->load->view('footer');?>