<?php $this->load->view('header'); ?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Tambah <?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
				<?php
                    $error      = $this->session->userdata('err_active_list');
                    $error_msg  = $this->session->userdata('msg_active_list');
                    if($this->session->userdata('msg_active_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('msg_active_list');
                    $this->session->unset_userdata('err_active_list');
                ?>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>food_serving_active/new_data" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tgl Awal <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="active_start" class="form-control datepicker-only-init" placeholder="Tgl Awal" />
                                <?php echo form_error('active_start');?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tgl Akhir <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="active_end" class="form-control datepicker-only-init" placeholder="Tgl Akhir" />
                                <?php echo form_error('active_end');?>
                            </div>
                        </div>

				        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Kirim</button>
                                    <a href="<?=base_url()?>food_serving_active"><button type="button" class="btn btn-default">Batal</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<script>
    $(function(){
		
		$('#summernote').summernote({
            height: 350
        });
		
        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY'
        });

    })
</script>
<?php $this->load->view('footer');?>