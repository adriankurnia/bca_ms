<?php $this->load->view('header'); ?>

<section class="page-content">
<div class="page-content-inner">
    <!--  -->
    <section class="panel">
        <div class="panel-heading">
            <h3>Laporan <?=$menu->menu_name?></h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
					<form action="<?php echo base_url() . 'api/export_data/export_to_excel/cms_log'?>" method="post" name="form_excel" id="form_excel">
						<input type="hidden" name="table_array" id="table_array" value="">
					</form>
					<button type="button" class="btn btn-rounded btn-info margin-inline" onclick="export_to_excel()">Export to Excel</button>
                    <?php
						$error		= $this->session->userdata('err_log_list');
						$error_msg	= $this->session->userdata('msg_log_list');
						if($this->session->userdata('msg_log_list'))
						{
							if($error == 0)
							{
								$class = "alert alert-primary";
							}
							else
							{
								$class = "alert alert-warning";
							}
							echo '
								<div class="'.$class.'" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<strong>'.$error_msg.'</strong>
								</div>';
						}
						$this->session->unset_userdata('msg_log_list');
						$this->session->unset_userdata('err_log_list');
					?>
                    <div class="margin-bottom-50">
                        <table class="table table-hover nowrap" id="table_view" width="100%">
                            <thead>
							<tr>
                                <th></th>
                                <th class="filterhead_text">Aktivitas</th>
                                <th class="filterhead_text">Keterangan Awal</th>
                                <th class="filterhead_text">Keterangan Akhir</th>
                                <th class="filterhead_text">Dilakukan Oleh</th>
                                <th class="filterhead_text">Tanggal</th>
                                <th class="filterhead_text">Waktu</th>
                            </tr>
                            <tr id="table_header">
                                <th>NO.</th>
                                <th>Aktivitas</th>
                                <th>Keterangan Awal</th>
                                <th>Keterangan Akhir</th>
                                <th>Dilakukan Oleh</th>
                                <th>Tanggal</th>
                                <th>Waktu</th>
                            </tr>
                            </thead>
                            <tbody id="table_body">
                           	<div class="margin-bottom-5">
		                        Filter: 
		                        <input class="form-control width-200 display-inline-block margin-inline datepicker-only-init" placeholder="Tanggal Awal" type="text" name="date_from" id="min">
		                        <span class="margin-right-10">—</span>
		                        <input class="form-control width-200 display-inline-block margin-inline datepicker-only-init" placeholder="Tanggal Akhir" type="text" name="date_to" id="max">
		                    	<button type="button" name="submit" class="btn btn-rounded btn-success margin-inline" onclick="filterDate()">Go</button>
		                    	<div id="filter_date_msg" style="color: red"></div>
		                    </div>
                            </tbody>
                        </table>
                    </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </section>
    <!-- End  -->

</div>

<!-- Page Scripts -->

<script>
    // DataTable Custom
    var table;
	$(document).ready(function() {
		
		$('.datepicker-only-init').datetimepicker({
	        widgetPositioning: {
	            horizontal: 'left'
	        },
	        icons: {
	            time: "fa fa-clock-o",
	            date: "fa fa-calendar",
	            previous: 'fa fa-chevron-left',
	            next: 'fa fa-chevron-right',
	            up: "fa fa-arrow-up",
	            down: "fa fa-arrow-down"
	        },
	        format: 'DD-MM-YYYY'
	    });

		$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

		table = $('#table_view').DataTable({
			"processing": true,
            "serverSide": true,
            "ajax": "<?=base_url()."report/getData_cmsLog"?>",
            "aLengthMenu": [
                [25, 50, 100, 200, -1],
                [25, 50, 100, 200, "All"]
            ],
            "columns": [
                        {
                            "data": null,
                            "orderable": false
                        },
                        {"data": "activity"},
                        {"data": "notes"},
                        {"data": "became"},
                        {"data": "activity_by"},
                        {"data": "tanggal"},
                        {"data": "waktu"}
                    ],
			"order": [[ 5, "desc" ]],
			"iDisplayLength": 100,
			"scrollX": true,
            "scrollY": 350,
			"language": {
				"search": "Search All:"
			},
			"rowCallback": function (row, data, iDisplayIndex) {
                            var info = this.fnPagingInfo();
                            var page = info.iPage;
                            var length = info.iLength;
                            var index = page * length + (iDisplayIndex + 1);
                            $('td:eq(0)', row).html(index);
            },
            initComplete: function () {
            	$('.filterhead_text').each( function () {
					var title = $('#table_view thead th').eq( $(this).index() ).text();
					$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
				});
				
				//disable repetitive 
				var table_index = [];
				$('.filterhead_dropdown').each( function (i) {
					var column_count = $(this).index();
					if(table_index.includes(column_count))
					{
						//Exist
						
					}
					else
					{
						//Push
						table_index.push(column_count);
						$(this).html( '<select id="dropdown_'+column_count+'" class="form-control"><option value="" ></option></select>' );
						table.column(column_count).data().unique().sort().each( function ( d, j ) {
							$('#dropdown_'+column_count).append( '<option value="'+d+'">'+d+'</option>' )
						});
					}
					
				});
				
				// Hide Table
		        //var column = table.column(0).visible(false);
				
				//Initialize table
				table.draw();
				
				// Apply the filter

				$(".filterhead_text input").on( 'keyup change', function () {
					table
						.column( $(this).parent().index()+':visible' )
						.search( this.value )
						.draw();
				});
				
				$(".filterhead_dropdown select").on( 'change', function () {
					table
						.column( $(this).parent().index()+':visible' )
						.search( this.value )
						.draw();
				});
            }
		});
	});
	
	function filterDate()
	{
		var minDate_split = ($("#min").val()).split("-");
		var maxDate_split = ($("#max").val()).split("-");

		var minDate = minDate_split[2] + "-" + minDate_split[1] + "-" + minDate_split[0];
		var maxDate = maxDate_split[2] + "-" + maxDate_split[1] + "-" + maxDate_split[0];

		var newMinDate = new Date(minDate);
		var newMaxDate = new Date(maxDate);
		if(newMaxDate < newMinDate)
		{
			$("#filter_date_msg").html("Tanggal Akhir lebih besar dari tanggal awal");
		}
		else
		{
			$("#filter_date_msg").html("");
			table.page.len( -1 ).draw();
			table.ajax.url('<?=base_url()."report/getData_cmsLog?lowDate="?>'+minDate+'&highDate='+maxDate).load();
		}
	}

	function export_to_excel()
	{
		var myTableHeader = [];
		var myTableArray = "";
		myTableArray += '{"table_data":[';
		
		$("table thead tr#table_header").each(function( index ) {
			if(index == 0)
			{
				var tableData = $(this).find('th');
				
				if (tableData.length > 0) {
					var column_num = 0;
					tableData.each(function() { 
						if(column_num < 7)
						{
							myTableHeader.push($(this).text());
							column_num++;
						}
						
					});
				}
			}
		});
		
		console.log(myTableHeader);
		
		$("table #table_body tr").each(function() {
			var arrayOfThisRow = "";
			var tableData = $(this).find('td');
			
			arrayOfThisRow += "{";
			if (tableData.length > 0) {
				var column_num = 0;
				tableData.each(function() { 
					if(column_num < 7)
					{
						arrayOfThisRow += '"' + myTableHeader[column_num] + '"' + ':' + '"' + $(this).text() + '"' + ','; 
						column_num++;
					}
				});
			}
			
			//Remove the last coma
			arrayOfThisRow = arrayOfThisRow.slice(0, -1);

			arrayOfThisRow += "},";
			
			myTableArray += arrayOfThisRow;
		});
		
		//Remove the last coma
		myTableArray = myTableArray.slice(0, -1);
		
		myTableArray += "]}";
		
		console.log(myTableArray);
		
		$("#table_array").val(myTableArray);
		
		$("form").submit();
	}
</script>

<!-- End Page Scripts -->
</section>
<?php $this->load->view('footer');?>
