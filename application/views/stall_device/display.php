<?php 
    $this->load->view('headerDisplay');
    // print_r($this->session->userdata('editing'));die("asd");
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3><?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>employee/edit_employee" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Nama Perangkat</label>
                            </div>
                            <div class="col-md-9">
                                <input type="hidden" name="s_id" value="<?= $device->s_id ?>" />
                                <input type="text" name="device_name" value="<?= $device->device_name ?>" class="form-control" placeholder="Nama Perangkat" disabled />
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Lokasi</label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" id="loc_id" name="loc_id" wtx-context="8701866D-9EAB-43BE-A5AF-12191A46D357" disabled>
                                    <?php
                                        foreach ($food_locations as $fl) { ?>
                                            <option value="<?=$fl->l_id?>" <?=($device->loc_id == $fl->l_id)?"selected":""?>><?=$fl->l_room." (".$fl->l_floor.")"?></option>;
                                        <?php }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Mac Address</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="mac_addr" value="<?= $device->mac_addr ?>" class="form-control" placeholder="Contoh: Jika MAC [24-BD-12-AB-51-EA] Maka Input 24BD12AB51EA, (Tanpa '-')" disabled />
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <a href="<?=base_url()?>stall_device"><button type="button" class="btn btn-default">Kembali</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<?php $this->load->view('footer');?>