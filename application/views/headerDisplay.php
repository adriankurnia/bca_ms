<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>BCA Kantin Management Sistem</title>

    <link href="<?= base_url() ?>assets/backend/assets/common/img/favicon.png" rel="icon" type="image/png">

    <!-- HTML5 shim and Respond.js for < IE9 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Vendors Styles -->
    <!-- v1.0.0 -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/jscrollpane/style/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/ladda/dist/ladda-themeless.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/cleanhtmlaudioplayer/src/player.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/cleanhtmlvideoplayer/src/player.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/summernote/dist/summernote.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/owl.carousel/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/ionrangeslider/css/ion.rangeSlider.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/datatables/media/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/c3/c3.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/chartist/dist/chartist.min.css">
    <!-- v1.4.0 -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/nprogress/nprogress.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/jquery-steps/demo/css/jquery.steps.css">
    <!-- v1.4.2 -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css">
    <!-- v1.7.0 -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/dropify/dist/css/dropify.min.css">

    <!-- Clean UI Styles -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/common/css/source/main.css">


    <!-- Custom -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/common/css/custom.css">

    <!-- Fixed Header -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/vendors/datatables-fixedheader/css/fixedHeader.bootstrap4.min.css">

    <style>
    html {
        overflow: -moz-scrollbars-vertical;
    }
    </style>

    <!-- Vendors Scripts -->
    <!-- v1.0.0 -->
    <script src="<?= base_url() ?>assets/backend/assets/vendors/jquery/jquery.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/tether/dist/js/tether.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/jscrollpane/script/jquery.jscrollpane.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/spin.js/spin.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/ladda/dist/ladda.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/html5-form-validation/dist/jquery.validation.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/jquery-typeahead/dist/jquery.typeahead.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/autosize/dist/autosize.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/bootstrap-show-password/bootstrap-show-password.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/moment/min/moment.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/cleanhtmlaudioplayer/src/jquery.cleanaudioplayer.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/cleanhtmlvideoplayer/src/jquery.cleanvideoplayer.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/summernote/dist/summernote.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/ionrangeslider/js/ion.rangeSlider.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/nestable/jquery.nestable.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/datatables/media/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/datatables-responsive/js/dataTables.responsive.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/editable-table/mindmup-editabletable.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/d3/d3.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/c3/c3.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/chartist/dist/chartist.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/peity/jquery.peity.min.js"></script>
    <!-- v1.0.1 -->
    <script src="<?= base_url() ?>assets/backend/assets/vendors/chartist-plugin-tooltip/dist/chartist-plugin-tooltip.min.js"></script>
    <!-- v1.1.1 -->
    <script src="<?= base_url() ?>assets/backend/assets/vendors/gsap/src/minified/TweenMax.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/hackertyper/hackertyper.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/jquery-countTo/jquery.countTo.js"></script>
    <!-- v1.4.0 -->
    <script src="<?= base_url() ?>assets/backend/assets/vendors/nprogress/nprogress.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/jquery-steps/build/jquery.steps.min.js"></script>
    <!-- v1.4.2 -->
    <script src="<?= base_url() ?>assets/backend/assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/vendors/chart.js/src/Chart.bundle.min.js"></script>
    <!-- v1.7.0 -->
    <script src="<?= base_url() ?>assets/backend/assets/vendors/dropify/dist/js/dropify.min.js"></script>

    <!-- Clean UI Scripts -->
    <script src="<?= base_url() ?>assets/backend/assets/common/js/common.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/common/js/demo.temp.js"></script>
    
    <!-- Lightbox -->
    <script src="<?= base_url() ?>assets/backend/assets/common/js/html5lightbox/html5lightbox.js?>"></script>

    <!-- Fixed Header -->
    <script src="<?= base_url() ?>assets/backend/assets/vendors/datatables-fixedheader/js/dataTables.fixedHeader.min.js"></script>

	<!-- Code Fandy - DELETE MODAL -->
	<script>
		//Modal For Delete
		$(document).on('click', '.open-deleteModal', function () {
			 var deleteId = $(this).data('id');
			 $('.modal-body #modal').val( deleteId );
		});
	</script>
</head>

<body class="theme-default">
