<?php 
    $this->load->view('header');
    // print_r($this->session->userdata('editing'));die("asd");
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Ubah Karyawan</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $error     = $this->session->userdata('err_employee_list');
                    $error_msg = $this->session->userdata('msg_employee_list');
                    if($this->session->userdata('msg_employee_list'))
                    {
                        if($error == 0)
                        {
                            $class = "alert alert-primary";
                        }
                        else
                        {
                            $class = "alert alert-warning";
                        }
                        echo '
                            <div class="'.$class.'" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>'.$error_msg.'</strong>
                            </div>';
                    }
                    $this->session->unset_userdata('err_employee_list');
                    $this->session->unset_userdata('msg_employee_list');
                ?>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>employee/edit_employee" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">NIP</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="employee_nip" value="<?= $employee->e_nip ?>" class="form-control" placeholder="NIP" readonly />
                                <div class="error_msg"><?php echo form_error('employee_nip');?></div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Nama</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="employee_name" value="<?= $employee->e_name ?>" class="form-control" placeholder="Nama" readonly />
                                <div class="error_msg"><?php echo form_error('employee_name');?></div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Unit Kerja</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="employee_work_unit" value="<?= $employee->e_work_unit ?>" class="form-control" placeholder="Unit Kerja" readonly />
                                <div class="error_msg"><?php echo form_error('employee_work_unit');?></div>
                            </div>
                        </div>
                        
                        <!-- <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tgl Lahir</label>
                            </div>
                            <div class="col-md-9">
                                <?php
                                    if($employee->e_birthdate != null || $employee->e_birthdate != "" || !empty($employee->e_birthdate))
                                    {
                                        $explode1 = explode(" ", $employee->e_birthdate);
                                        $explode2 = explode("-", $explode1[0]);
                                        $date = $explode2[2]."-".$explode2[1]."-".$explode2[0];
                                    }
                                    else
                                    {
                                        $date = "";
                                    }
                                ?>
                                <input type="text" name="employee_birthdate" value="<?= $date ?>" class="form-control" placeholder="Tgl Lahir" readonly />
                                <div class="error_msg"><?php echo form_error('employee_birthdate');?></div>
                            </div>
                        </div> -->

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Kadaluarsa Kartu</label>
                            </div>
                            <div class="col-md-9">
                                <?php
                                    if($employee->card_expired != null || $employee->card_expired != "" || !empty($employee->card_expired))
                                    {
                                        $explode1 = explode(" ", $employee->card_expired);
                                        $explode2 = explode("-", $explode1[0]);
                                        $date = $explode2[2]."-".$explode2[1]."-".$explode2[0];
                                    }
                                    else
                                    {
                                        $date = "";
                                    }
                                ?>
                                <input type="text" name="employee_expired" class="form-control datepicker-only-init" placeholder="Kadaluarsa Kartu" value="<?=$date?>" />
                                <div class="error_msg"><?php echo form_error('employee_expired');?></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">RFID <label class="mandatory">*</label></label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="employee_rfid" value="<?= $employee->e_rfid ?>" class="form-control" placeholder="Please Tap a Card" />
                                <div class="error_msg"><?php echo form_error('employee_rfid');?></div>
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" name="submit" class="btn width-150 btn-primary">Kirim</button>
                                    <a href="<?=base_url()?>employee"><button type="button" class="btn btn-default">Batal</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>

</section>

<script>
    $(function(){
        
        $('#summernote').summernote({
            height: 350
        });
        
        $('.datepicker-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD HH:mm:ss'
        });

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY'
        });

    })
</script>
<?php $this->load->view('footer');?>