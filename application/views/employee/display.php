<?php 
    $this->load->view('headerDisplay');
    // print_r($this->session->userdata('editing'));die("asd");
?>
<section class="page-content">
<div class="page-content-inner">

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3><?=$menu->menu_name?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <form action="<?=base_url()?>employee/edit_employee" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">NIP</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="employee_nip" value="<?= $employee->e_nip ?>" class="form-control" placeholder="NIP" disabled />
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Nama</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="employee_name" value="<?= $employee->e_name ?>" class="form-control" placeholder="Nama" disabled />
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Unit Kerja</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="employee_work_unit" value="<?= $employee->e_work_unit ?>" class="form-control" placeholder="Unit Kerja" disabled />
                            </div>
                        </div>
                        
                        <!-- <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tgl Lahir</label>
                            </div>
                            <div class="col-md-9">
                                <?php
                                    if($employee->e_birthdate != null || $employee->e_birthdate != "" || !empty($employee->e_birthdate))
                                    {
                                        $explode1 = explode(" ", $employee->e_birthdate);
                                        $explode2 = explode("-", $explode1[0]);
                                        $date = $explode2[2]."-".$explode2[1]."-".$explode2[0];
                                    }
                                    else
                                    {
                                        $date = "";
                                    }
                                ?>
                                <input type="text" name="employee_birthdate" value="<?= $date ?>" class="form-control datepicker-only-init" placeholder="Tgl Lahir" disabled />
                            </div>
                        </div> -->

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Kadaluarsa Kartu</label>
                            </div>
                            <div class="col-md-9">
                                <?php
                                    if($employee->card_expired != null || $employee->card_expired != "" || !empty($employee->card_expired))
                                    {
                                        $explode1 = explode(" ", $employee->card_expired);
                                        $explode2 = explode("-", $explode1[0]);
                                        $date = $explode2[2]."-".$explode2[1]."-".$explode2[0];
                                    }
                                    else
                                    {
                                        $date = "";
                                    }
                                ?>
                                <input type="text" name="employee_expired" class="form-control datepicker-only-init" placeholder="Kadaluarsa Kartu" value="<?=$date?>" disabled />
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">RFID</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="employee_rfid" value="<?= $employee->e_rfid ?>" class="form-control" placeholder="Please Tap a Card" disabled />
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 col-md-offset-3">
                                    <a href="<?=base_url()?>employee"><button type="button" class="btn btn-default">Kembali</button></a>
                                </div>
                            </div>
                        </div>
                    </form><!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->    
</div>
</section>

<?php $this->load->view('footer');?>