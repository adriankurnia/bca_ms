-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Oct 22, 2017 at 11:24 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bca_ms`
--

-- --------------------------------------------------------

--
-- Table structure for table `bca_module_dashboard_tb`
--

CREATE TABLE `bca_module_dashboard_tb` (
  `id` int(11) NOT NULL,
  `bca_group_privilege_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bca_module_dashboard_tb`
--

INSERT INTO `bca_module_dashboard_tb` (`id`, `bca_group_privilege_id`, `module_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(27, 5, 4),
(28, 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `module_tb`
--

CREATE TABLE `module_tb` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module_tb`
--

INSERT INTO `module_tb` (`id`, `name`) VALUES
(1, 'Stok makanan (Main, desert , drink) untuk di setiap lantai'),
(2, 'Jumlah peserta yang sudah makan'),
(3, 'Makanan favorit di hari ini'),
(4, 'Berhasil/gagal jumlah tapping'),
(5, 'Grafik biaya yang harus di keluarkan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bca_module_dashboard_tb`
--
ALTER TABLE `bca_module_dashboard_tb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_tb`
--
ALTER TABLE `module_tb`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bca_module_dashboard_tb`
--
ALTER TABLE `bca_module_dashboard_tb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `module_tb`
--
ALTER TABLE `module_tb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
