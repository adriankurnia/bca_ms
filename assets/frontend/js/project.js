$(function() {
	
	
	
	/* Time
	============================== */
	setDateTime();
	setInterval(setDateTime, 1000);
	
	
	
	/* Room
	============================== */
	$('#proj-room select').on('change',function() {
		var str = "";
		$('#proj-room select option:selected').each(function() {
			str += $( this ).text() + " ";
		});
		$('#proj-roomname').text( str );
	});
	
	
	
})



/* Time
============================== */
function setDateTime() {
	var varDate = new Date();
	var mySecond = varDate.getSeconds();
	var myMinute = varDate.getMinutes();
	var myHour = varDate.getHours();
	var myDay = varDate.getDay();
	var myDate = varDate.getDate();
	var myMonth = varDate.getMonth();
	var myYear = varDate.getFullYear();

	
	if (myHour<10) {
		myHour = '0' + myHour;
	}
	else {
		myHour = + myHour;
	}
	
	if (myMinute<10) {
		myMinute = '0' + myMinute;
	}
	else {
		myMinute = + myMinute;
	}
	
	$('#proj-time').empty().append(myHour + ':' + myMinute);
	$('#proj-day').empty().append(myDate + ' ' + monthNames[myMonth] + ' ' + myYear);
}