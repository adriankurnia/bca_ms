$(function() {
	
	
	
	/*
	Swiper
	============================
	*/
    var swiper = new Swiper('#proj-section-gallery-carousel .swiper-container', {
        nextButton: '#proj-section-gallery-carousel .swiper-button-next',
        prevButton: '#proj-section-gallery-carousel .swiper-button-prev',
        pagination: '#proj-section-gallery-carousel .swiper-pagination'
    });
    var swiper = new Swiper('#proj-section-gallery-carousel-mobile .swiper-container', {
        nextButton: '#proj-section-gallery-carousel-mobile .swiper-button-next',
        prevButton: '#proj-section-gallery-carousel-mobile .swiper-button-prev',
        pagination: '#proj-section-gallery-carousel-mobile .swiper-pagination',
        slidesPerView: 'auto'
    });
	
	
	
	/*
	MMENU
	============================
	*/
	$('nav#proj-mobilenav').mmenu({
		"extensions": [
			"pagedim-black",
            "border-full"
		],
		"offCanvas": {
			"position": "right"
		},
		lazySubmenus: {
		   	load: true
		}
	});
	
	
	
	/*
	BTN Caranya
	============================
	*/
   	$('#proj-btn-caranya').on('click',function(e) {
   		$('#proj-caranya-remove, #proj-caranya-container').addClass('active');
   		e.preventDefault();
   	})
	
	
	
	/*
	Popup
	============================
	*/
	$("#proj-section-title-video").fancybox({
		infobar   : false,
		buttons   : false,
		thumbs    : false,
        margin    : 0
	})
	$(".quick_view").fancybox({
		baseClass : 'quick-view-container',
		infobar   : false,
		buttons   : false,
		thumbs    : false,
        margin    : 0,
        touch     : {
            vertical : false
        },
        loop : true,
        animationEffect    : false,
        transitionEffect   : "fade",
        transitionDuration : 500,
		baseTpl :
            '<div class="fancybox-container" role="dialog">' +
				'<div class="quick-view-content">' +
					'<div class="quick-view-carousel">' +
						'<div class="fancybox-stage"></div>' +
					'</div>' +
					'<div class="quick-view-aside">' +
						'<div class="fancybox-caption-wrap">' +
							'<div class="fancybox-caption"></div>' +
						'</div>' +
					'</div>' +
					'<button data-fancybox-close="" class="fancybox-close-small quick-view-close " title="Close"></button>' +
				'</div>' +
			'</div>',

		onInit : function( instance ) {

            /*

                #1 Create bullet navigation links
                =================================

            */

            var bullets = '<ul class="quick-view-bullets">';

			for ( var i = 0; i < instance.group.length; i++ ) {
				bullets += '<li><a data-index="' + i + '" href="javascript:;"><span>' + ( i + 1 ) + '</span></a></li>';
			}

			bullets += '</ul>';

			$( bullets ).on('click touchstart', 'a', function() {
				var index = $(this).data('index');

				$.fancybox.getInstance(function() {
					this.jumpTo(index,0);
				});

			})
			.appendTo( instance.$refs.container.find('.quick-view-carousel') );

            /*

                #2 Create arrow navigation links
                =================================

            */
            var arrow = '<div class="quick-view-nav">';
            	arrow += '<div class="quick-view-arrow prev"><a href="javascript:;" onclick="ga_btn(\'PopUp-Img-Left\');"><i class="fa fa-arrow-left" aria-hidden="true"></i></a></div>';
				arrow += '<div class="quick-view-arrow next"><a href="javascript:;" onclick="ga_btn(\'PopUp-Img-Right\');"><i class="fa fa-arrow-right" aria-hidden="true"></i></a></div>';
				arrow += '</div>';

			$(arrow).on('click touchstart', '.quick-view-arrow.prev a', function() {
				$.fancybox.getInstance(function() {
					this.previous(0);
				});
			}).on('click touchstart', '.quick-view-arrow.next a', function() {
				$.fancybox.getInstance(function() {
					this.next(0);
				});
			})
			.appendTo( instance.$refs.container.find('.quick-view-carousel') );

            /*

                #3 Add product form
                ===================

            */

			var $element = instance.group[ instance.currIndex ].opts.$orig;
			var form_id  = $element.data('qw-form');
			
			/*
			instance.$refs.container.find('.quick-view-aside').append(
                // In this example, this element contains the form
                $( '#' + form_id ).clone( true ).removeClass('hidden')
            );
            */

        },

        beforeShow : function( instance ) {

            /*
                Mark current bullet navigation link as active
            */

            instance.$refs.container.find('.quick-view-bullets')
                .children()
                .removeClass('active')
                .eq( instance.currIndex )
                .addClass('active');

        }

    });
	
	
	
	/*
	Scroll
	============================
	*/
	$('#proj-menu a, #proj-mobilenav a').on('click',function(e) {
		var id = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(id).offset().top
		}, 1000);
		
		e.preventDefault();
	})
	
	
	
})
	
	
	
/*
Google Analytics
============================
*/
var GlobalPosition = '';

var $listSection = [
	'proj-section-title',
	'proj-section-cara',
	'proj-section-gallery'
];

$(window).scroll(function() {

	TempId = '';
	for(var i=0; i<$listSection.length; i++)
	{
		var scrollPage = $(window).scrollTop();
		var pageSectionLocation = $('#'+$listSection[i]).position().top;
		var getId = '#'+$listSection[i];
		if(scrollPage >= pageSectionLocation && TempId != getId)
		{
			TempId = getId;
		}
	}

	clearTimeout($.data(this, 'scrollTimer'));
	$.data(this, 'scrollTimer', setTimeout(function() {
		// do something
		if(GlobalPosition != TempId)
		{
			GlobalPosition = TempId;
			if(TempId == '#proj-section-title')
			{
				// console.log("A");
				//check if _gaq is set too
				if (typeof _gaq !== 'undefined') {
					_gaq.push(['_trackPageview', 'VP-tentang']);
				}
				else if (typeof ga !== 'undefined') {
					ga('send', 'pageview', 'VP-tentang');
				}
			}
			else if(TempId == '#proj-section-cara')
			{
				// console.log("B");
				//check if _gaq is set too
				if (typeof _gaq !== 'undefined') {
					_gaq.push(['_trackPageview', 'VP-share']);
				}
				else if (typeof ga !== 'undefined') {
					ga('send', 'pageview', 'VP-share');
				}
			}
			else if(TempId == '#proj-section-gallery')
			{
				// console.log("C");
				//check if _gaq is set too
				if (typeof _gaq !== 'undefined') {
					_gaq.push(['_trackPageview', 'VP-inspirasi']);
				}
				else if (typeof ga !== 'undefined') {
					ga('send', 'pageview', 'VP-inspirasi');
				}
			}
		}
		//console.log("Haven't scrolled in 1000ms!");
	}, 1000));
});

function changeUrl(url) {
    try{
        window.history.pushState("", "", url);
    }
    catch (err)
    {
        console.log(err);
    }
}

function ga_btn(name) {
	if (typeof _gaq !== 'undefined') {
		_gaq.push(['_trackEvent', name, 'Click', name]);
	}
	//check if _gaq is set too
	else if (typeof ga !== 'undefined') {
		ga('send', 'event', name, 'Click', name);
	}
	return true;
}